//-----------------------------------------------------------------
// Chef Bereft Episode 3: Malice of the Malevolent Moon Monsters
// C++ Source - Chef.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "Chef.h"
#include <stdio.h>
#include <stdlib.h>
#include "FileAPI.h"
#include <VFS.h>
#include <VFSZipArchiveLoader.h>
#include <SDL.h>
#include <SDL_mixer.h>
#include "tinyxml2.h"   //WHEE TINYXML
using namespace tinyxml2;


//-----------------------------------------------------------------
// External but neccassary variables
//-----------------------------------------------------------------
extern float g_iFallAmount;
extern float g_iJumpAmount;
extern bool g_bIsFalling;
extern bool g_bIsJumping;
bool g_bJumpBlockJump = false;
extern bool g_bIsWalkingRight;

extern ttvfs::VFSHelper vfs;

//-----------------------------------------------------------------
// Game Engine Functions
//-----------------------------------------------------------------
bool GameInitialize()//HINSTANCE hInstance)
{
//    g_pBackground[254]->Draw(g_rcViewport, 234,2443);
    string sOutputFile = GetSavegameLocation() + "lastrun.txt";
    ofile.open(sOutputFile.c_str());
    if(ofile.fail())
    {
        ofile.open("lastrun.txt");  //If this fails, open in current dir
        if(ofile.fail())
        {
            //Warn them that they have issues. This should never fail.
            #ifdef BUILD_FOR_WINDOWS
            MessageBox(NULL, "Unable to open output file \"lastrun.txt\". You've got some major problems with your hard disk, most likely, so back up any important data on it!. Game may crash.", "Chef Bereft", MB_OK);
            #endif
        }
    }
    ofile << "Initializing game" << endl;
    g_pBackground[0] = NULL;    //To avoid bg crash issues

  // Create the game engine
//  ofile << "Creating game engine" << endl;
  g_pGame = new GameEngine("Chef Bereft", IDI_CHEF3, 0, 0, true);
  if (g_pGame == NULL)
    return false;
//  ofile << "Setting frame rate" << endl;
  // Set the frame rate
  g_pGame->SetFrameRate(FRAME_RATE);//60);
  //g_pGame->SetMovementMultiplier(0.65);

  // Initialize the joystick
//  g_pGame->InitJoystick();

  //Read in our configuration for resolution, etc
  ReadConfig();

  return true;
}

void GameStart()//HWND hWindow)
{
  //Show the mouse cursor for the main menu
  g_pGame->ShowMouse();

  //Set mouse cursor
  g_pGame->SetMouse(CURSOR_SPOON, 3, 2);

  // Create and load the bitmaps
  g_pPersonBitmap = new Bitmap(IDB_PERSON);
  g_pPersonJetBoots = new Bitmap(IDB_PERSONJET);
  g_pPersonSuperJetBoots = new Bitmap(IDB_PERSONSUPERJET);
  g_pChefDieLBitmap = new Bitmap(IDB_CHEFDIEL);
  g_pChefDieRBitmap = new Bitmap(IDB_CHEFDIER);
  g_pPlatformBitmap = new Bitmap(IDB_PLATFORM);
  g_pSinkingPlatformBitmap = new Bitmap(IDB_SINKINGPLATFORM);
  g_pUpsideDownPlatformBitmap = new Bitmap(IDB_UPSIDE_DOWN);
  g_pLeftPlatformBitmap = new Bitmap(IDB_LEFT_PLATFORM);
  g_pRightPlatformBitmap = new Bitmap(IDB_RIGHT_PLATFORM);
  g_pHiddenBlockBitmap = new Bitmap(IDB_HIDDENBLOCK);
  g_pNullBlockBitmap = new Bitmap(IDB_NULLBLOCK);
//  g_pLogsBitmap = new Bitmap(IDB_LOGS);
  g_pGroundBitmap = new Bitmap(IDB_GROUND);
//  g_pGrassBitmap = new Bitmap(IDB_GRASS);
  g_pCrackedLogsBitmap = new Bitmap(IDB_CRACKEDLOGS);
  g_pBlueForceBitmap = new Bitmap(IDB_BLUEFORCE);
  g_pGreenForceBitmap = new Bitmap(IDB_GREENFORCE);
  g_pRedForceBitmap = new Bitmap(IDB_REDFORCE);
//  g_pBushBitmap = new Bitmap(IDB_BUSH);
  g_pCloud3Bitmap = new Bitmap(IDB_CLOUD3);
  g_pSlantedLeftBitmap = new Bitmap(IDB_SLANTEDLEFT);
  g_pSlantedRightBitmap = new Bitmap(IDB_SLANTEDRIGHT);
  g_pSlantedLeftCornerBitmap = new Bitmap(IDB_SLANTEDLEFTCORNER);
  g_pSlantedRightCornerBitmap = new Bitmap(IDB_SLANTEDRIGHTCORNER);
  g_pLeftRockSlant = new Bitmap(IDB_SLOPELEFTROCK);
  g_pRightRockSlant = new Bitmap(IDB_SLOPERIGHTROCK);
  g_pCLeftRockSlant = new Bitmap(IDB_CSLOPELEFTROCK);
  g_pCRightRockSlant = new Bitmap(IDB_CSLOPERIGHTROCK);
  g_pLeftRockCorner = new Bitmap(IDB_LEFTCORNER);
  g_pRightRockCorner = new Bitmap(IDB_RIGHTCORNER);
  g_pLava1Bitmap = new Bitmap(IDB_LAVA1);
  g_pLava2Bitmap = new Bitmap(IDB_LAVA2);
  g_pTruckBitmap = new Bitmap(IDB_TRUCK);

  g_pElevatorBitmap = new Bitmap(IDB_ELEVATOR);
  g_pChromeblockBitmap = new Bitmap(IDB_CHROMEBLOCK);
  g_pCoolblockBitmap = new Bitmap(IDB_COOLBLOCK);
  g_pGreyholeBitmap = new Bitmap(IDB_GREYHOLE);
  g_pSlimeRockBitmap = new Bitmap(IDB_SLIMEROCK);
  g_pCaveRockBitmap = new Bitmap(IDB_CAVEROCK);
  g_pGreenkeyBitmap = new Bitmap(IDB_GREENKEY);
  g_pBluekeyBitmap = new Bitmap(IDB_BLUEKEY);
  g_pRedkeyBitmap = new Bitmap(IDB_REDKEY);
  g_pOrangekeyBitmap = new Bitmap(IDB_ORANGEKEY);
  g_pYellowkeyBitmap = new Bitmap(IDB_YELLOWKEY);
  g_pGreendoorBitmap = new Bitmap(IDB_GREENDOOR);
  g_pBluedoorBitmap = new Bitmap(IDB_BLUEDOOR);
  g_pYellowdoorBitmap = new Bitmap(IDB_YELLOWDOOR);
  g_pReddoorBitmap = new Bitmap(IDB_REDDOOR);
  g_pOrangedoorBitmap = new Bitmap(IDB_ORANGEDOOR);
  g_pRedAmoebaBitmap = new Bitmap(IDB_AMOEBA);
  g_pGreenAmoebaBitmap = new Bitmap(IDB_GREENAMOEBA);
  g_pYellowAmoebaBitmap = new Bitmap(IDB_YELLOWAMOEBA);
  g_pFireBlowBitmap = new Bitmap(IDB_FIREBLOW);
  g_p100PointsBitmap = new Bitmap(IDB_100POINTS);
  g_p200PointsBitmap = new Bitmap(IDB_200POINTS);
  g_p300PointsBitmap = new Bitmap(IDB_300POINTS);
//  g_p400PointsBitmap = new Bitmap(IDB_400POINTS);
  g_p500PointsBitmap = new Bitmap(IDB_500POINTS);
  g_p1000PointsBitmap = new Bitmap(IDB_1000POINTS);
  g_p5000PointsBitmap = new Bitmap(IDB_5000POINTS);
//  g_pGetLifeBitmap = new Bitmap(IDB_GETLIFE);

  g_pAlienBitmap = new Bitmap(IDB_ALIEN);
  g_pBlueAmoebaBitmap = new Bitmap(IDB_BUBBLEBLOWER);
  g_pWormBitmap = new Bitmap(IDB_WORM);
  g_pRollerBitmap = new Bitmap(IDB_ROLLER);
  g_pBouncyBitmap = new Bitmap(IDB_BOUNCY);
  g_pBubbleBitmap = new Bitmap(IDB_BUBBLE);
//  g_pMandMGBitmap = new Bitmap(IDB_MANDMG);
  g_pNoodleBitmap = new Bitmap(IDB_MANDMB);
//  g_pMandMRBitmap = new Bitmap(IDB_MANDMR);
//  g_pMandMOBitmap = new Bitmap(IDB_MANDMO);
//  g_pMandMYBitmap = new Bitmap(IDB_MANDMY);
//  g_pMandMBrownBitmap = new Bitmap(IDB_MANDMBR);
  g_pPoisonVatBitmap = new Bitmap(IDB_POISONVAT);
  g_pPoisonPopBitmap = new Bitmap(IDB_POISONPOP);
  g_pStalagtiteBitmap = new Bitmap(IDB_STALAGTITE);
  g_pPoisonBlobBitmap = new Bitmap(IDB_POISONBLOB);
  g_pGrassyThingBitmap = new Bitmap(IDB_TRAP);
  g_pOrangeBitmap = new Bitmap(IDB_ORANGE);
  g_pCupcakeBitmap = new Bitmap(IDB_CUPCAKE);
  g_pGrapeJuiceBitmap = new Bitmap(IDB_GRAPEJUICE);
  g_pEggsBitmap = new Bitmap(IDB_EGGS);
  g_pMilkshakeBitmap = new Bitmap(IDB_MILKSHAKE);
//  g_pForkBitmap = new Bitmap(IDB_FORK);
//  g_pLifeBitmap = new Bitmap(IDB_LIFE);
  g_pFlySpoonRBitmap = new Bitmap(IDB_FLYSPOONR);
  g_pFlySpoonLBitmap = new Bitmap(IDB_FLYSPOONL);
  g_pGameScreen[0] = new Bitmap(IDB_KITCHENSCREEN);
  g_pGameScreen[1] = new Bitmap(IDB_CONTROLROOMSCREEN);
  g_pGameScreen[2] = new Bitmap(IDB_GALLEYSCREEN);
  g_pGameScreen[3] = new Bitmap(IDB_CARGOBAYSCREEN);
  g_pGameScreen[4] = new Bitmap(IDB_ENGINESCREEN);
  g_pGameScreen[5] = new Bitmap(IDB_CREWSCREEN);
  g_pGameScreen[6] = new Bitmap(IDB_DOCKINGBAYSCREEN);
  g_pGameScreen[7] = new Bitmap(IDB_INSANESCREEN);
  g_pGameScreen[8] = new Bitmap(IDB_FPMSCREEN);
  g_pGameScreen[9] = new Bitmap(IDB_INTRIGUESCREEN);
  g_pGameScreen[10] = new Bitmap(IDB_ASYMMETRYSCREEN);
  g_pGameScreen[11] = new Bitmap(IDB_WHICHWAYSCREEN);
  g_pGameScreen[12] = new Bitmap(IDB_RESORTSCREEN);
  g_pGameScreen[13] = new Bitmap(IDB_CAVERNSSCREEN);
  g_pGameScreen[14] = new Bitmap(IDB_CONFUSIONSCREEN);
  g_pGameScreen[15] = new Bitmap(IDB_INDUSTRIALSCREEN);
  g_pGameScreen[19] = new Bitmap(IDB_BOSSSCREEN);
  g_pGameScreen[20] = new Bitmap(IDB_HLSCREEN1);
  g_pGameScreen[21] = new Bitmap(IDB_HLSCREEN2);
  g_pGameOverBitmap = new Bitmap(IDB_GAMEOVER);
  g_pHighScoreBitmap = new Bitmap(IDB_HIGHSCORE);
  g_pCavesBackground = new Bitmap(IDB_CAVESBACKGROUND);
  g_pShipBackground = new Bitmap(IDB_SHIPBACKGROUND);
  g_pShipBackground2 = new Bitmap(IDB_SHIPBACKGROUND2);
  g_pShipBackground3 = new Bitmap(IDB_SHIPBACKGROUND3);
  g_pBottomBitmap = new Bitmap(IDB_BOTTOM);
  g_pSignBitmap = new Bitmap(IDB_SIGN);
  g_pBeeBitmap = new Bitmap(IDB_BEE);
  g_pElectrosphereBitmap = new Bitmap(IDB_ELECTROSPHERE);
  g_pJetBootsBitmap = new Bitmap(IDB_JETBOOTS);
  g_pSuperJetBootsBitmap = new Bitmap(IDB_SUPERJETBOOTS);
  g_pSlimeBitmap = new Bitmap(IDB_SLIME);
  g_pHoleBitmap = new Bitmap(IDB_HOLE);
  g_pGoThroughGroundBitmap = new Bitmap(IDB_GOTHROUGH);
  g_pSmallBlockBitmap = new Bitmap(IDB_SMALLBLOCK);
  g_pLaserGunDownBitmap = new Bitmap(IDB_LASERDOWN);
  g_pLaserGunRightBitmap = new Bitmap(IDB_LASERRIGHT);
  g_pLaserBoltHorizontalBitmap = new Bitmap(IDB_LASERBOLTRL);
  g_pLaserBoltVerticalBitmap = new Bitmap(IDB_LASERBOLTUPDOWN);
  g_pMirrorLeftBitmap = new Bitmap(IDB_MIRRORLEFT);
  g_pMirrorRightBitmap = new Bitmap(IDB_MIRRORRIGHT);
  g_pYellowStairsBitmap = new Bitmap(IDB_YELLOWSTAIR);
  g_pYellowStairBlockBitmap = new Bitmap(IDB_YELLOWSBLOCK);
  g_pBlueStairsBitmap = new Bitmap(IDB_BLUESTAIR);
  g_pBlueStairBlockBitmap = new Bitmap(IDB_BLUESBLOCK);
  g_pOrangeStairsBitmap = new Bitmap(IDB_ORANGESTAIR);
  g_pOrangeStairBlockBitmap = new Bitmap(IDB_ORANGESBLOCK);
  g_pBanisterBitmap = new Bitmap(IDB_BANISTER);
  g_pFulcrumBitmap = new Bitmap(IDB_FULCRUM);
  g_pOrangeButtonBitmap = new Bitmap(IDB_ORANGEBUTTON);
  g_pYellowButtonBitmap = new Bitmap(IDB_YELLOWBUTTON);
  g_pBlueButtonBitmap = new Bitmap(IDB_BLUEBUTTON);
  g_pSlantedLeftChromeBitmap = new Bitmap(IDB_CSLANTLEFT);
  g_pSlantedLeftChromeCornerBitmap = new Bitmap(IDB_CSLANTLCORNER);
  g_pSlantedRightChromeBitmap = new Bitmap(IDB_CSLANTRIGHT);
  g_pSlantedRightChromeCornerBitmap = new Bitmap(IDB_CSLANTRCORNER);
  g_pFallBlockBitmap = new Bitmap(IDB_FALLBLOCK);
  g_pLargeFallBlockBitmap = new Bitmap(IDB_FALLLARGE);
  g_pVortexBitmap = new Bitmap(IDB_VORTEX);
  g_pWoodenBlockBitmap = new Bitmap(IDB_WOODENBLOCK);
  g_pBatBitmap = new Bitmap(IDB_BAT);
  g_pBalloonBitmap = new Bitmap(IDB_BALLOON);
  g_pBalloonPopBitmap = new Bitmap(IDB_BALOONPOP);
  g_pUndergroundBackground = new Bitmap(IDB_UNDERGROUND);
  g_pCavesBackground2 = new Bitmap(IDB_CAVES2);
  g_pTunnelBackground = new Bitmap(IDB_TUNNELBACKGROUND);
  g_pSewerBackground = new Bitmap(IDB_SEWERBACKGROUND);
  g_pFloatingHiddenBitmap = new Bitmap(IDB_FLOATINGHIDDEN);
  g_pAlienFighterBitmap = new Bitmap(IDB_ALIENFIGHTER);
  g_pAlienBulletBitmap = new Bitmap(IDB_ALIENBULLET);
  g_pFlySpoonLElectricBitmap = new Bitmap(IDB_FLYESPOONL);
  g_pFlySpoonRElectricBitmap = new Bitmap(IDB_FLYESPOONR);
  g_pElevatorHiddenBitmap = new Bitmap(IDB_ELEVATORHIDDEN);
  g_pStripedLeftBitmap = new Bitmap(IDB_STRIPEDLEFT);
  g_pStripedRightBitmap = new Bitmap(IDB_STRIPEDRIGHT);
  g_pStripedLGoThroughBitmap = new Bitmap(IDB_STRIPEDLGOTHROUGH);
  g_pAmoebaBlockBitmap = new Bitmap(IDB_AMOEBABLOCK);
  g_pAmoebaOozeBitmap = new Bitmap(IDB_AMOEBAOOZE);
  g_pAmoebaRockCrumbleBitmap = new Bitmap(IDB_AMOEBAROCKCRUMBLE);
  g_pCoilBitmap = new Bitmap(IDB_COIL);
  g_pGreenLaserHzBitmap = new Bitmap(IDB_GREENLASER);
  g_pGreenHubBitmap = new Bitmap(IDB_GREENHUB);
  g_pYellowLaserVBitmap = new Bitmap(IDB_YELLOWLASERV);
  g_pOrangeLaserVBitmap = new Bitmap(IDB_ORANGELASERV);
  g_pGoThroughRockBitmap = new Bitmap(IDB_GOTHROUGHROCK);
  g_pTeleporterBitmap = new Bitmap(IDB_TELEPORTER);
  g_pMissileExplodeBitmap = new Bitmap(IDB_MISSLEEXPLODE);
  g_pGoThroughStarBitmap = new Bitmap(IDB_GOTHROUGHSTAR);
  g_pStarSlantR = new Bitmap(IDB_STARSLANTR);
  g_pStarSlantL = new Bitmap(IDB_STARSLANTL);
  g_pStarSlantCornerR = new Bitmap(IDB_STARSLANTRCORNER);
  g_pStarSlantCornerL = new Bitmap(IDB_STARSLANTLCORNER);
  g_pCaveBackground3 = new Bitmap(IDB_CAVEBACKGROUND3);
  g_pCoolRock2Bitmap = new Bitmap(IDB_ROCK2);
  g_pLavaSlopeR = new Bitmap(IDB_LAVASLOPER);
  g_pLavaSlopeL = new Bitmap(IDB_LAVASLOPEL);
  g_pLavaSlopeRCorner = new Bitmap(IDB_LAVASLOPERCORNER);
  g_pLavaSlopeLCorner = new Bitmap(IDB_LAVASLOPELCORNER);
  g_pLavaSlopeCR = new Bitmap(IDB_LAVASLOPERC);
  g_pLavaSlopeCL = new Bitmap(IDB_LAVASLOPELC);
  g_pLavaGoThrough = new Bitmap(IDB_LAVAGOTHROUGH);
  g_pTunnelHzBitmap = new Bitmap(IDB_TUNNELHZ);
  g_pTunnelVBitmap = new Bitmap(IDB_TUNNELV);
  g_pTunnelCrossBitmap = new Bitmap(IDB_TUNNELCROSS);
  g_pTunnelFBitmap = new Bitmap(IDB_TUNNELF);
  g_pTunnelTBitmap = new Bitmap(IDB_TUNNELT);
  g_pTunnelPerpBitmap = new Bitmap(IDB_TUNNELPERP);
  g_pTunnelLBitmap = new Bitmap(IDB_TUNNELL);
  g_pTunnelBackLBitmap = new Bitmap(IDB_TUNNELBACKL);
  g_pTunnelPBitmap = new Bitmap(IDB_TUNNELP);
  g_pTunnelBackPBitmap = new Bitmap(IDB_TUNNELBACKP);
  g_pSewerVatBitmap = new Bitmap(IDB_SEWERVAT);
  g_pSludgeDumpBitmap = new Bitmap(IDB_SLUDGEDUMP);
  g_pSludgeFlowBitmap = new Bitmap(IDB_SLUDGEFLOW);
  g_pSludgeFlowCornerBitmap = new Bitmap(IDB_SLUDGEFLOWCORNER);
  g_pSludgeFallBitmap = new Bitmap(IDB_SLUDGEFALL);
  g_pLaserDieUpBitmap = new Bitmap(IDB_LASERDIEUP);
  g_pLaserDieDownBitmap = new Bitmap(IDB_LASERDIEDOWN);
  g_pLaserDieLeftBitmap = new Bitmap(IDB_LASERDIELEFT);
  g_pLaserDieRightBitmap = new Bitmap(IDB_LASERDIERIGHT);
  g_pStarCielingLeft = new Bitmap(IDB_STARCIELINGLEFT);
  g_pStarCielingRight = new Bitmap(IDB_STARCIELINGRIGHT);
  g_pSuperJumpBlockBitmap = new Bitmap(IDB_SUPERJUMP);
  g_pShootJumpBitmap = new Bitmap(IDB_SHOOTJUMP);
  g_pCrackedLogsDieBitmap = new Bitmap(IDB_CRACKEDDIE);

  g_pJumpBlockBitmap = new Bitmap(IDB_JUMPBLOCK);
  g_pFloatingPlatformBitmap = new Bitmap(IDB_FLOATING);
  g_pYellowLaserBitmap = new Bitmap(IDB_YELLOWLASER);
  g_pOrangeLaserBitmap = new Bitmap(IDB_ORANGELASER);
  g_pBlueLaserBitmap = new Bitmap(IDB_BLUELASER);
  g_pStarBlockBitmap = new Bitmap(IDB_STARBLOCK);
  g_pStarBlockCrackedBitmap = new Bitmap(IDB_STARCRACK);
  g_pStarBlockCracked2Bitmap = new Bitmap(IDB_STARCRACK2);
  g_pXblockBitmap = new Bitmap(IDB_XBLOCK);
  g_pYellowHubBitmap = new Bitmap(IDB_YELLOWHUB);
  g_pOrangeHubBitmap = new Bitmap(IDB_ORANGEHUB);
  g_pBlueHubBitmap = new Bitmap(IDB_BLUEHUB);
  g_pExitClosedBitmap = new Bitmap(IDB_EXITCLOSED);
  g_pExitBitmap = new Bitmap(IDB_EXIT);
  g_pUFOBitmap = new Bitmap(IDB_UFO);
  g_pMissileBitmap = new Bitmap(IDB_MISSILE);
  g_pMissileHubBitmap = new Bitmap(IDB_MISSILEHUB);
  g_pCheckpointBitmap = new Bitmap(IDB_CHECKPOINT);
  g_pDefeatedBitmap = new Bitmap(IDB_DEFEATED);
  g_pKeyScreenBitmap = new Bitmap(IDB_KEYSCREEN);
  g_pSmRedKeyBitmap = new Bitmap(IDB_SMRED);
  g_pSmOrangeKeyBitmap = new Bitmap(IDB_SMORANGE);
  g_pSmYellowKeyBitmap = new Bitmap(IDB_SMYELLOW);
  g_pSmGreenKeyBitmap = new Bitmap(IDB_SMGREEN);
  g_pSmBlueKeyBitmap = new Bitmap(IDB_SMBLUE);
  g_pRedDoorDieBitmap = new Bitmap(IDB_REDDDIE);
  g_pOrangeDoorDieBitmap = new Bitmap(IDB_ORDDIE);
  g_pYellowDoorDieBitmap = new Bitmap(IDB_YELDDIE);
  g_pGreenDoorDieBitmap = new Bitmap(IDB_GREDDIE);
  g_pBlueDoorDieBitmap = new Bitmap(IDB_BLUEDDIE);
  g_pStarBgBitmap = new Bitmap(IDB_STARBG);
  g_pBlockingPlatformBitmap = new Bitmap(IDB_BLOCKPLATFORM);
  g_pBlockingPlatformDieBitmap = new Bitmap(IDB_BLOCKPLATFORMDIE);
  g_pPauseBmp = new Bitmap(IDB_PAUSEMENU);
  g_pPauseMenuBmps[0] = new Bitmap(IDB_PAUSE1);
  g_pPauseMenuBmps[1] = new Bitmap(IDB_PAUSE2);
  g_pPauseMenuBmps[2] = new Bitmap(IDB_PAUSE3);
  g_pPauseMenuBmps[3] = new Bitmap(IDB_PAUSE4);
  g_pPauseMenuBmps[4] = new Bitmap(IDB_PAUSE5);
  g_pMainMenuOverBmp[0]  = new Bitmap(IDB_MAINMENU1);
  g_pMainMenuOverBmp[1]  = new Bitmap(IDB_MAINMENU2);
  g_pMainMenuOverBmp[2]  = new Bitmap(IDB_MAINMENU3);
  g_pMainMenuOverBmp[3]  = new Bitmap(IDB_MAINMENU4);
  g_pMainMenuOverBmp[4]  = new Bitmap(IDB_MAINMENU5);
  g_pMainMenuOverBmp[5]  = new Bitmap(IDB_MAINMENU6);
  g_pProfileSelectBmp[0] = new Bitmap(IDB_PROFILESMENU_1);
  g_pProfileSelectBmp[1] = new Bitmap(IDB_PROFILESMENU_2);
  g_pProfileSelectBmp[2] = new Bitmap(IDB_PROFILESMENU_3);
  g_pProfileSelectBmp[3] = new Bitmap(IDB_PROFILESMENU_4);
  g_pProfileSelectBmp[4] = new Bitmap(IDB_PROFILESMENU_DEL1);
  g_pProfileSelectBmp[5] = new Bitmap(IDB_PROFILESMENU_DEL2);
  g_pProfileSelectBmp[6] = new Bitmap(IDB_PROFILESMENU_DEL3);
  g_pMenuBgBitmap[MAIN_MENU] = new Bitmap(IDB_MAINMENU_BG);
  g_pMenuBgBitmap[PROFILES_MENU] = new Bitmap(IDB_PROFILESMENU_BG);
  g_pMenuBgBitmap[OPTIONS_MENU] = new Bitmap(IDB_OPTIONSMENU_BG);
  g_pDeleteWarningBmp = new Bitmap(IDB_DELETEBG);
  g_pDeleteYNBmp[DELETE_YES] = new Bitmap(IDB_DELETEYES);
  g_pDeleteYNBmp[DELETE_NO] = new Bitmap(IDB_DELETENO);
  g_pSoundSliderBmps[0] = new Bitmap(IDB_OPTIONSMENU_SLIDER_UP);
  g_pSoundSliderBmps[1] = new Bitmap(IDB_OPTIONSMENU_SLIDER_OVER);
  g_pOptionsMenuBmp[OPTIONSMENU_FULLSCREENTOGGLE] = new Bitmap(IDB_OPTIONSMENU_FULLSCREEN);
  g_pOptionsMenuBmp[OPTIONSMENU_SOUNDTOGGLE] = new Bitmap(IDB_OPTIONSMENU_SOUND);
  g_pOptionsMenuBmp[OPTIONSMENU_MUSICTOGGLE] = new Bitmap(IDB_OPTIONSMENU_MUSIC);
  g_pOptionsMenuBmp[OPTIONSMENU_KEYCONFIGMENU] = new Bitmap(IDB_OPTIONSMENU_KEYCONFIG);
  g_pOptionsMenuBmp[OPTIONSMENU_RESOLUTIONMENU] = new Bitmap(IDB_OPTIONSMENU_RESOLUTION);
  g_pOptionsMenuBmp[OPTIONSMENU_BACK] = new Bitmap(IDB_OPTIONSMENU_BACK);
  g_pMenuBgBitmap[RESOLUTION_MENU] = new Bitmap(IDB_RESOLUTIONMENU_BG);
  g_pResolutionMenuButtons[RESOLUTIONMENU_UP] = new Bitmap(IDB_RESOLUTIONMENU_UP);
  g_pResolutionMenuButtons[RESOLUTIONMENU_DOWN] = new Bitmap(IDB_RESOLUTIONMENU_DOWN);
  g_pResolutionMenuButtons[RESOLUTIONMENU_BACK] = new Bitmap(IDB_RESOLUTIONMENU_BACK);
  g_pConfirmResolutionBg = new Bitmap(IDB_RESOLUTION_CONFIRM);
  g_pConfirmResolutionClick[RESOLUTION_CONFIRM_YES] = new Bitmap(IDB_RESOLUTION_YES);
  g_pConfirmResolutionClick[RESOLUTION_CONFIRM_NO] = new Bitmap(IDB_RESOLUTION_NO);
  g_pMenuBgBitmap[KEYCONFIG_MENU] = new Bitmap(IDB_KEYCONFIG_BG);
  g_pTutorialBitmap = new Bitmap(IDB_TUTORIAL);
  g_pDialogProfileBmp[0] = new Bitmap(IDB_PROFILECHEF);
  g_pDialogProfileBmp[1] = new Bitmap(IDB_PROFILECRAB);
  g_pBackBitmap = new Bitmap(IDB_LEVELSELECT_BACK);
  g_pLevelSelectBitmap[0] = new Bitmap(IDB_LEVELSELECT_LEVEL_OVER);
  g_pLevelSelectBitmap[1] = new Bitmap(IDB_LEVELSELECT_PREV_OVER);
  g_pLevelSelectBitmap[2] = new Bitmap(IDB_LEVELSELECT_NEXT_OVER);
  g_pLevelSelectBitmap[3] = new Bitmap(IDB_LEVELSELECT_BACK_OVER);
  g_pBgStoryBmp[0] = new Bitmap(IDB_BGSTORY_BG1);
  g_pBgStoryBmp[1] = new Bitmap(IDB_BGSTORY_BG2);
  g_pBgStoryBmp[2] = new Bitmap(IDB_BGSTORY_BG3);
  g_pBgStoryBmp[3] = new Bitmap(IDB_BGSTORY_BG4);
  g_pEgStoryBmp[0] = new Bitmap(IDB_EGSTORY_1);
  g_pEgStoryBmp[1] = new Bitmap(IDB_EGSTORY_2);
  g_pBgOverBmp[0] = new Bitmap(IDB_BGSTORY_PREV_OVER);
  g_pBgOverBmp[1] = new Bitmap(IDB_BGSTORY_NEXT_OVER);
  g_pBgOverBmp[2] = new Bitmap(IDB_BGSTORY_EXIT_OVER);

  //Load boss frames
  for(int i = 0; i < NUM_BOSS_FRAMES; i++)
  {
      char cText[256];
      sprintf(cText, "res/allyourbase/%d.bmp", i);
      string sFilename = cText;
      g_pCrabBitmap[i] = new Bitmap(sFilename);
  }

  //HACK: Forceload game screen bitmaps into texture memory, so no lagging while level select
  for(int i = 0; i < 20; i++)
  {
      if(i == 16 || i == 17 || i == 18)
        continue;
      g_pGameScreen[i]->GetWidth();
  }

  //Make the backgrounds now so that we have the bitmaps loaded already.
  g_pBackground[0] = new Background(g_pStarBgBitmap);
  g_pBackground[1] = new Background(g_pStarBgBitmap);
  g_pBackground[2] = new Background(g_pStarBgBitmap);
  g_pBackground[3] = new Background(g_pStarBgBitmap);
  g_pBackground[4] = new Background(g_pCavesBackground);
  g_pBackground[5] = new Background(g_pUndergroundBackground);
  g_pBackground[6] = new Background(g_pShipBackground3);
  g_pBackground[7] = new Background(g_pShipBackground3);
  g_pBackground[8] = new Background(g_pCavesBackground);
  g_pBackground[9] = new Background(g_pShipBackground2);
  g_pBackground[10] = new Background(g_pCavesBackground);
  g_pBackground[11] = new Background(g_pCavesBackground2);
  g_pBackground[12] = new Background(g_pCaveBackground3);
  g_pBackground[13] = new Background(g_pShipBackground);
  g_pBackground[14] = new Background(g_pStarBgBitmap);
  g_pBackground[15] = new Background(g_pShipBackground2);
  g_pBackground[16] = new Background(g_pStarBgBitmap);
  g_pBackground[17] = new Background(g_pStarBgBitmap);
  g_pBackground[18] = new Background(g_pCavesBackground2);
  g_pBackground[19] = new Background(g_pSewerBackground);
  for(int i = 0; i < 20; i++)
    g_pBackground[i]->SetParallax(1.5); //Set all these to parallax scroll
  //Make second backgrounds
  g_p2ndBackground[0] = new Background(g_pUndergroundBackground);
  g_p2ndBackground[1] = new Background(g_pUndergroundBackground);
  g_p2ndBackground[2] = new Background(g_pShipBackground);
  g_p2ndBackground[3] = new Background(g_pUndergroundBackground);
  g_p2ndBackground[7] = new Background(g_pCavesBackground);
  g_p2ndBackground[9] = new Background(g_pCavesBackground);
  g_p2ndBackground[13] = new Background(g_pCavesBackground);
  g_p2ndBackground[14] = new Background(g_pTunnelBackground);
  g_p2ndBackground[17] = new Background(g_pUndergroundBackground);
  //Make third background for level 15
  g_pLevel15Background = new Background(g_pSewerBackground);
  //That's all for now!

  //Add flickery stars to the backgrounds
  AddBgParticles();

  g_pTeleporterSprite[0] = NULL;
  g_pTeleporterSprite[1] = NULL;             //Set them both to null

   //Seed the random function
  srand(SDL_GetTicks());

  //Create jet boot blast sprite so following code doesn't crash
//  g_pJetBootBlast = new Sprite(g_pMissileExplodeBitmap, g_rcAll, BA_BOUNCE);
//  g_pJetBootBlast->SetZOrder(4);
//  g_pGame->AddSprite(g_pJetBootBlast);
  //Create Chef so game doesn't crash
  g_pPersonSprite = new PersonSprite(g_pPersonBitmap, g_rcAll, BA_STOP);
  g_pPersonSprite->SetNumFrames(20);
  g_pPersonSprite->SetZOrder(4);
  g_pPersonSprite->SetPosition(0, 0);
  g_pGame->AddSprite(g_pPersonSprite);

  ReadHiScores(); //Read in the high scores
  NewGame();  //Start a new game
  RedeemGame();     //Redeem the game, if we can
//  ofile << "Collected " << g_lElectrospheresCollected.size() << " Electrospheres" << endl;

//  g_iWidth = g_pGame->GetWidth(); //Set initial dimensions
//  g_iHeight = g_pGame->GetHeight();

  //myRECT rc;
  //GetWindowRect(GetDesktopWindow(), &rc);
  //SetWindowPos(hWindow, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE);
  //MoveWindow(hWindow, rc.left - GetSystemMetrics(SM_CXFIXEDFRAME), rc.top - GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYFIXEDFRAME), rc.right + (GetSystemMetrics(SM_CXFIXEDFRAME) * 2), rc.bottom + GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYFIXEDFRAME) * 2, true);
  //To be fullscreen, the code above is needed.

  //Create x button so players can quit.
//  myRECT rc;
//  rc.left = rc.top = 0;
//  rc.right = g_iWidth;
//  rc.bottom = g_iHeight;
//  g_pXButton = new ButtonSprite(g_pXButtonBitmap, rc, BA_STOP);
//  g_pXButton->SetNumFrames(2);
//  g_pXButton->SetZOrder(40000); //Yahooooooo!!!!!!!!!
//  g_pXButton->SetPosition(rc.right - g_pXButton->GetWidth(), 0); //Set to upper right corner
//  g_pXButton->SetHidden(true); //night, night
  //Notice we do not add the x button to the game engine.
  //...
  //It seemed like a good idea.
  //-The not very sign painterly person

  //Load font
  g_pGame->SetFont(FONT_VERAMONO);  //Vera Mono is a free font.

  //Play the menu music
  g_pGame->PlaySong(MUSIC_MENU);

  //Start teh power timer ticking
  g_iPowerLastChecked = g_pGame->GetTime();

  g_pGame->HideMouse();

  //Load ACK-3D
  //PrimeACK("res/ack/lev1.dtf");
  ReadInBgStory();
  //ofile << "bg story: " << endl << g_sBgStory << endl << endl;
}

void GameEnd()
{
  ShutdownACK();
  //EndLevel();     //To prevent electrosphere cheating
  WriteHiScores(); //Write the high scores
  WriteConfig();    //Write the config
  if(g_bPlayingGame)
  {
      EndLevel();
  }
  SaveGame();       //Save our progress

  // Cleanup the backgrounds
  for(int i = 0; i < 20; i++)
  {
    delete g_pBackground[i];
  }
  //and second backgrounds
  for(int i = 0; i < 4; i++)
  {
    delete g_p2ndBackground[i];
  }
  delete g_p2ndBackground[7];
  delete g_p2ndBackground[9];
  delete g_p2ndBackground[13];
  delete g_p2ndBackground[14];
  delete g_p2ndBackground[17];
  delete g_pLevel15Background;

  // Cleanup the game engine
  delete g_pGame;
}

bool GameActivate()
{
  //Restart song that we killed
  g_pGame->PlaySong("");

  //HGE dictates always return false
  return false;
}

bool GameDeactivate()
{
  //Pause (kill) song if playing (pause seems to not work for some reason, at least not on this soundcard)
  g_pGame->CleanupSong();

  //HGE says always return false
  return false;
}

void GamePaint()
{
  int iXViewportOffset = (g_pGame->GetWidth() - (g_rcViewport.right - g_rcViewport.left)) / 2;
  int iYViewportOffset = (g_pGame->GetHeight() - (g_rcViewport.bottom - g_rcViewport.top)) / 2;

  if(!g_bPlayingGame)
  {
    iXViewportOffset = iYViewportOffset = 0;
  }
  //ofile << "Current level: " << g_iCurrentLevel << endl;
  int iXStartPos = (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2; // Assuming all the gamescreens are the same size, of course...
  int iYStartPos = (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2;

  //Draw menu only if we should
  if(g_iMenuPos != NO_MENU)
  {
      g_pMenuBgBitmap[g_iMenuPos]->Draw(iXStartPos, iYStartPos);    //Draw bg in upper left corner

      if(g_iMenuPos == MAIN_MENU)
      {
          if(g_iMenuPosSelect != -1)
          {
              g_pMainMenuOverBmp[g_iMenuPosSelect]->Draw(iXStartPos + g_ptMainMenuDrawPos[g_iMenuPosSelect].x,
                                                         iYStartPos + g_ptMainMenuDrawPos[g_iMenuPosSelect].y);
          }

          string szText = GetMenuText(g_iMenuPos, g_iMenuPosSelect);
          myRECT rcTextPos = g_rcMainMenuTextPos;
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(szText.c_str(), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
      }
      else if(g_iMenuPos == PROFILES_MENU)
      {
          //ofile << "Menu pos select: " << g_iMenuPosSelect << endl;
          if(g_iMenuPosSelect != -1 && !g_bDeleteWarning)
              g_pProfileSelectBmp[g_iMenuPosSelect]->Draw(iXStartPos + g_ptProfileMenuBmpDrawLoc[g_iMenuPosSelect].x,
                                                          iYStartPos + g_ptProfileMenuBmpDrawLoc[g_iMenuPosSelect].y);

          string szText = GetMenuText(g_iMenuPos, g_iMenuPosSelect);
          myRECT rcTextPos = g_rcProfileTextDrawPos;
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(szText.c_str(), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);

          //Yay negative viewport so candle flames will draw right with viewport offsets
          myRECT rcViewport = {-iXStartPos,-iYStartPos,g_pGame->GetWidth()-iXStartPos, g_pGame->GetHeight()-iYStartPos};
          //Draw flames
          for(int i = 0; i < NUM_PROFILES; i++)
            g_pFlameSprites[i]->Draw(rcViewport);

          //If the delete warning menu thing
          if(g_bDeleteWarning)
          {
              g_pDeleteWarningBmp->Draw(iXStartPos + 144, iYStartPos + 39);

              if(g_iMenuPosSelect != -1)
              {
                  g_pDeleteYNBmp[g_iMenuPosSelect]->Draw(g_ptDeleteYNDraw[g_iMenuPosSelect].x + iXStartPos,
                                                         g_ptDeleteYNDraw[g_iMenuPosSelect].y + iYStartPos);
              }
          }
      }
      else if(g_iMenuPos == OPTIONS_MENU)
      {
          //Draw sliders for sound volume
          int iSliderDrawBmp = 0;
          if(g_iMenuPosSelect == OPTIONSMENU_SOUNDSLIDER && !g_bConfirmResolution)
            iSliderDrawBmp = 1;

          //Figure out the ratio of sliders, so we know where to draw
          int iXSliderDrawPos = GetSliderLoc(0) + 116;
          int iYSliderDrawPos = (g_iMenuPosSelect == OPTIONSMENU_SOUNDSLIDER)?(333):(328);

          g_pSoundSliderBmps[iSliderDrawBmp]->Draw(iXStartPos + iXSliderDrawPos,
                                                   iYStartPos + iYSliderDrawPos);

          //and music volume
          if(g_iMenuPosSelect == OPTIONSMENU_MUSICSLIDER && !g_bConfirmResolution)
            iSliderDrawBmp = 1;
          else
            iSliderDrawBmp = 0;

          iXSliderDrawPos = GetSliderLoc(1) + 81;
          iYSliderDrawPos = (g_iMenuPosSelect == OPTIONSMENU_MUSICSLIDER)?(425):(420);

          g_pSoundSliderBmps[iSliderDrawBmp]->Draw(iXStartPos + iXSliderDrawPos,
                                                   iYStartPos + iYSliderDrawPos);

          //Draw menu item they're selecting
          if(g_iMenuPosSelect != -1 &&
             g_iMenuPosSelect != OPTIONSMENU_MUSICSLIDER &&
             g_iMenuPosSelect != OPTIONSMENU_SOUNDSLIDER &&
             !g_bConfirmResolution)
          {
              g_pOptionsMenuBmp[g_iMenuPosSelect]->Draw(iXStartPos + g_ptOptionsMenuDrawPos[g_iMenuPosSelect].x,
                                                        iYStartPos + g_ptOptionsMenuDrawPos[g_iMenuPosSelect].y);
          }

          //Draw text of what they're selecting
          string szText = GetMenuText(g_iMenuPos, g_iMenuPosSelect);
          myRECT rcTextPos = g_rcOptionsTextPos;
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(szText.c_str(), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);

          //Draw flames for candles
          //HACK: Yay negative viewport so candle flames will draw right with viewport offsets
          myRECT rcViewport = {-iXStartPos,-iYStartPos,g_pGame->GetWidth()-iXStartPos, g_pGame->GetHeight()-iYStartPos};
          //Draw flames
          if(g_pGame->GetFullscreen())
            g_pFlameSprites[0]->Draw(rcViewport);
          if(g_pGame->GetSound())
            g_pFlameSprites[1]->Draw(rcViewport);
          if(g_pGame->GetMusic())
            g_pFlameSprites[2]->Draw(rcViewport);
      }
      else if(g_iMenuPos == RESOLUTION_MENU)
      {
          if(g_iMenuPosSelect != -1 && g_iMenuPosSelect < 3 && !g_bConfirmResolution)
            g_pResolutionMenuButtons[g_iMenuPosSelect]->Draw(g_ptResolutionDrawPos[g_iMenuPosSelect].x + iXStartPos,
                                                             g_ptResolutionDrawPos[g_iMenuPosSelect].y + iYStartPos);

          int iSelect = g_iMenuPosSelect - 3;// g_iResolutionScrollPos - 3;
          if(g_bConfirmResolution)
            iSelect = -4;
          //Draw the screen resolutions
          myRECT rc = g_rcResolutionPos;
          OffsetRect(&rc, iXStartPos, iYStartPos);
          g_pGame->DrawScreenResolutions(&rc, 0, RESOLUTION_INCREMENT_Y, g_iResolutionScrollPos, RESOLUTION_NUM_DRAW, iSelect);

          //Draw menu text
          //string szText = GetMenuText(g_iMenuPos, g_iMenuPosSelect);
          myRECT rcTextPos = g_rcResolutionTextPos;
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(GetMenuText(g_iMenuPos, g_iMenuPosSelect).c_str(), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
      }
      else if(g_iMenuPos == KEYCONFIG_MENU)
      {
          if(g_iMenuPosSelect != -1)
          {
              //Fill bg of rectangle that's being selected
              myRECT rc = g_rcKeyConfigItemsPos[g_iMenuPosSelect];
              rc.right++;
              rc.bottom++;  //Compensate for FillRect() not drawing one pixel on right and bottom sides
              OffsetRect(&rc, iXStartPos, iYStartPos);
              g_pGame->FillRect(&rc, 80, 65, 133);
          }
          //Draw text for the keys
          for(int i = 0; i < 6; i++)
          {
              int* iCurKeyDraw = NULL;
              switch(i)
              {
                  case 0:
                    iCurKeyDraw = &UP_KEY;
                    break;
                  case 1:
                    iCurKeyDraw = &DOWN_KEY;
                    break;
                  case 2:
                    iCurKeyDraw = &LEFT_KEY;
                    break;
                  case 3:
                    iCurKeyDraw = &RIGHT_KEY;
                    break;
                  case 4:
                    iCurKeyDraw = &JUMP_KEY;
                    break;
                  case 5:
                    iCurKeyDraw = &FIRE_KEY;
                    break;
              }
              //Draw key text
              myRECT rcTextPos = g_rcKeyConfigItemsPos[i];
              OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
              g_pGame->SetTextColor(0,0,0);
              if(iCurKeyDraw == g_pKeyToSet && iCurKeyDraw != NULL)
                g_pGame->DrawText((const char*)("Press Key"), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
              else
                g_pGame->DrawText(g_pGame->GetKeyName(*iCurKeyDraw), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
          }
          //Draw text for what we're mousing over
          myRECT rcTextPos = g_rcKeyConfigItemsPos[6];
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(GetMenuText(g_iMenuPos, g_iMenuPosSelect).c_str(), &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
      }



      if(g_bConfirmResolution)  //If need to confirm resolution change
      {
          g_pConfirmResolutionBg->Draw((g_pGame->GetWidth() - g_pConfirmResolutionBg->GetWidth())/2,
                                       (g_pGame->GetHeight() - g_pConfirmResolutionBg->GetHeight())/2);

          if(g_iMenuPosSelect != -1)
          {
              g_pConfirmResolutionClick[g_iMenuPosSelect]->Draw(g_rcConfirmResolutionClick[g_iMenuPosSelect].left + iXStartPos,
                                                                g_rcConfirmResolutionClick[g_iMenuPosSelect].top + iYStartPos);
          }

          //Draw time left
          char cData[64];
          sprintf(cData, "Resetting in %d...", RESOLUTION_CHANGE_PAUSE - ((g_pGame->GetTime() - g_iStartedCountdownAt) / 1000));
          myRECT rcTextPos = g_rcConfirmResolutionText;
          OffsetRect(&rcTextPos, iXStartPos, iYStartPos);
          g_pGame->SetTextColor(0,0,0);
          g_pGame->DrawText(cData, &rcTextPos, HGETEXT_CENTER | HGETEXT_MIDDLE);
      }

      return;
  }

  if(g_bHighScores)
  {
    //Fill bg black
//    PatBlt(hDC, 0, 0, g_pGame->GetWidth(), g_pGame->GetHeight(), BLACKNESS);

    //Draw the high score bitmap image before scores so that scores aren't overwritten
    g_pHighScoreBitmap->Draw(iXStartPos, iYStartPos);

    // Draw the high scores (and names)
    char szText[64];
    char szText2[12];
    g_pGame->SetTextColor(0, 0, 0);
    for (int i = 0; i < 5; i++)
    {
      myRECT  rect = g_rcHighScoreNamePos[i];
      myRECT  rect2 = g_rcHighScoreScorePos[i];
      OffsetRect(&rect, iXStartPos, iYStartPos);
      OffsetRect(&rect2, iXStartPos, iYStartPos);
      sprintf(szText2, "%d", g_iHiScores[i] * 10);
      for(int j = 0; j < 21; j++)
      {
        szText[j] = g_cNames[i][j];
        if(szText[j] == '~' || j == 20)
        {
          szText[j] = '\0';
          break;
        }
      }
      g_pGame->DrawText(szText, &rect, HGETEXT_CENTER | HGETEXT_MIDDLE);
      g_pGame->DrawText(szText2, &rect2, HGETEXT_CENTER | HGETEXT_MIDDLE);
    }
  }
  else if((g_bBgStory || g_bEgStory) && !g_bTypeName)
  {
      //Draw bg story stuff
      myRECT rc = {iXStartPos + DIALOG_MARGIN*2,iYStartPos + DIALOG_MARGIN,g_pGame->GetWidth() - iXStartPos - DIALOG_MARGIN, g_pGame->GetHeight() - iYStartPos - DIALOG_MARGIN};
      g_pGame->SetTextColor(0,0,0);
      if(g_bBgStory)
        g_pBgStoryBmp[g_iBgStoryPanel]->Draw(iXStartPos, iYStartPos);
      else
        g_pEgStoryBmp[g_iBgStoryPanel]->Draw(iXStartPos, iYStartPos);
      g_pGame->DrawText((g_bEgStory)?(g_sEndgameStory[g_iBgStoryPanel].c_str()):(g_sBgStory[g_iBgStoryPanel].c_str()), &rc, HGETEXT_LEFT | HGETEXT_TOP);

      //Draw icons for prev/next arrows
      if(g_iMenuPosSelect != NO_MENU)
      {
          g_pBgOverBmp[g_iMenuPosSelect]->Draw(g_rcBgStoryClickPos[g_iMenuPosSelect].left + iXStartPos,
                                               g_rcBgStoryClickPos[g_iMenuPosSelect].top + iYStartPos);
      }

      //Draw text saying what we're selecting
      switch(g_iMenuPosSelect)
      {
        case BG_STORY_PREV:
            if(g_iBgStoryPanel != 0)
                DisplayMsg("Previous", 10);
            break;
        case BG_STORY_NEXT:
            if(g_bBgStory)
            {
                if(g_iBgStoryPanel != NUM_BGSTORY_PANELS-1)
                    DisplayMsg("Next", 10);
            }
            else if(g_iBgStoryPanel != NUM_EGSTORY_PANELS-1)
                DisplayMsg("Next", 10);
            break;
        case BG_STORY_SKIP:
            if(g_iBgStoryPanel == NUM_BGSTORY_PANELS-1 || g_bEgStory)
                DisplayMsg("Exit", 10);
            else
                DisplayMsg("Skip", 10);
            break;
        case NO_MENU:
            break;
      }
  }
  else if(g_bPlayingGame) //If game not over
  {
      if(g_b3D)
      {
        AckPaint(g_pGame->GetWidth(), g_pGame->GetHeight()); //Draw ack-3d
        SetRect(&g_rcViewport, 0,0,g_pGame->GetWidth(), g_pGame->GetHeight());
        //return;
      }
      else {    //If not in 3D mode
      if(g_iCheckpointInvincibility && !g_bPaused) //Flicker person on checkpoint respawn during invincibility period
      {
          //if(--g_iCheckpointInvincibility)
          //{
              if(!(g_iCheckpointInvincibility % 2))
                g_pPersonSprite->SetHidden(true);   //Flicker every 3 cycles
          //}
      }

      // Draw the background
      if(g_iCurrentLevel == 9)
      {
        if(g_rcViewport.top < 2545)
          g_pBackground[g_iCurrentLevel]->Draw(g_rcViewport, iXViewportOffset, iYViewportOffset);  //Optimize for level 10
      }
      else if(g_iCurrentLevel == 13)
      {
        if(g_rcViewport.left < 2545)
          g_pBackground[g_iCurrentLevel]->Draw(g_rcViewport, iXViewportOffset, iYViewportOffset);  //And 14
      }
      else
        g_pBackground[g_iCurrentLevel]->Draw(g_rcViewport, iXViewportOffset, iYViewportOffset);

      //Draw second background
      myRECT rect = g_rcViewport;
      if(g_iCurrentLevel == 0)
        rect.top = 2325;
      else if(g_iCurrentLevel == 1)
        rect.top = 1350;
      else if(g_iCurrentLevel == 2)
        rect.top = 300;
      else if(g_iCurrentLevel == 3)
        rect.top = 1050;
      else if(g_iCurrentLevel == 7)
      {
        rect.top = 1575;
        rect.left = 2250;
        rect.bottom = 2025;
        rect.right = 2925;
        if(rect.left > g_rcViewport.right)
          rect.top = rect.bottom + 1;
        else if(rect.right < g_rcViewport.left)
          rect.top = rect.bottom + 1;
        else if(rect.bottom < g_rcViewport.top)
          rect.top = rect.bottom + 1;
        else if(rect.top > g_rcViewport.bottom)
          rect.top = rect.bottom + 1;
      }
      else if(g_iCurrentLevel == 9)
        rect.top = 34 * 75;
      else if(g_iCurrentLevel == 17)
        rect.top = 600;
      else if(g_iCurrentLevel == 13)
      {
        rect.left = 35 * 75;
        if(rect.left > g_rcViewport.right)
          rect.top = rect.bottom + 1;
      }
      else if(g_iCurrentLevel == 14)
      {
        if(rect.top >= 32 * 75)
          rect.top = rect.bottom + 1;
        else
          rect.top = 12 * 75;
      }
      else
        rect.top = rect.bottom + 1;

      //Normalize for viewport before drawing
      //rect.left -= g_rcViewport.left;
      //rect.right -= g_rcViewport.left;
      //rect.top -= g_rcViewport.top;
      //rect.bottom -= g_rcViewport.top;

      if(g_bPlayingGame && rect.top < rect.bottom)
        g_p2ndBackground[g_iCurrentLevel]->Draw(rect, iXViewportOffset + (rect.left - g_rcViewport.left), iYViewportOffset + (rect.top - g_rcViewport.top));

      if(g_iCurrentLevel == 14 && g_bPlayingGame)
      {
        myRECT rc = g_rcViewport;
        rc.top = 32 * 75;
        //Normalize for viewport
        //rc.left -= g_rcViewport.left;
        //rc.right -= g_rcViewport.left;
        //rc.top -= g_rcViewport.top;
        //rc.bottom -= g_rcViewport.top;
        if(g_bPlayingGame && rc.top < rc.bottom)
          g_pLevel15Background->Draw(rc, iXViewportOffset + (rc.left - g_rcViewport.left), iYViewportOffset + (rc.top - g_rcViewport.top));
      }
      //ofile << "Before draw:" << endl;
      //ofile << "jet boot Left: " << g_pJetBootBlast->GetPosition().left << ", top: " << g_pJetBootBlast->GetPosition().top << endl;
      //ofile << "person Left: " << g_pPersonSprite->GetPosition().left << ", top: " << g_pPersonSprite->GetPosition().top << endl;

    // Draw the sprites
    g_pGame->DrawSprites(g_rcViewport, iXViewportOffset, iYViewportOffset);

    //Draw jet boot blast if we should
    if(JetbootBlasting())
    {
      g_pPersonSprite->FireParticles();
    }



    //Draw the points gotten
    char szText[64];
    myRECT rectText = {iXViewportOffset, iYViewportOffset, g_pGame->GetWidth() - iXViewportOffset, iYViewportOffset + 20};
    if(g_iCurrentLevel == 10)
    {
        sprintf(szText, "Deaths: %d", g_iKillCounter);
        g_pGame->SetTextColor(0, 0, 0);
    }
    else
    {
        sprintf(szText, "%d", g_iPointsGotten * 10);
        g_pGame->SetTextColor(0, 0, 0);
    }

    //Fill in text background
    myRECT rcTextBg = rectText;
    rcTextBg.left = rcTextBg.right - (g_pGame->GetStringWidth(szText)) - 1;
    g_pGame->FillRect(&rcTextBg, 255,255,255, 128);

    g_pGame->DrawText(szText, &rectText, HGETEXT_RIGHT | HGETEXT_MIDDLE);

    //Draw hubs destroyed
    if(g_iCurrentLevel == 15)
    {
      sprintf(szText, "%d Hubs Destroyed", g_iMissileCount);

      //Draw text bg
      rcTextBg = rectText;
      float fTextWidth = g_pGame->GetStringWidth(szText);
      rcTextBg.left = rcTextBg.right = (g_pGame->GetWidth()) / 2.0;
      rcTextBg.left -= ((fTextWidth / 2.0) + 1);
      rcTextBg.right += ((fTextWidth / 2.0) + 1);
      g_pGame->FillRect(&rcTextBg, 255,255,255, 128);

      g_pGame->SetTextColor(0, 0, 0);
      g_pGame->DrawText(szText, &rectText, HGETEXT_CENTER | HGETEXT_MIDDLE);
    }

    //Do dissolve effect if dying
    if(g_bDying && g_bFadeWhite && g_iDieCount < 30)
    {
        myRECT rcWhite = {0,0,g_pGame->GetWidth(), g_pGame->GetHeight()};
        g_pGame->FillRect(&rcWhite, 255, 255, 255, (255 / (g_iDieCount - 30)));
    }

    if(g_iCurrentLevel == 19)   //Boss level
    {
        //Draw boss health meter
        float fSegment = (float)(g_pGame->GetWidth() - 72)/(float)(BOSS_MAX_HEALTH);
        int iLen = fSegment * (float)(g_iBossHealth);

        //First draw a simple blue bg
        myRECT rcHealthBg = {0, g_pGame->GetHeight() - 28, g_pGame->GetWidth(), g_pGame->GetHeight()};
        if(g_iBossHitThisCycle)
            g_pGame->FillRect(&rcHealthBg, 255, 0, 0);
        else
            g_pGame->FillRect(&rcHealthBg, 88, 132, 196);

        //Draw text
        char szText[64];
        myRECT rcText = {9, g_pGame->GetHeight() - 26, 59, g_pGame->GetHeight() - 4};
        sprintf(szText, "Boss:");
        g_pGame->SetTextColor(255, 255, 255);
        g_pGame->DrawText(szText, &rcText, HGETEXT_CENTER | HGETEXT_TOP);

        myRECT rcBossHealthPos = {63, g_pGame->GetHeight() - 24, 63 + iLen, g_pGame->GetHeight() - 4};
        if(g_iBossHealth <= BOSS_MAX_HEALTH/4)
        {
            if(g_iBossHitThisCycle)
                g_pGame->FillRect(&rcBossHealthPos, 160, 0, 0);
            else
                g_pGame->FillRect(&rcBossHealthPos, 255, 0, 0);
        }
        else
            g_pGame->FillRect(&rcBossHealthPos, 0,255,0);

        //DeleteObject(hBrush);
        if(g_iBossHitThisCycle)
            g_iBossHitThisCycle--;
    }

    //HACK: Draw box coloring the rest of the bg black, rather than fixing the problem
    if(iXViewportOffset)
    {
      myRECT rcHack = {g_pGame->GetWidth() - iXViewportOffset - 1, 0, g_pGame->GetWidth(), g_pGame->GetHeight()};
      g_pGame->FillRect(&rcHack, 0, 0, 0);
      rcHack.left = 0;
      rcHack.right = iXViewportOffset;
      g_pGame->FillRect(&rcHack, 0, 0, 0);
    }
    if(iYViewportOffset)
    {
      myRECT rcHack = {0, g_pGame->GetHeight() - iYViewportOffset - 1, g_pGame->GetWidth(), g_pGame->GetHeight()};
      g_pGame->FillRect(&rcHack, 0, 0, 0);
      rcHack.top = 0;
      rcHack.bottom = iYViewportOffset;
      g_pGame->FillRect(&rcHack, 0, 0, 0);
    }

      } //End else if not 3D

    if(g_iCurrentLevel != 19)   //Don't draw key screen in final boss level (there's no keys to get)
      g_pKeyScreenBitmap->Draw(g_pGame->GetWidth() - iXViewportOffset - g_pKeyScreenBitmap->GetWidth(), g_pGame->GetHeight() - g_pKeyScreenBitmap->GetHeight() - iYViewportOffset);

    if(g_bRedKey)
    {
      g_pSmRedKeyBitmap->Draw(g_pGame->GetWidth() - 96 - iXViewportOffset, g_pGame->GetHeight() - 32 - iYViewportOffset);
    }
    if(g_bOrangeKey)
    {
      g_pSmOrangeKeyBitmap->Draw(g_pGame->GetWidth() - 77 - iXViewportOffset, g_pGame->GetHeight() - 32 - iYViewportOffset);
    }
    if(g_bYellowKey)
    {
      g_pSmYellowKeyBitmap->Draw(g_pGame->GetWidth() - 58 - iXViewportOffset, g_pGame->GetHeight() - 32 - iYViewportOffset);
    }
    if(g_bGreenKey)
    {
      g_pSmGreenKeyBitmap->Draw(g_pGame->GetWidth() - 39 - iXViewportOffset, g_pGame->GetHeight() - 32 - iYViewportOffset);
    }
    if(g_bBlueKey)
    {
      g_pSmBlueKeyBitmap->Draw(g_pGame->GetWidth() - 20 - iXViewportOffset, g_pGame->GetHeight() - 32 - iYViewportOffset);
    }

    //Draw pause menu stuff if paused
    if(g_bPaused)
    {
        myPOINT ptPauseDrawPos = {( g_pGame->GetWidth() - g_pPauseBmp->GetWidth()) / 2.0, (g_pGame->GetHeight() - g_pPauseBmp->GetHeight()) / 2.0};
        g_pPauseBmp->Draw(ptPauseDrawPos.x, ptPauseDrawPos.y);

        //Draw selection bitmaps
        if(g_iPausePos != -1)
        {
            myRECT rcDrawPos = g_rcPauseMenuLocations[g_iPausePos];
            OffsetRect(&rcDrawPos, (g_pGame->GetWidth() - g_pPauseBmp->GetWidth()) / 2, (g_pGame->GetHeight() - g_pPauseBmp->GetHeight()) / 2);
            g_pPauseMenuBmps[g_iPausePos]->Draw(rcDrawPos.left, rcDrawPos.top);
        }
    }
  }
  else  //Not playing game
  {
    g_pGameScreen[g_iCurrentLevel]->Draw(iXStartPos, iYStartPos);
    if(g_iMenuPosSelect != LEVELSELECT_BACK)
        g_pBackBitmap->Draw(g_ptLevelSelectDrawPos[3].x + iXStartPos, g_ptLevelSelectDrawPos[3].y + iYStartPos);
    if(g_iCurrentLevel <= 19 && g_bParts[g_iCurrentLevel])
    {
      g_pDefeatedBitmap->Draw(iXStartPos, iYStartPos);
    }

    //Draw select bmp. Check and see if we're out of bounds first, though
    if(g_iCurrentLevel == 0 && g_iMenuPosSelect == LEVELSELECT_PREV)
        g_iMenuPosSelect = NO_MENU;
    if(g_iCurrentLevel == 21 && g_iMenuPosSelect == LEVELSELECT_NEXT)
        g_iMenuPosSelect = NO_MENU;
    if(g_iMenuPosSelect != NO_MENU)
    {
        g_pLevelSelectBitmap[g_iMenuPosSelect]->Draw(g_ptLevelSelectDrawPos[g_iMenuPosSelect].x + iXStartPos,
                                                     g_ptLevelSelectDrawPos[g_iMenuPosSelect].y + iYStartPos);
    }

    if(!g_bTypeName)
    {
        if(g_iCheckpointInvincibility)
            g_pPersonSprite->SetHidden(false);
        return;
    }
    g_pGameOverBitmap->Draw(iXStartPos, iYStartPos); //Draw dialog box bg



    //Draw the name they're typing
    myRECT rect;
    bool bEnd = false;
    char cName[22];
    for(int i = 0; i < 21; i++)
    {
      cName[i] = g_cName[i];
    }
    cName[21] = '\0';
    rect.left = iXStartPos+259;
    rect.top = iYStartPos+239;
    rect.right = iXStartPos+540;
    rect.bottom = iYStartPos+272;
    for(int i = 0; i < 20; i++)
    {
      if(cName[i] == '~')
      {
        cName[i] = ' ';  //Reset to spaces
        if(!bEnd)
        {
          if(g_iBlinkDelay <= 30)
            cName[i] = '|';
          else
            cName[i] = ' ';
          bEnd = true;
        }
      }
    }
    if(!bEnd)
    {
        if(g_iBlinkDelay <= 30)
            cName[20] = '|';
        else
            cName[20] = ' ';
    }
    else
      cName[20] = ' ';
    g_pGame->SetTextColor(0, 0, 0);
    g_pGame->DrawText(cName, &rect, HGETEXT_LEFT | HGETEXT_MIDDLE);
  }
  if(g_iCheckpointInvincibility)
    g_pPersonSprite->SetHidden(false);
}

void DrawOverlay()  //Draw overlay stuff on top of everything else drawn
{
    //We'll use this for drawing errors/ dialog stuff
    if(g_sDialogText != "")
    {
        if(g_iDisplayDiagTime != DISPLAY_FOREVER)
        {
            if(g_pGame->GetTime() - (unsigned int)g_iStartedDisplayDiag > (unsigned int)g_iDisplayDiagTime) //Since this is time-based, we can do this in the drawing cycle. YAY
            {
                g_sDialogText = "";
                g_iDisplayDiagTime = DISPLAY_FOREVER;
                g_pDiagBmp = NULL;
                return;
            }
        }

        if(g_pDiagBmp == NULL)  //If no bitmap, center text on upper top of screen
        {
            //Draw text
            myRECT rectText;// = {0,0,g_pGame->GetWidth(), g_pGame->GetHeight()};
            float fTextWidth = g_pGame->GetStringWidth(g_sDialogText.c_str());
            fTextWidth += DIALOG_MARGIN * 4;
            float fTextHeight = g_pGame->GetStringHeight();
            fTextHeight += DIALOG_MARGIN * 2;
            rectText.left = (g_pGame->GetWidth() - fTextWidth) / 2;
            rectText.right = rectText.left + fTextWidth;
            rectText.top = fTextHeight * 3;
            if(!g_bPlayingGame)
                rectText.top = g_pGame->GetHeight() - (fTextHeight * 2.0);  //If not playing game, display text at bottom of screen
            rectText.bottom = rectText.top + fTextHeight;

            g_pGame->FillRect(&rectText, 255,255,255, 128);

            g_pGame->SetTextColor(0, 0, 0);
            g_pGame->DrawText(g_sDialogText.c_str(), &rectText, HGETEXT_CENTER | HGETEXT_MIDDLE);
        }
        else    //Draw bitmap as well as text
        {
            myRECT rect = {0, g_pGame->GetHeight() - (g_pDiagBmp->GetHeight() + DIALOG_MARGIN * 2), g_pGame->GetWidth(), g_pGame->GetHeight()};
            g_pGame->FillRect(&rect, 255, 255, 255, 192);   //Fill in the text bg
            if(g_iDialogBitmapAlign == DIALOG_BITMAP_ALIGN_LEFT)
            {
                g_pDiagBmp->Draw(rect.left + DIALOG_MARGIN, rect.top + DIALOG_MARGIN);  //Draw bitmap
                rect.left += g_pDiagBmp->GetHeight() + DIALOG_MARGIN * 2;
                rect.right -= DIALOG_MARGIN;
            }
            else
            {
                g_pDiagBmp->Draw(rect.right - DIALOG_MARGIN - g_pDiagBmp->GetWidth(), rect.top + DIALOG_MARGIN);  //Draw bitmap
                rect.left += DIALOG_MARGIN;
                rect.right -= DIALOG_MARGIN * 2 + g_pDiagBmp->GetWidth();
            }
            rect.top += DIALOG_MARGIN;
            rect.bottom -= DIALOG_MARGIN;
            g_pGame->SetTextColor(0, 0, 0);
            g_pGame->DrawText(g_sDialogText.c_str(), &rect, HGETEXT_LEFT | HGETEXT_TOP);    //Draw text
        }
    }
}

void GameCycle()
{
    //Check power
    if(g_pGame->GetTime() - g_iPowerLastChecked >= POWER_CHECK_FREQUENCY)
    {
        g_iPowerLastChecked = g_pGame->GetTime();   //Might lose a millisecond or two here, but who cares?
        int iCurPower = g_pGame->GetPowerLevel();
        if(iCurPower != HGEPWR_UNSUPPORTED && iCurPower != HGEPWR_AC && iCurPower <= 10)
        {
            string sMsg = "Warning: Battery power is at ";
            sMsg += iCurPower;
            sMsg += "%";
            DisplayMsg(sMsg, 5000); //Display for 5 secs
            //Save, just in case
            SaveGame();
        }

        if(iCurPower != HGEPWR_UNSUPPORTED && iCurPower != HGEPWR_AC)
            ofile << "Current battery level: " << iCurPower << "%" << endl;
    }

    if(g_iMenuPos != NO_MENU)
    {
        if(g_bConfirmResolution)
        {
            if(g_pGame->GetTime() - g_iStartedCountdownAt > RESOLUTION_CHANGE_PAUSE * 1000)
                ResetResolution();
            return;
        }
        if(g_iMenuPos == PROFILES_MENU && !g_bDeleteWarning)
        {
            for(int i = 0; i < NUM_PROFILES; i++)
                g_pFlameSprites[i]->Update(1.0);  //Update these so particles update
        }
        else if(g_iMenuPos == OPTIONS_MENU)
            for(int i = 0; i < NUM_PROFILES; i++)
                g_pFlameSprites[i]->Update(1.0);
        return;
    }

    if(g_bPaused)
    {
        //HACK: Keep person on right frame if jumping while paused
        if((g_bIsJumping) && (!g_bIsFiring) && (!g_bWinning))//If chef is jumping
            g_pPersonSprite->Jumping();

        //Stop boss loop sound playing, so that isn't so obscenely annoying
        if(g_iCurrentLevel == 19)
        {
            g_pGame->SetSoundVol(g_iBossWalkLoopChannel, 0);
        }

        return; //Stop here if paused-don't update anything
    }

    if(g_b3D && g_bPlayingGame)   //If in 3D mode
    {
        if(MessageLoop())
            EndLevel();
        return; //Break out here
    }

    if(g_bPlayingGame && g_iCheckpointInvincibility && !g_bPaused)
    {
          --g_iCheckpointInvincibility;
    }

    if(g_bPlayingGame && g_iCurrentLevel == 19)
    {
        if(g_iBossHitThisCycle)
            g_iBossHitThisCycle--;
    }

    if(g_bTypeName)
    {
        if(++g_iBlinkDelay > 60)
            g_iBlinkDelay = 0;
    }

  //Handle spacing between our jump noises
  if(g_iJumpSoundDelay)
  {
    if(++g_iJumpSoundDelay >= JUMP_SOUND_DELAY)
        g_iJumpSoundDelay  = 0;
  }

  g_bBeside = false;
  g_bOnLeftSlant = false;
  g_bOnRightSlant = false;

  if((g_iFallAmount > 2 && g_bIsFalling) || g_bIsJumping)
    g_bOnPlatform = false;

//  if(CheckWon())
//  {
//    UpdateHiScores(); //Update the high score list
//    WriteHiScores(); //Write the high scores
//    WonGame(); //Do the won game story
//    NewGame(); //Start a new game
//  }

  if(g_bWinning) //If he's winning
  {
    if(--g_iWinCount < 1) //If the winning count is done
    {
      bool bOldPart = g_bParts[g_iCurrentLevel];
      //Check and display a message if we've unlocked something
      if(!g_bParts[g_iCurrentLevel])    //If we hadn't defeated this level before now
      {
          if(g_iCurrentLevel == 1)
          #ifndef DEMO_VERSION
            DisplayMsg("Levels 3-15 unlocked. Scroll right to view.", 7000);
          #else
            DisplayMsg("Level 3 unlocked. Scroll right to view.", 7000);
          #endif

          #ifdef DEMO_VERSION
          if(g_iCurrentLevel == 2)
            DisplayMsg("Thanks for playing the demo. Please upvote and spread the word!", 10000);
          #endif
      }

      ofile << "Defeated level." << endl;
      if(g_bCurLevelPart)
        g_bParts[g_iCurrentLevel] = true;
      if(g_iCurrentLevel == 1 && g_bJetBoots)
        g_bParts[g_iCurrentLevel] = true;   //Special case for secret level

      if(CheckWon() && !bOldPart)   //Defeated last hub level
      {
          //Make sure this is a level that needs to be beaten, otherwise message will display more than once
          for(int i = 0; g_iHubLevels[i] != -1; i++)
          {
              if(g_iHubLevels[i] == g_iCurrentLevel)
              {
                  DisplayMsg("Final level unlocked. Scroll right to view.",7000);
                  break;
              }
          }
      }

      SaveGame();       //Save our game
      g_rcViewport.left = 0;
      g_rcViewport.top = 0;
      g_rcViewport.right = g_pGame->GetWidth();
      g_rcViewport.bottom = g_pGame->GetHeight();
      g_bPlayingGame = false;   //THIS LINE IS SOOO 1337
      g_pBackground[g_iCurrentLevel]->RemoveParticles();
//      ShowCursor(true);     //Show mouse cursor again
      g_bWinning = false;
      g_iWinCount = 30;
      g_pGame->CleanupSong();   //Stop the MIDI playing
      EmptyRespawn();
      g_pLastCheckpoint = NULL;
      if(g_iCurrentLevel == 19)
        g_pGame->KillChannel(g_iBossWalkLoopChannel);

      //Play the menu music
//      if(!g_bNoSound)
        g_pGame->PlaySong(MUSIC_MENU);
        g_iMenuPosSelect = NO_MENU;

      //Display message for unlocking a bonus, if we have
      if(g_iOldElectroCount < NUM_ELECTROSPHERES_3D1 &&
         g_lElectrospheresCollected.size() >= NUM_ELECTROSPHERES_3D1)
        DisplayMsg("Bonus level 61 unlocked! Scroll right to view.", 9001); //IT'S OVER 9000!
      if(g_iOldElectroCount < NUM_ELECTROSPHERES_3D2 &&
         g_lElectrospheresCollected.size() >= NUM_ELECTROSPHERES_3D2)
        DisplayMsg("Bonus level 75 unlocked! Scroll right to view.", 9001);

      //See if player beat final boss after beating other levels
      if(g_iCurrentLevel == 19)
      {
          UpdateHiScores(); //Update the high score list
          WonGame();
      }
    }
    else
    {
      g_pPersonSprite->SetVelocity(0, 0);
      if(g_bFacingLeft)
      {
        g_pPersonSprite->WalkLeft();
        g_pPersonSprite->OffsetPosition(-2, 0);
      }
      else
      {
        g_pPersonSprite->Walk();
        g_pPersonSprite->OffsetPosition(2, 0);
      }
    }
  }

  //Pan camera someplace
  static int iCameraPause = 0;
  if(g_ptPanTo.x != -1 && g_ptPanTo.y != -1)    //Move camera logarithmically towards where it needs to go
  {
//      ofile << "Movement it not 0" << endl;
      //Player can't move
      g_pPersonSprite->SetVelocity(0,0);

    int xViewport = g_pGame->GetWidth() / 2.0;
    int yViewport = g_pGame->GetHeight() / 2.0;
    if(g_rcAll.right - g_rcAll.left < g_pGame->GetWidth())
        xViewport = (g_rcAll.right - g_rcAll.left) / 2;
    if(g_rcAll.bottom - g_rcAll.top < g_pGame->GetHeight())
        yViewport = (g_rcAll.bottom - g_rcAll.top) / 2;

    //Check and make sure it's inside viewport range
    if(g_ptPanTo.x < g_rcAll.left + xViewport)
        g_ptPanTo.x = g_rcAll.left + xViewport;
    if(g_ptPanTo.x > g_rcAll.right - xViewport)
        g_ptPanTo.x = g_rcAll.right - xViewport;
    if(g_ptPanTo.y < g_rcAll.top + yViewport)
        g_ptPanTo.y = g_rcAll.top + yViewport;
    if(g_ptPanTo.y > g_rcAll.bottom - yViewport)
        g_ptPanTo.y = g_rcAll.bottom - yViewport;

    myPOINT ptCenterScreen = {g_rcViewport.left + g_pGame->GetWidth() / 2, g_rcViewport.top + g_pGame->GetHeight() / 2};
    //Some major problems on larger displays without this...
    if(g_rcAll.right - g_rcAll.left < g_pGame->GetWidth())
        ptCenterScreen.x = g_rcViewport.left + (g_rcAll.right - g_rcAll.left) / 2;
    if(g_rcAll.bottom - g_rcAll.top < g_pGame->GetHeight())
        ptCenterScreen.y = g_rcViewport.left + (g_rcAll.bottom - g_rcAll.top) / 2;
    myPOINT ptMove = { g_ptPanTo.x - ptCenterScreen.x, g_ptPanTo.y - ptCenterScreen.y};
    if(abs(ptMove.x) <= 2.0 && abs(ptMove.y) <= 2.0)
    {
        //ofile << "Camera pause: " << iCameraPause << endl;
        if(++iCameraPause >= 45)    //pause for a bit
        {
            iCameraPause = 0;
            g_ptPanTo.x = g_ptPanTo.y = -1;
            if(g_bPanBackToPlayer)
            {
                g_ptPanTo = g_pPersonSprite->GetCenter();
                iCameraPause = 45;  //Don't pause when you get back
            }
            g_bPanBackToPlayer = false;
        }
        if(g_pExitSprite != NULL && g_bExitOpen)
        {
            g_pExitSprite->SetBitmap(g_pExitBitmap);
            g_bExitOpen = false;
            //Play open exit noise
            PlayMySound(IDW_EXITOPEN);
        }
    }
    else
    {
        //ofile << "Movement total: " << ptMove.x << ", " << ptMove.y << endl;
        ptMove.x *= 0.15;
        ptMove.y *= 0.15;
        //Don't move less than 1 pixel at a time
        if(ptMove.x < 0 &&
           ptMove.x > -1)
            ptMove.x = -1;
        if(ptMove.y < 0 &&
           ptMove.y > -1)
            ptMove.y = -1;
        if(ptMove.x > 0 &&
           ptMove.x < 1)
            ptMove.x = 1;
        if(ptMove.y > 0 &&
           ptMove.y < 1)
            ptMove.y = 1;

        //ofile << "Moving " << ptMove.x << ", " << ptMove.y << endl;
        OffsetRect(&g_rcViewport, ptMove.x, ptMove.y);
    }
    //Check and make sure viewport is in bounds
    KeepViewportOnScreen();

  }
//  else
//    ofile << "Movement is 0" << endl;

  //Check and make sure exit is open if it's supposed to be
  if(g_ptPanTo.x == -1 &&
     g_ptPanTo.y == -1 &&
     g_bExitOpen &&
     g_pExitSprite != NULL &&
     IntersectRect(&(g_pExitSprite->GetPosition()), &g_rcViewport) &&
     g_pExitSprite->GetBitmap() == g_pExitClosedBitmap)
  {
      g_pExitSprite->SetBitmap(g_pExitBitmap);
      g_bExitOpen = false;
  }

  if(g_bDying)    //If Chef is dying
  {
//    if(g_iDieCount == 15 && g_bFadeWhite)
//        g_pGame->SetFrameRate(FRAME_RATE);
    if(--g_iDieCount == 0)    //If he's dead
    {
      if(g_iCurrentLevel == 19) //Boss level
      {
          g_rcViewport.left = 0;
          g_rcViewport.top = 0;
          g_rcViewport.right = g_pGame->GetWidth();
          g_rcViewport.bottom = g_pGame->GetHeight();
          EmptyRespawn();
          g_iDieCount = 30;      //Reset dying counter
          g_bDying = false;     //He's not dying anymore
          g_pPersonSprite->SetHidden(false);
          StartLevel();   //Restart whole thing
      }
      else
      {
          g_iDieCount = 30;      //Reset dying counter
          g_bDying = false;     //He's not dying anymore
          g_pPersonSprite->SetHidden(false);
          g_pPersonSprite->SetPosition(g_rcLastCheckpointPos.left, g_rcLastCheckpointPos.top);
          CopyRect(&g_rcAll,&g_rcLastViewport);
          g_pPersonSprite->SetVelocity(0,0);        //No movement
          g_iFallAmount = g_iJumpAmount = 0;
          RespawnAll();                             //Make player get all items again that he had to get before
          g_iCheckpointInvincibility = CHECKPOINT_INVINCIBLE_TIME;      //Start checkpoint invincibility timer
          g_ptPanTo.x = g_ptPanTo.y = -1;   //For explosion ending, go back to player
//          if(g_pGame->GetFrameRate() != FRAME_RATE)
//            g_pGame->SetFrameRate(FRAME_RATE);
      }
    }
  }

  if(g_bHole)        //If hit hole this cycle,
  {
    //HACK hack around our test to keep track of how many electrospheres we had
    int iTempElecCount = g_iOldElectroCount;
    g_pBackground[g_iCurrentLevel]->RemoveParticles();
//    bool b = g_pGame->GetMusic();        //Supress music starting
//    g_pGame->SetMusic(false);// = true;
    if(g_iCurrentLevel == 1)  //If in second level
    {
      g_iCurrentLevel = 16;    //Start secret level 1
      StartLevel();
    }
    else if(g_iCurrentLevel == 6)  //If in seventh level
    {
      g_iCurrentLevel = 17;    //Start secret level 2
      StartLevel();
    }
    else if(g_iCurrentLevel == 8)  //If in ninth level
    {
      g_iCurrentLevel = 18;    //Start level continuation
      StartLevel();
    }
    else if(g_iCurrentLevel == 15)  //If in last level, leaving
    {
        g_bParts[g_iCurrentLevel] = true;   //We've beaten this level now
        g_iCurrentLevel = 19;   //FINAL BOSS TIEM
//        g_pGame->SetMusic(b);         //Hack over our hack to play final boss music
        StartLevel();
    }
    else if(g_iCurrentLevel == 16)  //If in secret level 1
    {
      if(g_bCurLevelPart)
        g_bParts[g_iCurrentLevel] = true;   //Record us as having gotten super jet boots
      g_iCurrentLevel = 1;        //Back to second level
      StartLevel();
      g_pPersonSprite->OffsetPosition(0, 375);   //Set position to right below hole in second level
    }
    else if(g_iCurrentLevel == 17)  //If in secret level 2
    {
      g_iCurrentLevel = 6;        //Back to seventh level
      StartLevel();
      g_pPersonSprite->OffsetPosition(0, 900);   //Set position to right below hole in seventh level
    }
    else if(g_iCurrentLevel == 18)  //If in continuation level
    {
      g_iCurrentLevel = 8;        //Back to ninth level
      StartLevel();
      g_pPersonSprite->SetPosition(3225, 2850);   //Set position to right below hole in ninth level
    }
    g_bHole = false;
    g_iJetbootsTime = MAX_JETBOOT_TIME; //Do allow jetboosting when starting secret levels / continuations / final boss
//    g_pGame->SetMusic(b);
    g_iOldElectroCount = iTempElecCount;    //So we don't miss our message if we get the right number in a secret level
  }

  if(!g_bPlayingGame)
  {
      if(g_iCurrentLevel == 16)
        g_iCurrentLevel = 1;
      else if(g_iCurrentLevel == 17)
        g_iCurrentLevel = 6;
      else if(g_iCurrentLevel == 18)
        g_iCurrentLevel = 8;
//      else if(g_iCurrentLevel > 19)
//        g_iCurrentLevel = 15;
  }

  if(g_iCurrentLevel == 14 && (g_bPlayingGame)&& (rand() % 60 == 0) && (g_rcViewport.top > 150))//If in lower part of level 15
  {
    switch(rand() % 3)  //Play a random drip sound
    {
      case 0:
        // Play the first drip sound
        PlayMySound(IDW_DRIPECHO1);
        break;
      case 1:
        // Play the second drip sound
        PlayMySound(IDW_DRIPECHO2);
        break;
      default:
        // Play the third drip sound
        PlayMySound(IDW_DRIPECHO3);
        break;
    }
  }

  if(g_bPlayingGame && g_ptPanTo.x == -1 && g_ptPanTo.y == -1)
  {
      if((g_bIsJumping) && (!g_bIsFiring) && (!g_bWinning))//If chef is jumping
      {
        g_pPersonSprite->Jumping();
      }
      if((g_bIsJumping) && (!g_bWinning))
      {
        ControlJump();
      }
      if(g_bIsFalling && (!g_bIsJumping) && (!g_bWinning))  //If he's falling but not jumping
      {
        g_pPersonSprite->Gravity(); //Make chef fall
      }
      else if(!g_bIsFalling && (!g_bIsJumping) && (!g_bWinning))
      {
        g_bIsFalling = true;
        g_iFallAmount = 0;
      }
      g_bIsFiring = false;

      CheckJetboots();
  }

  // Update the sprites if playing level and not winning level
  if((g_bPlayingGame) && (!g_bWinning))
  {
      g_pGame->UpdateSprites();
      g_pBackground[g_iCurrentLevel]->Update(g_rcViewport);
  }

  //Update the current viewport
  if(!g_bDying && g_bPlayingGame && g_ptPanTo.x == -1 && g_ptPanTo.y == -1)   //If he's dying or not playing, freeze the viewport
  {
     if(!(g_iCurrentLevel == 19 && g_bBossDying))
        UpdateViewport();

     //If viewport shake, do so
     if(g_iViewportShakeAmt > 0)
     {
         float fXOffset = ((rand() % g_iViewportShakeAmt) - (g_iViewportShakeAmt / 2.0));
         float fYOffset = ((rand() % g_iViewportShakeAmt) - (g_iViewportShakeAmt / 2.0));

         OffsetRect(&g_rcViewport, fXOffset, fYOffset);

         --g_iViewportShakeAmt; //Decrease the amount it shakes for next cycle

         KeepViewportOnScreen();
     }
     //Done screen scrolling!
  }

  static int iPlaformDeathTimer = 0;
  //If in last level, see if door should open
  if(g_iCurrentLevel == 15 && g_iMissileCount >= FINAL_MISSILE_COUNT && g_pBlockingPlatform != NULL && IntersectRect(&(g_pBlockingPlatform->GetPosition()), &g_rcViewport))
  {
      if(++iPlaformDeathTimer >= 20)
      {
          AddToRespawn(g_pBlockingPlatform);
          g_pBlockingPlatform = NULL;
          iPlaformDeathTimer = 0;
      }
  }

  if(g_iCurrentLevel == 19 && g_bPlayingGame && !g_bWinning)    //Update the boss if we're in the last level
    UpdateBoss();
}

void HandleKeys()
{
    //DEBUG
    if(g_pGame->GetAsyncKeyState(HGEK_F10))
    {
        ofile << "DEBUG: Resetting tutorial variables." << endl;
        g_iFinalBossDialogLocation = 0;
        g_bTutorialMoved = false;
        g_bTutorialJumped = false;
        g_bTutorialFired = false;
        g_bTutorialSlopeFired = false;
        g_bTutorialElectrosphere = false;
        g_bTutorialWASD = false;
    }

    if(g_pGame->GetAsyncKeyState(HGEK_0))
    {
        g_iKillCounter = 0;
    }

  bool bMoving = false;
  myPOINT ptVelocity;

  //Fullscreen toggle happens basically no matter what
  static bool bFullscreenToggle = false;
  if(g_pGame->GetAsyncKeyState(HGEK_ALT) && g_pGame->GetAsyncKeyState(HGEK_ENTER))
  {
      if(!bFullscreenToggle)
      {
          g_pGame->ToggleFullscreeen();
          bFullscreenToggle = true;
      }
  }
  else
    bFullscreenToggle = false;

  if(g_bEscPressed && !g_pGame->GetAsyncKeyState(HGEK_ESCAPE))
    g_bEscPressed = false;

  //Handle key presses if in menu
  if(g_iMenuPos != -1)
  {
      //Pressing Esc in main menu ends game
      if(!g_bEscPressed && g_pGame->GetAsyncKeyState(HGEK_ESCAPE))
      {
          if(g_bConfirmResolution)  //Cancel resolution change if hit esc while in confirm resolution dialog
          {
              ResetResolution();
              g_bEscPressed = true;
          }
          else if(g_iMenuPos == MAIN_MENU)   //Exit if pressing Esc in main menu
            g_pGame->End();
          else if(g_iMenuPos == PROFILES_MENU)  //Go back to main menu from profiles menu
          {
              g_bEscPressed = true;
              g_iMenuPos = MAIN_MENU;
              g_iMenuPosSelect = -1;
              //Kill particles for flames
              for(int i = 0; i < NUM_PROFILES; i++)
                  g_pFlameSprites[i]->Kill();
              WriteConfig();
          }
          else if(g_iMenuPos == OPTIONS_MENU)
          {
              for(int i = 0; i < NUM_PROFILES; i++)
                g_pFlameSprites[i]->Kill();
              g_bEscPressed = true;
              g_iMenuPos = g_iCameToOptionsFrom;
              g_iMenuPosSelect = -1;
              WriteConfig();
          }
          else if(g_iMenuPos == RESOLUTION_MENU)
          {
              g_iMenuPos = OPTIONS_MENU;
              g_iMenuPosSelect = -1;
              g_bEscPressed = true;
              LoadOptionsMenuStuff();
              WriteConfig();
          }
          else if(g_iMenuPos == KEYCONFIG_MENU)
          {
              if(g_pKeyToSet == NULL)
              {
                  g_iMenuPos = OPTIONS_MENU;
                  g_iMenuPosSelect = -1;
                  g_bEscPressed = true;
                  LoadOptionsMenuStuff();
                  WriteConfig();
              }
              else
              {
                  g_pKeyToSet = NULL;
                  g_bEscPressed = true;
                  g_pGame->ShowMouse();
              }
          }
      }

      if(g_iMenuPos == MAIN_MENU && g_pGame->GetAsyncKeyState(HGEK_ENTER) && !g_bEnter)
      {
          if(g_iMenuPosSelect == MAINMENU_CONTINUE) //Start game
          {
              g_iMenuPos = -1;
              g_iMenuPosSelect = -1;
              g_bEnter = true;
              PlayMySound(IDW_SHOOTFORK);
          }
      }

      return;   //Don't allow more keypresses
  }

  //Pressing Esc exits high scores
  if(g_bHighScores && g_pGame->GetAsyncKeyState(HGEK_ESCAPE) && !g_bEscPressed)
  {
      g_bHighScores = false;
      g_iMenuPos = g_iCameToHighScoresFrom;
      g_iMenuPosSelect = -1;
      g_bEscPressed = true;
      PlayMySound(IDW_GOTFORK);
  }

  //Handle keypresses for bg story dialog
  if((g_bBgStory || g_bEgStory) && !g_bTypeName)
  {
      if(g_pGame->GetAsyncKeyState(HGEK_ESCAPE) && !g_bEscPressed)
      {
          g_bEscPressed = true;
          g_iMenuPosSelect = NO_MENU;
          g_bBgStory = false;
          g_iBgStoryPanel = 0;
          if(g_bEgStory)
          {
              g_bEgStory = false;
              g_bHighScores = true;
              g_iCameToHighScoresFrom = MAIN_MENU;
          }
          PlayMySound(IDW_GOTFORK);
      }
      //Previous panel
      if(LeftKey() && !g_bLeft)
      {
          if(g_iBgStoryPanel > 0)
          {
              g_iBgStoryPanel--;
              PlayMySound(IDW_PREVIOUSLEVEL);
          }
          g_bLeft = true;
          g_iMenuPosSelect = NO_MENU;   //To prevent crashes
      }
      else if(!LeftKey())
        g_bLeft = false;
      //Next panel/end
      if(RightKey() && !g_bRight)
      {
          g_iBgStoryPanel++;
          if(g_bBgStory && g_iBgStoryPanel >= NUM_BGSTORY_PANELS)
          {
              g_bBgStory = false;
              g_iBgStoryPanel = 0;
              g_iMenuPosSelect = NO_MENU;
              PlayMySound(IDW_GOTFORK);
          }
          else if(g_bEgStory && g_iBgStoryPanel >= NUM_EGSTORY_PANELS)
          {
              g_bEgStory = false;
              g_iBgStoryPanel = 0;
              g_iMenuPosSelect = NO_MENU;
              PlayMySound(IDW_GOTFORK);
              g_bHighScores = true;
              g_iCameToHighScoresFrom = MAIN_MENU;
          }
          else
              PlayMySound(IDW_NEXTLEVEL);
          g_bRight = true;
          g_iMenuPosSelect = NO_MENU;
      }
      else if(!RightKey())
        g_bRight = false;
      //Enter also advances
      if(g_pGame->GetAsyncKeyState(HGEK_ENTER) && !g_bEnter)
      {
          g_iBgStoryPanel++;
          if(g_bBgStory && g_iBgStoryPanel >= NUM_BGSTORY_PANELS)
          {
              g_bBgStory = false;
              g_iBgStoryPanel = 0;
              g_iMenuPosSelect = NO_MENU;
              PlayMySound(IDW_GOTFORK);
          }
          else if(g_bEgStory && g_iBgStoryPanel >= NUM_EGSTORY_PANELS)
          {
              g_bEgStory = false;
              g_iBgStoryPanel = 0;
              g_iMenuPosSelect = NO_MENU;
              PlayMySound(IDW_GOTFORK);
              g_bHighScores = true;
              g_iCameToHighScoresFrom = MAIN_MENU;
          }
          else
              PlayMySound(IDW_NEXTLEVEL);
          g_bEnter = true;
          g_iMenuPosSelect = NO_MENU;
      }
      else if(!g_pGame->GetAsyncKeyState(HGEK_ENTER))
        g_bEnter = false;

      return;
  }

  //Pressing Esc while not playing game exits to main menu
  if(!g_bPlayingGame && g_pGame->GetAsyncKeyState(HGEK_ESCAPE) && !g_bEscPressed)
  {
      g_iMenuPos = MAIN_MENU;
      g_iMenuPosSelect = -1;
      g_bEscPressed = true;
      PlayMySound(IDW_GOTFORK);
  }

  //Don't allow keystrokes if winning or typing name in
  if(g_bWinning || g_bTypeName)
  {
    return;
  }

  //or if camera panning
  if(g_ptPanTo.x != -1 && g_ptPanTo.y != -1)
    return;

  // If the game is paused, handle up-down key presses to select menu items
  if(g_bPaused)
  {
      //Choose this menu item if pressing Enter or jump key
      if(g_pGame->GetAsyncKeyState(HGEK_ENTER) || JumpKey())
      {
            HandlePauseMenuItem(g_iPausePos);
            g_bEnter = true;
            PlayMySound(IDW_SHOOTFORK);
      }

      //Up key
      if(g_pGame->GetAsyncKeyState(UP_KEY) && !g_bUpKeyPressed)
      {
          g_bUpKeyPressed = true;
          g_iPausePos--;
          if(g_iPausePos < 0)
            g_iPausePos = 4;

          PlayMySound(IDW_PREVIOUSLEVEL);
          g_pGame->HideMouse();
      }
      else if(!g_pGame->GetAsyncKeyState(UP_KEY))
        g_bUpKeyPressed = false;

      //Down key
      if(g_pGame->GetAsyncKeyState(DOWN_KEY) && !g_bDownKeyPressed)
      {
          g_bDownKeyPressed = true;
          g_iPausePos++;
          if(g_iPausePos > 4)
            g_iPausePos = 0;

          PlayMySound(IDW_NEXTLEVEL);
          g_pGame->HideMouse();
      }
      else if(!g_pGame->GetAsyncKeyState(DOWN_KEY))
        g_bDownKeyPressed = false;
  }

  if((g_pGame->GetAsyncKeyState(HGEK_SHIFT)) && (g_pGame->GetAsyncKeyState(HGEK_F12))) //If pressing F12 and shift
  {
      if(g_iCurrentLevel < 20)
        g_bParts[g_iCurrentLevel] = true;       //Cheat and get part
  }
  if((g_pGame->GetAsyncKeyState(HGEK_SHIFT)) && (g_pGame->GetAsyncKeyState(73))) //If pressing I and shift
  {
    g_bInvincible = true;       //Cheat and become invincible
  }
//  if(g_pGame->GetAsyncKeyState(HGEK_F1)) //If pressing F1
//  {
//    DoHelp();
//  }
  //if((g_pGame->GetAsyncKeyState(HGEK_B)) && (!g_bPlayingGame)) //If pressing B
  //{
  //  BackgroundStory();
  //}
  /*if(g_pGame->GetAsyncKeyState(HGEK_F2)) //If pressing F2
  {
    if(g_bNoSound)
    {
        g_bNoSound = false;
        if(g_bPlayingGame)
            g_pGame->PlaySong("");
    }
    else
    {
        g_bNoSound = true;
        if(g_bPlayingGame)
          g_pGame->CleanupSong();
    }
  }*/

  /*if(g_pGame->GetAsyncKeyState(HGEK_F3))
  {
      //ofile << g_rcViewport.left << ", " << g_rcViewport.top << " " << g_rcViewport.right << ", " << g_rcViewport.bottom << endl;
      //ofile << g_pEnemyCrab->GetFrame() << endl;
      //ofile << "spawned: " << g_iBossEnemiesSpawned << " killed: " << g_iBossEnemiesKilled << endl;
      //ofile << "DEBUG: Type name enabled" << endl;
      g_bTypeName = true;
  }*/
  //If pressing F4, toggle to windowed 800x600 screen resolution
  /*if(g_pGame->GetAsyncKeyState(HGEK_F4) && !(g_pGame->GetAsyncKeyState(HGEK_ALT)))
  {
      //ofile << "DEBUG: Changing resolution" << endl;
      g_pGame->ChangeResolution(800, 600, false);
  }*/
  //If pressing F9, end
  //if(g_pGame->GetAsyncKeyState(HGEK_F9))
  //{
  //    ofile << "DEBUG: F9 ending game" << endl;
  //    g_pGame->End();
  //}

  if (g_bPlayingGame  && (!g_bDying) && !g_bPaused)
  {
    if (!g_b3D && LeftKey() && !(g_iCurrentLevel == 19 && g_iFinalBossDialogLocation < DIALOG_OVER))
    {
      if(!g_bIsJumping && !g_bIsFalling)       // Make sure he isn't jumping or falling
      {
        g_pPersonSprite->WalkLeft();           // Make chef walk left
        g_bTutorialMoved = true;
//        if(!g_bNoSound)
//        {
            if(g_iMoveSoundDelayTime++ >= WALK_SOUND_DELAY)
            {
                //Make it sound like Chef is walking
                PlayMySound(IDW_CHEFWALK);
                g_iMoveSoundDelayTime = 0;
            }
 //       }
      }
      g_bFacingLeft = true;                 //Record chef facing left (used for dying animation)
      MoveChef(1);
      g_iMoveHorizontal = 1;
      bMoving = true;
    }
    else if (!g_b3D && RightKey() && !(g_iCurrentLevel == 19 && g_iFinalBossDialogLocation < DIALOG_OVER))
    {
      if(!g_bIsJumping && !g_bIsFalling)       // Make sure he isn't jumping or falling
      {
        g_pPersonSprite->Walk();               // Make chef look like he's walking
        g_bTutorialMoved = true;
//        if(!g_bNoSound)
//        {
            if(g_iMoveSoundDelayTime++ >= WALK_SOUND_DELAY)
            {
                //Make it sound like Chef is walking
                PlayMySound(IDW_CHEFWALK);
                g_iMoveSoundDelayTime = 0;
            }
//        }
      }
      g_bFacingLeft = false;                 //Record chef facing right (used for dying animation)
      MoveChef(2);                             // Move him right
      g_iMoveHorizontal = 2;
      bMoving = true;
    }
    else if(!bMoving && !g_b3D)          // If player isn't pressing arrow keys
    {
      g_iMoveSoundDelayTime = 2;
      ptVelocity = g_pPersonSprite->GetVelocity();
      // Make chef slow down and stop moving horizontal
      if(ptVelocity.x > 0)
      {
          ptVelocity.x = max((int)(ptVelocity.x - 2), 0);
      }
      else
      {
          ptVelocity.x = min((int)(ptVelocity.x + 2), 0);
      }
      g_pPersonSprite->SetVelocity(ptVelocity);
      if(ptVelocity.x == 0 && !g_bIsFalling)
      {
          g_pPersonSprite->StopWalking();
      }
    }

    if(g_iCurrentLevel == 19 && g_iFinalBossDialogLocation < DIALOG_OVER)
        return; //Don't accept keystrokes if in last level not fighting boss yet

    if (g_pGame->GetAsyncKeyState(HGEK_ESCAPE) && !g_bPaused)
    {
        g_bPaused = true;
        g_ptLastMousePos = g_pGame->GetMousePos();
        g_iPausePos = 0;
        PlayMySound(IDW_GOTFORK);
    }

    if(g_b3D)
        return;

    if(JumpKey())       // Time to jump!
    {
      if(!g_bIsFalling && !g_bIsJumping)       // Make sure he isn't falling or jumping
      {                                        // already.
        g_bIsJumping = true;                   // Start his jumping
        g_bIsFalling = false;
        g_bDoubleJumpPressThisCycle = false;
        g_bJumpBlockJump = false;
        g_bTutorialJumped = true;   //Record us as having jumped

          g_iJumpAmount = 21;   //Three block jump height
//          if(!g_bNoSound)
//          {
              if(!g_iJumpSoundDelay)
              {
                  // Play the jump sound
                  PlayMySound(IDW_CHEFJUMPING);

                  g_iJumpSoundDelay = 1;
              }
 //         }
      }
    }
    if((!(JumpKey())) && (g_bIsJumping) && (!g_bJumpBlockJump) && !(g_iJumpAmount <= 5))
    {  //If jumping but not holding up key and jumping more than ten high each cycle
      g_iJumpAmount -= 5;                         //Make him jump less
    }
    if(FireKey())
    {
      g_pPersonSprite->Fire();                 // Make it look like he's shooting
      g_bIsFiring = true;                      // and keep it that way
      if(!g_bFired)           // Actually fire if not already this
      {                                         // spacebar press and if he has any spoons
        // Create a new spoon sprite
        myRECT  rcPos = g_pPersonSprite->GetPosition();
        Sprite* pSprite;
        g_bTutorialFired = true;    //Learned how to shoot spoons
        if(g_iMoveHorizontal == 2)  //Facing right
        {
          pSprite = new Sprite(g_pFlySpoonRBitmap, g_rcAll, BA_DIE);
          if((g_bOnLeftSlant) && (DownKey()))      //If on left slant facing right
          {
            pSprite->SetVelocity(15, -15);       //Shoot diagonally up
            g_bTutorialSlopeFired = true;   //Learned how to shoot up/down slopes
          }
          else if((g_bOnRightSlant) && (DownKey()))  //If on right slant facing right
          {
            pSprite->SetVelocity(15, 15);       //Shoot diagonally down
            g_bTutorialSlopeFired = true;   //Learned how to shoot up/down slopes
          }
          else
          {
            pSprite->SetVelocity(24, 0);
          }
          pSprite->SetZOrder(5);
          pSprite->SetNumFrames(8);
          if((DownKey()) && (!g_bOnRightSlant) && (!g_bOnLeftSlant)) //If pressing the down key and not on a slant
          {
            pSprite->SetPosition(rcPos.right - 8 - pSprite->GetWidth(), rcPos.top + 23);
            pSprite->SetVelocity(0, 24);              //Shoot down
          }
          else if(UpKey())  //If pressing up
          {
            pSprite->SetPosition(rcPos.right - 8 - pSprite->GetWidth(), rcPos.top + 23);
            pSprite->SetVelocity(0, -24);              //Shoot up
          }
          else
          {
            pSprite->SetPosition(rcPos.right - 1 - pSprite->GetWidth(), rcPos.top + 23);
          }
          g_pGame->AddSprite(pSprite);
        }
        else if(g_iMoveHorizontal == 1)     //Facing left
        {
          pSprite = new Sprite(g_pFlySpoonLBitmap, g_rcAll, BA_DIE);
          if((g_bOnRightSlant) && (DownKey()))    //If on right slant facing left
          {
            pSprite->SetVelocity(-15, -15);              //Shoot diagonally up
            g_bTutorialSlopeFired = true;   //Learned how to shoot up/down slopes
          }
          else if((g_bOnLeftSlant) && (DownKey())) //If on left slant facing left
          {
            pSprite->SetVelocity(-15, 15);             //Shoot diagonally down
            g_bTutorialSlopeFired = true;   //Learned how to shoot up/down slopes
          }
          else
          {
            pSprite->SetVelocity(-24, 0);
          }
          pSprite->SetZOrder(5);
          pSprite->SetNumFrames(8);
          if((DownKey()) && (!g_bOnRightSlant) && (!g_bOnLeftSlant)) //If pressing the down key and not on a slant
          {
            pSprite->SetPosition(rcPos.left + 8, rcPos.top + 23);
            pSprite->SetVelocity(0, 24);              //Shoot down
          }
          else if(UpKey())  //If pressing c or v or b or n or m
          {
            pSprite->SetPosition(rcPos.left + 8, rcPos.top + 23);
            pSprite->SetVelocity(0, -24);              //Shoot up
          }                                                                 //THIS LINE OF CODE IS SOO LAST YEAR
          else
          {
            pSprite->SetPosition(rcPos.left - 1, rcPos.top + 23);
          }
          g_pGame->AddSprite(pSprite);
        }
//        if(!g_bNoSound)
//        {
          // Play the fire fork sound
          PlayMySound(IDW_SHOOTFORK);
//        }
        g_bFired = true;                         //Record him firing this spacebar press
        //g_iSpoons--;                              //Make him lose a spoon
      }
    }
    else
    {
        g_bFired = false;                       //Make it so he can fire next spacebar press
    }
  }
  else if((!g_bPlayingGame)) //If he's not playing a game
  {
    if(g_pGame->GetAsyncKeyState(HGEK_ENTER) || JumpKey())       // Time to start a game
    {
      if((!g_bHighScores) && (!g_bEnter))
      {
        g_bPlayingGame = true;
        g_pGame->HideMouse();
//        ShowCursor(false);  //Hide cursor while in level
        PlayMySound(IDW_GOTFORK);
        StartLevel();
      }
      else if(!g_bEnter)
      {
        g_bHighScores = false;
        PlayMySound(IDW_GOTFORK);
        g_iMenuPos = g_iCameToHighScoresFrom;
        g_iMenuPosSelect = -1;
//        if(CheckWon())
//            NewGame();
        g_bEnter = true;       //Make level not start
      }
    }
    else if((LeftKey())  && (!g_bHighScores) && (!g_bLeft))  //Go to previous level
    {
        if(g_iCurrentLevel != 0)      //If there is one
        {
          if(g_iCurrentLevel == 19)
            g_iCurrentLevel = 15;
          else
            g_iCurrentLevel--;
//          if(!g_bNoSound)
//          {
            // Play the go to previous level sound
            PlayMySound(IDW_PREVIOUSLEVEL);
//          }
        }
        g_bLeft = true;
    }
    else if((RightKey())  && (!g_bHighScores) && (!g_bRight))    //Go to next level
    {
        #if defined DEMO_VERSION
        if(g_iCurrentLevel == 2)
            DisplayMsg("Please purchase the full game to play further.",7000);
        if(g_iCurrentLevel != 2)     //Limit players to 3 levels in the demo
        #else
        if(g_iCurrentLevel != 15)    //If there is one
        #endif
        {
          if(g_iCurrentLevel == 14 && !CheckWon())
          {
              g_iCurrentLevel = 20; //First secret level
              PlayMySound(IDW_NEXTLEVEL);
          }
          else if(!(g_iCurrentLevel == 1 && !g_bParts[g_iCurrentLevel]) && g_iCurrentLevel != 21)   //If we're at level 2, & haven't gotten jet boots yet, block from going further.
          {
            g_iCurrentLevel++;
            // Play the go to next level sound
            PlayMySound(IDW_NEXTLEVEL);
          }
          else
            DisplayMsg("Complete this level in order to progress.",7000);
        }
        else if(g_iCurrentLevel == 15)
        {
            if(g_bParts[g_iCurrentLevel])
                g_iCurrentLevel = 19;
            else
                g_iCurrentLevel = 20;
            PlayMySound(IDW_NEXTLEVEL);
        }
        g_bRight = true;
    }
    if(g_iCurrentLevel == 21 && g_lElectrospheresCollected.size() < NUM_ELECTROSPHERES_3D2)
        g_iCurrentLevel--;
    if(g_iCurrentLevel == 20 && g_lElectrospheresCollected.size() < NUM_ELECTROSPHERES_3D1)
        g_iCurrentLevel--;
    if(g_iCurrentLevel == 19 && !g_bParts[15])
        g_iCurrentLevel = 15;
    if(g_iCurrentLevel == 15 && !CheckWon())
    {
        g_iCurrentLevel--;
        if(RightKey())
            DisplayMsg("Complete the previous levels in order to progress.",7000);
    }
  }
  if(!(g_pGame->GetAsyncKeyState(HGEK_ENTER)) && !JumpKey())
  {
    g_bEnter = false;
  }
  if(!(LeftKey()))
  {
    g_bLeft = false;
  }
  if(!(RightKey()))
  {
    g_bRight = false;
  }
}

/*bool UpKeyPressed(void)
{
  if(JumpKey())       //pressing jump
  {
    return true;
  }
  return false;
}*/

void MouseButtonDown(int x, int y, bool bLeft)     //Only used for cheat codes and menus
{
  myPOINT ptClickPos = {x,y};
  if(!bLeft)
  {
    if(g_bPlayingGame)
    {
      if((g_pGame->GetAsyncKeyState(HGEK_SHIFT)) && (g_pGame->GetAsyncKeyState(HGEK_CTRL)))
      {
        g_pPersonSprite->SetPosition(x + g_rcViewport.left, y + g_rcViewport.top);  //Set his position to the mouse click
      }
    }
  }

  if(g_bHighScores)
  {
      g_bHighScores = false;
      PlayMySound(IDW_GOTFORK);
      g_iMenuPos = g_iCameToHighScoresFrom;
      g_iMenuPosSelect = -1;
      return;
  }

  if(g_iMenuPos != -1 && g_bConfirmResolution)
  {
      if(g_iMenuPosSelect == RESOLUTION_CONFIRM_YES)
      {
          //We're good
          g_bConfirmResolution = false;
          PlayMySound(IDW_SHOOTFORK);
          WriteConfig();
      }
      else if(g_iMenuPosSelect == RESOLUTION_CONFIRM_NO)
      {
          //Reset
          PlayMySound(IDW_SHOOTFORK);
          ResetResolution();
      }
      return;
  }

  //Clicking on menu item
  if(g_iMenuPos == OPTIONS_MENU)
  {
      myPOINT ptSliderPos = ptClickPos;
      ptSliderPos.x -= (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2.0;
      ptSliderPos.y -= (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2.0;
      if(PtInRect(&g_rcOptionsSliderPos[0], &ptSliderPos))   //Clicking somewhere in sound slider rectangle
      {
          //Start dragging this
          g_iSliderDrag = OPTIONSMENU_SOUNDSLIDER;
          //Simulate mouse move
          g_ptLastMousePos.x--; //HACK: Force the mouse move to process
          MouseMove(x, y);
          return;   //stop here
      }
      if(PtInRect(&g_rcOptionsSliderPos[1], &ptSliderPos))   //Clicking somewhere in music slider rectangle
      {
          //Start dragging this
          g_iSliderDrag = OPTIONSMENU_MUSICSLIDER;
          //Simulate mouse move
          g_ptLastMousePos.x--; //HACK: Force the mouse move to process
          MouseMove(x, y);
          return;   //stop here
      }
  }
  if(g_iMenuPos != -1 && g_pGame->IsMouseVisible())
  {
      if(g_iMenuPosSelect != -1)
        PlayMySound(IDW_SHOOTFORK);
      HandleMenuSelect(g_iMenuPos, g_iMenuPosSelect);
      return;   //Stop here
  }

  //If game paused, handle mouse clicking on menu items
  if(g_bPaused)
  {
    //find if the mouse is inside one of the click rectangles
    bool bFound = false;
    for(int i = 0; i < 5; i++)
    {
        myRECT rcClickPos = g_rcPauseMenuLocations[i];
        OffsetRect(&rcClickPos, (g_pGame->GetWidth() - g_pPauseBmp->GetWidth()) / 2, (g_pGame->GetHeight() - g_pPauseBmp->GetHeight()) / 2);
        if(PtInRect(&rcClickPos, &ptClickPos))
        {
            PlayMySound(IDW_SHOOTFORK);
            g_iPausePos = i;
            bFound = true;
        }
    }

    //Decide what to do about it
    if(bFound)
        HandlePauseMenuItem(g_iPausePos);
  }

  if(!g_bPlayingGame && !g_bHighScores && !g_bTypeName)
  {
      if(g_bBgStory || g_bEgStory)
      {
          switch(g_iMenuPosSelect)
          {
              case NO_MENU:
                break;
              case BG_STORY_PREV:
                g_iBgStoryPanel--;
                PlayMySound(IDW_PREVIOUSLEVEL);
                if(g_iBgStoryPanel == 0)
                    g_iMenuPosSelect = NO_MENU; //Don't show thingy if thingy shouldn't be shown
                break;
              case BG_STORY_NEXT:
                g_iBgStoryPanel++;
                if(g_bBgStory && g_iBgStoryPanel >= NUM_BGSTORY_PANELS)
                {
                    g_bBgStory = false;
                    g_iBgStoryPanel = 0;
                    PlayMySound(IDW_GOTFORK);
                    g_iMenuPosSelect = NO_MENU;
                }
                else if(g_bEgStory && g_iBgStoryPanel >= NUM_EGSTORY_PANELS)
                {
                  g_bEgStory = false;
                  g_iBgStoryPanel = 0;
                  g_iMenuPosSelect = NO_MENU;
                  PlayMySound(IDW_GOTFORK);
                  g_bHighScores = true;
                  g_iCameToHighScoresFrom = MAIN_MENU;
                }
                else
                {
                    PlayMySound(IDW_NEXTLEVEL);
                    if((g_bBgStory && g_iBgStoryPanel == NUM_BGSTORY_PANELS-1) ||
                       (g_bEgStory && g_iBgStoryPanel == NUM_EGSTORY_PANELS-1))
                        g_iMenuPosSelect = NO_MENU;
                }
                break;
              case BG_STORY_SKIP:
                g_iMenuPosSelect = NO_MENU;
                g_bBgStory = false;
                if(g_bEgStory)
                {
                    g_bEgStory = false;
                    g_bHighScores = true;
                    g_iCameToHighScoresFrom = MAIN_MENU;
                }
                PlayMySound(IDW_GOTFORK);
                break;
          }

          return;
      }

      //Selecting level
      if(g_iMenuPosSelect != -1)
      {
          switch(g_iMenuPosSelect)
          {
              case LEVELSELECT_PLAY_LEVEL:
                g_bPlayingGame = true;
                g_pGame->HideMouse(); //Hide cursor while in level
                PlayMySound(IDW_GOTFORK);
                StartLevel();
                break;
              case LEVELSELECT_PREV:
                if(g_iCurrentLevel != 0)      //If there is one
                {
                    if(g_iCurrentLevel == 19)
                        g_iCurrentLevel = 15;
                    else
                        g_iCurrentLevel--;
                    PlayMySound(IDW_PREVIOUSLEVEL);
                }
                break;
              case LEVELSELECT_NEXT:
                #if defined DEMO_VERSION
                if(g_iCurrentLevel == 2)
                    DisplayMsg("Please purchase the full game to play further.",7000);
                if(g_iCurrentLevel != 2)     //Limit players to 3 levels in the demo
                #else
                if(g_iCurrentLevel != 15)    //If there is one
                #endif
                {
                  if(g_iCurrentLevel == 14 && !CheckWon())
                  {
                      g_iCurrentLevel = 20; //First secret level
                      PlayMySound(IDW_NEXTLEVEL);
                  }
                  else if(!(g_iCurrentLevel == 1 && !g_bParts[g_iCurrentLevel]) && g_iCurrentLevel != 21)   //If we're at level 2, & haven't gotten jet boots yet, block from going further.
                  {
                    g_iCurrentLevel++;
                    // Play the go to next level sound
                    PlayMySound(IDW_NEXTLEVEL);
                  }
                  else
                    DisplayMsg("Complete this level in order to progress.",7000);
                }
                else if(g_iCurrentLevel == 15)
                {
                    if(g_bParts[g_iCurrentLevel])
                        g_iCurrentLevel = 19;
                    else
                        g_iCurrentLevel = 20;
                    // Play the go to next level sound
                    PlayMySound(IDW_NEXTLEVEL);
                }
                break;
              case LEVELSELECT_BACK:
                g_iMenuPos = MAIN_MENU;
                g_iMenuPosSelect = -1;
                PlayMySound(IDW_GOTFORK);
                break;
          }
          //Keep within sane bounds
          if(g_iCurrentLevel == 21 && g_lElectrospheresCollected.size() < NUM_ELECTROSPHERES_3D2)
                g_iCurrentLevel--;
          if(g_iCurrentLevel == 20 && g_lElectrospheresCollected.size() < NUM_ELECTROSPHERES_3D1)
                g_iCurrentLevel--;
          if(g_iCurrentLevel == 19 && !g_bParts[15])
                g_iCurrentLevel = 15;
          if(g_iCurrentLevel == 15 && !CheckWon())
          {
              g_iCurrentLevel--;
              if(g_iMenuPosSelect == LEVELSELECT_NEXT)
                DisplayMsg("Complete the previous levels in order to progress.",7000);
          }
      }
  }

}

bool MouseButtonUp(int x, int y, bool bLeft)
{
  //if(bLeft)
    g_iSliderDrag = -1;   //Stop dragging whatever we were dragging
  return false;
}

void MouseMove(int x, int y)
{
    myPOINT ptClickPos = {x,y};

    //Make sure we actually moved (WHAI WE HAVE TO TEST DIS, HUH?)
    if(abs(ptClickPos.x - g_ptLastMousePos.x) < 1.0 &&
       abs(ptClickPos.y - g_ptLastMousePos.y) < 1.0)
        return;

    if(g_ptLastMousePos.x == -1 && g_ptLastMousePos.y == -1)
    {
        g_ptLastMousePos = ptClickPos;
        return;
    }

    g_ptLastMousePos = ptClickPos;

    if(g_iMenuPos != NO_MENU)
    {
        if(g_pGame->IsMouseVisible())
        {
            int iXStartPos = (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2; // Assuming all the gamescreens are the same size, of course...
            int iYStartPos = (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2;
            int iOldPosSelect = g_iMenuPosSelect;
            g_iMenuPosSelect = -1;
            if(g_bConfirmResolution)    //Check for changing-resolution confirmation dialog
            {
                for(int i = 0; i < 2; i++)
                {
                    if(PtInRect(&g_rcConfirmResolutionClick[i], x - iXStartPos, y - iYStartPos))
                    {
                        g_iMenuPosSelect = i;
                    }
                }
                if(g_iMenuPosSelect != NO_MENU &&
                   g_iMenuPosSelect != iOldPosSelect)
                {
                    PlayMySound(IDW_NEXTLEVEL);
                }
                return; //End here, so don't process more movement
            }
            switch(g_iMenuPos)
            {
                case MAIN_MENU:
                    for(int i = 0; i < NUM_MAINMENU_ITEMS; i++)
                    {
                        if(pnpoly(4, g_fMainMenuPosx[i], g_fMainMenuPosy[i], x - iXStartPos, y - iYStartPos))
                        {
                            g_iMenuPosSelect = i;
                            break;
                        }
                    }
                    if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                    {
                        PlayMySound(IDW_NEXTLEVEL);
                    }
                    break;
                case PROFILES_MENU:
                    if(g_bDeleteWarning)
                    {
                        for(int i = 0; i < 2; i++)
                        {
                            myPOINT ptClick = {x-iXStartPos, y-iYStartPos};
                            if(PtInRect(&g_rcDeleteYNClick[i], &ptClick))
                            {
                                g_iMenuPosSelect = i;
                                break;
                            }
                        }
                        if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                            PlayMySound(IDW_NEXTLEVEL);
                        break;
                    }
                    for(int i = 0; i < NUM_PROFILEMENU_ITEMS; i++)
                    {
                        if(pnpoly(g_iProfileSelectNumPts[i], g_fProfileSelectx[i], g_fProfileSelecty[i], x - iXStartPos, y - iYStartPos))
                        {
                            g_iMenuPosSelect = i;
                            break;
                        }
                    }
                    if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                        PlayMySound(IDW_NEXTLEVEL);
                    break;
                case OPTIONS_MENU:
                    if(g_iSliderDrag != -1)
                    {
                        if(g_iSliderDrag == OPTIONSMENU_SOUNDSLIDER)    //Drag sound
                        {
                            g_iMenuPosSelect = OPTIONSMENU_SOUNDSLIDER;
                            int iVol = ((float)(x - iXStartPos - 11.0 - 116.0)*(128.0))/(339.0);
                            iVol = max(iVol, 0);
                            iVol = min(iVol, 128);
                            g_pGame->SetSoundVol(iVol);
                        }    //(mouse_x * 128)/(total_x)
                        else if(g_iSliderDrag == OPTIONSMENU_MUSICSLIDER)   //drag music
                        {
                            g_iMenuPosSelect = OPTIONSMENU_MUSICSLIDER;
                            int iVol = ((float)(x - iXStartPos - 11.0 - 81.0)*(128.0))/(385.0);
                            iVol = max(iVol, 0);
                            iVol = min(iVol, 128);
                            g_pGame->SetMusicVol(iVol);
                        }
                        return; //Stop here
                    }
                    for(int i = 0; i < 8; i++)
                    {
                        if(pnpoly(g_iOptionsMenuCount[i], g_fOptionsMenuItemsx[i], g_fOptionsMenuItemsy[i], x - iXStartPos, y - iYStartPos))
                        {
                            g_iMenuPosSelect = i;
                            break;
                        }
                    }
                    if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                        PlayMySound(IDW_NEXTLEVEL);
                    break;
                case RESOLUTION_MENU:
                    for(int i = 0; i < 3; i++)
                    {
                        if(pnpoly(4, g_fResolutionMenuItemx[i], g_fResolutionMenuItemy[i], x - iXStartPos, y - iYStartPos))
                        {
                            g_iMenuPosSelect = i;
                            break;
                        }
                    }
                    //Now test and see if we're inside one of them boxes for selecting resolution
                    if(g_iMenuPosSelect == -1)
                    {
                        myRECT rc = g_rcResolutionPos;
                        for(int i = 0; i < RESOLUTION_NUM_DRAW; i++, OffsetRect(&rc, 0, RESOLUTION_INCREMENT_Y))
                        {
                            if(PtInRect(&rc, x - iXStartPos, y - iYStartPos))
                            {
                                g_iMenuPosSelect = i + 3;
                                if(i + g_iResolutionScrollPos < 0 ||
                                   i + g_iResolutionScrollPos >= g_pGame->GetNumScreenModes())
                                    g_iMenuPosSelect = -1;
                                break;
                            }
                        }
                    }
                    if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                        PlayMySound(IDW_NEXTLEVEL);
                    break;
                case KEYCONFIG_MENU:
                    for(int i = 0; i < 7; i++)
                    {
                        if(PtInRect(&g_rcKeyConfigItemsPos[i], x - iXStartPos, y - iYStartPos))
                        {
                            g_iMenuPosSelect = i;
                            break;
                        }
                    }
                    if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                        PlayMySound(IDW_NEXTLEVEL);
                    break;
            }
        }
        else if(g_iMenuPos != KEYCONFIG_MENU)
            g_pGame->ShowMouse();

        g_sMenuText = GetMenuText(g_iMenuPos, g_iMenuPosSelect);

        return;
    }

    if(g_bPaused)
    {
        //find if the mouse is inside one of the click rectangles
        if(g_pGame->IsMouseVisible())
        {
            for(int i = 0; i < 5; i++)
            {
                myRECT rcClickPos = g_rcPauseMenuLocations[i];
                OffsetRect(&rcClickPos, (g_pGame->GetWidth() - g_pPauseBmp->GetWidth()) / 2, (g_pGame->GetHeight() - g_pPauseBmp->GetHeight()) / 2);
                if(PtInRect(&rcClickPos, &ptClickPos))
                {
                    if(g_iPausePos != i)
                        PlayMySound(IDW_NEXTLEVEL);
                    g_iPausePos = i;
                }
            }
        }
        else
            g_pGame->ShowMouse();
    }

    if((g_bBgStory || g_bEgStory) && !g_bTypeName)  //Bg story- handle mouse movement to click on arrows/exit thing
    {
        if(!g_pGame->IsMouseVisible())
        {
            g_pGame->ShowMouse();
            return;
        }
        int iXStartPos = (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2;
        int iYStartPos = (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2;
        int iOldPosSelect = g_iMenuPosSelect;
        g_iMenuPosSelect = NO_MENU;
        for(int i = 0; i < 3; i++)
        {
            if(PtInRect(&(g_rcBgStoryClickPos[i]), x-iXStartPos, y-iYStartPos))
            {
                //Don't accept prev if we're at beginning, or next if we're at end
                if((i == BG_STORY_PREV && g_iBgStoryPanel == 0) ||
                   (i == BG_STORY_NEXT && g_bBgStory && g_iBgStoryPanel == NUM_BGSTORY_PANELS-1) ||
                   (i == BG_STORY_NEXT && g_bEgStory && g_iBgStoryPanel == NUM_EGSTORY_PANELS-1))
                    break;

                g_iMenuPosSelect = i;
                break;
            }
        }

        if(g_iMenuPosSelect != iOldPosSelect &&
           g_iMenuPosSelect != NO_MENU)
        {
            PlayMySound(IDW_NEXTLEVEL);
        }

        return;
    }

    if(!g_bPlayingGame && !g_bHighScores && !g_bTypeName && !g_bBgStory && ! g_bEgStory)
    {
        int iXStartPos = (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2;
        int iYStartPos = (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2;
        //if in level-select screen
        if(g_pGame->IsMouseVisible())
        {
            int iOldPosSelect = g_iMenuPosSelect;
            g_iMenuPosSelect = -1;
            for(int i = 0; i < 4; i++)
            {
                if(pnpoly(4, g_fLevelSelectx[i], g_fLevelSelecty[i], x - iXStartPos, y - iYStartPos))
                {
                    if(g_iCurrentLevel == 0 && i == LEVELSELECT_PREV)
                        continue;
                    if(g_iCurrentLevel == 21 && i == LEVELSELECT_NEXT)
                        continue;
                    g_iMenuPosSelect = i;
                    break;
                }
            }
            if(g_iMenuPosSelect != iOldPosSelect && g_iMenuPosSelect != -1)
                PlayMySound(IDW_NEXTLEVEL);
        }
        else
            g_pGame->ShowMouse();
    }
//  g_pXButton->SetFrame(0);
//  if(g_pXButton->IsPointInside(x, y))
//  {
//    g_pXButton->SetFrame(1);
//  }
}

void MouseWheel(int iAmt)
{
    //Mousewheel makes resolution select menu scroll up and down
    if(g_iMenuPos == RESOLUTION_MENU)
    {
        if(iAmt > 0)    //Rotated up
        {
            if(--g_iResolutionScrollPos < -3)
                g_iResolutionScrollPos = -3;
            else
                PlayMySound(IDW_PREVIOUSLEVEL);
        }
        else if(iAmt < 0)   //Rotated down
        {
            if(++g_iResolutionScrollPos > g_pGame->GetNumScreenModes() - RESOLUTION_NUM_DRAW + 3)
                g_iResolutionScrollPos = g_pGame->GetNumScreenModes() - RESOLUTION_NUM_DRAW + 3;
            else
                PlayMySound(IDW_NEXTLEVEL);
        }
    }
}

void HandleJoystick(JOYSTATE jsJoystickState)
{
    g_jsCurrJoyState = jsJoystickState;
}

bool SpriteCollision(Sprite* pSpriteHitter, Sprite* pSpriteHittee)
{
  //If either hitter or hittee is hidden, ignore
  if((pSpriteHittee->IsHidden() || pSpriteHitter->IsHidden()) && !(pSpriteHittee->GetBitmap() == g_pNullBlockBitmap || pSpriteHitter->GetBitmap() == g_pNullBlockBitmap))
        return false;


  myRECT  rcChefRect,
        rcPlatformRect;
  myPOINT ptAmoebaVelocity;
  bool  bIsChef;

  if((pSpriteHitter->GetBitmap()) == g_pPersonBitmap ||
     pSpriteHitter->GetBitmap() == g_pPersonJetBoots ||
     pSpriteHitter->GetBitmap() == g_pPersonSuperJetBoots)
  {
    bIsChef = true;
    if(g_bDying)   //If chef is dying
      return false;  //Stop right there
  }
  else
  {
    bIsChef = false;
  }

  //Test to see if something hitting chef?
  if((pSpriteHittee->GetBitmap()) == g_pPersonBitmap ||
     pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
     pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots)
  {
    if(g_bDying)
      return false;
  }

  if(pSpriteHitter->GetBitmap() == g_pBubbleBitmap ||
     pSpriteHitter->GetBitmap() == g_pFireBlowBitmap ||
     pSpriteHitter->GetBitmap() == g_pLaserBoltHorizontalBitmap ||
     pSpriteHitter->GetBitmap() == g_pLaserBoltVerticalBitmap ||
     pSpriteHitter->GetBitmap() == g_pAlienBulletBitmap)  //If bubble or fire or a laser bolt hitting someting
  {
    if(pSpriteHittee->GetBitmap() == g_pChromeblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCrackedLogsBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pJumpBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pSlimeRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCaveRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolRock2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pGroundBitmap ||
       pSpriteHittee->GetBitmap() == g_pSmallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pXblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedLeftBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedRightBitmap ||
       pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pAmoebaBlockBitmap)
    {
      pSpriteHitter->Kill();
      pSpriteHitter->SetHidden(true); //Don't draw inside a block
      if((pSpriteHitter->GetBitmap() == g_pLaserBoltHorizontalBitmap ||
         pSpriteHitter->GetBitmap() == g_pLaserBoltVerticalBitmap) && IsInView(pSpriteHitter) && (!g_bDying))
      {
        // Play the laser hit something sound
        PlayMySound(IDW_LASERHIT);
      }
      //Make laser splatting sprite
      if(pSpriteHitter->GetBitmap() == g_pLaserBoltVerticalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        myRECT rc = pSpriteHitter->GetPosition();
        if(pt.y < 0)                          //If laser moving up
        {
          //Make an upwards laser splat sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserDieUpBitmap, g_rcAll, BA_DIE);
          pSprite->SetNumFrames(3, true);
          pSprite->SetPosition(rc.left, rect.bottom);
          pSprite->SetZOrder(6);
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else       //Laser moving down
        {
          //Make a downwards laser splat sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserDieDownBitmap, g_rcAll, BA_DIE);
          pSprite->SetNumFrames(3, true);
          pSprite->SetPosition(rc.left, rect.top - pSprite->GetHeight());
          pSprite->SetZOrder(6);
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
      else if(pSpriteHitter->GetBitmap() == g_pLaserBoltHorizontalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        myRECT rc = pSpriteHitter->GetPosition();
        if(pt.x < 0)                          //If laser moving left
        {
          //Make a left laser splat sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserDieLeftBitmap, g_rcAll, BA_DIE);
          pSprite->SetNumFrames(3, true);
          pSprite->SetPosition(rect.right, rc.top);
          pSprite->SetZOrder(6);
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else      //Laser moving right
        {
          //Make a right laser splat sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserDieRightBitmap, g_rcAll, BA_DIE);
          pSprite->SetNumFrames(3, true);
          pSprite->SetPosition(rect.left - pSprite->GetWidth(), rc.top);
          pSprite->SetZOrder(6);
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
    }
    else if(pSpriteHittee->GetBitmap() == g_pMirrorLeftBitmap)
    {
      if(IsInView(pSpriteHitter) && (!g_bDying))
      {
        // Play the laser deflect sound
        PlayMySound(IDW_LASERDEFLECT);
      }
      if(pSpriteHitter->GetBitmap() == g_pLaserBoltHorizontalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        if(pt.x > 0)                          //If laser moving right
        {
          pSpriteHitter->Kill();
          //Make a vertical laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltVerticalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left + pSpriteHittee->GetWidth() / 2, rect.top - pSprite->GetHeight());
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(0, -25);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else                              //If laser moving left
        {
          pSpriteHitter->Kill();
          //Make a vertical laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltVerticalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left + pSpriteHittee->GetWidth() / 2, rect.bottom);
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(0, 25);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
      if(pSpriteHitter->GetBitmap() == g_pLaserBoltVerticalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        if(pt.y > 0)                          //If laser moving down
        {
          pSpriteHitter->Kill();
          //Make a horizontal laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltHorizontalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left - pSprite->GetWidth(), rect.top + (pSpriteHittee->GetHeight() / 2) - (pSprite->GetHeight() / 2));
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(-25, 0);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else                          //If laser moving up
        {
          pSpriteHitter->Kill();
          //Make a horizontal laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltHorizontalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.right, rect.top + (pSpriteHittee->GetHeight() / 2) - (pSprite->GetHeight() / 2));
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(25, 0);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
    }
    else if(pSpriteHittee->GetBitmap() == g_pMirrorRightBitmap)
    {
      if(IsInView(pSpriteHitter) && (!g_bDying))
      {
        // Play the laser deflect sound
        PlayMySound(IDW_LASERDEFLECT);
      }
      if(pSpriteHitter->GetBitmap() == g_pLaserBoltHorizontalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        if(pt.x < 0)                          //If laser moving left
        {
          pSpriteHitter->Kill();
          //Make a vertical laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltVerticalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left + (pSpriteHittee->GetWidth() / 2) - (pSprite->GetWidth() / 2), rect.top - pSprite->GetHeight());
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(0, -25);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else                          //If laser moving right
        {
          pSpriteHitter->Kill();
          //Make a vertical laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltVerticalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left + (pSpriteHittee->GetWidth() / 2) - (pSprite->GetWidth() / 2), rect.bottom);
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(0, 25);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
      if(pSpriteHitter->GetBitmap() == g_pLaserBoltVerticalBitmap)
      {
        myPOINT pt = pSpriteHitter->GetVelocity();
        if(pt.y > 0)                          //If laser moving down
        {
          pSpriteHitter->Kill();
          //Make a horizontal laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltHorizontalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.right, rect.top + (pSpriteHittee->GetHeight() / 2) - (pSprite->GetHeight() / 2));
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(25, 0);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
        else                               //If laser moving up
        {
          pSpriteHitter->Kill();
          //Make a horizontal laser sprite
          myRECT rect = pSpriteHittee->GetPosition();
          Sprite* pSprite = new Sprite(g_pLaserBoltHorizontalBitmap, g_rcAll, BA_DIE);
          //pSprite->SetNumFrames(5);
          pSprite->SetPosition(rect.left - pSprite->GetWidth(), rect.top + (pSpriteHittee->GetHeight() / 2) - (pSprite->GetHeight() / 2));
          pSprite->SetZOrder(6);    //High z- order
          pSprite->SetVelocity(-25, 0);  //Fast velocity
          g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        }
      }
    }
    //Test for it hitting a slant
    //Left slants
    else if(pSpriteHittee->GetBitmap() == g_pSlantedLeftBitmap ||
            pSpriteHittee->GetBitmap() == g_pSlantedLeftChromeBitmap ||
            pSpriteHittee->GetBitmap() == g_pLeftRockSlant ||
            pSpriteHittee->GetBitmap() == g_pStarSlantL ||
            pSpriteHittee->GetBitmap() == g_pLavaSlopeL)
    {
        //Make it die if inside slant
        myRECT rcBlock = pSpriteHittee->GetPosition();
        myRECT rcSpoon = pSpriteHitter->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(ptCenter.x - rcBlock.left > iYPos)
        {
            pSpriteHitter->Kill();
            pSpriteHitter->SetHidden(true);
        }
    }
    //Right slants
    else if(pSpriteHittee->GetBitmap() == g_pSlantedRightBitmap ||
            pSpriteHittee->GetBitmap() == g_pSlantedRightChromeBitmap ||
            pSpriteHittee->GetBitmap() == g_pRightRockSlant ||
            pSpriteHittee->GetBitmap() == g_pStarSlantR ||
            pSpriteHittee->GetBitmap() == g_pLavaSlopeR)
    {
        //Make it die if inside slant
        myRECT rcBlock = pSpriteHittee->GetPosition();
        myRECT rcSpoon = pSpriteHitter->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(rcBlock.right - ptCenter.x > iYPos)
        {
            pSpriteHitter->Kill();
            pSpriteHitter->SetHidden(true);
        }
    }
    //Left cieling slants
    else if(pSpriteHittee->GetBitmap() == g_pLavaSlopeCL ||
            pSpriteHittee->GetBitmap() == g_pStarCielingLeft ||
            pSpriteHittee->GetBitmap() == g_pCLeftRockSlant)
    {
        //Make it die if inside slant
        myRECT rcBlock = pSpriteHittee->GetPosition();
        myRECT rcSpoon = pSpriteHitter->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = ptCenter.y - rcBlock.top;
        if(rcBlock.right - ptCenter.x > iYPos)
        {
            pSpriteHitter->Kill();
            pSpriteHitter->SetHidden(true);
        }
    }
    //Right cieling slants
    else if(pSpriteHittee->GetBitmap() == g_pLavaSlopeCR ||
            pSpriteHittee->GetBitmap() == g_pStarCielingRight ||
            pSpriteHittee->GetBitmap() == g_pCRightRockSlant)
    {
        //Make it die if inside slant
        myRECT rcBlock = pSpriteHittee->GetPosition();
        myRECT rcSpoon = pSpriteHitter->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = ptCenter.y - rcBlock.top;
        if(ptCenter.x - rcBlock.left > iYPos)
        {
            pSpriteHitter->Kill();
            pSpriteHitter->SetHidden(true);
        }
    }
  }
  else if((pSpriteHitter->GetBitmap()) == g_pPoisonBlobBitmap) //If poison blob hitting someting
  {
    if(pSpriteHittee->GetBitmap() == g_pPlatformBitmap ||
       pSpriteHittee->GetBitmap() == g_pCrackedLogsBitmap ||
       pSpriteHittee->GetBitmap() == g_pSinkingPlatformBitmap ||
       pSpriteHittee->GetBitmap() == g_pChromeblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolRock2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pPoisonVatBitmap ||
       pSpriteHittee->GetBitmap() == g_pLava1Bitmap ||
       pSpriteHittee->GetBitmap() == g_pJumpBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pFloatingPlatformBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pSlimeRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCaveRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pXblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pGroundBitmap ||
       pSpriteHittee->GetBitmap() == g_pSmallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
       pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
       pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots ||
       pSpriteHittee->GetBitmap() == g_pFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedLeftBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedRightBitmap ||
       pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pAmoebaBlockBitmap)
    {
      pSpriteHitter->Kill();
      if(pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
         pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
         pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots)
      {
        DieChef();
      }
      //Make a 200-points sprite
      myRECT rect = pSpriteHitter->GetPosition();
      Sprite* pSprite = new Sprite(g_pPoisonPopBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(6);    //High z- order
      pSprite->SetNumFrames(4, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      if((!g_bDying) && IsInView(pSpriteHitter))
      {
        switch(rand() % 3)
        {
          case 0:
            // Play the first drip sound
            PlayMySound(IDW_DRIP1);
            break;
          case 1:
            // Play the second drip sound
            PlayMySound(IDW_DRIP2);
            break;
          default:
            // Play the third drip sound
            PlayMySound(IDW_DRIP3);
            break;
        }
      }
      return false;
    }
    return false;
  }
  //----------------------------------------------------
  bool bCrab = false;
  for(int i = 0; i < NUM_BOSS_FRAMES; i++)
  {
      if(pSpriteHitter->GetBitmap() == g_pCrabBitmap[i])
        bCrab = true;
  }
  //----------------------------------------------------
  if(bCrab)   //If crab hitting something
  {
      myRECT rc1, rc2, rc3;
      GetClawRects(&rc1, &rc2);
      rc3 = pSpriteHittee->GetCollision();
      //If the claws are hitting the object
      if(IntersectRect(&rc1, &rc3) ||
         IntersectRect(&rc2, &rc3))
      {
          if(pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
             pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
             pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots)
            DieChef();
          if(pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
             pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap ||
             pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap)
          {
              pSpriteHittee->Kill();    //Kill the block
              PlayMySound(IDW_BOOM);    //Make boom noise
              g_iViewportShakeAmt = 20; //Make viewport shake
          }
          //if(pSpriteHittee->GetBitmap() == g_pFlySpoonLBitmap ||
          //   pSpriteHittee->GetBitmap() == g_pFlySpoonRBitmap)
          //{
              //Kill spooon
          //    pSpriteHittee->Kill();
          //    PlayMySound(IDW_FORKHITBLOCK);
          //}
      }
      rc1 = GetBodyRect();  //Grab the body rect of the crab
      if(IntersectRect(&rc1, &rc3))
      {
          if(pSpriteHittee->GetBitmap() == g_pFlySpoonLBitmap ||
             pSpriteHittee->GetBitmap() == g_pFlySpoonRBitmap)
          {
              pSpriteHittee->Kill();
              if(!g_bBossDying)
              {
                  if(g_iBossHealth)
                    g_iBossHealth--;  //Hit boss
                  //Play sound
                  PlayMySound(IDW_HITBADGUY);
                  pSpriteHitter->FlashColor(3, 0, 0, 255);  //Flash blue
              }
              if(!g_iBossHealth)    //Killed boss
              {
                  //Set boss to floor if he's jumping already
                  if(g_iCurBossAction == BOSS_JUMP)
                  {
                      g_pEnemyCrab->SetPosition(g_pEnemyCrab->GetPosition().left, g_iBossFloorYLevel);
                      g_pEnemyCrab->SetVelocity(0,0);
                  }
                  g_iCurBossAction = BOSS_PAUSE;
                  g_iTimeLeftInThisAction = 100;
                  g_ptPanTo = g_pEnemyCrab->GetCenter();
                  g_bPanBackToPlayer = false;
                  g_bBossDying = true;
                  g_bParts[g_iCurrentLevel] = true;
                  g_pEnemyCrab->SetBoundsAction(BA_BOUNCE); //Keep boss on screen if dying
                  //Stop music
                  g_pGame->CleanupSong();
              }
              else
                g_iBossHitThisCycle += 2;

              //If the boss is avoiding the player, and player hits him too much, though, boss gets angry
              if(g_iCurBossAction == BOSS_AVOIDPLAYER && ++g_iBossAvoidToleranceCount > BOSS_MAX_HIT_TOLERANCE)
              {
                //Charge at player
                if(GetFastestWayToPlayer() < 0)
                  g_iCurBossAction = BOSS_RUNL;
                else
                  g_iCurBossAction = BOSS_RUNR;
                g_iTimeLeftInThisAction = 280;  //Run for 7 seconds
                PlayMySound(IDW_CRABROAR);  //Roar as charges at player
                g_iBossFrame = -1;
                UpdateBossFrame();
              }
          }
      }
  }
  if((pSpriteHitter->GetBitmap() == g_pRedAmoebaBitmap ||
      pSpriteHitter->GetBitmap() == g_pAlienBitmap ||
      pSpriteHitter->GetBitmap() == g_pAlienFighterBitmap ||
      pSpriteHitter->GetBitmap() == g_pBlueAmoebaBitmap ||
      pSpriteHitter->GetBitmap() == g_pBeeBitmap ||
      pSpriteHitter->GetBitmap() == g_pBatBitmap ||
      pSpriteHitter->GetBitmap() == g_pYellowAmoebaBitmap ||
      pSpriteHitter->GetBitmap() == g_pGreenAmoebaBitmap ||
      pSpriteHitter->GetBitmap() == g_pBouncyBitmap) && //If amoeba or alien or bee or bouncy creature or bat hitting Chef
     (pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
      pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
      pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots))
  {
    DieChef();
    return false;
  }
  else if(((pSpriteHitter->GetBitmap()) == g_pRollerBitmap) &&
          ((pSpriteHittee->GetBitmap()) == g_pPersonBitmap ||
           pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
           pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots)) //If hitting a roller
  {
    if(g_bDying)
      return false;
    if(pSpriteHitter->GetFrame() > 4) //If rolling around
    {
      rcChefRect = pSpriteHittee->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHitter->GetCollision();      // and the roller's
      rcChefRect.bottom -= COLLISION_EDGE_ADD;               // Make the rectangles smaller, because
      rcPlatformRect.top += COLLISION_EDGE_ADD;              // they will already be past each other

      if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the roller
      {
        AddToRespawn(pSpriteHitter);
        //g_iPointsGotten += 40;   //increase points (People like Paul will like this!)
          // Play the fork hit amoeba sound
          PlayMySound(IDW_FORKHITAMOEBA);

        //Make a 400-points sprite
        /*myRECT rect = pSpriteHitter->GetPosition();
        Sprite* pSprite = new Sprite(g_p400PointsBitmap, g_rcViewport, BA_DIE);
        pSprite->SetPosition(rect.left, rect.top);
        pSprite->SetZOrder(7);    //High z- order
        pSprite->SetNumFrames(12, true);
        g_pGame->AddSprite(pSprite);         //Add the sprite later, not now*/

        g_bIsFalling = false;
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bIsJumping = true;                   // Make him bounce a bit
        g_bJumpBlockJump = true;
        g_bDoubleJumpPressThisCycle = false;
        g_iJumpAmount = 15;
      }
      else
      {
        DieChef();
      }
    }
    else  //Start roller rolling
    {
      AddToRespawn(pSpriteHitter);
    }
    return false;
  }
  if((pSpriteHitter->GetBitmap() == g_pWormBitmap) &&
     (pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
      pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
      pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots))
  {
    rcChefRect = pSpriteHittee->GetCollision();          // Get his collision rectangle
    rcPlatformRect = pSpriteHitter->GetCollision();      // and the worm's
    rcChefRect.bottom -= COLLISION_EDGE_ADD;               // Make the rectangles smaller, because
    rcPlatformRect.top += COLLISION_EDGE_ADD;              // they will already be past each other

    if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the worm
    {
      AddToRespawn(pSpriteHitter);
      g_iPointsGotten += 20;   //increase points (People like Paul will like this!)
        // Play the fork hit amoeba sound
        PlayMySound(IDW_FORKHITAMOEBA);

      //Make a 200-points sprite
      myRECT rect = pSpriteHitter->GetPosition();
      Sprite* pSprite = new Sprite(g_p200PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      g_bIsFalling = false;
      g_iJetbootsTime = MAX_JETBOOT_TIME;
      g_bIsJumping = true;                   // Make him bounce a bit
      g_bJumpBlockJump = true;
      g_bDoubleJumpPressThisCycle = false;
      g_iJumpAmount = 18;
    }
    else
    {
      DieChef();
    }
    return false;
  }
  if((pSpriteHitter->GetBitmap() == g_pBalloonBitmap) &&
     (pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
      pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
      pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots))    //If chef hitting balloon
  {
    rcChefRect = pSpriteHittee->GetCollision();          // Get his collision rectangle
    rcPlatformRect = pSpriteHitter->GetCollision();      // and the balloon's
    rcChefRect.bottom -= COLLISION_EDGE_ADD;               // Make the rectangles smaller, because
    rcPlatformRect.top += COLLISION_EDGE_ADD;              // they will already be past each other

    if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the balloon
    {
      AddToRespawn(pSpriteHitter);
      //g_iPointsGotten += 20;   //increase points
        // Play the balloon pop sound
        PlayMySound(IDW_FORKHITAMOEBA);

      g_bIsFalling = false;
      g_iJetbootsTime = MAX_JETBOOT_TIME;
      g_bIsJumping = true;                   // Make him bounce a lot
      g_bJumpBlockJump = true;
      g_bDoubleJumpPressThisCycle = false;
      g_iJumpAmount = 21;
    }
  }
  if(pSpriteHittee->GetBitmap() == g_pFlySpoonRBitmap ||
     pSpriteHittee->GetBitmap() == g_pFlySpoonLBitmap ||
     pSpriteHittee->GetBitmap() == g_pFlySpoonLElectricBitmap ||
     pSpriteHittee->GetBitmap() == g_pFlySpoonRElectricBitmap)  //If a spoon is hitting something
  {                                              //(Called as if something hitting spoon)
    if(pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap ||
            pSpriteHitter->GetBitmap() == g_pCoolblockBitmap ||
            pSpriteHitter->GetBitmap() == g_pFallBlockBitmap ||
            pSpriteHitter->GetBitmap() == g_pSmallBlockBitmap ||
            pSpriteHitter->GetBitmap() == g_pStripedRightBitmap ||
            pSpriteHitter->GetBitmap() == g_pStripedLeftBitmap ||
            pSpriteHitter->GetBitmap() == g_pCoolRock2Bitmap ||
            pSpriteHitter->GetBitmap() == g_pCaveRockBitmap ||
            pSpriteHitter->GetBitmap() == g_pSlimeRockBitmap ||
            pSpriteHitter->GetBitmap() == g_pXblockBitmap ||
            pSpriteHitter->GetBitmap() == g_pStarBlockBitmap ||
            pSpriteHitter->GetBitmap() == g_pStarBlockCrackedBitmap ||
            pSpriteHitter->GetBitmap() == g_pStarBlockCracked2Bitmap ||
            pSpriteHitter->GetBitmap() == g_pChromeblockBitmap) //If spoon hitting a block
    {
      pSpriteHittee->Kill();                  //Kill it (But not the block!)
//      if(!g_bNoSound)
//      {
        // Play the fork hit block sound
        PlayMySound(IDW_FORKHITBLOCK);
//      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pMissileHubBitmap && !g_iCheckpointInvincibility) //If spoon hitting missile hub
    {
      pSpriteHittee->Kill();                  //Kill it
      if(pSpriteHitter->GetFrame() == 0 && !g_bDying)    //Only if this hub hasn't been hit yet...
      {
          if(g_iCurrentLevel == 15)               //If in last level
          {
             if(++g_iMissileCount >= FINAL_MISSILE_COUNT)  //if destroyed all hubs
             {
                 g_bCurLevelPart = true;       //Record hub destroyed
                 //Make it pan to the opening blocking platform
                 if(g_pBlockingPlatform != NULL && IntersectRect(&(g_pBlockingPlatform->GetPosition()), &g_rcAll))
                 {
                     g_ptPanTo = g_pBlockingPlatform->GetCenter();
                     g_bPanBackToPlayer = true;
                 }
             }
          }
          else                                   //If in any other level
          {
            g_bCurLevelPart = true;       //Record hub destroyed
            //Pan to exit if it's in view
            if(g_pExitSprite != NULL && IntersectRect(&(g_pExitSprite->GetPosition()), &g_rcAll))
            {
                g_ptPanTo = g_pExitSprite->GetCenter();
                g_bPanBackToPlayer = true;
            }
            if(g_pExitSprite)
            {
                g_bExitOpen = true;
            }
          }
          pSpriteHitter->SetFrame(1);             //Make hub look destroyed
          AddToRespawn(pSpriteHitter);
          pSpriteHitter->SetHidden(false);  //Unhide again after dat
      }
//      if(!g_bNoSound)
 //     {
        // Play the fork hit block sound
        PlayMySound(IDW_FORKHITBLOCK);
//      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pGroundBitmap) //If spoon hitting ground
    {
      pSpriteHittee->Kill();                  //Kill it (But not the block!)
//      if(!g_bNoSound)
    //  {                                  //Don't play same sound
        // Play the fork hit ground sound
        PlayMySound(IDW_HITGROUND);
    //  }
      return false;
    }
    //Left slants
    else if(pSpriteHitter->GetBitmap() == g_pSlantedLeftBitmap)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(ptCenter.x - rcBlock.left > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true); //Don't draw if it's inside slant
//            if(!g_bNoSound)
                PlayMySound(IDW_HITGROUND);
        }
    }
    else if(pSpriteHitter->GetBitmap() == g_pSlantedLeftChromeBitmap ||
            pSpriteHitter->GetBitmap() == g_pLeftRockSlant ||
            pSpriteHitter->GetBitmap() == g_pStarSlantL ||
            pSpriteHitter->GetBitmap() == g_pLavaSlopeL)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(ptCenter.x - rcBlock.left > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true);
//            if(!g_bNoSound)
                PlayMySound(IDW_FORKHITBLOCK);
        }
    }
    //Right slants
    else if(pSpriteHitter->GetBitmap() == g_pSlantedRightBitmap)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(rcBlock.right - ptCenter.x > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true);
//            if(!g_bNoSound)
                PlayMySound(IDW_HITGROUND);
        }
    }
    else if(pSpriteHitter->GetBitmap() == g_pSlantedRightChromeBitmap ||
            pSpriteHitter->GetBitmap() == g_pRightRockSlant ||
            pSpriteHitter->GetBitmap() == g_pStarSlantR ||
            pSpriteHitter->GetBitmap() == g_pLavaSlopeR)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = rcBlock.bottom - ptCenter.y;
        if(rcBlock.right - ptCenter.x > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true);
//            if(!g_bNoSound)
                PlayMySound(IDW_FORKHITBLOCK);
        }
    }
    //Left cieling slants
    else if(pSpriteHitter->GetBitmap() == g_pLavaSlopeCL ||
            pSpriteHitter->GetBitmap() == g_pStarCielingLeft ||
            pSpriteHitter->GetBitmap() == g_pCLeftRockSlant)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = ptCenter.y - rcBlock.top;
        if(rcBlock.right - ptCenter.x > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true);
//            if(!g_bNoSound)
                PlayMySound(IDW_FORKHITBLOCK);
        }
    }
    //Right cieling slants
    else if(pSpriteHitter->GetBitmap() == g_pLavaSlopeCR ||
            pSpriteHitter->GetBitmap() == g_pStarCielingRight ||
            pSpriteHitter->GetBitmap() == g_pCRightRockSlant)
    {
        //Make spoon die if inside slant
        myRECT rcBlock = pSpriteHitter->GetPosition();
        myRECT rcSpoon = pSpriteHittee->GetCollision();

        myPOINT ptCenter = {(rcSpoon.right - rcSpoon.left) / 2 + rcSpoon.left, (rcSpoon.bottom - rcSpoon.top) / 2 + rcSpoon.top};
        int iYPos = ptCenter.y - rcBlock.top;
        if(ptCenter.x - rcBlock.left > iYPos)
        {
            pSpriteHittee->Kill();
            pSpriteHittee->SetHidden(true);
//            if(!g_bNoSound)
                PlayMySound(IDW_FORKHITBLOCK);
        }
    }

    if(g_bDying)
        return false;   //Skip the rest of these
    if(pSpriteHitter->GetBitmap() == g_pRedAmoebaBitmap ||
       pSpriteHitter->GetBitmap() == g_pAlienBitmap ||
       pSpriteHitter->GetBitmap() == g_pBlueAmoebaBitmap ||
       pSpriteHitter->GetBitmap() == g_pYellowAmoebaBitmap ||
       pSpriteHitter->GetBitmap() == g_pGreenAmoebaBitmap ||
       pSpriteHitter->GetBitmap() == g_pBouncyBitmap ||
       pSpriteHitter->GetBitmap() == g_pBatBitmap ||
       (pSpriteHitter->GetBitmap() == g_pAlienFighterBitmap && pSpriteHitter->GetFrame() != 17 && pSpriteHitter->GetFrame() != 16 && pSpriteHitter->GetFrame() != 20 && pSpriteHitter->GetFrame() != 21) ||
       (pSpriteHitter->GetBitmap() == g_pRollerBitmap && pSpriteHitter->GetFrame() > 4)) //If spoon hitting amoeba or alien or bubble blower or roller or bouncy creature or bat
    {
      pSpriteHittee->Kill();                   //Kill the fork
      if(g_bDying)
        return false;   //Ignore if dying
      int iLives = pSpriteHitter->GetLives() - 1;
      if(!iLives)
      //if(pSpriteHitter->Kill())                //if the bad guy is dead for good
      {
        if(g_iCurrentLevel == 19)
            pSpriteHitter->Kill();
        else
            AddToRespawn(pSpriteHitter);  //Add this creature to respawn list
        // Play the fork hit amoeba sound
        PlayMySound(IDW_FORKHITAMOEBA);
      }
      else
      {
        // Play the fork hit bad guy but didn't kill sound
        PlayMySound(IDW_HITBADGUY);
        if(pSpriteHitter->GetBitmap() == g_pRedAmoebaBitmap)
            pSpriteHitter->FlashColor(3, 0, 0, 255);
        else
            pSpriteHitter->FlashColor(3, 255, 0, 0);
      }
      if(iLives)
      {
          int iZ = pSpriteHitter->GetZOrder();
          pSpriteHitter->SetZOrder(HACK_ZORDER_NOSPAWN);
          if(g_iCurrentLevel != 19)
            AddToRespawn(pSpriteHitter);  //Add this creature to respawn list
          pSpriteHitter->Kill();
          pSpriteHitter->SetHidden(false);  //Unhide
          pSpriteHitter->SetZOrder(iZ);
      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pCrackedLogsBitmap) //If fork hitting cracked logs
    {
      pSpriteHittee->Kill();                  //Kill it (And the logs!)
      AddToRespawn(pSpriteHitter);
//      if(!g_bNoSound)
//      {
        // Play the spoon hit ground sound
        PlayMySound(IDW_HITGROUND);
//      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pShootJumpBitmap) //If spoon hitting shoot and jump block
    {
      pSpriteHittee->Kill();                  //Kill it (And the block!)
      AddToRespawn(pSpriteHitter);
//      if(!g_bNoSound)
//      {
        // Play the spoon hit block sound
        PlayMySound(IDW_FORKHITBLOCK);
//      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pWoodenBlockBitmap) //If fork hitting wooden block
    {
      pSpriteHittee->Kill();                  //Kill it (And the block!)
      pSpriteHitter->Kill();    //Don't respawn this. That would get complicated.
      //But do make hidden block, so that amoebas and such don't walk through this...
      /*Sprite* pS = new Sprite(g_pNullBlockBitmap, g_rcAll, BA_BOUNCE);
      pS->SetPosition(pSpriteHitter->GetPosition());
      pS->SetZOrder(pSpriteHitter->GetZOrder());
      g_pGame->AddSprite(pS);*/
        // Play the fork hit block sound
        PlayMySound(IDW_FORKHITBLOCK);
        //And crumble sound
        PlayMySound(IDW_CRUMBLE);
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pAmoebaBlockBitmap) //If spoon hitting amoeba block
    {
      pSpriteHittee->Kill();                  //Kill it (And the block!)
      AddToRespawn(pSpriteHitter);
//      if(!g_bNoSound)
//      {
        // Play the fork hit block sound
        PlayMySound(IDW_FORKHITBLOCK);
 //     }
      //Add green splat particle effect
      myRECT rect = pSpriteHittee->GetPosition();
      myPOINT ptParticleOffset = {pSpriteHittee->GetWidth() / 2.0, pSpriteHittee->GetHeight() / 2.0};
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(pSpriteHitter->GetZOrder() + 1);
      pSprite->AddParticles(PARTICLE_AMOEBASPLAT, BITMAP_PARTICLES, 9, 9, 6, 6, ptParticleOffset);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      pSprite->SetHidden(true);
      g_pGame->AddSprite(pSprite);

      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pMissileBitmap) //If spoon hitting missile
    {
      pSpriteHittee->Kill();                  //Kill it
      if(g_iCheckpointInvincibility)
        return false; //Stop here if invincible
      //if(!g_bInvincible)
      //  AddToRespawn(pSpriteHitter);                  //Not the missile - explosion looks better with it there
      if(g_bDying || g_iCheckpointInvincibility)  //If dying already, ignore
        return false;
      bool b = g_pGame->GetSound();
      g_pGame->SetSound(false);                      //Stop dying sound from playing
      DieChef(65);                             //Oops!   KA-BOOM!
      g_pGame->SetSound(b);

//      if(!g_bNoSound)
 //     {
        // Play the explosion sound
        PlayMySound(IDW_EXPLODE);
  //    }
      //if(!g_bInvincible)
      //  g_pGame->SetFrameRate(FRAME_RATE / 5);    //Slooow doooown
      //Make explosion particle effect
      myPOINT pt2 = g_ptPanTo = pSpriteHitter->GetCenter();
      myRECT rect = pSpriteHitter->GetPosition();
      pt2.x -= rect.left;
      pt2.y -= rect.top;
      //pSpriteHitter->AddParticles(PARTICLE_EXPLODE, BITMAP_EXPLODEPARTICLE, 0, 0, 46, 46, pt2);
      //pSpriteHitter->SetParticlesInFront(true);
      //pSpriteHitter->FireParticles();
      for(int i = 0; i < 4; i++)
      {
          //Make missile explosion sprite for main explosion particles
          Sprite* pSprite = new Sprite(g_pMissileExplodeBitmap, g_rcAll, BA_DIE);
          pSprite->SetZOrder(12);
          pSprite->SetPosition(rect.left, rect.top);
          //pSprite->SetHidden(true);
          pSprite->AddParticles(PARTICLE_EXPLODE, BITMAP_EXPLODEPARTICLE, 0, 0, 46, 46, pt2);
          pSprite->SetParticlesInFront(true);
          pSprite->FireParticles();
          //pSprite->DieOnParticleFinish();
//          pSprite->SetAlwaysDrawParticles(true);
          g_pGame->AddSprite(pSprite);       //Add the sprite
          AddToDespawn(pSprite);    //Despawn this when you respawn on checkpoint
      }
      for(int i = 0; i < 5; i++)
      {
          //Make missile explosion sprite for exterior explosion particles
          Sprite* pSprite = new Sprite(g_pMissileExplodeBitmap, g_rcAll, BA_DIE);
          pSprite->SetZOrder(12);
          pSprite->SetPosition(rect.left, rect.top);
          //pSprite->SetHidden(true);
          pSprite->AddParticles(PARTICLE_EXPLODEOUTSIDE, BITMAP_EXPLODEPARTICLE, 0, 0, 46, 46, pt2);
          pSprite->SetParticlesInFront(true);
          pSprite->FireParticles();
          //pSprite->DieOnParticleFinish();
//          pSprite->SetAlwaysDrawParticles(true);
          g_pGame->AddSprite(pSprite);       //Add the sprite
          AddToDespawn(pSprite);    //Despawn this when you respawn on checkpoint
      }
      for(int i = 0; i < 2; i++)
      {
          //Make missile explosion sprite for shock wave particles
          Sprite* pSprite = new Sprite(g_pMissileExplodeBitmap, g_rcAll, BA_DIE);
          pSprite->SetZOrder(12);
          pSprite->SetPosition(rect.left, rect.top);
          //pSprite->SetHidden(true);
          pSprite->AddParticles(PARTICLE_SHOCKWAVE, BITMAP_EXPLODEPARTICLE, 0, 0, 46, 46, pt2);
          pSprite->SetParticlesInFront(true);
          pSprite->FireParticles();
          //pSprite->DieOnParticleFinish();
          g_pGame->AddSprite(pSprite);       //Add the sprite
          AddToDespawn(pSprite);    //Despawn this when you respawn on checkpoint
      }

      return false;  //Done
    }
    else if(pSpriteHitter->GetBitmap() == g_pYellowHubBitmap) //If fork hitting yellow hub
    {
      AddToRespawn(pSpriteHitter);                  //Kill the hub, but not the fork
      g_bYellowHub = true;                      //Record hub dead
//      if(!g_bNoSound)
      {
        // Play the fork hit hub sound
        PlayMySound(IDW_HITHUB);
      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pOrangeHubBitmap) //If fork hitting orange hub
    {
      AddToRespawn(pSpriteHitter);                  //Kill the hub, but not the fork
      g_bOrangeHub = true;                      //Record hub dead
//      if(!g_bNoSound)
      {
        // Play the fork hit hub sound
        PlayMySound(IDW_HITHUB);
      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pBlueHubBitmap) //If fork hitting blue hub
    {
      AddToRespawn(pSpriteHitter);                  //Kill the hub, but not the fork
      g_bBlueHub = true;                      //Record hub dead
//      if(!g_bNoSound)
      {
        // Play the fork hit hub sound
        PlayMySound(IDW_HITHUB);
      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pGreenHubBitmap) //If spoon hitting green hub
    {
      AddToRespawn(pSpriteHitter);                  //Kill the hub, but not the fork
      g_bGreenHub = true;                      //Record hub dead
//      if(!g_bNoSound)
      {
        // Play the fork hit hub sound
        PlayMySound(IDW_HITHUB);
      }
      return false;
    }
    else if(pSpriteHitter->GetBitmap() == g_pAlienFighterBitmap) //If hitting alien with shield up
    {
      myRECT rect = pSpriteHitter->GetPosition();
      myRECT rcSpoon = pSpriteHittee->GetPosition();
      myPOINT pt = pSpriteHittee->GetVelocity();
      pSpriteHittee->Kill();
      Sprite* pSprite = new Sprite(((pSpriteHittee->GetVelocity()).x > 0) ? g_pFlySpoonLElectricBitmap : g_pFlySpoonRElectricBitmap, g_rcAll, BA_DIE);
      pSprite->SetVelocity(-pt.x, -pt.y);
      pSprite->SetNumFrames(8);
      pSprite->SetPosition(rcSpoon.left, rcSpoon.top);
      pSprite->SetZOrder(5);
      myPOINT pt2 = {pSprite->GetWidth()/2.0 + pSprite->GetVelocity().x,pSprite->GetHeight()/2.0};
      pSprite->AddParticles(PARTICLE_BLUEAMOEBADEATHFLARETHINGY, BITMAP_PARTICLES, 25,0,25,25, pt2);
      pSprite->SetParticlesInFront(false);
      pSprite->FireParticles();
      g_pGame->AddSprite(pSprite);

//      if(!g_bNoSound)
      {
        // Play the fork hit shield sound
        PlayMySound(IDW_HITSHIELD);
      }
    }
    //If spoon hitting electrosphere in last level (so can get electrosphere from yellow amoeba at the end)
    if(pSpriteHitter->GetBitmap() == g_pElectrosphereBitmap && g_iCurrentLevel == 15 && rand() % 50 == 0)
    {
        pSpriteHitter->SetVelocity(-pSpriteHittee->GetVelocity().x / 4.0, 0);
    }

  }
  if(pSpriteHitter->GetBitmap() == g_pFloatingPlatformBitmap || pSpriteHitter->GetBitmap() == g_pFloatingHiddenBitmap) //If floating platform hitting something
  {
    if(pSpriteHittee->GetBitmap() == g_pGreyholeBitmap ||
       pSpriteHittee->GetBitmap() == g_pCrackedLogsBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pChromeblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pXblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pSlimeRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCaveRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolRock2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pGroundBitmap ||
       pSpriteHittee->GetBitmap() == g_pGoThroughRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedLeftBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedRightBitmap ||
       pSpriteHittee->GetBitmap() == g_pSmallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pAmoebaBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pNullBlockBitmap ||
       (pSpriteHittee->GetBitmap() == g_pHiddenBlockBitmap && pSpriteHitter->GetBitmap() == g_pFloatingHiddenBitmap))
    {
      if(pSpriteHitter->GetBitmap() == g_pFloatingPlatformBitmap && IsInView(pSpriteHitter))
        PlayMySound(IDW_ELEVATORBOUNCE);
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
  }
  else if(pSpriteHitter->GetBitmap() == g_pElevatorBitmap ||
          pSpriteHitter->GetBitmap() == g_pBatBitmap || pSpriteHitter->GetBitmap() == g_pElevatorHiddenBitmap) //If elevator platform or bat hitting something
  {
    if(pSpriteHittee->GetBitmap() == g_pGreyholeBitmap ||
       pSpriteHittee->GetBitmap() == g_pCrackedLogsBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap ||
       pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pChromeblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pXblockBitmap ||
       pSpriteHittee->GetBitmap() == g_pSlimeRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCaveRockBitmap ||
       pSpriteHittee->GetBitmap() == g_pCoolRock2Bitmap ||
       pSpriteHittee->GetBitmap() == g_pGroundBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedLeftBitmap ||
       pSpriteHittee->GetBitmap() == g_pStripedRightBitmap ||
       pSpriteHittee->GetBitmap() == g_pSmallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pAmoebaBlockBitmap ||
       pSpriteHittee->GetBitmap() == g_pRightRockSlant ||
       pSpriteHittee->GetBitmap() == g_pLeftRockSlant ||
       pSpriteHittee->GetBitmap() == g_pCRightRockSlant ||
       pSpriteHittee->GetBitmap() == g_pCLeftRockSlant ||
       (pSpriteHittee->GetBitmap() == g_pNullBlockBitmap && pSpriteHitter->GetBitmap() != g_pBatBitmap))
    {
      if(pSpriteHitter->GetBitmap() == g_pElevatorBitmap && IsInView(pSpriteHitter))
        PlayMySound(IDW_ELEVATORBOUNCE);  //Play elevator bounce noise if elevator bounces off something (ok, self-explanitory)
      return ElevatorHit(pSpriteHitter, pSpriteHittee);
    }
    else if(pSpriteHittee->GetBitmap() == g_pElevatorBitmap) //If hitting another platform, bounce off each other.
    {
      myPOINT pt = pSpriteHittee->GetVelocity();
      pt.y = -pt.y;
      pSpriteHittee->SetVelocity(pt);
      return ElevatorHit(pSpriteHitter, pSpriteHittee);
    }
    else if(pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
            pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
            pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots)
    {
      return false;
    }
  }
  if((pSpriteHitter->GetBitmap()) == g_pPersonBitmap ||
     bIsChef ||
     (pSpriteHitter->GetBitmap()) == g_pRedAmoebaBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pBeeBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pGreenAmoebaBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pAlienBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pBlueAmoebaBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pYellowAmoebaBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pWormBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pBouncyBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pFallBlockBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pLargeFallBlockBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pRollerBitmap ||
     (pSpriteHitter->GetBitmap()) == g_pAlienFighterBitmap)
  {

    if((pSpriteHitter->GetBitmap() == g_pFallBlockBitmap || pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap) &&
       (pSpriteHittee->GetBitmap() == g_pPersonBitmap ||
        pSpriteHittee->GetBitmap() == g_pPersonJetBoots ||
        pSpriteHittee->GetBitmap() == g_pPersonSuperJetBoots))
    {
      return false;
    }
    else if((pSpriteHittee->GetBitmap() == g_pPlatformBitmap) ||
            (((pSpriteHittee->GetBitmap() == g_pHiddenBlockBitmap)) && bIsChef))  // If hitting a platform or chef hitting a bush or hidden brick
    {
      rcChefRect = pSpriteHitter->GetCollision();
      rcPlatformRect = pSpriteHittee->GetCollision();
      rcChefRect.bottom -= COLLISION_EDGE_ADD;                // Add to collision rectangle
      rcPlatformRect.top += COLLISION_EDGE_ADD;               //(don't take these increments out!)
      if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If he is right above the block
      {
        if(bIsChef)                         // If it's chef and not an aboeba
        {
          if(g_bIsFalling)
          {
            g_bIsFalling = false;             // Make him stop falling
            g_iJetbootsTime = MAX_JETBOOT_TIME;

            //Set chef's position to right above the platform but down a bit
            myRECT rcHitter = pSpriteHitter->GetPosition();
            myRECT rcHit    = pSpriteHittee->GetPosition();
            rcHitter.top = rcHit.top - 70;
            pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
          }
        }
        else
        {
          ptAmoebaVelocity = pSpriteHitter->GetVelocity();
          if(ptAmoebaVelocity.y > 0)
            ptAmoebaVelocity.y = 0;
          pSpriteHitter->SetVelocity(ptAmoebaVelocity);

          if(pSpriteHitter->GetBitmap() == g_pBouncyBitmap) //Make bouncy guys jump
          {
              pSpriteHitter->Jump();
              if(ptAmoebaVelocity.x > 0)
                pSpriteHitter->SetFrame(6);  //And make it look like they're jumping
              else
                pSpriteHitter->SetFrame(2);
          }

          myRECT rcPos = pSpriteHitter->GetPosition();
          myRECT rcBlock = pSpriteHittee->GetPosition();
          rcPos.top = rcBlock.top - pSpriteHitter->GetHeight();
          rcPos.top += 3;          //Set down a bit
          pSpriteHitter->SetPosition(rcPos.left, rcPos.top);

          return false;
        }
        return true;                        // Keep his old position
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pUpsideDownPlatformBitmap))  // If hitting an upside-down platform
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.top += 17;                  // Add to collision rectangles
      rcPlatformRect.bottom -= 16;

      if((rcChefRect.top - rcPlatformRect.bottom) >= 1) //If he is right below the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pFallBlockBitmap) && bIsChef)  // If chef hitting a falling block
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.bottom -= COLLISION_EDGE_ADD;                // Add to collision rectangle
      rcPlatformRect.top += COLLISION_EDGE_ADD;               //(don't take these increments out!)
      rcChefRect.top += COLLISION_EDGE_ADD;                  // Add to collision rectangles
      rcPlatformRect.bottom -= COLLISION_EDGE_ADD;
      rcChefRect.left += COLLISION_EDGE_ADD;                // Add to collision rectangles
      rcPlatformRect.right -= COLLISION_EDGE_ADD;
      rcChefRect.right -= COLLISION_EDGE_ADD;               //Add to the collision rectangles
      rcPlatformRect.left += COLLISION_EDGE_ADD;

      if((rcPlatformRect.top - rcChefRect.bottom) >= 1 && (!g_bBeside))// && g_bIsFalling) //If he is right above the block
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        //if(g_bIsJumping && g_iJumpAmount < 21)
          g_bIsJumping = false;          //Or jumping

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 70;
        int i = (int)(rcHitter.top - 5) % 75;
        if(i > 0)  //if too far down
          rcHitter.top -= i;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
        return false;
      }
      else if((rcPlatformRect.left - rcChefRect.right) >= 1)//If he is left of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      else if((rcChefRect.left - rcPlatformRect.right) >= 1)//If he is right of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      else if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If he is right above the block (test again)
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        //if(g_bIsJumping && g_iJumpAmount < 21)
          g_bIsJumping = false;                  //Or jumping

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 70;
        int i = (int)(rcHitter.top - 5) % 75;
        if(i > 0)  //if too far down
          rcHitter.top -= i;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
        return false;
      }
      else if((rcChefRect.top - rcPlatformRect.bottom) >= 1) //If he is right below the block
      {
        if(!g_bIsJumping)
        {
          DieChef();            //Chef got crushed by the block
        }
        else
        {
          return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
        }
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap)  && bIsChef)  // If Chef hitting a large falling block
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.bottom -= COLLISION_EDGE_ADD;                // Add to collision rectangle
      rcPlatformRect.top += COLLISION_EDGE_ADD;               //(don't take these increments out!)
      rcChefRect.top += COLLISION_EDGE_ADD;                  // Add to collision rectangles
      rcPlatformRect.bottom -= COLLISION_EDGE_ADD;
      rcChefRect.left += COLLISION_EDGE_ADD;                // Add to collision rectangles
      rcPlatformRect.right -= COLLISION_EDGE_ADD;
      rcChefRect.right -= COLLISION_EDGE_ADD;               //Add to the collision rectangles
      rcPlatformRect.left += COLLISION_EDGE_ADD;

      if((rcPlatformRect.top - rcChefRect.bottom) >= 1 && (!g_bBeside)) //If he is right above the block
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bIsJumping = false;          //Or jumping

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 70;
        int i = (int)(rcHitter.top - 5) % 75;
        if(i > 0)  //if too far down
          rcHitter.top -= i;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
        return false;
      }
      else if((rcPlatformRect.left - rcChefRect.right) >= 1)//If he is left of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      else if((rcChefRect.left - rcPlatformRect.right) >= 1)//If he is right of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      else if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If he is right above the block (test again)
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bIsJumping = false;                  //Or jumping

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 70;
        int i = (int)(rcHitter.top - 5) % 75;
        if(i > 0)  //if too far down
          rcHitter.top -= i;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
        return false;
      }
      else if((rcChefRect.top - rcPlatformRect.bottom) >= 1) //If he is right below the block
      {
        if((!g_bIsJumping) && g_bIsFalling)
        {
          DieChef();            //Chef got crushed by the block
        }
        else
            return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pLeftPlatformBitmap))  // If hitting a left facing platform
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.right -= 9;               //Add to the collision rectangles
      rcPlatformRect.left += 9;

      if((rcPlatformRect.left - rcChefRect.right) >= 1)//If he is left of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pRightPlatformBitmap))  // If hitting a right facing platform
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.left += 9;                // Add to collision rectangles
      rcPlatformRect.right -= 9;

      if((rcChefRect.left - rcPlatformRect.right) >= 1)//If he is right of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if(pSpriteHittee->GetBitmap() == g_pSinkingPlatformBitmap)  // If hitting a sinking platform
    {
      if(bIsChef)
      {
        if(g_bDying)    //If Chef is dying
        {
          return false; //Don't do anything
        }
        AddToRespawn(pSpriteHittee);    //Make this respawn at where it started
        pSpriteHittee->SetHidden(false);
        rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
        rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
        rcChefRect.bottom -= COLLISION_EDGE_ADD;               // Make the rectangles smaller, because
        rcPlatformRect.top += COLLISION_EDGE_ADD;              // they will already be past each other

        if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the platform
        {
          g_bIsFalling = false;             // Make him stop falling
          g_iJetbootsTime = MAX_JETBOOT_TIME;

          //Set chef's position to right above the block
          myRECT rcHitter = pSpriteHitter->GetPosition();
          myRECT rcHit    = pSpriteHittee->GetPosition();
          rcHitter.top = rcHit.top - 73;
          pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);

          pSpriteHittee->OffsetPosition(0, 19);  //Make both move down
          pSpriteHitter->OffsetPosition(0, 20);  //These numbers are important! The platform has to move down less than 20, or chef will
                                                 // fall through blocks. Chef has to fall down by 20, though, or else he won't fall into
                                                 // the trap in level 1, for instance. Yay for complex hackery.
          return false;
        }
      }
      else
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pFloatingPlatformBitmap || pSpriteHittee->GetBitmap() == g_pFloatingHiddenBitmap) && bIsChef)  // If hitting a floating platform
    {
      /*myRECT rcFloatingPlatform = pSpriteHittee->GetPosition();
      myRECT rcPerson = pSpriteHitter->GetPosition();

      //Make sure he's at least mostly overtop platform
      if(rcPerson.left < rcFloatingPlatform.left ||// - pSpriteHitter->GetWidth() / 2 ||
         rcPerson.right > rcFloatingPlatform.right)// + pSpriteHitter->GetWidth() / 2)
        return false;*/

//      myPOINT ptChefVelocity;
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.bottom -= COLLISION_EDGE_ADD;               // Make the rectangles smaller, because
      rcPlatformRect.top += COLLISION_EDGE_ADD;              // they will already be past each other
      rcChefRect.top += COLLISION_EDGE_ADD;
      rcPlatformRect.bottom -= COLLISION_EDGE_ADD;
      rcChefRect.left += COLLISION_EDGE_ADD;                // Add to collision rectangles
      rcPlatformRect.right -= COLLISION_EDGE_ADD;
      rcChefRect.right -= COLLISION_EDGE_ADD;               //Add to the collision rectangles
      rcPlatformRect.left += COLLISION_EDGE_ADD;

      if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the platform
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 73;        //Set down a bit
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);

        myPOINT ptPlatformVelocity = pSpriteHittee->GetVelocity();//g_pGame->GetNormalizedVelocity(pSpriteHittee);
        //Don't offset if platform is moving into Chef
        if(ptPlatformVelocity.x < 0)    //Platform moving left
        {
            if(rcHitter.left - rcHit.left < 10)
                return false;
        }
        else    //Platform moving right
        {
            if(rcHit.right - rcHitter.right < 10)
                return false;
        }
        rcHitter.top = rcHit.top - 71;        //Set down a bit
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
        pSpriteHitter->OffsetPosition(ptPlatformVelocity.x + 0.5, 0);  //Make it so that Chef moves along with the platform
        g_bOnPlatform = true;              //Record Chef on the platform
        return false;
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pElevatorBitmap || pSpriteHittee->GetBitmap() == g_pElevatorHiddenBitmap) && bIsChef)  // If hitting an elevator platform
    {
      if(g_bDying || (g_bIsJumping && abs(pSpriteHittee->GetVelocity().y) < g_iJumpAmount))    //If Chef is dying, or jumping faster
                                                                                               // than the platform is moving up
      {
        return false; //Don't do anything
      }
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();

      //Check and see if he's too far off the side of the elevator
      if(rcHitter.right - rcHit.right > 18 ||
         rcHit.left - rcHitter.left > 18)
            return false;   //If we're off the end of the platform too far, ignore

      //if(g_iFallAmount > 1 && g_bIsJumping)    //If falling (needed because of falling-through-platform problems)
      //{
      //  rcChefRect.bottom -= 12;               // Make the rectangles smaller, because
      //  rcPlatformRect.top += 12;              // they will already be past each other
      //}
      //else
      //{
        rcChefRect.bottom -= 24;               // Make the rectangles smaller, because
        rcPlatformRect.top += 24;              // they will already be past each other
      //}

      if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If right above the platform
      {
        g_bIsFalling = false;
        g_iJetbootsTime = MAX_JETBOOT_TIME;

        //Set chef's position to right above the platform, but slightly down
        rcHitter.top = rcHit.top - 62;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);

        return false;
      }
    }
    else if((pSpriteHittee->GetBitmap()) == g_pChromeblockBitmap)// If hitting a chrome block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelHzBitmap)// If hitting a horizontal tunnel
    {
      Top(pSpriteHitter, pSpriteHittee, bIsChef);
      Bottom(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelVBitmap)// If hitting a vertical tunnel
    {
      Left(pSpriteHitter, pSpriteHittee, bIsChef);
      Right(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelPBitmap && bIsChef)// If hitting a P tunnel
    {
      Top(pSpriteHitter, pSpriteHittee, bIsChef);
      Left(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelBackPBitmap && bIsChef)// If hitting a backwards P tunnel
    {
      Right(pSpriteHitter, pSpriteHittee, bIsChef);
      Top(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelLBitmap)// If hitting a L tunnel
    {
      Left(pSpriteHitter, pSpriteHittee, bIsChef);
      Bottom(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelBackLBitmap)// If hitting a backwards L tunnel
    {
      Right(pSpriteHitter, pSpriteHittee, bIsChef);
      Bottom(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelFBitmap && bIsChef)// If hitting a F tunnel
    {
      Left(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelPerpBitmap)// If hitting a perpendicular tunnel
    {
      Bottom(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pTunnelTBitmap && bIsChef)// If hitting a T tunnel
    {
      Top(pSpriteHitter, pSpriteHittee, bIsChef);
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCrackedLogsBitmap)// If hitting a cracked log wall block
    {
      DoBlock(pSpriteHitter, pSpriteHittee, bIsChef);
      //if(bIsChef)
      //{

          //ofile << "Firing particles" << endl;
          myRECT rc = pSpriteHittee->GetPosition();
          myPOINT pt = pSpriteHittee->GetCenter();
          myPOINT ptHitter = pSpriteHitter->GetCenter();
          //Make gravely crumbly stuff
          if((pSpriteHitter->GetVelocity().x != 0 && !pSpriteHittee->ParticlesFiring()))
          {
              pSpriteHittee->FireParticles();
              pt.x += ptHitter.x - pt.x;
              if(pt.x < rc.left + 10)
                pt.x = rc.left + 10;
              if(pt.x > rc.right - 10)
                pt.x = rc.right - 10;
              pt.x -= rc.left;
              pt.y = 55;
              pSpriteHittee->OffsetParticles(pt);

              //Play crumble noise
              PlayMySound(IDW_CRUMBLE);
          }
          //Make dust/smoke
          if(pSpriteHitter->GetVelocity().x != 0 || pSpriteHitter->GetVelocity().y != 0 || (bIsChef && (g_iFallAmount > 5 || g_iJumpAmount > 5)))
          {
              Sprite* pSprite = new Sprite(g_pCrackedLogsDieBitmap, g_rcAll, BA_DIE);
              pSprite->SetPosition(pSpriteHittee->GetPosition());
              pSprite->SetZOrder(3);
              //pSprite->SetFrameDelay(20);
              pSprite->SetHidden(true);
              //Add particle effect
              pt.x = ptHitter.x - rc.left;
              pt.y = 0;
              pSprite->AddParticles(PARTICLE_SMALLSMOKE, BITMAP_PARTICLES, 25,0,25,25, pt, false);
              pSprite->SetParticlesInFront(true);
              pSprite->FireParticles();
              pSprite->DieOnParticleFinish();
              g_pGame->AddSprite(pSprite);
          }
      //}
    }
    else if((pSpriteHittee->GetBitmap()) == g_pShootJumpBitmap ||
            pSpriteHittee->GetBitmap() == g_pBlockingPlatformBitmap)// If hitting a shoot and jump block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if(((pSpriteHittee->GetBitmap()) == g_pWoodenBlockBitmap) && (!bIsChef))// If bad guy, not Chef, hitting a wooden block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCoolblockBitmap)// If hitting a cool-looking block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGreyholeBitmap && pSpriteHitter->GetBitmap() != g_pFallBlockBitmap)// If hitting a grey block with hole in it
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if(pSpriteHittee->GetBitmap() == g_pStarBlockBitmap ||
            pSpriteHittee->GetBitmap() == g_pStarBlockCrackedBitmap  ||
            pSpriteHittee->GetBitmap() == g_pStarBlockCracked2Bitmap)// If hitting a star block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pXblockBitmap)// If hitting a x block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap() == g_pTeleporterBitmap) && !bIsChef)// If something other than Chef hitting a teleporter
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap() == g_pTunnelPBitmap) && !bIsChef)// If something other than Chef hitting a |` tunnel joint
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap() == g_pTunnelFBitmap) && !bIsChef)// If something other than Chef hitting a |- tunnel joint
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap() == g_pTunnelBackPBitmap) && !bIsChef)// If something other than Chef hitting a `| tunnel joint
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap() == g_pTunnelTBitmap) && !bIsChef)// If something other than Chef hitting a T tunnel joint
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlimeRockBitmap)// If hitting a slime rock
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCaveRockBitmap)// If hitting a cave rock
    {
      //Make boom noise if fall blocks hitting something forcefully
      if((pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap ||
          pSpriteHitter->GetBitmap() == g_pFallBlockBitmap) && pSpriteHitter->GetVelocity().y > 6)
      {
        PlayMySound(IDW_BOOM);   //Boom sound
        pSpriteHitter->FireParticles();
      }
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCoolRock2Bitmap)// If hitting a second cave rock
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGroundBitmap)// If hitting the ground
    {
      //Make boom noise if fall blocks hitting something forcefully
      if((pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap ||
          pSpriteHitter->GetBitmap() == g_pFallBlockBitmap) && pSpriteHitter->GetVelocity().y > 6)
      {
        PlayMySound(IDW_BOOM);   //Boom sound
        pSpriteHitter->FireParticles();
      }
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStripedLeftBitmap)// If hitting the left-striped block
    {
      //Make boom noise if fall blocks hitting ground forcefully (striped left blocks in Vortical Volvox Vertigo)
      if((pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap ||
          pSpriteHitter->GetBitmap() == g_pFallBlockBitmap) && pSpriteHitter->GetVelocity().y > 6)
      {
        PlayMySound(IDW_BOOM);   //Boom sound
        pSpriteHitter->FireParticles();
      }
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStripedRightBitmap)// If hitting the right-striped block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSmallBlockBitmap)// If hitting a small block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pAmoebaBlockBitmap)// If hitting amoeba block
    {
      if(pSpriteHitter->GetBitmap() == g_pGreenAmoebaBitmap)  //If amoeba hitting
        AddToRespawn(pSpriteHittee);                                //Cause amoeba to ooze out

      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pJumpBlockBitmap && (!bIsChef) && g_iCurrentLevel != 4)// If something other than Chef hitting a jump block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLargeFallBlockBitmap)// If hitting a large falling block
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.top += 17;                  // Add to collision rectangles
      rcPlatformRect.bottom -= 16;

      if(((rcChefRect.top - rcPlatformRect.bottom) >= 1) && (pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap || pSpriteHitter->GetBitmap() == g_pFallBlockBitmap)) //If one falling block hitting another
      {
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit = pSpriteHittee->GetPosition();
        pSpriteHittee->SetPosition(rcHit.left, rcHitter.top - 75);
        return false;
      }
      else if(!bIsChef)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap()) == g_pFallBlockBitmap)// If hitting a falling block
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.top += 17;                  // Add to collision rectangles
      rcPlatformRect.bottom -= 16;

      if(((rcChefRect.top - rcPlatformRect.bottom) >= 1) && (pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap || pSpriteHittee->GetBitmap() == g_pFallBlockBitmap)) //If he is right below the block
      {
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit = pSpriteHittee->GetPosition();
        pSpriteHittee->SetPosition(rcHit.left, rcHitter.top - 75);
        return false;
      }
      else if(!bIsChef)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowStairsBitmap)// If hitting yellow stairs
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowStairBlockBitmap)// If hitting yellow stair block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBlueStairsBitmap)// If hitting blue stairs
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBlueStairBlockBitmap)// If hitting blue stair block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangeStairsBitmap)// If hitting orange stairs
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangeStairBlockBitmap)// If hitting orange stair block
    {
      return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlantedLeftBitmap)// If hitting the left-facing ground
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlantedRightBitmap)// If hitting the right-facing ground
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlantedLeftChromeBitmap)// If hitting the chrome left-facing ground
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlantedRightChromeBitmap)// If hitting the chrome right-facing ground
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLeftRockSlant)// If hitting the rock left-facing ground
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pRightRockSlant)// If hitting the rock right-facing ground
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStarSlantL)// If hitting the star block left-facing ground
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStarSlantR)// If hitting the star block right-facing ground
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStarCielingLeft)// If hitting the star block left-facing cieling
    {
      return DoLeftCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStarCielingRight)// If hitting the star block right-facing cieling
    {
      return DoRightCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLavaSlopeL)// If hitting the lava rock block left-facing ground
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLavaSlopeR)// If hitting the lava rock block right-facing ground
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCLeftRockSlant)// If hitting the rock left-facing cieling
    {
      return DoLeftCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCRightRockSlant)// If hitting the rock right-facing cieling
    {
      return DoRightCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLavaSlopeCL)// If hitting the lava rock left-facing cieling
    {
      return DoLeftCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLavaSlopeCR)// If hitting the lava rock right-facing cieling
    {
      return DoRightCielingSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap() == g_pNullBlockBitmap) && !bIsChef)// If bad guy hitting an invisible block
    {
      rcChefRect = pSpriteHitter->GetCollision();          // Get his collision rectangle
      rcPlatformRect = pSpriteHittee->GetCollision();      // and the platform's
      rcChefRect.right -= 9;               //Add to the collision rectangles
      rcPlatformRect.left += 9;
      rcChefRect.left += 9;                // Add to collision rectangles
      rcPlatformRect.right -= 9;

      if((rcChefRect.left - rcPlatformRect.right) >= 1)//If he is right of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }

      if((rcPlatformRect.left - rcChefRect.right) >= 1)//If he is left of the block
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      if(g_iCurrentLevel == 17)//If in secret level 2
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));  //Test for tops as well
      }
      return false;  //Not on either side
    }
    else if((!bIsChef))
    {   //Enemy hitting door, but not last level!
        if(pSpriteHittee->GetBitmap() == g_pGreendoorBitmap ||
           pSpriteHittee->GetBitmap() == g_pYellowdoorBitmap ||
           pSpriteHittee->GetBitmap() == g_pOrangedoorBitmap ||
           pSpriteHittee->GetBitmap() == g_pReddoorBitmap ||
           pSpriteHittee->GetBitmap() == g_pRedForceBitmap ||
           pSpriteHittee->GetBitmap() == g_pGreenForceBitmap ||
           pSpriteHittee->GetBitmap() == g_pBlueForceBitmap ||
           (pSpriteHittee->GetBitmap() == g_pBluedoorBitmap && g_iCurrentLevel != 15))  //HACK ALERT
        {
            return DoBlock(pSpriteHitter, pSpriteHittee, bIsChef);
        }
    }
  }

  //---------------------------------------------------
  // Chef hitting something
  //---------------------------------------------------

  if(bIsChef)
  {
    if(g_bDying)    //If Chef is dying
    {
      return false; //Don't do anything
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCheckpointBitmap)    //Hitting checkpoint
    {
        if(pSpriteHittee->GetFrame() == 0)
        {
            pSpriteHittee->SetFrame(1);             //Set checkpoint to on
            g_rcLastCheckpointPos = pSpriteHittee->GetPosition();
            if(g_pLastCheckpoint != NULL)
            {
                g_pLastCheckpoint->SetFrame(0);
            }
            g_pLastCheckpoint = pSpriteHittee;
            CopyRect(&g_rcLastViewport, &g_rcAll);
//            if(!g_bNoSound)
                PlayMySound(IDW_CHECKPOINT);
        }
        EmptyRespawn(); //Empty all respawn items, whatever the case
    }
    else if((pSpriteHittee->GetBitmap()) == g_pJetBootsBitmap ||
            pSpriteHittee->GetBitmap() == g_pSuperJetBootsBitmap)//If hitting jet boots
    {
      AddToRespawn(pSpriteHittee);
      if(g_iCurrentLevel == 1)  //If in second level, get jet boots
      {
        g_bJetBoots = true;
        if(!g_bSuperJetBoots)
            g_pPersonSprite->SetBitmap(g_pPersonJetBoots);
        g_bCurLevelPart = true;
      }
      if(g_iCurrentLevel == 16)  //If in secret level, get super jet boots
      {
        g_bSuperJetBoots = true;
        g_pPersonSprite->SetBitmap(g_pPersonSuperJetBoots);
        g_bCurLevelPart = true;
        MAX_JETBOOT_TIME = 25;
      }
      // Play the got jet boots sound
      PlayMySound(IDW_JETBOOTS);
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGreenkeyBitmap)//If hitting a green key
    {
      AddToRespawn(pSpriteHittee);
      g_bGreenKey = true;
//      if(!g_bNoSound)
      {
        // Play the got key sound
        PlayMySound(IDW_GOTKEY);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBluekeyBitmap)//If hitting a blue key
    {
      AddToRespawn(pSpriteHittee);
      g_bBlueKey = true;
//      if(!g_bNoSound)
      {
        // Play the got key sound
        PlayMySound(IDW_GOTKEY);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pRedkeyBitmap)//If hitting a red key
    {
      AddToRespawn(pSpriteHittee);
      g_bRedKey = true;
//      if(!g_bNoSound)
      {
        // Play the got key sound
        PlayMySound(IDW_GOTKEY);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangekeyBitmap)//If hitting an orange key
    {
      AddToRespawn(pSpriteHittee);
      g_bOrangeKey = true;
//      if(!g_bNoSound)
      {
        // Play the got key sound
        PlayMySound(IDW_GOTKEY);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowkeyBitmap)//If hitting a yellow key
    {
      AddToRespawn(pSpriteHittee);
      g_bYellowKey = true;
//      if(!g_bNoSound)
      {
        // Play the got key sound
        PlayMySound(IDW_GOTKEY);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangeButtonBitmap)//If hitting an orange button
    {
        if((!g_bOrangeStair) && (DownKey()) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
          pSpriteHittee->SetFrame(1);
          g_bOrangeStair = true;
          //Play the hit switch sound
          PlayMySound(IDW_SWITCH);
          /////////////////////HACK Checkpoint here too HACK/////////////////////
          //Just because I don't want to have to deal with respawning staircases
          g_rcLastCheckpointPos = pSpriteHittee->GetPosition();
          if(g_pLastCheckpoint != NULL)
            g_pLastCheckpoint->SetFrame(0);
          g_pLastCheckpoint = NULL;   //Adding to the hackery
          CopyRect(&g_rcLastViewport, &g_rcAll);
          EmptyRespawn(); //Empty all respawn items, whatever the case
          ///////////////////////////////////////////////////////////////////////
        }
        else if((!g_bOrangeStair) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
            //If not hitting down key, display dialog saying to hit down key
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
                sMsg += "Down";
            else
                sMsg += g_pGame->GetKeyName(DOWN_KEY);
            sMsg += " to activate switch";
            DisplayMsg(sMsg, 100);
        }
        return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowButtonBitmap)//If hitting a yellow button
    {
        if((!g_bYellowStair) && (DownKey()) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
          pSpriteHittee->SetFrame(1);
          g_bYellowStair = true;
          //Play the hit switch sound
          PlayMySound(IDW_SWITCH);
          /////////////////////HACK Checkpoint here too HACK/////////////////////
          //Just because I don't want to have to deal with respawning staircases
          g_rcLastCheckpointPos = pSpriteHittee->GetPosition();
          if(g_pLastCheckpoint != NULL)
            g_pLastCheckpoint->SetFrame(0);
          g_pLastCheckpoint = NULL;   //Adding to the hackery
          CopyRect(&g_rcLastViewport, &g_rcAll);
          EmptyRespawn(); //Empty all respawn items, whatever the case
          ///////////////////////////////////////////////////////////////////////
        }
        else if((!g_bYellowStair) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
            //If not hitting down key, display dialog saying to hit down key
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
                sMsg += "Down";
            else
                sMsg += g_pGame->GetKeyName(DOWN_KEY);
            sMsg += " to activate switch";
            DisplayMsg(sMsg, 100);
        }
        return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBlueButtonBitmap)//If hitting a blue button
    {
        if((!g_bBlueStair) && (DownKey()) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
          pSpriteHittee->SetFrame(1);
          g_bBlueStair = true;
          //Play the hit switch sound
          PlayMySound(IDW_SWITCH);
          /////////////////////HACK Checkpoint here too HACK/////////////////////
          //Just because I don't want to have to deal with respawning staircases
          g_rcLastCheckpointPos = pSpriteHittee->GetPosition();
          if(g_pLastCheckpoint != NULL)
            g_pLastCheckpoint->SetFrame(0);
          g_pLastCheckpoint = NULL;   //Adding to the hackery
          CopyRect(&g_rcLastViewport, &g_rcAll);
          EmptyRespawn(); //Empty all respawn items, whatever the case
          ///////////////////////////////////////////////////////////////////////
        }
        else if((!g_bBlueStair) && (pSpriteHittee->GetFrame() == 0) && (!g_bIsJumping) && (g_iFallAmount <= 2))
        {
            //If not hitting down key, display dialog saying to hit down key
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
                sMsg += "Down";
            else
                sMsg += g_pGame->GetKeyName(DOWN_KEY);
            sMsg += " to activate switch";
            DisplayMsg(sMsg, 100);
        }
        return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pTeleporterBitmap && (pSpriteHittee->GetFrame() == 0))//If hitting a teleporter
    {
      if((DownKey()))
      {
          if(++g_iTeleporterCount == 30)
          {
            //Show message of teleporting too many times
            DisplayMsg("Teleporting 30 times kills you. Just because I can.", 9001);
            DieChef();
            g_iTeleporterCount = 0;
            return false;
          }
          pSpriteHittee->SetFrame(1);
          //Play the switch click sound
          PlayMySound(IDW_SWITCH);
      }
      else
      {
          //If not hitting down key, display dialog saying to hit down key
          string sMsg = "Press ";
          if(g_pGame->HasJoystick())
                sMsg += "Down";
            else
                sMsg += g_pGame->GetKeyName(DOWN_KEY);
          sMsg += " to activate teleporter";
          DisplayMsg(sMsg, 100);
      }
    }
    else if((pSpriteHittee->GetBitmap() == g_pTeleporterBitmap) && (pSpriteHittee->GetFrame() != 0))//If hitting a teleporter that is starting to go down
    {
      if(pSpriteHittee->GetFrame() != 9)
      {
        myRECT rc = pSpriteHittee->GetPosition();
        pSpriteHitter->SetPosition(rc.left, rc.top + 2);
        pSpriteHitter->OffsetPosition((pSpriteHittee->GetWidth() / 2) - (pSpriteHitter->GetWidth() / 2), 0);  //Center inside teleporter
        return false;         //Exit function
      }

      //Play teleport sound
//      if(!g_bNoSound)
        PlayMySound(IDW_TELEPORT);

      g_bIncompleteViewportDrawn = false; //Redraw viewport if we need to

      //Set position to right inside other teleporter (If frame ten and time to transport)
      myRECT rc = g_pPersonSprite->GetPosition();
      if(g_iCurrentLevel == 9)      //Level 10
      {
        if(pSpriteHittee == g_pTeleporterSprite[0])    //Upper teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[1]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.bottom = 3225;     //Set viewing area to only see part of level
          g_rcAll.top = 2550;
          g_pTeleporterSprite[1]->SetFrame(10);   //Set to look like coming in frame
        }
        else                                        //Lower teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[0]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.bottom = 2550;     //Set viewing area to see only part of level
          g_rcAll.top = 0;
          g_pTeleporterSprite[0]->SetFrame(10);  //Set to look like coming in frame
        }
      }
      else if(g_iCurrentLevel == 12)            //Level 13
      {
        if(pSpriteHittee == g_pTeleporterSprite[1])    //Lower teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[0]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_pTeleporterSprite[0]->SetFrame(10);  //Set to look like coming in frame
        }
        else                                          //Upper teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[1]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_pTeleporterSprite[1]->SetFrame(10);  //Set to look like coming in frame
        }
      }
      if(g_iCurrentLevel == 13)      //Level 14
      {
        if(pSpriteHittee == g_pTeleporterSprite[0])    //Upper teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[1]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.left = 0;     //Set viewing area to see most of level
          g_rcAll.right = 34 * 75;
          g_pTeleporterSprite[1]->SetFrame(10);   //Set to look like coming in frame
        }
        else                                        //Lower teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[0]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.left = 34 * 75;     //Set viewing area to only see part of level
          g_rcAll.right = 45 * 75;
          g_pTeleporterSprite[0]->SetFrame(10);  //Set to look like coming in frame
        }
      }
      if(g_iCurrentLevel == 14)      //Level 15
      {
        if(pSpriteHittee == g_pTeleporterSprite[0])    //Upper teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[1]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.left = 75;   //Only see part of level
          g_rcAll.top = 9 * 75;
          g_rcAll.bottom = 43 * 75;
          g_pTeleporterSprite[1]->SetFrame(10);   //Set to look like coming in frame
        }
        else                                        //Lower teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[0]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.left = 0;     //Set viewing area to see other part of level
          g_rcAll.top = 0;
          g_rcAll.bottom = 9 * 75;
          g_pTeleporterSprite[0]->SetFrame(10);  //Set to look like coming in frame
        }
      }
      if(g_iCurrentLevel == 15)      //Level 16
      {
        if(pSpriteHittee == g_pTeleporterSprite[0])    //Upper teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[1]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.right = 21 * 75;   //Only see part of level
          g_rcAll.left = 0;
          g_pTeleporterSprite[1]->SetFrame(10);   //Set to look like coming in frame
        }
        else                                        //Lower teleporter
        {
          myRECT rcHit = g_pTeleporterSprite[0]->GetPosition();
          rc.top = rcHit.top + 2;
          rc.left = rcHit.left;
          g_rcAll.left = 21 * 75;     //Set viewing area to see other part of level
          g_rcAll.right = 43 * 75;
          g_pTeleporterSprite[0]->SetFrame(10);  //Set to look like coming in frame
        }
      }
      g_pPersonSprite->SetPosition(rc.left, rc.top);
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangeHubBitmap && (!g_bOrangeHub) && (pSpriteHittee->GetFrame() == 0))//If hitting an orange hub
    {
      pSpriteHittee->SetFrame(1);
      AddToRespawn(pSpriteHittee);  //Also keep track of this one, to set its frame back to 0 on respawn
      pSpriteHittee->SetHidden(false);  //And unhide
      g_bOrangeHub = true;
//      if(!g_bNoSound)
      {
         //Play the hit switch sound
        PlayMySound(IDW_SWITCH);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowHubBitmap && (!g_bYellowHub) && (pSpriteHittee->GetFrame() == 0))//If hitting a yellow hub
    {
      pSpriteHittee->SetFrame(1);
      AddToRespawn(pSpriteHittee);  //Also keep track of this one, to set its frame back to 0 on respawn
      pSpriteHittee->SetHidden(false);  //And unhide
      g_bYellowHub = true;
//      if(!g_bNoSound)
      {
         //Play the hit switch sound
        PlayMySound(IDW_SWITCH);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBlueHubBitmap && (!g_bBlueHub) && (pSpriteHittee->GetFrame() == 0))//If hitting a blue hub
    {
      pSpriteHittee->SetFrame(1);
      AddToRespawn(pSpriteHittee);  //Also keep track of this one, to set its frame back to 0 on respawn
      pSpriteHittee->SetHidden(false);  //And unhide
      g_bBlueHub = true;
//      if(!g_bNoSound)
      {
         //Play the hit switch sound
        PlayMySound(IDW_SWITCH);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGreenHubBitmap && (!g_bGreenHub) && (pSpriteHittee->GetFrame() == 0))//If hitting a green hub
    {
      pSpriteHittee->SetFrame(1);
      AddToRespawn(pSpriteHittee);  //Also keep track of this one, to set its frame back to 0 on respawn
      pSpriteHittee->SetHidden(false);  //And unhide
      g_bGreenHub = true;
//      if(!g_bNoSound)
      {
         //Play the hit switch sound
        PlayMySound(IDW_SWITCH);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pJumpBlockBitmap)//If hitting a jumping block
    {
      if(!(DownKey())) //If not pressing down arrow key
      {
        g_iFallAmount = 0;                      //Make him stop falling
        g_bIsFalling = false;
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bIsJumping = true;                   // Start his jumping
        g_bJumpBlockJump = true;
        g_bDoubleJumpPressThisCycle = false;
        g_iJumpAmount = 30;                   //Double his jump amount (it's exponential)
//        if(!g_bNoSound)
        {
          if(!g_iJumpSoundDelay)
          {
              // Play the jump sound
              PlayMySound(IDW_CHEFJUMPING);

              g_iJumpSoundDelay = 1;
          }
        }
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSuperJumpBlockBitmap)//If hitting a super jumping block
    {
      if(!(DownKey())) //If not pressing down arrow key
      {
        g_iFallAmount = 0;                      //Make him stop falling
        g_bIsFalling = false;
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bIsJumping = true;                   // Start his jumping
        g_bJumpBlockJump = true;
        g_bDoubleJumpPressThisCycle = false;
        g_iJumpAmount = 35;                   //Make him able to jump eight blocks up (it's exponential)
//        if(!g_bNoSound)
        {
          if(!g_iJumpSoundDelay)
          {
              // Play the jump sound
              PlayMySound(IDW_CHEFJUMPING);

              g_iJumpSoundDelay = 1;
          }
        }
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowLaserBitmap ||
            (pSpriteHittee->GetBitmap()) == g_pYellowLaserVBitmap ||
            (pSpriteHittee->GetBitmap()) == g_pOrangeLaserBitmap ||
            (pSpriteHittee->GetBitmap()) == g_pOrangeLaserVBitmap ||
            (pSpriteHittee->GetBitmap()) == g_pBlueLaserBitmap ||
            (pSpriteHittee->GetBitmap()) == g_pGreenLaserHzBitmap)//If hitting a laser
    {
      DoBlock(pSpriteHitter, pSpriteHittee, bIsChef);   //Don't let invincible players go through, either
      DieChef();   //Kill Chef Bereft
    }
    else if(pSpriteHittee->GetBitmap() == g_pLaserBoltHorizontalBitmap ||
            pSpriteHittee->GetBitmap() == g_pLaserBoltVerticalBitmap)//If hitting a horizontal or vertical laser bolt
    {
      DieChef();   //Kill Chef! NOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBubbleBitmap)//If hitting a blue amoeba flame
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pFireBlowBitmap)//If hitting some fire a yellow amoeba blew
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pStalagtiteBitmap)//If hitting a dripping lava spot
    {
      return DoBlock(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pExitBitmap)//If hitting an exit
    {
      if(g_iCurrentLevel == 0 ||   //On levels with no missile, make you done level
         g_iCurrentLevel == 3 ||
         g_iCurrentLevel == 7 ||
         g_iCurrentLevel == 8 ||
         g_iCurrentLevel == 10 ||
         g_iCurrentLevel == 11 ||
         g_iCurrentLevel == 14)
      {
        g_bCurLevelPart = true;
      }

      //Done the level
      DoWon(pSpriteHittee);
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pRedAmoebaBitmap) //If hitting an amoeba
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pAlienBitmap) //If hitting an alien
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pPoisonVatBitmap) //If hitting some poison vat
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pLava1Bitmap ||
            pSpriteHittee->GetBitmap() == g_pLava2Bitmap) //If hitting some lava vat
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSewerVatBitmap) //If hitting some sewer sludge vat (Ewwww....)
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGrassyThingBitmap) //If hitting a bear trap
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pAlienBulletBitmap) //If hitting a bullet shot by an alien warrior
    {
      DieChef();
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pFlySpoonLElectricBitmap)  //If hitting an electrified spoon
    {
      pSpriteHittee->Kill();
      DieChef();
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pFlySpoonRElectricBitmap)  //If hitting an electrified spoon
    {
      pSpriteHittee->Kill();
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pHoleBitmap) //If hitting a hole
    {
      if(JetbootBlasting())
        return DoBlock(pSpriteHitter, pSpriteHittee, bIsChef);  //Can't blast back into an area
      g_bHole = true;
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pSlimeBitmap) //If hitting some cieling slime
    {
      DieChef();
      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pSludgeFlowBitmap) //If hitting flowing sludge
    {
      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit = pSpriteHittee->GetPosition();
      if(rcHitter.left >= rcHit.left - 8)
        pSpriteHitter->OffsetPosition(2, 0);         //Make him slowly drift along with it
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCoilBitmap) //If hitting a heating coil
    {
      DieChef();            //Hisssssssssssssssssssssssssssssssssssssssss...................
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pVortexBitmap) //If hitting a vortex
    {
      DieChef();
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pNoodleBitmap) //If hitting a noodle
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 10;  //The points are only increased by ten, but when printed
                              //on the screen, are multiplied by ten, so it works out
                              //to 100 more points.
      PlayPointsNoise();
      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p100PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangeBitmap) //If hitting an orange
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 20;
      PlayPointsNoise();

      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p200PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pCupcakeBitmap) //If hitting a cupcake
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 30;
      PlayPointsNoise();

      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p300PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGrapeJuiceBitmap) //If hitting some grape juice
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 50;
      PlayPointsNoise();

      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p500PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pEggsBitmap) //If hitting a dozen eggs
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 100;
      PlayPointsNoise();

      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p1000PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pMilkshakeBitmap) //If hitting a banana milkshake
    {
      AddToRespawn(pSpriteHittee);
      g_iPointsGotten += 500;
      PlayPointsNoise();

      myRECT rect = pSpriteHittee->GetPosition();
      Sprite* pSprite = new Sprite(g_p5000PointsBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(7);    //High z- order
      pSprite->SetNumFrames(12, true);
      g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pElectrosphereBitmap) //If hitting an electrosphere
    {
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the got life sound
        PlayMySound(IDW_NEWLIFE);
      }
      if(!g_bTutorialElectrosphere)
      {
          DisplayMsg("Collect electrospheres to unlock secrets");
          g_bTutorialElectrosphere = true;
      }
      //ofile << "Orig pos: " << pSpriteHittee->GetOrigPos().x << ", " << pSpriteHittee->GetOrigPos().y << endl;
      g_lElectrospheresCollected.push_front(pSpriteHittee->GetOrigPos());   //Add this to the list of electrospheres we have

      return false;
    }
    else if(pSpriteHittee->GetBitmap() == g_pUFOBitmap && !g_bTutorialMoved) //If hitting the UFO in level 1, give tutorial prompt
    {
        if(g_pGame->HasJoystick())
            DisplayMsg("Use Stick 1 to move", 500);
        else
            DisplayMsg("Use arrow keys to move", 500);
    }
    else if(pSpriteHittee->GetBitmap() == g_pTutorialBitmap)    //If hitting tutorial message thing
    {
        if(pSpriteHittee->GetLives() == TUTORIAL_JUMP && !g_bTutorialJumped)    //HACK: Find what message to display based on the # of lives the sprite has
        {
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
            {
                sMsg += "Button ";
                sMsg += g_pGame->GetJoystickFire1() + 1 + '0';
            }
            else
                sMsg += g_pGame->GetKeyName(JUMP_KEY);
            sMsg += " to jump";
            DisplayMsg(sMsg, 500);
        }
        else if(pSpriteHittee->GetLives() == TUTORIAL_FIRE && !g_bTutorialFired)
        {
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
            {
                sMsg += "Button ";
                sMsg += g_pGame->GetJoystickFire2() + 1 + '0';
            }
            else
                sMsg += g_pGame->GetKeyName(FIRE_KEY);
            sMsg += " to shoot at enemies";
            DisplayMsg(sMsg, 500);
        }
        else if(pSpriteHittee->GetLives() == TUTORIAL_SLOPEFIRE && !g_bTutorialSlopeFired)
        {
            string sMsg = "Press ";
            if(g_pGame->HasJoystick())
            {
                sMsg += "Button ";
                sMsg += g_pGame->GetJoystickFire2() + 1 + '0';
            }
            else
                sMsg += g_pGame->GetKeyName(FIRE_KEY);
            sMsg += " while holding ";
            if(g_pGame->HasJoystick())
                sMsg += "Down";
            else
                sMsg += g_pGame->GetKeyName(DOWN_KEY);
            sMsg += " to shoot up or down slopes";
            DisplayMsg(sMsg, 500);
        }
    }
    /*else if((pSpriteHittee->GetBitmap()) == g_pForkBitmap) //If hitting a fork
    {
      pSpriteHittee->Kill();
//      if(!g_bNoSound)
      {
        // Play the got fork sound
        PlayMySound(IDW_GOTFORK);
      }
      return false;
    }*/
    else if((pSpriteHittee->GetBitmap()) == g_pGreendoorBitmap)//If hitting a green door
    {
      if(!g_bGreenKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the going through door sound
        PlayMySound(IDW_GODOOR);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pReddoorBitmap)//If hitting a red door
    {
      if(!g_bRedKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the going through door sound
        PlayMySound(IDW_GODOOR);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBluedoorBitmap)//If hitting a blue door
    {
      if(!g_bBlueKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the going through door sound
        PlayMySound(IDW_GODOOR);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pYellowdoorBitmap)//If hitting a yellow door
    {
      if(!g_bYellowKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the going through door sound
        PlayMySound(IDW_GODOOR);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pOrangedoorBitmap)//If hitting an orange door
    {
      if(!g_bOrangeKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      AddToRespawn(pSpriteHittee);
//      if(!g_bNoSound)
      {
        // Play the going through door sound
        PlayMySound(IDW_GODOOR);
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pBlueForceBitmap)//If hitting a blue force block
    {
      if(g_bBlueKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pGreenForceBitmap)//If hitting a green force block
    {
      if(g_bGreenKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pRedForceBitmap)//If hitting a red force block
    {
      if(g_bRedKey)
      {
        return (DoBlock(pSpriteHitter, pSpriteHittee, bIsChef));
      }
      return false;
    }
    else if((pSpriteHittee->GetBitmap()) == g_pMirrorRightBitmap) //If hitting a right mirror
    {
      return DoRightSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
    else if((pSpriteHittee->GetBitmap()) == g_pMirrorLeftBitmap) //If hitting a left mirror
    {
      return DoLeftSlant(pSpriteHitter, pSpriteHittee, bIsChef);
    }
  }
  return false;
}

void SpriteDying(Sprite* pSpriteDying)
{
    if(pSpriteDying->GetZOrder() == HACK_ZORDER_NOSPAWN)
        return;
  Bitmap* pBitmap = pSpriteDying->GetBitmap();

  if(pBitmap == g_pGreenAmoebaBitmap ||
     pBitmap == g_pAlienBitmap ||
     pBitmap == g_pWormBitmap)
  {
      //Add splat particles
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(pSpriteDying->GetPosition());
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
      pSprite->AddParticles(PARTICLE_AMOEBASPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset);
      pSprite->SetParticlesInFront(true);
      pSprite->SetHidden(true);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pRedAmoebaBitmap)
  {
      //Add splat particles
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(pSpriteDying->GetPosition());
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
      pSprite->AddParticles(PARTICLE_REDAMOEBASPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset);
      pSprite->SetParticlesInFront(true);
      pSprite->SetHidden(true);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pBlueAmoebaBitmap ||
          pBitmap == g_pAlienFighterBitmap)
  {
      //Add splat particles
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(pSpriteDying->GetPosition());
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
      pSprite->AddParticles(PARTICLE_BLUEAMOEBASPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset);
      pSprite->SetParticlesInFront(true);
      pSprite->SetHidden(true);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pBatBitmap)
  {
      //Add splat particles
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(pSpriteDying->GetPosition());
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
      pSprite->AddParticles(PARTICLE_BATSPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset,false);
      pSprite->SetParticlesInFront(true);
      pSprite->SetHidden(true);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pBouncyBitmap)
  {
      //Add splat particles
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(pSpriteDying->GetPosition());
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
      pSprite->AddParticles(PARTICLE_JUMPERSPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset,false);
      pSprite->SetParticlesInFront(true);
      pSprite->SetHidden(true);
      pSprite->FireParticles();
      pSprite->DieOnParticleFinish();
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pStarBlockBitmap)
  {
      myRECT rcPos = pSpriteDying->GetPosition();
      Sprite* pSprite = new Sprite(g_pStarBlockCrackedBitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(rcPos.left, rcPos.top);
      pSprite->SetZOrder(pSpriteDying->GetZOrder());

      //Add smoke particles
      myPOINT ptParticleOffset = {17,25};
      pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
      pSprite->SetParticleTile(2,25);
      pSprite->SetParticlesInFront(true);
      //pSprite->DieOnParticleFinish();
      pSprite->FireParticles();

      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pStarBlockCrackedBitmap)
  {
      myRECT rcPos = pSpriteDying->GetPosition();
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(rcPos.left, rcPos.top);
      pSprite->SetZOrder(pSpriteDying->GetZOrder());

      //Add smoke particles
      myPOINT ptParticleOffset = {17,25};
      pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
      pSprite->SetParticleTile(2,25);
      pSprite->SetParticlesInFront(true);
      //pSprite->DieOnParticleFinish();
      pSprite->FireParticles();

      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pStarBlockCracked2Bitmap && !(pSpriteDying->IsHidden()))
  {
      myRECT rcPos = pSpriteDying->GetPosition();
      Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(rcPos.left, rcPos.top);
      pSprite->SetZOrder(pSpriteDying->GetZOrder());

      //Add smoke particles
      myPOINT ptParticleOffset = {17,25};
      pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
      pSprite->SetParticleTile(2,25);
      pSprite->SetParticlesInFront(true);
      pSprite->FireParticles();
      //pSprite->DieOnParticleFinish();
      pSprite->SetHidden(true);

      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pBalloonBitmap)
  {
      myRECT rcPos = pSpriteDying->GetPosition();
      Sprite* pSprite = new Sprite(g_pBalloonPopBitmap, g_rcViewport, BA_DIE);
      pSprite->SetPosition(rcPos.left, rcPos.top);
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      pSprite->SetNumFrames(4, true);
      g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pYellowAmoebaBitmap)
  {
    g_iPointsGotten += 460; //Add 4600 more because 400 already added
    //Make a 5000-points sprite
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_p5000PointsBitmap, g_rcViewport, BA_DIE);
    pSprite->SetPosition(rect.left + (pSpriteDying->GetWidth() / 2), rect.top + (pSpriteDying->GetHeight() / 2));
    pSprite->SetZOrder(7);    //High z- order
    pSprite->SetNumFrames(12, true);
    g_pGame->AddSprite(pSprite);         //Add the sprite later, not now

    //Make a yellow key sprite
    if(g_iCurrentLevel != 15 && //If not in last level
       g_iCurrentLevel != 6 &&    //and not in vortical volvox vertigo
       g_iCurrentLevel != 7)        //And not in Heirarchy Heating Houses
    {
        pSprite = new Sprite(g_pYellowkeyBitmap, g_rcViewport, BA_BOUNCE);
        pSprite->SetPosition(rect.left, rect.top);
        pSprite->SetZOrder(6);
        pSprite->SetNumFrames(5);
        g_pGame->AddSprite(pSprite);         //Add the sprite later, not now
        AddToDespawn(pSprite);
    }

    //Make a electrosphere sprite, only if we haven't gotten this one already
    if(!CheckElectrosphere(pSpriteDying->GetOrigPos()))
    {
        pSprite = new Sprite(g_pElectrosphereBitmap, g_rcViewport, BA_BOUNCE);
        pSprite->SetNumFrames(10);
        pSprite->SetPosition(rect.right - (pSprite->GetWidth()), rect.top);
        pSprite->SetZOrder(5);  //Slightly lower than key so it doesn't cover it
        g_pGame->AddSprite(pSprite);
        pSprite->SetOrigPos(pSpriteDying->GetOrigPos());
        AddToDespawn(pSprite);
    }

    //Add splat particles
    pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(pSpriteDying->GetPosition());
    pSprite->SetZOrder(pSpriteDying->GetZOrder());
    myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
    pSprite->AddParticles(PARTICLE_YELLOWAMOEBASPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset);
    pSprite->SetParticlesInFront(true);
    pSprite->SetHidden(true);
    pSprite->FireParticles();
    pSprite->DieOnParticleFinish();
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pYellowStairBlockBitmap)
  {
    myRECT rcPos = pSpriteDying->GetPosition();
    int i = (int)rcPos.top % 75;
    if(i > 37)
    {
      rcPos.top -= i;
      rcPos.top += 75;
    }
    rcPos.top /= 75;
    rcPos.top *= 75;    //Set to unseen block grid
    Sprite* pSprite = new Sprite(g_pYellowStairBlockBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetVelocity(0, 0);
    pSprite->SetZOrder(3);
    pSprite->SetNumFrames(4);
    pSprite->SetFrameDelay(2);
    pSprite->SetFrame(pSpriteDying->GetFrame());
    pSprite->SetPosition(rcPos.left, rcPos.top);
    g_pGame->AddSprite(pSprite);
    PlayMySound(IDW_ELEVATORLOUDER);    //Play noise for blocks hitting end
  }
  else if(pBitmap == g_pOrangeStairBlockBitmap)
  {
    myRECT rcPos = pSpriteDying->GetPosition();
    int i = (int)rcPos.top % 75;
    if(i > 37)
    {
      rcPos.top -= i;
      rcPos.top += 75;
    }
    rcPos.top /= 75;
    rcPos.top *= 75;    //Set to unseen block grid
    Sprite* pSprite = new Sprite(g_pOrangeStairBlockBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetVelocity(0, 0);
    pSprite->SetZOrder(3);
    pSprite->SetNumFrames(4);
    pSprite->SetFrameDelay(2);
    pSprite->SetFrame(pSpriteDying->GetFrame());
    pSprite->SetPosition(rcPos.left, rcPos.top);
    g_pGame->AddSprite(pSprite);
    PlayMySound(IDW_ELEVATORLOUDER);    //Play noise for blocks hitting end
  }
  else if(pBitmap == g_pBlueStairBlockBitmap)
  {
    myRECT rcPos = pSpriteDying->GetPosition();
    int i = (int)rcPos.top % 75;
    if(i > 37)
    {
      rcPos.top -= i;
      rcPos.top += 75;
    }
    rcPos.top /= 75;
    rcPos.top *= 75;    //Set to unseen block grid
    Sprite* pSprite = new Sprite(g_pBlueStairBlockBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetVelocity(0, 0);
    pSprite->SetZOrder(3);
    pSprite->SetNumFrames(4);
    pSprite->SetFrameDelay(2);
    pSprite->SetFrame(pSpriteDying->GetFrame());
    pSprite->SetPosition(rcPos.left, rcPos.top);
    g_pGame->AddSprite(pSprite);
    PlayMySound(IDW_ELEVATORLOUDER);    //Play noise for blocks hitting end
  }
  else if(pBitmap == g_pAmoebaBlockBitmap)
  {
    //Make an amoeba oozing sprite
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pAmoebaOozeBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(3);    //same z- order
    pSprite->SetNumFrames(8, true);
    pSprite->SetFrameDelay(2);  //Slow down the animation
    g_pGame->AddSprite(pSprite);  //Add sprite now
    AddToDespawn(pSprite);

    //Play amoeba oozing sound
    PlayMySound(IDW_AMOEBAOOZE);
  }
  else if(pBitmap == g_pAmoebaOozeBitmap)
  {
    //Make an amoeba rock crumbling sprite
    RemoveFromDespawn(pSpriteDying);    //Grr segfaults
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pAmoebaRockCrumbleBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);    //Low z- order
    pSprite->SetNumFrames(8, true);
    pSprite->SetFrameDelay(2);  //Slow down the animation
    g_pGame->AddSprite(pSprite);

    // Create amoeba sprite
    BadGuySprite* pBASprite;
    pBASprite = new BadGuySprite(g_pGreenAmoebaBitmap, g_rcAll, BA_BOUNCE);
    pBASprite->SetZOrder(2);
    pBASprite->SetPosition(rect.left, rect.top + 32);
    pBASprite->SetNumFrames(8);
    if(rand() % 2 == 1)   //Set his starting direction randomly
      pBASprite->SetVelocity(-6, 0);
    else
      pBASprite->SetVelocity(6, 0);
    g_pGame->AddSprite(pBASprite);  //Add sprite later
    AddToDespawn(pBASprite);

    //Play rock crumble noise
    PlayMySound(IDW_CRUMBLE);
  }
  else if(pBitmap == g_pShootJumpBitmap) //if shoot-jump block dying
  {
    //Make a super jumping block sprite
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pSuperJumpBlockBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(6);
    pSprite->SetNumFrames(5);
    g_pGame->AddSprite(pSprite);
    AddToDespawn(pSprite);
  }
  else if(pBitmap == g_pReddoorBitmap) //if red door dying
  {
    //Make a red door dying sprite
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pRedDoorDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pOrangedoorBitmap) //if orange door dying
  {
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pOrangeDoorDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pYellowdoorBitmap) //if yellow door dying
  {
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pYellowDoorDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pGreendoorBitmap) //if green door dying
  {
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pGreenDoorDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pBluedoorBitmap) //if blue door dying
  {
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pBlueDoorDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(1);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pCrackedLogsBitmap) //if crumbly ground dying
  {
    //Add crumble sprite
    myRECT rect = pSpriteDying->GetPosition();
    Sprite* pSprite = new Sprite(g_pCrackedLogsDieBitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(3);
    pSprite->SetNumFrames(4, true);
    g_pGame->AddSprite(pSprite);

    //Add smoke particles
    myPOINT ptParticleOffset = {17,25};
    pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
    pSprite->SetPosition(rect.left, rect.top);
    pSprite->SetZOrder(pSpriteDying->GetZOrder());
    pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
    pSprite->SetParticleTile(2,25);
    pSprite->SetParticlesInFront(true);
    pSprite->FireParticles();
    pSprite->DieOnParticleFinish();
    pSprite->SetHidden(true);
    g_pGame->AddSprite(pSprite);
  }
  else if(pBitmap == g_pRollerBitmap)
  {
      if(pSpriteDying->GetFrame() < 5)
      {
          RollerSprite* pSprite = new RollerSprite(g_pRollerBitmap, g_rcAll, BA_BOUNCE);
          if(g_iCurrentLevel == 15)  //Level 16 shrink left side of bounds
          {
            myRECT rc = g_rcAll;
            rc.left = 75;
            pSprite->SetBounds(rc);
          }
          pSprite->SetPosition(pSpriteDying->GetPosition().left, pSpriteDying->GetPosition().top);
          pSprite->SetZOrder(pSpriteDying->GetZOrder());
          pSprite->SetFrameDelay(2);
          pSprite->SetNumFrames(pSpriteDying->GetNumFrames());
          pSprite->SetFrame(5);
          pSprite->SetVelocity((g_bFacingLeft)? 10 : -10, 0);
          myRECT rect = g_pPersonSprite->GetPosition();
          myRECT rcroller = pSprite->GetPosition();
          if(g_bFacingLeft)
            pSprite->SetPosition(rect.right, rcroller.top);
          else
            pSprite->SetPosition(rect.left - pSprite->GetWidth(), rcroller.top);

          //Play starting to roll sound
//          if(!g_bNoSound)
          {
            switch(rand() % 3) //Randomly, of course
            {
              case 0:
                // Play the first roller sound
                PlayMySound(IDW_ROLLER1);
                break;
              case 1:
                // Play the second roller sound
                PlayMySound(IDW_ROLLER2);
                break;
              default:
                // Play the third roller sound
                PlayMySound(IDW_ROLLER3);
                break;
            }
          }
          g_pGame->AddSprite(pSprite);
          AddToDespawn(pSprite);
      }
      else
      {
          //Add splat particles
          Sprite* pSprite = new Sprite(g_pStarBlockCracked2Bitmap, g_rcAll, BA_BOUNCE);
          pSprite->SetPosition(pSpriteDying->GetPosition());
          pSprite->SetZOrder(pSpriteDying->GetZOrder());
          myPOINT ptParticleOffset = {pSpriteDying->GetWidth() / 2.0, pSpriteDying->GetHeight() / 2.0};
          pSprite->AddParticles(PARTICLE_ROLLERSPLAT, BITMAP_PARTICLES, 9,9, 6, 6, ptParticleOffset);
          pSprite->SetParticlesInFront(true);
          pSprite->SetHidden(true);
          pSprite->FireParticles();
          pSprite->DieOnParticleFinish();
          g_pGame->AddSprite(pSprite);
      }
  }
  else if (pBitmap == g_pBlockingPlatformBitmap)
  {
      //Colorless green ideas sleep furiously
      myRECT rect = pSpriteDying->GetPosition();
      Sprite* pSprite = new Sprite(g_pBlockingPlatformDieBitmap, g_rcAll, BA_BOUNCE);
      pSprite->SetPosition(rect.left, rect.top);
      pSprite->SetZOrder(pSpriteDying->GetZOrder());
      pSprite->SetNumFrames(4, true);
      g_pGame->AddSprite(pSprite);

      PlayMySound(IDW_PLATFORMSLIDE);
  }
}

//-----------------------------------------------------------------
// Functions to move Chef Bereft
//-----------------------------------------------------------------
void MoveChef(int idir)
{
  myPOINT ptChefVelocity = g_pPersonSprite->GetVelocity(); // Keep chef's velocity

  if(idir == 1)
  {
    if(g_bOnLeftSlant)
    {
        g_pPersonSprite->SetVelocity(max(ptChefVelocity.x - 3.0, -13.0), ptChefVelocity.y);  // Move chef left
        g_pPersonSprite->OffsetPosition(0,13);
    }
    else if(g_bOnRightSlant)
      g_pPersonSprite->SetVelocity(max(ptChefVelocity.x - 2.0, -11.0), ptChefVelocity.y);  // Move chef left
    else
      g_pPersonSprite->SetVelocity(max(ptChefVelocity.x - 3.0, -12.0), ptChefVelocity.y);  // Move chef left
    g_bIsWalkingRight = false;
  }
  else if(idir == 2)
  {
    if(g_bOnRightSlant)
    {
        g_pPersonSprite->OffsetPosition(0,13);
        g_pPersonSprite->SetVelocity(min(ptChefVelocity.x + 3.0, 13.0), ptChefVelocity.y);  // Move chef right
    }
    else if(g_bOnLeftSlant)
      g_pPersonSprite->SetVelocity(min(ptChefVelocity.x + 2.0, 11.0), ptChefVelocity.y);  // Move chef right
    else
      g_pPersonSprite->SetVelocity(min(ptChefVelocity.x + 3.0, 12.0), ptChefVelocity.y);  // Move chef right
    g_bIsWalkingRight = true;
  }
}

void ControlJump()
{
  if(g_iJumpAmount >= 0)                  //Make sure chef can jump
  {
    g_pPersonSprite->OffsetPosition(0, -g_iJumpAmount); //Make chef jump up
    g_iJumpAmount --;//= g_pGame->GetMovementMultiplier();                 //Decrement amount he can jump
  }
  else
  {
    g_bIsJumping = false;             //If amount is zero, make him not jump
    g_bIsFalling = true;              //and make him fall
    g_iFallAmount = 0;
  }
}

bool IsSpriteBadGuy(Sprite* pSprite) //Test engine calls to see if a sprite is a bad guy
{
    Bitmap* pBitmap = pSprite->GetBitmap();
    //First, check and make clouds (stars) parallax scroll
    if(pBitmap == g_pCloud3Bitmap)// && IsInView(pSprite))
    {
        float fScroll = 1.0 / g_pBackground[g_iCurrentLevel]->GetParallax();
        myRECT rcPos = pSprite->GetPosition();
        myPOINT ptScrollAmount = {rcPos.left - g_rcViewport.left, rcPos.top - g_rcViewport.top};
        ptScrollAmount.x *= fScroll;
        ptScrollAmount.y *= fScroll;
        pSprite->SetPosition(ptScrollAmount.x + g_rcViewport.left, ptScrollAmount.y + g_rcViewport.top);
    }
    //Now check for actual bad guise
  else if(pBitmap == g_pRedAmoebaBitmap ||
     pBitmap == g_pGreenAmoebaBitmap ||
     pBitmap == g_pAlienBitmap ||
     pBitmap == g_pBlueAmoebaBitmap ||
     pBitmap == g_pYellowAmoebaBitmap ||
     pBitmap == g_pBouncyBitmap ||
     pBitmap == g_pWormBitmap ||
     pBitmap == g_pFallBlockBitmap ||
     pBitmap == g_pLargeFallBlockBitmap ||
     pBitmap == g_pAlienFighterBitmap) //If it's an amoeba or alien or bubble blower or worm or falling block
  {
    return true;                              //Yep, bad guy
  }
  else if(pBitmap == g_pRollerBitmap && pSprite->GetFrame() > 4)  //If roller and active
    return true;        //Yep
  return false;                               //Nope, not bad guy
}

// Make chef or an amoeba stop when hitting some block
bool DoBlock(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myRECT  rcChefRect,
        rcPlatformRect;
  myPOINT ptChefVelocity;
  myPOINT ptAmoebaVelocity;

  rcChefRect = pSpriteHitter->GetCollision();
  rcPlatformRect = pSpriteHittee->GetCollision();
  rcChefRect.bottom -= COLLISION_EDGE_ADD;                // Add to collision rectangle
  rcPlatformRect.top += COLLISION_EDGE_ADD;               //(don't take these increments out!)
  if(pSpriteHittee->GetBitmap() == g_pLargeFallBlockBitmap || pSpriteHittee->GetBitmap() == g_pFallBlockBitmap || pSpriteHitter->GetBitmap() == g_pLargeFallBlockBitmap || pSpriteHitter->GetBitmap() == g_pFallBlockBitmap)
  {
    rcChefRect.bottom -= 5;                // Add to collision rectangles more
    rcPlatformRect.top += 5;
    rcChefRect.top += 10;
    rcPlatformRect.bottom -= 10;
  }
  rcChefRect.top += COLLISION_EDGE_ADD;                  // Add to collision rectangles
  rcPlatformRect.bottom -= COLLISION_EDGE_ADD;
  rcChefRect.left += COLLISION_EDGE_ADD-4;                // Add to collision rectangles
  rcPlatformRect.right -= COLLISION_EDGE_ADD-4;                 //These two are smaller because of corner clipping cases
  rcChefRect.right -= COLLISION_EDGE_ADD;               //Add to the collision rectangles
  rcPlatformRect.left += COLLISION_EDGE_ADD;
  //if(!bIsChef)
  //{
      //hack some more. Add more to enemies so they don't clip through walls
      //rcChefRect.left += 12;
      //rcChefRect.right -= 12;
      //rcPlatformRect.left += 12;
      //rcPlatformRect.right -= 12;
      //rcChefRect.top += 12;
      //rcChefRect.bottom -= 12;
      //rcPlatformRect.top += 12;
      //rcPlatformRect.bottom -= 12;
  //}

  //if(!bIsChef)
  //{
  //    rcChefRect.left += 4;
  //    rcPlatformRect.right -= 4;    //Add BACK to enemies... yes, this sucks
  //}

  if((rcPlatformRect.top - rcChefRect.bottom) >= 1 && ((!g_bBeside) || (!bIsChef))) //If he is right above the block
  {
    if(bIsChef)                         // If it's chef and not an aboeba
    {
      if(g_bIsFalling || g_bIsJumping) //Test if jumping too, to avid jump-through-corner issues
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 73;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
    else
    {
      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.y > 0)
        ptAmoebaVelocity.y = 0;
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);

      if(pSpriteHitter->GetBitmap() == g_pBouncyBitmap) //Make bouncy guys jump
      {
          pSpriteHitter->Jump();
          if(ptAmoebaVelocity.x > 0)
            pSpriteHitter->SetFrame(6);  //And make it look like they're jumping
          else
            pSpriteHitter->SetFrame(2);
      }

      myRECT rcPos = pSpriteHitter->GetPosition();
      myRECT rcBlock = pSpriteHittee->GetPosition();
      rcPos.top = rcBlock.top - pSpriteHitter->GetHeight();
      int i = (int)rcBlock.top % 75;
      if(i > 0)  //if too far down
      {
        rcPos.top -= i;           //Set to position above nearest block position
      }
      pSpriteHitter->SetPosition(rcPos.left, rcPos.top);

      return false;
    }
    return false;//true;                        // Keep old position
  }

  if((rcChefRect.left - rcPlatformRect.right) >= 1)//If he is right of the block
  {
    if(bIsChef)
    {
      ptChefVelocity = pSpriteHitter->GetVelocity();//Get old velocity
      //Without this test, he'd be stuck to the block
      if(ptChefVelocity.x < 1)     //If he is trying to go left, stop him
        ptChefVelocity.x = 0;
      //if(ptChefVelocity.x > 5)
      //  ptChefVelocity.x -= 2;
      pSpriteHitter->SetVelocity(ptChefVelocity); //Set his new velocity
      g_bBeside = true;
      //Set chef's position to right beside the block
      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      rcHitter.left = rcHit.right - 9;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      return true;
    }
    else
    {
      myRECT rcEnemy = pSpriteHitter->GetPosition();
      myRECT rcBlock = pSpriteHittee->GetPosition();
      rcEnemy.left = rcBlock.right;
      pSpriteHitter->SetPosition(rcEnemy.left, rcEnemy.top);

      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.x < 0) //If he's headed toward the block
        ptAmoebaVelocity.x = -ptAmoebaVelocity.x;  //Reverse his direction
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);
      return true;
    }
  }

  if((rcPlatformRect.left - rcChefRect.right) >= 1)//If he is left of the block
  {
    if(bIsChef) //If it's chef
    {
      ptChefVelocity = pSpriteHitter->GetVelocity();//Get his old velocity
      //Without this test, he'd be stuck to the block
      if(ptChefVelocity.x > 1)    //If he is trying to go right, stop him
        ptChefVelocity.x = 0;
      pSpriteHitter->SetVelocity(ptChefVelocity); //Set his new velocity
      //if(g_bOnPlatform)
      //  pSpriteHitter->OffsetPosition(-8, 0);
      g_bBeside = true;
      //Set chef's position to right beside the block
      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      rcHitter.left = rcHit.left - 33;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      return true;
    }
    else //It's an amoeba
    {
      myRECT rcEnemy = pSpriteHitter->GetPosition();
      myRECT rcBlock = pSpriteHittee->GetPosition();
      rcEnemy.left = rcBlock.left - pSpriteHitter->GetWidth();
      pSpriteHitter->SetPosition(rcEnemy.left, rcEnemy.top);

      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.x > 0) //If he's headed toward the block
        ptAmoebaVelocity.x = -ptAmoebaVelocity.x;  //Reverse his direction
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);
      return true;
    }
  }

  if((rcPlatformRect.top - rcChefRect.bottom) >= 1) //If he is right above the block (test again)
  {
    if(bIsChef)                         // If it's chef and not an aboeba
    {
      if(g_bIsFalling)
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;

        //Set chef's position to right above the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.top - 73;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
    else
    {
      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.y > 0)
        ptAmoebaVelocity.y = 0;
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);

      if(pSpriteHitter->GetBitmap() == g_pBouncyBitmap) //Make bouncy guys jump
      {
          pSpriteHitter->Jump();
          if(ptAmoebaVelocity.x > 0)
            pSpriteHitter->SetFrame(6);  //And make it look like they're jumping
          else
            pSpriteHitter->SetFrame(2);
      }

      return true;
    }
    return true;                        // Keep his old position
  }

  if((rcChefRect.top - rcPlatformRect.bottom) >= 1) //If he is right below the block (Check last for jumping reasons)
  {
    if(bIsChef)                        // If it's chef and not an aboeba
    {
      if(g_bIsJumping)
      {
        g_bIsJumping = false;              // Make him stop jumping
        g_bIsFalling = true;               // Make him fall
        g_iFallAmount = 2;
        //Set chef's position to right below the block
        myRECT rcHitter = pSpriteHitter->GetPosition();
        myRECT rcHit    = pSpriteHittee->GetPosition();
        rcHitter.top = rcHit.bottom + 2;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
      else
      {
        pSpriteHitter->OffsetPosition(0, 8);
      }
    }
    else
    {
      pSpriteHitter->StopJump();       //Stop its jumping

      //Set its position to right below the block
      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      rcHitter.top = rcHit.bottom + 2;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);

      return true;
    }
    return true;                       // Keep his old position
  }
  return false;
}  //Whew! a lot of work for just one block!

void Left(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myRECT  rcHitter,
        rcHit;
  myPOINT ptAmoebaVelocity;

  rcHitter = pSpriteHitter->GetPosition();
  rcHit = pSpriteHittee->GetPosition();
  if(rcHitter.left < rcHit.left)//If he is going too far left
  {
    if(bIsChef && (rcHitter.bottom - rcHit.top > 14)) //If it's chef
    {
      //Set chef's position to right inside the block
      rcHitter.left = rcHit.left;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
    else if(!bIsChef)
    {
      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.x < 0)
        ptAmoebaVelocity.x = -ptAmoebaVelocity.x;
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);
    }
  }
}

void Right(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myRECT  rcHitter,
        rcHit;
  myPOINT ptAmoebaVelocity;

  rcHitter = pSpriteHitter->GetPosition();
  rcHit = pSpriteHittee->GetPosition();
  if(rcHitter.right > rcHit.right)//If he is going too far right
  {
    if(bIsChef && (rcHitter.bottom - rcHit.top > 14)) //If it's chef
    {
      //Set chef's position to right inside the block
      rcHitter.left = rcHit.right - pSpriteHitter->GetWidth();
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
    else if(!bIsChef)
    {
      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.x > 0)
        ptAmoebaVelocity.x = -ptAmoebaVelocity.x;
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);
    }
  }
}

void Top(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myRECT  rcHitter,
        rcHit;
//  myPOINT ptAmoebaVelocity;

  rcHitter = pSpriteHitter->GetPosition();
  rcHit = pSpriteHittee->GetPosition();
  if(rcHitter.top < rcHit.top)//If he is going too far up
  {
    if(bIsChef) //If it's chef
    {
      g_bIsJumping = false;
      g_bIsFalling = true;
      g_iFallAmount = 5;
      //Set chef's position to right inside the block
      rcHitter.top = rcHit.top + 2;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
  }//Don't test for the non-jumping amoebas
}

void Bottom(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myRECT  rcHitter,
        rcHit;
  myPOINT ptAmoebaVelocity;

  rcHitter = pSpriteHitter->GetPosition();
  rcHit = pSpriteHittee->GetPosition();
  if(rcHitter.bottom >= rcHit.bottom)//If he is going too far down
  {
    if(bIsChef) //If it's chef
    {
      g_bIsFalling = false;
      g_iJetbootsTime = MAX_JETBOOT_TIME;
      //Set chef's position to right inside the block
      rcHitter.top = (rcHit.bottom - pSpriteHitter->GetHeight()) + 2;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
    else if(!bIsChef) //It's an amoeba
    {
      ptAmoebaVelocity = pSpriteHitter->GetVelocity();
      if(ptAmoebaVelocity.y > 0) //If it's headed down
        ptAmoebaVelocity.y = 0;  //Stop it
      rcHitter.top = rcHit.bottom - pSpriteHitter->GetHeight();
      pSpriteHitter->SetVelocity(ptAmoebaVelocity);
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
  }
}

// Make chef or an amoeba go up when hitting some slant
bool DoLeftSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myPOINT ptAmoebaVelocity;

  if(bIsChef)                         // If it's chef and not an aboeba
  {
    if(g_bIsFalling || g_iJumpAmount < 5)
    {

      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      if(rcHitter.top - rcHit.top > 20)    //If below slant
      {
        return false;
      }
      if((rcHit.bottom - rcHitter.bottom - 28 <= (rcHitter.left - rcHit.left)) || (g_iFallAmount < 7 && g_bFacingLeft))
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bOnLeftSlant = true;

        //Set chef's position to right above the block, but on a slant
        rcHitter.top = rcHit.top;
        rcHitter.top -= (rcHitter.left - rcHit.left) + 28;
        if(rcHitter.top <= (rcHit.top - pSpriteHitter->GetHeight() + 2))
          rcHitter.top = rcHit.top - pSpriteHitter->GetHeight() + 2;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
  }
  else  //Amoeba or other villian
  {
    myRECT rcPos = pSpriteHitter->GetPosition();
    myRECT rcBlock = pSpriteHittee->GetPosition();

    ptAmoebaVelocity = pSpriteHitter->GetVelocity();
    if(ptAmoebaVelocity.y > 0)
      ptAmoebaVelocity.y = 0;
    if(ptAmoebaVelocity.x < 0 && rcPos.bottom - rcBlock.top > abs(ptAmoebaVelocity.x))
      pSpriteHitter->OffsetPosition(0,abs(ptAmoebaVelocity.x));
    pSpriteHitter->SetVelocity(ptAmoebaVelocity);

    rcPos.top = rcBlock.top;
    rcPos.top -= (rcPos.left - rcBlock.left) + 28;
    if(pSpriteHitter->GetBitmap() == g_pBouncyBitmap && (rcPos.top > (rcBlock.top - pSpriteHitter->GetHeight() - 2))) //Make bouncy guys jump
    {
      pSpriteHitter->Jump();
      if(ptAmoebaVelocity.x > 0)
        pSpriteHitter->SetFrame(6);  //And make it look like they're jumping
      else
        pSpriteHitter->SetFrame(2);
      return false;
    }
    if(rcPos.top < (rcBlock.top - pSpriteHitter->GetHeight() - 2))
    {
      rcPos.top = rcBlock.top - pSpriteHitter->GetHeight() + 2;
    }
    pSpriteHitter->SetPosition(rcPos.left, rcPos.top);

    return false;
  }
  return true;                        // Keep old position
}  //Done the Slant!

/////////////////////////////////////////////////////////////////////////////////////////
// left cieling slant
/////////////////////////////////////////////////////////////////////////////////////////
// Make chef or an amoeba fall when hitting some slant
bool DoLeftCielingSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
//  myPOINT ptAmoebaVelocity;

  if(bIsChef)                         // If it's chef and not an aboeba
  {
    //if(g_bIsJumping)
    {

      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      if(rcHitter.top - rcHit.top <= (rcHit.right - rcHitter.left - 24) && (rcHitter.top >= rcHit.top - 10))
      {
        g_bIsJumping = false;             // Make him stop jumping
        g_bIsFalling = true;
        g_iFallAmount = 5;

        //Set chef's position to right below the block, but on a slant
        rcHitter.top = rcHit.top;
        rcHitter.top += rcHit.right - rcHitter.left;
        rcHitter.top -= 24;
        if(rcHitter.top > rcHit.bottom)
          rcHitter.top = rcHit.bottom;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
  }
  else  //Amoeba or other villian
  {
    myRECT rcHitter = pSpriteHitter->GetPosition();
    myRECT rcHit    = pSpriteHittee->GetPosition();
    if(rcHitter.top - rcHit.top <= (rcHit.right - rcHitter.left - 8) && rcHitter.top >= rcHit.top)
    {
      pSpriteHitter->StopJump();      //Stop them jumping
      rcHitter.top = rcHit.top;
      rcHitter.top += rcHit.right - rcHitter.left - 8;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
    return false;
  }
  return true;                        // Keep old position
}  //Done the Slant!

// Make chef or an amoeba go up when hitting some slant
bool DoRightSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{
  myPOINT ptAmoebaVelocity;

  if(bIsChef)                         // If it's chef and not an aboeba
  {
    if(g_bIsFalling || g_iJumpAmount < 5)
    {

      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      if(rcHitter.top - rcHit.top > 20)    //If below slant
      {
        return false;
      }
      if((rcHit.bottom - rcHitter.bottom - 28 <= (rcHit.right - rcHitter.right)) || (g_iFallAmount < 7 && (!g_bFacingLeft)))
      {
        g_bIsFalling = false;             // Make him stop falling
        g_iJetbootsTime = MAX_JETBOOT_TIME;
        g_bOnRightSlant = true;

        //Set chef's position to right above the block, but on a slant
        rcHitter.top = rcHit.top;
        rcHitter.top -= (rcHit.right - rcHitter.right) + 28;
        if(rcHitter.top <= (rcHit.top - pSpriteHitter->GetHeight() + 2))
          rcHitter.top = rcHit.top - pSpriteHitter->GetHeight() + 2;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
  }
  else    //Amoeba or other villian
  {
    myRECT rcPos = pSpriteHitter->GetPosition();
    myRECT rcBlock = pSpriteHittee->GetPosition();

    ptAmoebaVelocity = pSpriteHitter->GetVelocity();
    if(ptAmoebaVelocity.y > 0)
      ptAmoebaVelocity.y = 0;
    if(ptAmoebaVelocity.x > 0 && rcPos.bottom - rcBlock.top > ptAmoebaVelocity.x)
      pSpriteHitter->OffsetPosition(0,ptAmoebaVelocity.x);
    pSpriteHitter->SetVelocity(ptAmoebaVelocity);

    rcPos.top = rcBlock.top;
    rcPos.top -= (rcBlock.right - rcPos.right) + 28;
    if(pSpriteHitter->GetBitmap() == g_pBouncyBitmap && (rcPos.top > (rcBlock.top - pSpriteHitter->GetHeight() - 2))) //Make bouncy guys jump
    {
      pSpriteHitter->Jump();
      if(ptAmoebaVelocity.x > 0)
        pSpriteHitter->SetFrame(6);  //And make it look like they're jumping
      else
        pSpriteHitter->SetFrame(2);
      return false;
    }
    if(rcPos.top < (rcBlock.top - pSpriteHitter->GetHeight() - 2))
    {
      rcPos.top = rcBlock.top - pSpriteHitter->GetHeight() + 2;
    }
    pSpriteHitter->SetPosition(rcPos.left, rcPos.top);

    return false;
  }
  return true;                        // Keep old position
}  //Done the Slant!

///////////////////////////////////////////////////////////////////////////////
//      right cieling slant
///////////////////////////////////////////////////////////////////////////////

// Make chef or an amoeba fall when hitting some slant
bool DoRightCielingSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef)
{

  if(bIsChef)                         // If it's chef and not an aboeba
  {
    //if(g_bIsJumping)
    {

      myRECT rcHitter = pSpriteHitter->GetPosition();
      myRECT rcHit    = pSpriteHittee->GetPosition();
      if((rcHitter.top - rcHit.top <= rcHitter.right - rcHit.left - 24) && (rcHitter.top >= rcHit.top - 10))
      {
        g_bIsJumping = false;             // Make him stop jumping
        g_bIsFalling = true;
        g_iFallAmount = 5;

        //Set chef's position to right below the block, but on a slant
        rcHitter.top = rcHit.top;
        rcHitter.top += rcHitter.right - rcHit.left;
        rcHitter.top -= 24;
        if(rcHitter.top > rcHit.bottom)
          rcHitter.top = rcHit.bottom;
        pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
      }
    }
  }
  else  //Amoeba or other villian
  {
    myRECT rcHitter = pSpriteHitter->GetPosition();
    myRECT rcHit    = pSpriteHittee->GetPosition();
    if(rcHitter.top - rcHit.top <= rcHitter.right - rcHit.left - 8 && rcHitter.top >= rcHit.top)
    {
      pSpriteHitter->StopJump();      //Stop them jumping
      rcHitter.top = rcHit.top;
      rcHitter.top += rcHit.right - rcHitter.left - 8;
      pSpriteHitter->SetPosition(rcHitter.left, rcHitter.top);
    }
    return false;
  }
  return true;                        // Keep old position
}  //Done the Slant!

void CenterOnGridSquare(Sprite* pSprite)
{
    myRECT rcPos = pSprite->GetPosition();
    rcPos.left += (75 - pSprite->GetWidth()) / 2.0;
    rcPos.top += (75 - pSprite->GetHeight()) / 2.0;
    pSprite->SetPosition(rcPos.left, rcPos.top);
}

bool ReadMapFile()
{
  g_pLastCheckpoint = NULL; //To avoid accidental segfaults
  int i;
  int j;
  int iWidth;
  int iHeight;
  myRECT rcBounds = g_rcAll;   //Where the sprites are allowed to be
  char cData[4];
  VFILE* hFile;
  char  szName[256];
  Sprite* pSprite; //General sprite used to create other sprites
  g_pBlockingPlatform = NULL;
  g_pExitSprite = NULL;
  g_bExitOpen = false;
  //See if this level has a hub
  bool bHubLevel = false;
  for(int i = 0; g_iHubLevels[i] != -1; i++)
  {
      if(g_iCurrentLevel == g_iHubLevels[i])
      {
          bHubLevel = true;
          break;
      }
  }

  switch(g_iCurrentLevel)
  {
      case 0:
        sprintf(szName, "Climbing Cliff City.CB3");
        break;
      case 1:
        sprintf(szName, "Mighty Moon Metropolis.CB3");
        break;
      case 2:
        sprintf(szName, "Devious Deranged Decapolis.CB3");
        break;
      case 3:
        sprintf(szName, "Flagrant Floating Flagellates.CB3");
        break;
      case 4:
        sprintf(szName, "Clever Collapsing Caves.CB3");
        break;
      case 5:
        sprintf(szName, "Wacko Weapons Warehouse.CB3");
        break;
      case 6:
        sprintf(szName, "Vortical Volvox Vertigo.CB3");
        break;
      case 7:
        sprintf(szName, "Hierarchy Of Heating Houses.CB3");
        break;
      case 8:
        sprintf(szName, "Andesite Amoeba Avengers.CB3");
        break;
      case 9:
        sprintf(szName, "Lurid Laser Lookout.CB3");
        break;
      case 10:
        sprintf(szName, "Caves Of Stupidity.CB3");
        break;
      case 11:
        sprintf(szName, "Sinister Slopes of Sandstone.CB3");
        break;
      case 12:
        sprintf(szName, "Lunar Lava Landscape.CB3");
        break;
      case 13:
        sprintf(szName, "Horrible Household of Helplessness.CB3");
        break;
      case 14:
        sprintf(szName, "Slimy Sewer Systems.CB3");
        break;
      case 15:
        sprintf(szName, "Frightening Finale.CB3");
        break;
      case 16:
        sprintf(szName, "HL1.CB3");
        break;
      case 17:
        sprintf(szName, "HL2.CB3");
        break;
      case 18:
        sprintf(szName, "Lev9Cont.CB3");
        break;
      case 19:
        sprintf(szName, "AllYourBase.CB3");
        break;
      default:
        return false;   //Something got messed up

  }
  ofile << "Opening map " << szName << endl;
  hFile = vfopen(szName, "rb");
  if (hFile == NULL)    //Something went wrong
  {
    vfclose(hFile);
    return false;                       //Bail out
  }

  if(vfread(&cData, 1, 3, hFile) != 3)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }
  cData[3] = '\0';

  // Extract the width from the data
  iWidth = atoi(cData);

  //Skip over the "\n\r" in the file
  if(vfread(&cData, 1, 2, hFile) != 2)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }

  if(vfread(&cData, 1, 3, hFile) != 3)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }
  cData[3] = '\0';

  // Extract the height from the data
  iHeight = atoi(cData);

  //Skip over the "\n\r" in the file
  if(vfread(&cData, 1, 2, hFile) != 2)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }

  g_rcAll.top = 0;
  g_rcAll.left = 0;
  g_rcAll.right = iWidth * 75;
  g_rcAll.bottom = iHeight * 75;
  rcBounds = g_rcAll;

  for(i = 0; i < iHeight; i++)
  {
    for(j = 0; j < iWidth; j++)
    {
      if(vfread(&cData, 1, 1, hFile) != 1)
      {
        // Something went wrong, so close the file handle
        vfclose(hFile);
        return false;
      }
      switch (cData[0])
      {
        case ' ':  //Blank slot
          if(i == iHeight - 1)
          {   //If on last row make hole-looking sprite
            pSprite = new Sprite(g_pBottomBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;   //Count it but do nothing else
        case '_':    //hole warning sign
          pSprite = new Sprite(g_pSignBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '2':        //Green door
          // Create green door sprite
          pSprite = new Sprite(g_pGreendoorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'M':         //Platform
          //Create platform sprite
          pSprite = new Sprite(g_pPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'u':         //Upside down platform
          //Create platform sprite
          pSprite = new Sprite(g_pUpsideDownPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '[':         //Left facing platform
          //Create platform sprite
          pSprite = new Sprite(g_pLeftPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case ']':         //Right facing platform
          //Create platform sprite
          pSprite = new Sprite(g_pRightPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '1':         //Green key
          // Create green key sprite
          pSprite = new Sprite(g_pGreenkeyBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition((j * 75) + 22, (i * 75) + 36);  //Set green key over faurther (right amount) because of secret level 2
          g_pGame->AddSprite(pSprite);
          break;
        case '3':         //blue key
          // Create blue key sprite
          pSprite = new Sprite(g_pBluekeyBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case '4':        //blue door
          // Create blue door sprite
          pSprite = new Sprite(g_pBluedoorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          if(g_iCurrentLevel == 15)
            pSprite->SetZOrder(1);    //HACK ALERT
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '5':         //Red key
          // Create red key sprite
          pSprite = new Sprite(g_pRedkeyBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case '6':        //Red door
          // Create red door sprite
          pSprite = new Sprite(g_pReddoorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '7':         //Orange key
          // Create orange key sprite
          pSprite = new Sprite(g_pOrangekeyBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case '8':        //Orange door
          // Create orange door sprite
          pSprite = new Sprite(g_pOrangedoorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '9':         //Yellow key
          // Create yellow key sprite
          pSprite = new Sprite(g_pYellowkeyBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition((j * 75) + 16, (i * 75) + 36);
          g_pGame->AddSprite(pSprite);
          break;
        case '0':        //Yellow door
          // Create yellow door sprite
          pSprite = new Sprite(g_pYellowdoorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'P':    //Person (Hopefully one, and only one, per map)
        {
          //Create jet boot blast sprite, for blast particles
          myPOINT ptParticleOffset = {32,70};
          // Create the person sprite
          g_pPersonSprite = new PersonSprite(g_pPersonBitmap, rcBounds, BA_STOP);
          if(g_bJetBoots)
            g_pPersonSprite->SetBitmap(g_pPersonJetBoots);
          if(g_bSuperJetBoots)
            g_pPersonSprite->SetBitmap(g_pPersonSuperJetBoots);
          g_pPersonSprite->SetNumFrames(20);
          g_pPersonSprite->SetZOrder(4);
          g_pPersonSprite->SetPosition(j * 75, i * 75);
          g_pPersonSprite->AddParticles(PARTICLE_JETBOOTBLAST, BITMAP_PARTICLES, 25,25,25,25,ptParticleOffset);
          g_pPersonSprite->SetParticleTile(1,-23);
          g_pGame->AddSprite(g_pPersonSprite);
          g_rcLastCheckpointPos = g_pPersonSprite->GetPosition();
          g_pLastCheckpoint = NULL;
          break;
        }
        case 'A':   //Amoeba
          // Create amoeba sprite
          BadGuySprite* pBASprite;
          pBASprite = new BadGuySprite(g_pRedAmoebaBitmap, rcBounds, BA_BOUNCE);
          if(g_iCurrentLevel == 3)  //Flagrant floating flagellates
            pBASprite->SetBoundsAction(BA_DIE);
          pBASprite->SetZOrder(2);
          pBASprite->SetPosition(j * 75, i * 75);
          pBASprite->SetNumFrames(8);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pBASprite->SetVelocity(-6, 0);
          else
            pBASprite->SetVelocity(6, 0);
          pBASprite->SetLives(3);
          g_pGame->AddSprite(pBASprite);
          if(g_iCurrentLevel == 14)   //If in tunnel level, make tunnel section
          {
            pSprite = new Sprite(g_pTunnelHzBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '-':   //Green Amoeba
          // Create amoeba sprite
          pBASprite = new BadGuySprite(g_pGreenAmoebaBitmap, rcBounds, BA_BOUNCE);
          if(g_iCurrentLevel == 3)  //Flagrant floating flagellates
            pBASprite->SetBoundsAction(BA_DIE);
          pBASprite->SetZOrder(2);
          pBASprite->SetPosition(j * 75, i * 75);
          pBASprite->SetNumFrames(8);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pBASprite->SetVelocity(-6, 0);
          else
            pBASprite->SetVelocity(6, 0);
          g_pGame->AddSprite(pBASprite);
          break;
        case 'a':   //Alien
          // Create alien sprite
          AlienSprite* pASprite;
          pASprite = new AlienSprite(g_pAlienBitmap, rcBounds, BA_BOUNCE);
          pASprite->SetZOrder(2);
          pASprite->SetPosition(j * 75, i * 75);
          pASprite->SetNumFrames(8);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pASprite->SetVelocity(-8, 0); //Notice that he is as fast as the player.
          else
            pASprite->SetVelocity(8, 0);
          pASprite->SetLives(2);
          g_pGame->AddSprite(pASprite);
          break;
        case '}':   //Alien Fighter
          // Create alien fighter sprite
          AlienFighterSprite* pAFSprite;
          pAFSprite = new AlienFighterSprite(g_pAlienFighterBitmap, rcBounds, BA_BOUNCE);
          pAFSprite->SetZOrder(2);
          pAFSprite->SetPosition(j * 75, i * 75);
          pAFSprite->SetNumFrames(22);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pAFSprite->SetVelocity(-8, 0); //Notice that he is as fast as the player
          else
            pAFSprite->SetVelocity(8, 0);
          pAFSprite->SetLives(2);
          g_pGame->AddSprite(pAFSprite);
          break;
        case 'b':   //Blue amoeba
          // Create blue amoeba sprite
          BubbleBlowerSprite* pBBSprite;
          pBBSprite = new BubbleBlowerSprite(g_pBlueAmoebaBitmap, rcBounds, BA_BOUNCE);
          pBBSprite->SetZOrder(2);
          pBBSprite->SetLives(2);  //Two lives
          pBBSprite->SetPosition(j * 75, i * 75);
          pBBSprite->SetNumFrames(10);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pBBSprite->SetVelocity(-5, 0); //Notice that he is almost twice as slow as the player.
          else
            pBBSprite->SetVelocity(5, 0);
          g_pGame->AddSprite(pBBSprite);
          break;
        case 'y':   //yellow amoeba
          // Create blob blower Yellow Amoeba sprite
          pBBSprite = new BubbleBlowerSprite(g_pYellowAmoebaBitmap, rcBounds, BA_BOUNCE);
          pBBSprite->SetZOrder(2);
          pBBSprite->SetPosition(j * 75, i * 75);
          pBBSprite->SetNumFrames(10);
          pBBSprite->SetLives(20); //Whew! Hard guy to kill!
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pBBSprite->SetVelocity(-6, 0);
          else
            pBBSprite->SetVelocity(6, 0);
          g_pGame->AddSprite(pBBSprite);
          break;
        case 'W':   //Worm
          // Create Worm sprite
          BadGuySprite* pBGSprite;
          pBGSprite = new BadGuySprite(g_pWormBitmap, rcBounds, BA_BOUNCE);
          pBGSprite->SetZOrder(2);
          pBGSprite->SetPosition(j * 75, i * 75);
          pBGSprite->SetNumFrames(12);
          if(rand() % 2 == 1)   //Set his starting direction randomly
            pBGSprite->SetVelocity(-6, 0);
          else
            pBGSprite->SetVelocity(6, 0);
          g_pGame->AddSprite(pBGSprite);
          break;
        case '|':   //Bouncy guy (or horizontal tunnel in level 15)
          if(g_iCurrentLevel == 14)
          {
            pSprite = new Sprite(g_pTunnelVBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            // Create bouncy guy sprite
            pBGSprite = new BadGuySprite(g_pBouncyBitmap, rcBounds, BA_BOUNCE);
            pBGSprite->SetZOrder(2);
            pBGSprite->SetPosition(j * 75, i * 75);
            pBGSprite->SetNumFrames(8);
            if(rand() % 2 == 1)   //Set his starting direction randomly and at a random speed
              pBGSprite->SetVelocity(-5 - (rand() % 5), 0);
            else
              pBGSprite->SetVelocity(5 + (rand() % 5), 0);
            g_pGame->AddSprite(pBGSprite);
          }
          break;
        case '+':   //Roller
          // Create roller sprite
          RollerSprite* pRSprite;
          pRSprite = new RollerSprite(g_pRollerBitmap, rcBounds, BA_BOUNCE);
          if(g_iCurrentLevel == 3)  //Flagrant floating flagellates
            pRSprite->SetBoundsAction(BA_DIE);
          pRSprite->SetZOrder(2);
          pRSprite->SetFrameDelay(2);  //Make animation slower
          pRSprite->SetNumFrames(13);
          pRSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pRSprite);
          g_pGame->AddSprite(pRSprite);
          break;
        case 'v':   //Vortex or lava vat lower part
          if(g_iCurrentLevel == 12)
          {
            pSprite = new Sprite(g_pLava2Bitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            // Create vortex sprite
            VortexSprite* pVSprite;
            pVSprite = new VortexSprite(g_pVortexBitmap, rcBounds, BA_BOUNCE);
            pVSprite->SetZOrder(2);
            pVSprite->SetPosition((j * 75) + 38, i * 75);
            pVSprite->SetNumFrames(8);
            pVSprite->SetVelocity(16, 0);
            g_pGame->AddSprite(pVSprite);
          }
          break;
        case '\"':   //bat
          // Create bat sprite
          pSprite = new Sprite(g_pBatBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(2);
          pSprite->SetNumFrames(4);
          pSprite->SetFrameDelay(2);
          pSprite->SetVelocity(rand() % 6 + 2, rand() % 6 + 2);   //Set starting x and y velocities random
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'C':   //Chrome block
          // Create chrome block sprite
          pSprite = new Sprite(g_pChromeblockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'R': // Grey block
          // Create grey block with hole sprite
          pSprite = new Sprite(g_pGreyholeBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'c': // Cool-looking block
          // Create cool block sprite
          pSprite = new Sprite(g_pCoolblockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'I':   //I SHALL KILL YOU! (giant enemy crab)
          g_pEnemyCrab = new TrapSprite(g_pCrabBitmap[0], rcBounds, BA_WRAP);
          g_pEnemyCrab->SetZOrder(-1);
          g_pEnemyCrab->SetPosition(j * 75, i * 75 + 42);  //42! WOO WOO (actually, this is the correct value anyway)
          g_iBossFloorYLevel = i * 75 + 42;
          g_pGame->AddSprite(g_pEnemyCrab);
          break;
        case 'n':  //Noodle
          pSprite = new Sprite(g_pNoodleBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          //Offset slightly so centered
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'N':  //Blocking platform
          g_pBlockingPlatform = new Sprite(g_pBlockingPlatformBitmap, rcBounds, BA_BOUNCE);
          g_pBlockingPlatform->SetZOrder(3);
          g_pBlockingPlatform->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(g_pBlockingPlatform);
          break;
        case 'V':  //poison Vat
          if(g_iCurrentLevel == 12) //Lava vat
          {
            pSprite = new Sprite(g_pLava1Bitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(4);
            pSprite->SetPosition(j * 75, (i * 75) + 15); //Offset slightly on y axis
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 14)  //Sewer vat (Eww...)
          {
            pSprite = new Sprite(g_pSewerVatBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(4);
            pSprite->SetFrameDelay(2);
            if(i < 40) //If in certain part of level, make nearer to top
              pSprite->SetPosition(j * 75, (i * 75) - 10); //Offset slightly back on y axis
            else
              pSprite->SetPosition(j * 75, (i * 75) + 15); //Offset slightly on y axis
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pPoisonVatBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(4);
            pSprite->SetPosition(j * 75, (i * 75) + 15); //Offset slightly on y axis
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 't':  //stalagTite (or, in this case, lava dripper or Truck)
          if(g_iCurrentLevel == 12)
          {
            PoisonSprite* pPSprite;
            pPSprite = new PoisonSprite(g_pStalagtiteBitmap, rcBounds, BA_BOUNCE);
            pPSprite->SetZOrder(6);
            pPSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pPSprite);
          }
          else
          {
            TruckSprite* pTSprite;
            pTSprite = new TruckSprite(g_pTruckBitmap, rcBounds, BA_DIE);
            pTSprite->SetZOrder(6);
            pTSprite->SetNumFrames(4);
            pTSprite->SetFrameDelay(4);
            pTSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pTSprite);
          }
          break;
        case 'B':  // Grass-like thing or heating coil
          if(g_iCurrentLevel > 6)
          {
            pSprite = new Sprite(g_pCoilBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(25);
            pSprite->SetPosition(j * 75, (i * 75) + 34); //Offset on y axis
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pGrassyThingBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(5);
            pSprite->SetPosition(j * 75, (i * 75) + 64); //Offset on y axis
            myRECT rcGrassy = pSprite->GetCollision();
            rcGrassy.left += 10;
            rcGrassy.right -= 10;
            pSprite->SetCollision(rcGrassy);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '{':  //Ceiling slime (or heating coil)
          if(g_iCurrentLevel > 6)
          {
            pSprite = new Sprite(g_pCoilBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(25);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pSlimeBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(5);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 'O':  //Orange
          pSprite = new Sprite(g_pOrangeBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          //Offset position slightly so centered
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'k':  //CupcaKe
          pSprite = new Sprite(g_pCupcakeBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          //Offset position slightly so centered
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'j':  //grape Juice
          pSprite = new Sprite(g_pGrapeJuiceBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          //Offset position slightly so centered
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'z':  //DoZen Eggs
          pSprite = new Sprite(g_pEggsBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'f':  //fork (or, in this case, checkpoint! Yeah, random)
        {
          TrapSprite* pTrapSprite = new TrapSprite(g_pCheckpointBitmap, rcBounds, BA_BOUNCE);
          pTrapSprite->SetZOrder(1);
          pTrapSprite->SetNumFrames(2);
          pTrapSprite->SetPosition(j * 75 + 29, i * 75);
          g_pGame->AddSprite(pTrapSprite);
          break;
        }
        case 'J':   //jump block
          if(g_iCurrentLevel == 15) //Level 16 make super jump block
          {
            pSprite = new Sprite(g_pSuperJumpBlockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetPosition(j * 75, i * 75);
            pSprite->SetNumFrames(5);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pJumpBlockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetPosition(j * 75, i * 75);
            pSprite->SetNumFrames(5);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '!':   //Yellow button
          if(g_iCurrentLevel > 6)
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pYellowHubBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(5);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75 + 8);
            g_pGame->AddSprite(pTrSprite);
          }
          else
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pYellowButtonBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(4);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pTrSprite);
          }
          break;
        case '#':   //Orange button
          if(g_iCurrentLevel > 6)
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pOrangeHubBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(5);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75 + 8);
            g_pGame->AddSprite(pTrSprite);
          }
          else
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pOrangeButtonBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(4);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pTrSprite);
          }
          break;
        case '%':   //Blue button
          if(g_iCurrentLevel > 6)
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pBlueHubBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(5);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75 + 8);
            g_pGame->AddSprite(pTrSprite);
          }
          else
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pBlueButtonBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(4);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pTrSprite);
          }
          break;
        case '&':   //Green switch
          if(g_iCurrentLevel > 6)
          {
            TrapSprite*  pTrSprite = new TrapSprite(g_pGreenHubBitmap, rcBounds, BA_BOUNCE);
            pTrSprite->SetZOrder(1);
            pTrSprite->SetNumFrames(5);
            pTrSprite->SetFrameDelay(2);
            pTrSprite->SetPosition(j * 75, i * 75 + 8);
            g_pGame->AddSprite(pTrSprite);
          }
          break;
        case 'F':  //Floating Platform
          pSprite = new Sprite(g_pFloatingPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          if(rand() % 2 == 0)
            pSprite->SetVelocity(10, 0);
          else
            pSprite->SetVelocity(-10, 0);
          g_pGame->AddSprite(pSprite);
          break;
        case 'h':  //Hidden Floating Platform
          pSprite = new Sprite(g_pFloatingHiddenBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          if(rand() % 2 == 0)
            pSprite->SetVelocity(7, 0);
          else
            pSprite->SetVelocity(-7, 0);
          g_pGame->AddSprite(pSprite);
          break;
        case 'E':  //Elevator Platform
          pSprite = new Sprite(g_pElevatorBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          if(rand() % 2 == 0)
            pSprite->SetVelocity(0, 10);
          else
            pSprite->SetVelocity(0, -10);
          g_pGame->AddSprite(pSprite);
          break;
        case ':':  //Hidden Elevator Platform
          pSprite = new Sprite(g_pElevatorHiddenBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75 + 20, i * 75);
          if(rand() % 2 == 0)
            pSprite->SetVelocity(0, 7);
          else
            pSprite->SetVelocity(0, -7);
          g_pGame->AddSprite(pSprite);
          break;
        case '@':  //Yellow stairs
          LaserSprite* pLSprite;
          if(g_iCurrentLevel > 6)
          {
            if(g_iCurrentLevel == 18)  //vertical laser in level 9
              pLSprite = new LaserSprite(g_pYellowLaserVBitmap, rcBounds, BA_BOUNCE);
            else
              pLSprite = new LaserSprite(g_pYellowLaserBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else
          {
            pLSprite = new LaserSprite(g_pYellowStairsBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(4);
            pLSprite->SetFrameDelay(2);
            pLSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pLSprite);
            pSprite = new Sprite(g_pBanisterBitmap, rcBounds, BA_BOUNCE);  //Add Banister
            pSprite->SetZOrder(1);
            pSprite->SetPosition((j + 3) * 75, (i - 1) * 75);
            g_pGame->AddSprite(pSprite);
            pSprite = new Sprite(g_pFulcrumBitmap, rcBounds, BA_BOUNCE);  //Add Fulcrum
            pSprite->SetZOrder(6);
            pSprite->SetPosition(((j + 3) * 75) + 50, (i + 1) * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '$':  //Orange Stairs
          if(g_iCurrentLevel > 6 && g_iCurrentLevel != 9)
          {
            pLSprite = new LaserSprite(g_pOrangeLaserBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else if(g_iCurrentLevel == 9)
          {
            pLSprite = new LaserSprite(g_pOrangeLaserVBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else
          {
            pLSprite = new LaserSprite(g_pOrangeStairsBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(4);
            pLSprite->SetFrameDelay(2);
            pLSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pLSprite);
            pSprite = new Sprite(g_pBanisterBitmap, rcBounds, BA_BOUNCE);  //Add Banister
            pSprite->SetZOrder(1);
            pSprite->SetPosition((j + 3) * 75, (i - 1) * 75);
            g_pGame->AddSprite(pSprite);
            pSprite = new Sprite(g_pFulcrumBitmap, rcBounds, BA_BOUNCE);  //Add Fulcrum
            pSprite->SetZOrder(6);
            pSprite->SetPosition(((j + 3) * 75) + 50, (i + 1) * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '^':  //Blue Stairs
          if(g_iCurrentLevel > 6)
          {
            pLSprite = new LaserSprite(g_pBlueLaserBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else
          {
            pLSprite = new LaserSprite(g_pBlueStairsBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(4);
            pLSprite->SetFrameDelay(2);
            pLSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pLSprite);
            pSprite = new Sprite(g_pBanisterBitmap, rcBounds, BA_BOUNCE);  //Add Banister
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, (i - 1) * 75);
            g_pGame->AddSprite(pSprite);
            pSprite = new Sprite(g_pFulcrumBitmap, rcBounds, BA_BOUNCE);  //Add Fulcrum
            pSprite->SetZOrder(6);
            pSprite->SetPosition(j * 75, (i + 1) * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 'S':  //Star block (or Sewage being dumped)
          if(g_iCurrentLevel == 14)
          {
            pSprite = new Sprite(g_pSludgeDumpBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetPosition(j * 75, i * 75);
            pSprite->SetHorizFrames(true);    //WAY too tall of a bitmap otherwise
            pSprite->SetNumFrames(8, true);
            pSprite->SetFrameDelay(3);
            pSprite->SetFrame(0);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pStarBlockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 's':  //Sinking Platform
          pSprite = new Sprite(g_pSinkingPlatformBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(7);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'x':  //x - shaped block
          if(g_iCurrentLevel == 7)
          {
            pSprite = new Sprite(g_pCaveRockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else
          {
            pSprite = new Sprite(g_pXblockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 'D':  //SlimeD rock
          pSprite = new Sprite(g_pSlimeRockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'r':  //Cave Rock
          if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)
          {
            pSprite = new Sprite(g_pStripedRightBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else if(g_iCurrentLevel == 12)
          {
            pSprite = new Sprite(g_pCoolRock2Bitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else
          {
            pSprite = new Sprite(g_pCaveRockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
        case 'X':  //eXit
          if(bHubLevel)
            g_pExitSprite = new Sprite(g_pExitClosedBitmap, rcBounds, BA_BOUNCE);
          else
            g_pExitSprite = new Sprite(g_pExitBitmap, rcBounds, BA_BOUNCE);
          g_pExitSprite->SetZOrder(2);
          g_pExitSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(g_pExitSprite);
          break;
        case 'U':  //UFO
          pSprite = new Sprite(g_pUFOBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(-6);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '.':
            pSprite = new Sprite(g_pTutorialBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pSprite);
            pSprite->SetLives(TUTORIAL_JUMP);   //HACK: Use the lives as a way to determine what tutorial text to display
            g_pGame->AddSprite(pSprite);
            break;
        case ';':
            pSprite = new Sprite(g_pTutorialBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pSprite);
            pSprite->SetLives(TUTORIAL_FIRE);   //HACK: Use the lives as a way to determine what tutorial text to display
            g_pGame->AddSprite(pSprite);
            break;
        case '(':
            pSprite = new Sprite(g_pTutorialBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pSprite);
            pSprite->SetLives(TUTORIAL_SLOPEFIRE);   //HACK: Use the lives as a way to determine what tutorial text to display
            g_pGame->AddSprite(pSprite);
            break;
        case 'e':  //MissilE!!!! KABOOM!! (If player isn't careful........)
        {
          pSprite = new Sprite(g_pMissileBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          //Add missile hub
          TrapSprite* pTrapSprite = new TrapSprite(g_pMissileHubBitmap, rcBounds, BA_BOUNCE);
          pTrapSprite->SetZOrder(1);
          pTrapSprite->SetNumFrames(2);
          pTrapSprite->SetPosition((j + 1) * 75, (i + 1) * 75);
          g_pGame->AddSprite(pTrapSprite);
          break;
        }
        case 'H':  //Hidden brick
          pSprite = new Sprite(g_pHiddenBlockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'd':  //crackeD logs (or sluDge flow)
          if(g_iCurrentLevel == 14)
          {
            pSprite = new Sprite(g_pSludgeFlowBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(5);
            pSprite->SetNumFrames(5);
            pSprite->SetFrameDelay(2);
            pSprite->SetPosition(j * 75, (i * 75) + 58);
            g_pGame->AddSprite(pSprite);
            pSprite = new Sprite(g_pTunnelHzBitmap, rcBounds, BA_BOUNCE);  //Also make horizontal tunnel section
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pCrackedLogsBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            //Add particle effect
            myPOINT ptOffset = { 75.0 / 2.0, 55 };
            pSprite->AddParticles(PARTICLE_BLOCKCRUMBLE, BITMAP_PARTICLES, 9, 9, 6, 6, ptOffset, false);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 'Q':  //Blue force block (Or sludge falling)
          if(g_iCurrentLevel == 14)
          {
            pSprite = new Sprite(g_pSludgeFallBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(5);
            pSprite->SetNumFrames(5);
            pSprite->SetFrameDelay(2);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            pSprite = new Sprite(g_pTunnelVBitmap, rcBounds, BA_BOUNCE);  //Also make vertical tunnel section
            pSprite->SetZOrder(1);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pBlueForceBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case 'G':  //Green force block
          pSprite = new Sprite(g_pGreenForceBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'q':  //Red Force block
          pSprite = new Sprite(g_pRedForceBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'g':  //Ground you walk over
          if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)
          {
            pSprite = new Sprite(g_pStripedLeftBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else
          {
            pSprite = new Sprite(g_pGroundBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
        case '�':  //Ground you go through
          if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)
          {
            pSprite = new Sprite(g_pStripedLGoThroughBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else if(g_iCurrentLevel == 18 || g_iCurrentLevel == 11 || (g_iCurrentLevel == 9 && i > 30) || g_iCurrentLevel == 13)
          {
            pSprite = new Sprite(g_pGoThroughRockBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else if(g_iCurrentLevel == 9 || g_iCurrentLevel == 15)
          {
            pSprite = new Sprite(g_pGoThroughStarBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else if(g_iCurrentLevel == 12)  //Level 13 make lava rock go through block
          {
            pSprite = new Sprite(g_pLavaGoThrough, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
          else
          {
            pSprite = new Sprite(g_pGoThroughGroundBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
            break;
          }
        case 'o':  //hOle you can go into
          pSprite = new Sprite(g_pHoleBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '/':  //slanted left ground
          if(g_iCurrentLevel == 1 && i > 20)  //lower than twentieth row in level 2
          {
            pSprite = new Sprite(g_pSlantedLeftChromeBitmap, rcBounds, BA_BOUNCE);  //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 2 && i > 4)  //or lower than fourth row in level 3
          {
            pSprite = new Sprite(g_pSlantedLeftChromeBitmap, rcBounds, BA_BOUNCE);    //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)  //or all of level 7 and 8
          {
            pSprite = new Sprite(g_pSlantedLeftChromeBitmap, rcBounds, BA_BOUNCE);    //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if((g_iCurrentLevel == 9 && i < 34) || (g_iCurrentLevel == 13 && j < 35) || g_iCurrentLevel == 15)  //Level 10 or 14 or 16
          {
            pSprite = new Sprite(g_pStarSlantL, rcBounds, BA_BOUNCE);    //make star block slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 11 || g_iCurrentLevel == 18 || g_iCurrentLevel == 8 || g_iCurrentLevel == 4 || g_iCurrentLevel == 9 || g_iCurrentLevel == 13)  //Level 12, 9, 10, 14 and 5
          {
            pSprite = new Sprite(g_pLeftRockSlant, rcBounds, BA_BOUNCE);    //make rock slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 12)  //Level 13
          {
            pSprite = new Sprite(g_pLavaSlopeL, rcBounds, BA_BOUNCE);    //make lava rock slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pSlantedLeftBitmap, rcBounds, BA_BOUNCE);    //make regular slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          if(g_iCurrentLevel == 12 && i == 11 && j < 22)  //If in certain part of level 13
          {
            pSprite = new Sprite(g_pLava2Bitmap, rcBounds, BA_BOUNCE);     //Make lava, too
            pSprite->SetZOrder(1);   //With low z- order
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '\\':  //slanted right ground
          if(g_iCurrentLevel == 1 && i > 20)  //lower than twentieth row in level 2
          {
            pSprite = new Sprite(g_pSlantedRightChromeBitmap, rcBounds, BA_BOUNCE);  //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 2 && i > 4)  //or lower than fourth row in level 3
          {
            pSprite = new Sprite(g_pSlantedRightChromeBitmap, rcBounds, BA_BOUNCE);    //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)  //or all of level 7 and 8
          {
            pSprite = new Sprite(g_pSlantedRightChromeBitmap, rcBounds, BA_BOUNCE);    //make chrome slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if((g_iCurrentLevel == 9 && i < 34) || (g_iCurrentLevel == 13 && j < 35) || g_iCurrentLevel == 15)  //Level 10 or 14 or 16
          {
            pSprite = new Sprite(g_pStarSlantR, rcBounds, BA_BOUNCE);    //make star slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 11 || g_iCurrentLevel == 18 || g_iCurrentLevel == 8 || g_iCurrentLevel == 4 || g_iCurrentLevel == 9 || g_iCurrentLevel == 13)  //Level 12, 10, 9, 14 and 5
          {
            pSprite = new Sprite(g_pRightRockSlant, rcBounds, BA_BOUNCE);    //make rock slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 12)  //Level 13
          {
            pSprite = new Sprite(g_pLavaSlopeR, rcBounds, BA_BOUNCE);    //make lava rock slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pSlantedRightBitmap, rcBounds, BA_BOUNCE);    //make regular slant
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          if(g_iCurrentLevel == 12 && i == 11 && j < 22)  //If in certain part of level 13
          {
            pSprite = new Sprite(g_pLava2Bitmap, rcBounds, BA_BOUNCE);     //Make lava, too
            pSprite->SetZOrder(1);   //With low z- order
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '>':      //Left cieling slope (/)
          if(g_iCurrentLevel == 12)
          {
            pSprite = new Sprite(g_pLavaSlopeCL, rcBounds, BA_BOUNCE);    //Make lava rock slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 15)
          {
            pSprite = new Sprite(g_pStarCielingLeft, rcBounds, BA_BOUNCE);    //Make star block slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pCLeftRockSlant, rcBounds, BA_BOUNCE);    //Make rock slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '<':     //Right cieling slope (\)
          if(g_iCurrentLevel == 12)
          {
            pSprite = new Sprite(g_pLavaSlopeCR, rcBounds, BA_BOUNCE);    //Make lava rock slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 15)
          {
            pSprite = new Sprite(g_pStarCielingRight, rcBounds, BA_BOUNCE);    //Make star block slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pCRightRockSlant, rcBounds, BA_BOUNCE);    //Make rock slant
            pSprite->SetZOrder(5);        //Higher than the player
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '~':  //Electrosphere
        {
          myPOINT ptTest = {j * 75, i * 75};
          ptTest.x += (75 - g_pElectrosphereBitmap->GetWidth()) / 2;    //HACK Bypass our usual grid-square-centering
          ptTest.y += (75 - (g_pElectrosphereBitmap->GetHeight() / 10)) / 2;
          if(CheckElectrosphere(ptTest))
          {
              //ofile << "Electrosphere at " << ptTest.x << "," << ptTest.y << " found" << endl;
              break;  //If we've gotten this one already, break out
          }
          pSprite = new Sprite(g_pElectrosphereBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(10);
          pSprite->SetPosition(ptTest);
          //CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        }
        case 'm':  //banana Milkshake
          pSprite = new Sprite(g_pMilkshakeBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetNumFrames(5);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case '*':  //Bee creature
          if(g_iCurrentLevel > 6)
          {
            if(g_iCurrentLevel == 18)
              pLSprite = new LaserSprite(g_pOrangeLaserVBitmap, rcBounds, BA_BOUNCE);
            else
              pLSprite = new LaserSprite(g_pGreenLaserHzBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else
          {
            pBASprite = new BadGuySprite(g_pBeeBitmap, rcBounds, BA_BOUNCE);
            pBASprite->SetZOrder(2);
            pBASprite->SetPosition(j * 75, (i * 75) + 25);
            pBASprite->SetNumFrames(8);
            if(rand() % 2 == 1)   //Set his starting direction randomly (and fast!)
              pBASprite->SetVelocity(-10, 0);
            else
              pBASprite->SetVelocity(10, 0);
            g_pGame->AddSprite(pBASprite);
          }
          break;
        case '`':  //slanted right corner
          if(g_iCurrentLevel == 1 && i > 20)  //lower than twentieth row in level 2
          {
            pSprite = new Sprite(g_pSlantedRightChromeCornerBitmap, rcBounds, BA_BOUNCE);  //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 2 && i > 4)  //or lower than fourth row in level 3
          {
            pSprite = new Sprite(g_pSlantedRightChromeCornerBitmap, rcBounds, BA_BOUNCE);    //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7) //If in level 7 or 8
          {
            pSprite = new Sprite(g_pSlantedRightChromeCornerBitmap, rcBounds, BA_BOUNCE);    //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if((g_iCurrentLevel == 9 && i < 34) || (g_iCurrentLevel == 13 && j < 35) || g_iCurrentLevel == 15) //If in level 10 or 14 or 16
          {
            pSprite = new Sprite(g_pStarSlantCornerR, rcBounds, BA_BOUNCE);    //make star slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 11 || g_iCurrentLevel == 18 || g_iCurrentLevel == 8 || g_iCurrentLevel == 4 || g_iCurrentLevel == 9 || g_iCurrentLevel == 13) //If in level 12, 9, 10, 14 or 5
          {
            pSprite = new Sprite(g_pRightRockCorner, rcBounds, BA_BOUNCE);    //make rock slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 12) //If in level 13
          {
            pSprite = new Sprite(g_pLavaSlopeRCorner, rcBounds, BA_BOUNCE);    //make lava rock slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pSlantedRightCornerBitmap, rcBounds, BA_BOUNCE);    //make regular slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '\'':  //slanted left corner (\' instead of ' because of special characters)
          if(g_iCurrentLevel == 1 && i > 20)  //lower than twentieth row in level 2
          {
            pSprite = new Sprite(g_pSlantedLeftChromeCornerBitmap, rcBounds, BA_BOUNCE);  //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 2 && i > 4)  //or lower than fourth row in level 3
          {
            pSprite = new Sprite(g_pSlantedLeftChromeCornerBitmap, rcBounds, BA_BOUNCE);    //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 6 || g_iCurrentLevel == 7)  //or in level 7 or 8
          {
            pSprite = new Sprite(g_pSlantedLeftChromeCornerBitmap, rcBounds, BA_BOUNCE);    //make chrome slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if((g_iCurrentLevel == 9 && i < 34) || (g_iCurrentLevel == 13 && j < 35)  || g_iCurrentLevel == 15)  //In level 10 or 14 or 16
          {
            pSprite = new Sprite(g_pStarSlantCornerL, rcBounds, BA_BOUNCE);    //make star slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 11 || g_iCurrentLevel == 18 || g_iCurrentLevel == 8 || g_iCurrentLevel == 4 || g_iCurrentLevel == 9 || g_iCurrentLevel == 13)  //or in level 12, 9, 10, 14 or 5
          {
            pSprite = new Sprite(g_pLeftRockCorner, rcBounds, BA_BOUNCE);    //make rock slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else if(g_iCurrentLevel == 12)  //or in level 13
          {
            pSprite = new Sprite(g_pLavaSlopeLCorner, rcBounds, BA_BOUNCE);    //make lava rock slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pSlantedLeftCornerBitmap, rcBounds, BA_BOUNCE);    //make regular slant corner
            pSprite->SetZOrder(3);
            pSprite->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '?':  //block that is invisible and only affects bad guys
          pSprite = new Sprite(g_pNullBlockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          pSprite->SetHidden(true);  //Don't draw
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //Jet boots (or orange vertical laser in level 14)
          if(g_iCurrentLevel == 13)
          {
            pLSprite = new LaserSprite(g_pOrangeLaserVBitmap, rcBounds, BA_BOUNCE);
            pLSprite->SetZOrder(3);
            pLSprite->SetNumFrames(5);
            pLSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pLSprite);
            g_pGame->AddSprite(pLSprite);
          }
          else if(g_iCurrentLevel == 16)
          {
            pSprite = new Sprite(g_pSuperJetBootsBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(5);
            pSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pSprite);
            g_pGame->AddSprite(pSprite);
          }
          else
          {
            pSprite = new Sprite(g_pJetBootsBitmap, rcBounds, BA_BOUNCE);
            pSprite->SetZOrder(6);
            pSprite->SetNumFrames(5);
            pSprite->SetPosition(j * 75, i * 75);
            CenterOnGridSquare(pSprite);
            g_pGame->AddSprite(pSprite);
          }
          break;
        case '�':  //Small block
          pSprite = new Sprite(g_pSmallBlockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          //CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':
        {
          PoisonSprite* pPSprite = new PoisonSprite(g_pLaserGunRightBitmap, rcBounds, BA_BOUNCE);
          pPSprite->SetZOrder(6);
          pPSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pPSprite);
        }
          break;
        case '�':
        {
          PoisonSprite* pPSprite = new PoisonSprite(g_pLaserGunDownBitmap, rcBounds, BA_BOUNCE);
          pPSprite->SetZOrder(6);
          pPSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pPSprite);
        }
          break;
        case '�':      //Left mirror (/)
          pSprite = new Sprite(g_pMirrorLeftBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':     //Right mirror(\)
          pSprite = new Sprite(g_pMirrorRightBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':     //Falling block
        {
          myPOINT ptParticleOffset = {17,75};
          pSprite = new Sprite(g_pFallBlockBitmap, rcBounds, BA_DIE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
          pSprite->SetParticleTile(2,25);
          pSprite->SetParticlesInFront(true);
          g_pGame->AddSprite(pSprite);
          break;
        }
        case '�':     //Large Falling block
        {
          myPOINT ptParticleOffset = {17,75};
          pSprite = new Sprite(g_pLargeFallBlockBitmap, rcBounds, BA_DIE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          pSprite->AddParticles(PARTICLE_SMOKEPUFF, BITMAP_PARTICLES, 50,0,25,25,ptParticleOffset,false);
          pSprite->SetParticleTile(15,25);
          pSprite->SetParticlesInFront(true);
          g_pGame->AddSprite(pSprite);
          break;
        }
        case '�':     //Wooden block
          pSprite = new Sprite(g_pWoodenBlockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'K':     //amoeba ooze blocK
          pSprite = new Sprite(g_pAmoebaBlockBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'L':     //BaLLoon
          pSprite = new Sprite(g_pBalloonBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(6);
          pSprite->SetPosition(j * 75, i * 75);
          CenterOnGridSquare(pSprite);
          g_pGame->AddSprite(pSprite);
          break;
        case 'p':     //telePorter
          if(g_pTeleporterSprite[0] == NULL)
          {
            g_pTeleporterSprite[0] = new TeleporterSprite(g_pTeleporterBitmap, rcBounds, BA_BOUNCE);
            g_pTeleporterSprite[0]->SetZOrder(6);
            g_pTeleporterSprite[0]->SetNumFrames(20);
            g_pTeleporterSprite[0]->SetFrameDelay(2);
            g_pTeleporterSprite[0]->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(g_pTeleporterSprite[0]);
          }
          else
          {
            g_pTeleporterSprite[1] = new TeleporterSprite(g_pTeleporterBitmap, rcBounds, BA_BOUNCE);
            g_pTeleporterSprite[1]->SetZOrder(6);
            g_pTeleporterSprite[1]->SetNumFrames(20);
            g_pTeleporterSprite[1]->SetFrameDelay(2);
            g_pTeleporterSprite[1]->SetPosition(j * 75, i * 75);
            g_pGame->AddSprite(g_pTeleporterSprite[1]);
          }
          break;
        case ',':  //Sludge corner and cross block (-|-) bitmap
          pSprite = new Sprite(g_pSludgeFlowCornerBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(5);
          pSprite->SetNumFrames(5);
          pSprite->SetFrameDelay(2);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          pSprite = new Sprite(g_pTunnelCrossBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel |- section
          pSprite = new Sprite(g_pTunnelFBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel _|_ section
          if(i == 17)           //If in certain row
          {
            pSprite = new Sprite(g_pSludgeFlowBitmap, rcBounds, BA_BOUNCE);  //Make flowing sludge, too
            pSprite->SetZOrder(5);
            pSprite->SetNumFrames(5);
            pSprite->SetFrameDelay(2);
            pSprite->SetPosition(j * 75, (i * 75) + 58);
            g_pGame->AddSprite(pSprite);
          }
          pSprite = new Sprite(g_pTunnelPerpBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case 'T':  //tunnel T section
          pSprite = new Sprite(g_pTunnelTBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel `| section
          pSprite = new Sprite(g_pTunnelBackPBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel |` section
          pSprite = new Sprite(g_pTunnelPBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel _| section
          pSprite = new Sprite(g_pTunnelBackLBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel - section
          pSprite = new Sprite(g_pTunnelHzBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //tunnel L section
          pSprite = new Sprite(g_pTunnelLBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(1);
          pSprite->SetPosition(j * 75, i * 75);
          g_pGame->AddSprite(pSprite);
          break;
        case '�':  //Shoot and jump block
          pSprite = new Sprite(g_pShootJumpBitmap, rcBounds, BA_BOUNCE);
          pSprite->SetZOrder(3);
          pSprite->SetPosition(j * 75, i * 75 + 30); //down a bit
          pSprite->SetLives(2); //two lives
          g_pGame->AddSprite(pSprite);
          break;
        case '\n':    //If it's an enter, not a character
        case '\r':
          j--;        //Don't count this one
          break;
        default:
          break;      //Count this one, but don't do anything
      }
    }
  }

  if(g_iCurrentLevel == 9)
    g_rcAll.bottom = 2550;   //Only see part of level
  else if(g_iCurrentLevel == 13)
    g_rcAll.right = 34 * 75;   //Only see part of level
  else if(g_iCurrentLevel == 14)
  {
    g_rcAll.left = 75;   //Only see part of level
    g_rcAll.top = 9 * 75;
  }
  else if(g_iCurrentLevel == 15)
  {
    g_rcAll.right = 21 * 75;   //Only see part of level
  }

  UpdateViewport(); //Set initial viewport coordinates
  CopyRect(&g_rcLastViewport, &g_rcAll);    //Save this viewport to the last viewport (for checkpoint reasons)
  return(!vfclose(hFile));
}

bool IsInView(Sprite* pSprite)
{
  myRECT& rcSprite = pSprite->GetPosition();

  if((rcSprite.right <= g_rcViewport.left) ||
     (rcSprite.bottom <= g_rcViewport.top) ||
     (rcSprite.left >= g_rcViewport.right) ||
     (rcSprite.top >= g_rcViewport.bottom))
  {
    return false;
  }
  return true;
}

void StartLevel()
{
  SaveGame();     //Save our current game
  // Cleanup the sprites
  g_pGame->CleanupSprites();

  //Stop the menu music from playing
//  if(!g_bNoSound)
    g_pGame->CleanupSong();

  g_pTeleporterSprite[0] = NULL;
  g_pTeleporterSprite[1] = NULL;          //Set teleporters to null

  g_iTeleporterCount = 0;                 //Not used teleporter
  g_iMissileCount = 0;                    //Not destroyed any missiles

  g_bIncompleteViewportDrawn = false;     //In case the users' screen is larger than the level. Not likely, but possible.
  g_pLastCheckpoint = NULL; //No last checkpoint
  while(!g_lRespawnList.empty())
    g_lRespawnList.pop_front();
  while(!g_lDespawnList.empty())
    g_lDespawnList.pop_front();

  g_bGreenKey = false;   //Make all the keys and hubs false
  g_bRedKey = false;
  g_bBlueKey = false;
  g_bYellowKey = false;
  g_bOrangeKey = false;
  g_bYellowHub = false;
  g_bOrangeHub = false;
  g_bBlueHub = false;
  g_bGreenHub = false;
  g_bYellowStair = false;
  g_bOrangeStair = false;
  g_bBlueStair = false;
  g_bCurLevelPart = false;
  g_iJumpAmount = g_iFallAmount = 0;    //Don't start out jumping or falling
  g_bIsJumping = false;
  g_bIsFalling = true;
  g_iPointsLevelStart = g_iPointsGotten;
  g_sDialogText = ""; //Clear any dialog text if there's text being displayed
  if(g_iCurrentLevel < 16)
    g_iJetbootsTime = 0;  //Don't allow jetboosting before hitting the ground on level start
  else
    g_iJetbootsTime = MAX_JETBOOT_TIME; //But do allow in secret levels / continuations / final boss
  if(g_iCurrentLevel < 16 || g_iCurrentLevel == 19)
    g_iOldElectroCount = g_lElectrospheresCollected.size();   //Keep track of how many electrospheres we used to have before this level


  //Create the game map and check for errors
  if(g_iCurrentLevel < 20)  //Only read standard .cb3 map if not in 3D area
  {
      if(!ReadMapFile())
      {
       ofile << "Error reading a .CB3 file, check to see if you have all of them." << endl;
       //exit(1);
      }
  }
//  if(!g_bNoSound)
  {
    if(g_iCurrentLevel == 0)
    {
      g_pGame->PlaySong(MUSIC_LEVEL1);
    }
    else if(g_iCurrentLevel == 1)
    {
      g_pGame->PlaySong(MUSIC_LEVEL2);
    }
    else if(g_iCurrentLevel == 2)
    {
      g_pGame->PlaySong(MUSIC_LEVEL3);
    }
    else if(g_iCurrentLevel == 3)
    {
      g_pGame->PlaySong(MUSIC_LEVEL4);
    }
    else if(g_iCurrentLevel == 4)
    {
      g_pGame->PlaySong(MUSIC_LEVEL5);
    }
    else if(g_iCurrentLevel == 5)
    {
      g_pGame->PlaySong(MUSIC_LEVEL6);
    }
    else if(g_iCurrentLevel == 6)
    {
      g_pGame->PlaySong(MUSIC_LEVEL7);
    }
    else if(g_iCurrentLevel == 7)
    {
      g_pGame->PlaySong(MUSIC_LEVEL8);
    }
    else if(g_iCurrentLevel == 8)
    {
      g_pGame->PlaySong(MUSIC_LEVEL5);
    }
    else if(g_iCurrentLevel == 9)
    {
      g_pGame->PlaySong(MUSIC_LEVEL2);
    }
    else if(g_iCurrentLevel == 10)
    {
      g_pGame->PlaySong(MUSIC_LEVEL3);
    }
    else if(g_iCurrentLevel == 11)
    {
      g_pGame->PlaySong(MUSIC_LEVEL4);
    }
    else if(g_iCurrentLevel == 12)
    {
      g_pGame->PlaySong(MUSIC_LEVEL5);
    }
    else if(g_iCurrentLevel == 13)
    {
      g_pGame->PlaySong(MUSIC_LEVEL6);
    }
    else if(g_iCurrentLevel == 14)
    {
      g_pGame->PlaySong(MUSIC_LEVEL7);
    }
    else if(g_iCurrentLevel == 15)
    {
      g_pGame->PlaySong(MUSIC_LASTLEVEL);
    }
    else if(g_iCurrentLevel == 16)
    {
        g_pGame->PlaySong(MUSIC_SECRET);
    }
    else if(g_iCurrentLevel == 17)
    {
        g_pGame->PlaySong(MUSIC_SECRET);
    }
    else if(g_iCurrentLevel == 18)
    {
        g_pGame->PlaySong(MUSIC_LEVEL7);
    }
    else if(g_iCurrentLevel == 19)  //Start EPIC BOSS MUSIC.
    {
        g_pGame->PlaySong(MUSIC_BOSS);
        g_pGame->KillChannel(g_iBossWalkLoopChannel);   //Kill loop sound if it's playing
        g_iBossWalkLoopChannel = g_pGame->PlayLoop(IDW_CRABRATTLE); //Play loop sound
        g_bBossDying = false;
    }
    else if(g_iCurrentLevel == 20)
    {
        g_pGame->PlaySong(MUSIC_SECRET);
        g_b3D = true;
        PrimeACK("res/ack/lev1.dtf");
    }
    else if(g_iCurrentLevel == 21)
    {
        g_pGame->PlaySong(MUSIC_SECRET);
        g_b3D = true;
        PrimeACK("res/ack/lev2.dtf");
    }
  }

  if(g_iCurrentLevel == 19)
  {
      g_iTimeLeftInThisAction = 40;
      g_iBossFrame = 0;
      g_iBossFrameDelayCounter = 0;
      g_iBossEnemiesSpawned = 0;
      g_iBossEnemiesKilled  = 0;
      g_iCurBossAction = BOSS_PAUSE;
      g_iBossHealth = BOSS_MAX_HEALTH;
      g_iCurBossJumpAmt = 0;
      g_iBossJumpDelay = BOSS_JUMP_DELAY;
      g_iBossAvoidToleranceCount = 0;
      g_iBossHitThisCycle = 0;
      g_iFallAmount = 3;    //HACK for displaying dialog only when hits ground
  }
}

void DieChef(int iDieTime)
{
  if(g_iCheckpointInvincibility || g_bWinning || (g_ptPanTo.x != -1 && g_ptPanTo.y != -1))
        return; //Don't die during checkpoint respawn grace period
  myRECT rcPos;
  rcPos = g_pPersonSprite->GetPosition();

  if(g_iCurrentLevel == 19 && g_bBossDying) //If in boss die animation, chef should be invincible.
    return;

  if((!(rcPos.top >= g_rcAll.bottom)) && g_bInvincible)  //Only make chef die if not invincible or falling off screen
  {
    return;
  }

  if(!g_bDying)
  {
    g_iKillCounter++;   //Increase kill counter
    if(iDieTime != 30)
      g_bFadeWhite = true;  //HACK
    else
      g_bFadeWhite = false;
    g_bDying = true;
    g_iDieCount = iDieTime;
      g_pPersonSprite->SetHidden(true);
      // Create a Chef dying sprite at Chef's position
      myRECT rcPos;
      rcPos = g_pPersonSprite->GetPosition();
      if(rcPos.top < g_rcAll.bottom - 2)  //Only make chef dying sprite if not dying by falling off screen
      {
        Sprite* pSprite = new Sprite((g_bFacingLeft ? g_pChefDieLBitmap : g_pChefDieRBitmap), g_rcAll, BA_DIE);
        pSprite->SetNumFrames(25, true);
        pSprite->SetZOrder(8);
        if(g_bFacingLeft)
          pSprite->SetVelocity(-10, 15);
        else
          pSprite->SetVelocity(10, 15);
        pSprite->SetPosition(rcPos.left, rcPos.top);
        g_pGame->AddSprite(pSprite);
      }
//      if(!g_bNoSound)
      {
        // Play the dying Chef sound
        PlayMySound(IDW_DIE);
      }
  }
}

void DoWon(Sprite* pExit)
{
  //Stop boss loop sound
  if(g_iCurrentLevel == 19)
  {
    g_pGame->KillChannel(g_iBossWalkLoopChannel);
  }

  myRECT rect = pExit->GetPosition();

//  if(!g_bNoSound)
  {
    // Play the win level sound
    PlayMySound(IDW_WINLEVEL);
  }

  //Record him winning
  g_bWinning = true;
  g_pPersonSprite->RemoveParticles();   //So don't draw particles while exiting

  if(g_bFacingLeft)
  {
    g_pPersonSprite->SetPosition(rect.right, rect.top);
  }
  else
  {
    g_pPersonSprite->SetPosition(rect.left - 42, rect.top);
  }
}

void NewGame()
{
  EmptyElectrosphereList();
  for(int i = 0; i < 21; i++)
    g_cName[i] = '~';
  g_rcViewport.left = 0;
  g_rcViewport.top = 0;
  g_rcViewport.right = g_pGame->GetWidth();
  g_rcViewport.bottom = g_pGame->GetHeight();
  g_iCurrentLevel = 0;
//  g_iSpoons = 5;
  g_iDieCount= 30;
//  g_iInputDelay = 0;
//  g_iGameOverCount = 90;
  g_bDying = false;
  g_bFacingLeft = false;
  g_bOnLeftSlant = false;
  g_bOnRightSlant = false;
  //Set global booleans and other variables
  g_bIsJumping = false;
  g_bIsFalling = true;
  g_iJumpAmount = 0;
  g_iFallAmount = 0;
  g_iPointsGotten = 0;
  g_bIsWalkingRight = true;
  g_bIsFiring = false;
  g_bGreenKey = false;
  g_bRedKey = false;
  g_bBlueKey = false;
  g_bYellowKey = false;
  g_bOrangeKey = false;
  g_bYellowHub = false;
  g_bOrangeHub = false;
  g_bBlueHub = false;
  g_bGreenHub = false;
  g_bYellowStair = false;
  g_bOrangeStair = false;
  g_bBlueStair = false;
  g_iLives = 5;
  g_bFired = false;
  g_iMoveHorizontal = 2; //Used for firing right
  g_bPlayingGame = false;
  g_bWinning = false;
  g_iWinCount = 30;
  g_bHighScores = false;
  g_bJetBoots = false;
  g_bSuperJetBoots = false;
  MAX_JETBOOT_TIME = 15;
  for(int i = 0; i < 20; i++)
  {
    g_bParts[i] = false;
  }
  g_bTypeName = true;
  g_bTutorialMoved = false;
  g_bTutorialJumped = false;
  g_bTutorialFired = false;
  g_bTutorialSlopeFired = false;
  g_bTutorialElectrosphere = false;
  g_iFinalBossDialogLocation = 0;
  g_bBgStory = true;
  g_bEgStory = false;
  g_iBgStoryPanel = 0;
  g_bTutorialWASD = false;
}

void UpdateHiScores()
{
  // See if the current score made the hi score list
  int i;
  for (i = 0; i < 5; i++)
  {
    if (g_iPointsGotten > g_iHiScores[i])
    {
      //g_bTypeName = true; //Read in name
      g_iHighScoreSlot = i;
      break;
    }
  }

  // Insert the current score into the hi score list
  if (i < 5)
  {
    for (int j = 4; j > i; j--)
    {
      g_iHiScores[j] = g_iHiScores[j - 1];
      for(int k = 0; k < 21; k++)
      {
        g_cNames[j][k] = g_cNames[j - 1][k];
      }
    }
    g_iHiScores[i] = g_iPointsGotten;

    for(int l = 0; l < 21; l++)
    {
        g_cNames[i][l] = g_cName[l];
    }
  }

  g_bEgStory = true;
  //g_iCameToHighScoresFrom = MAIN_MENU;  //exit to main menu when done
}

bool ReadHiScores()
{
  // Open the high score file (HighScores.CB3)
  string sFilename = GetSavegameLocation() + "HighScores.CB3";
  VFILE* hFile = vfopen(sFilename.c_str(), "rb");
  if (hFile == NULL)
  {
    // The high score file doesn't exist, so initialize the scores to 100
    for (int i = 0; i < 5; i++)
      g_iHiScores[i] = 10;
    return false;
  }

  // Read the scores
  for (int i = 0; i < 5; i++)
  {
    //Read the name
    char  cData[22];
    if(vfread(&cData, 1, 21, hFile) != 21)
    {
      // Something went wrong, so close the file handle
      vfclose(hFile);
      return false;
    }
    //Get the names out of it
    for(int j = 0; j < 21; j++)
    {
      g_cNames[i][j] = cData[j];
    }

    // Read the score
    if(vfread(&g_iHiScores[i], sizeof(g_iHiScores[i]), 1, hFile) != 1)
    {
      // Something went wrong, so close the file handle
      vfclose(hFile);
      return false;
    }
  }

  // Close the file
  return !vfclose(hFile);
}

bool WriteHiScores()
{
  // Create the high score file (HighScores.CB3) for writing
  //Of course a bit of work since this isn't implemented in ttvfs yet...
  string sFilename = GetSavegameLocation() + "HighScores.CB3";
  FILE* hFile = fopen(sFilename.c_str(), "wb");
  if (hFile == NULL)
    // The high score file couldn't be created, so bail
    return false;

  // Write the scores
  for (int i = 0; i < 5; i++)
  {
    //Write the name first
    if(fwrite(&g_cNames[i], 1, 21, hFile) != 21)
    {
      // Something went wrong, so close the file handle
      fclose(hFile);
      return false;
    }

    // Write the score
    if(fwrite(&g_iHiScores[i], sizeof(g_iHiScores[i]), 1, hFile) != 1)
    {
      // Something went wrong, so close the file handle
      fclose(hFile);
      return false;
    }
  }

  // Close the file
  return !fclose(hFile);
}

bool CheckWon()  //Function to see if player won
{
    for(int i = 0; g_iHubLevels[i] != -1; i++)
    {
        if(!g_bParts[g_iHubLevels[i]])
        {
            return false;
        }
    }
    return true;
}

bool IsNearby(Sprite* pSprite)//, bool bBiggerArea)
{
    if(g_iCurrentLevel == 19)
        return true;        //Boss level: Everybody is updated.

  Bitmap* pBitmap = pSprite->GetBitmap();

  if(pBitmap == g_pMissileExplodeBitmap)
    return true;

  else if(pBitmap == g_pYellowStairsBitmap || pBitmap == g_pOrangeStairsBitmap || pBitmap == g_pBlueStairsBitmap)
    return true;                             //Always update stairs (so panning effect works)

  //Check and make force blocks have low alpha until you get their key
    else if(pBitmap == g_pRedForceBitmap)
    {
        if(g_bRedKey)
            pSprite->SetAlpha(255);
        else
            pSprite->SetAlpha(100);
    }
    else if(pBitmap == g_pGreenForceBitmap)
    {
        if(g_bGreenKey)
            pSprite->SetAlpha(255);
        else
            pSprite->SetAlpha(100);
    }
    else if(pBitmap == g_pBlueForceBitmap)
    {
        if(g_bBlueKey)
            pSprite->SetAlpha(255);
        else
            pSprite->SetAlpha(100);
    }

  myRECT rect = pSprite->GetPosition();
  myRECT rcArea = g_rcViewport;

    rcArea.left -= 450;     //Add to area that sprites are updated in
    rcArea.right += 450;
    if(g_iCurrentLevel == 18 || g_iCurrentLevel == 11)
    {
      rcArea.top -= 375;       //Smaller area in slower levels
      rcArea.bottom += 375;
    }
    else
    {
      rcArea.top -= 450;
      rcArea.bottom += 450;
    }

  if((IsSpriteBadGuy(pSprite)) ||
     (pBitmap == g_pBeeBitmap) ||
     (pBitmap == g_pBatBitmap) ||
     (pBitmap == g_pFloatingPlatformBitmap) ||
     (pBitmap == g_pFloatingHiddenBitmap) ||
     (pBitmap == g_pElevatorBitmap) ||
     (pBitmap == g_pElevatorHiddenBitmap))
  {
      if(g_ptPanTo.x != -1)
        return false;   //No move enemies when screen panning
    if(rect.right > rcArea.right ||  //Notice something subtle here. The amoebas are
       rect.left < rcArea.left ||    //only updated if all of them are in the screen,
       rect.top < rcArea.top ||      //while blocks are updated if only part of them
       rect.bottom > rcArea.bottom)  //are in the screen. This is because otherwise, the
      {                              //amoebas would fall through the blocks. Try taking
        return false;                //this out to see what I mean. It's difficult to
      }                              //explain.
    //Secondary check: If they're going to move off the screen this cycle
    myPOINT ptVel = pSprite->GetVelocity();
    rect.right += ptVel.x;//g_pGame->GetNormalizedVelocity(pSprite).x;
    rect.left += ptVel.x;//g_pGame->GetNormalizedVelocity(pSprite).x;
    rect.top += ptVel.y;//g_pGame->GetNormalizedVelocity(pSprite).y;
    rect.bottom += ptVel.y;//g_pGame->GetNormalizedVelocity(pSprite).y;
    //Test again
    if(rect.right > rcArea.right ||
       rect.left < rcArea.left ||
       rect.top < rcArea.top ||
       rect.bottom > rcArea.bottom)
      {
        return false;
      }
    return true;
  }
  else
  {
    if(rect.left > rcArea.right ||
       rect.right < rcArea.left ||
       rect.bottom < rcArea.top ||
       rect.top > rcArea.bottom)
      {
        return false;
      }
    return true;
  }
}

void WonGame()
{
//  TODO: Winning game dialog
//  "Chef Bereft, Master of Culinary Arts, sucessfully foils the plans of the vile aliens by deactivating their hideously sinister missile systems.\n     \"Brussels sprouts!\" Cries Chef once he's done, \"That was pretty hard! I didn't realize there were that many evil creatures on the Moon!\" He races back to his spaceship probe and prepares for takeoff. Meanwhile, the aliens have realized their heinous plot has been foiled. They quickly band together with their trusty allies, the amoebas, and attempt to chase after him in their spaceships. After leading them on a wild goose chase that involved seven laps around Jupiter, Chef manages to outwit them and leaves them orbiting around Io, one of Jupiter's moons.\n     With a sigh of satisfaction, Chef lands his spaceship in his backyard and gives it a nice washing off before he reopens it as his restaurant, \"The Flying Saucer Grill and Deli.\"\n     \"I'm just in time for the dinner rush,\" Chef muses as the customers begin lining up outside the front door.\n\n     Congratulations, Chef Bereft Master!"
}

bool SaveGame()
{
  //There's a method to the madness here. For proper, safe file I/O, it's a good idea when you're saving a file to
  // wait before overwriting the old file until the new file is completely written. This way, if something goes wrong
  // (power goes out, that sort of thing), the old file will still be there and you won't have lost both. This is a
  // simple implementation here; a more complex one would hold onto both saves, keeping the old one as an autosave
  // of sorts. Here, we'll just write to a new, temporary file, and once that succeeds, delete the old file and rename
  // the new file to replace it.
  char cData1[25];
  sprintf(cData1, "Profile%d.CB3", g_iGame);
  //Of course a bit of work since this isn't implemented in ttvfs yet...
  string sFilename = GetSavegameLocation() + "tempsave.CB3";// cData1;
  string sFinalName = GetSavegameLocation() + cData1;

  // Open the save game file (SaveGameX.CB3)
  FILE* hFile = fopen(sFilename.c_str(), "wb");
  if (hFile == NULL)
  {
    // The file couldn't be created, so bail
    return false;
  }

  // write the stuff needed

  //Write the name
  if(fwrite(&g_cName, sizeof(g_cName), 1, hFile) != 1)
  {
      fclose(hFile);
      return false;
  }

  //write the current level
  if(fwrite(&g_iCurrentLevel, sizeof(g_iCurrentLevel), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    fclose(hFile);
    return false;
  }

  // write the score
  if(fwrite(&g_iPointsGotten, sizeof(g_iPointsGotten), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    fclose(hFile);
    return false;
  }

  //write the parts gotten
  for(int i = 0; i < 20; i++)
  {
    if(fwrite(&g_bParts[i], sizeof(g_bParts[i]), 1, hFile) != 1)
    {
      // Something went wrong, so close the file handle
      fclose(hFile);
      return false;
    }
  }

  //Write jet boots
  if(fwrite(&g_bJetBoots, sizeof(g_bJetBoots), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    fclose(hFile);
    return false;
  }

  //Write super jet boots
  if(fwrite(&g_bSuperJetBoots, sizeof(g_bSuperJetBoots), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    fclose(hFile);
    return false;
  }

  //Write electrosphere tutorial. Don't care about this, so no error-checking
  fwrite(&g_bTutorialElectrosphere, sizeof(g_bTutorialElectrosphere), 1, hFile);
  //Same for final boss dialog
  fwrite(&g_iFinalBossDialogLocation, sizeof(g_iFinalBossDialogLocation), 1, hFile);

  //Write the electrospheres we have
  int iSize = g_lElectrospheresCollected.size();
  if(fwrite(&iSize, sizeof(iSize), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    //ofile << "Wrote wrong size" << endl;
    //exit(1);
    fclose(hFile);
    return false;
  }
  //ofile << "Electrospheres writing: " << iSize << endl;
  for(list<myPOINT>::iterator i = g_lElectrospheresCollected.begin(); i != g_lElectrospheresCollected.end(); i++)
  {
      if(fwrite(&(*i), sizeof(myPOINT), 1, hFile) != 1)
      {
        // Something went wrong, so close the file handle
        //ofile << "Wrote wrong size2" << endl;
        //exit(1);
        fclose(hFile);
        return false;
      }
      //ofile << "Writing electrosphere " << (*i).x << "," << (*i).y << endl;
  }
  if(fwrite(&g_bTutorialWASD, sizeof(g_bTutorialWASD), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    fclose(hFile);
    return false;
  }

  // Close the file
  if(!fclose(hFile))
  {
      //We're good
      unlink(sFinalName.c_str());    //Delete old save
      //  return false;
      return(!rename(sFilename.c_str(), sFinalName.c_str()));   //Rename new save to old save's filename
      //Done
  }
  return false; //Couldn't close file
}

bool RedeemGame()
{
  char cData[25];
  sprintf(cData, "Profile%d.CB3", g_iGame);
  string sFilename = GetSavegameLocation() + cData;

  // Open the redeem game file (SaveGameX.CB3)
  VFILE* hFile = vfopen(sFilename.c_str(), "rb");
  if (hFile == NULL)
  {
    // The file couldn't be read, so bail
    return false;
  }

  //Read in the name
  if(vfread(&g_cName, sizeof(g_cName), 1, hFile) != 1)
  {
      vfclose(hFile);
      return false;
  }

  //Read the current level
  if(vfread(&g_iCurrentLevel, sizeof(g_iCurrentLevel), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }
  //Make sure it's reasonable
  if(g_iCurrentLevel == 16)
    g_iCurrentLevel = 1;
  else if(g_iCurrentLevel == 17)
    g_iCurrentLevel = 6;
  else if(g_iCurrentLevel == 18)
    g_iCurrentLevel = 8;

  // read the score
  if(vfread(&g_iPointsGotten, sizeof(g_iPointsGotten), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }

  //read the parts gotten
  for(int i = 0; i < 20; i++)
  {
    if(vfread(&g_bParts[i], sizeof(g_bParts[i]), 1, hFile) != 1)
    {
      // Something went wrong, so close the file handle
      vfclose(hFile);
      return false;
    }
  }

  //Read jet boots
  if(vfread(&g_bJetBoots, sizeof(g_bJetBoots), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }

  //Read super jet boots
  if(vfread(&g_bSuperJetBoots, sizeof(g_bSuperJetBoots), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }

  g_bTutorialElectrosphere = false;
  //Read electrosphere tutorial. Don't really care too much about this, so no error-checking
  vfread(&g_bTutorialElectrosphere, sizeof(g_bTutorialElectrosphere), 1, hFile);
  //Same with final boss dialog
  g_iFinalBossDialogLocation = 0;
  vfread(&g_iFinalBossDialogLocation, sizeof(g_iFinalBossDialogLocation), 1, hFile);

  EmptyElectrosphereList();
  //Read in the electrospheres we have
  int iSize = 0;
  if(vfread(&iSize, sizeof(iSize), 1, hFile) != 1)
  {
    // Something went wrong, so close the file handle
    vfclose(hFile);
    return false;
  }
  //ofile << "Size of electrosphere list: " << iSize << endl;
  for(int i = 0; i < iSize; i++)
  {
      myPOINT pt;
      if(vfread(&pt, sizeof(pt), 1, hFile) != 1)
      {
          // Something went wrong, so close the file handle
          vfclose(hFile);
          return false;
      }
      g_lElectrospheresCollected.push_front(pt);
      //ofile << "Reading Electrosphere: " << pt.x << "," << pt.y << endl;
  }

  g_iOldElectroCount = g_lElectrospheresCollected.size();   //Bug if beating boss-- displayed secret unlock message again

  if(vfread(&g_bTutorialWASD, sizeof(g_bTutorialWASD), 1, hFile) != 1)
  {
    // Something went wrong, so COMPLETELY IGNORE IT! AHAHAAHA
    g_bTutorialWASD = false;
  }

  if(g_bSuperJetBoots)
    MAX_JETBOOT_TIME = 25;
  else
    MAX_JETBOOT_TIME = 15;

  //Reset everything else
  g_iDieCount= 30;
//  g_iInputDelay = 0;
//  g_iGameOverCount = 180;
  g_bDying = false;
  g_bFacingLeft = false;
  g_bOnLeftSlant = false;
  g_bOnRightSlant = false;
  //Set global booleans and other variables
  g_bIsJumping = false;
  g_bIsFalling = true;
  g_iJumpAmount = 0;
  g_iFallAmount = 0;
  g_bIsWalkingRight = true;
  g_bIsFiring = false;
  g_bGreenKey = false;
  g_bRedKey = false;
  g_bBlueKey = false;
  g_bYellowKey = false;
  g_bOrangeKey = false;
  g_bYellowHub = false;
  g_bOrangeHub = false;
  g_bYellowStair = false;
  g_bOrangeStair = false;
  g_bBlueStair = false;
  g_bBlueHub = false;
  g_bGreenHub = false;
  g_bFired = false;
  g_iMoveHorizontal = 2; //Used for firing right
  g_rcViewport.top = 0;
  g_rcViewport.left = 0;
  g_rcViewport.right = g_pGame->GetWidth();
  g_rcViewport.bottom = g_pGame->GetHeight();
  g_bPlayingGame = false;
  g_bWinning = false;
  g_iWinCount = 30;
  g_bHighScores = false;
  g_bTypeName = false;
  g_bTutorialMoved = true;  //Assume they know what they're doing already
  g_bTutorialJumped = true;
  g_bTutorialFired = true;
  g_bTutorialSlopeFired = true;
  g_bBgStory = false;
  g_bEgStory = false;
  g_iBgStoryPanel = 0;

  // Close the file
  return (!vfclose(hFile));
}

bool ElevatorHit(Sprite* pSpriteHitter, Sprite* pSpriteHittee)
{
  myRECT  rcHitRect,
        rcPlatformRect;
  myPOINT ptVelocity;

  rcHitRect = pSpriteHitter->GetCollision();
  rcPlatformRect = pSpriteHittee->GetCollision();
  rcHitRect.bottom -= COLLISION_EDGE_ADD;                // Add to collision rectangles
  rcPlatformRect.top += COLLISION_EDGE_ADD;               //(don't take these increments out!)
  rcHitRect.top += COLLISION_EDGE_ADD;
  rcPlatformRect.bottom -= COLLISION_EDGE_ADD;
  rcHitRect.left += COLLISION_EDGE_ADD;
  rcPlatformRect.right -= COLLISION_EDGE_ADD;
  rcHitRect.right -= COLLISION_EDGE_ADD;
  rcPlatformRect.left += COLLISION_EDGE_ADD;

  if((rcPlatformRect.top - rcHitRect.bottom) >= 1) //If right above the block
  {
    ptVelocity = pSpriteHitter->GetVelocity();
    if(ptVelocity.y > 0)
      ptVelocity.y = -ptVelocity.y;
    pSpriteHitter->SetVelocity(ptVelocity);
    return true;
  }
  else if((rcHitRect.top - rcPlatformRect.bottom) >= 1) //If right below the block
  {
    ptVelocity = pSpriteHitter->GetVelocity();
    if(ptVelocity.y < 0)
      ptVelocity.y = -ptVelocity.y;
    pSpriteHitter->SetVelocity(ptVelocity);
    return true;
  }
  else if((rcHitRect.left - rcPlatformRect.right) >= 1)//If right of the block
  {
    ptVelocity = pSpriteHitter->GetVelocity();
    if(ptVelocity.x < 0)
      ptVelocity.x = -ptVelocity.x;
    pSpriteHitter->SetVelocity(ptVelocity);
    return true;
  }
  else if((rcPlatformRect.left - rcHitRect.right) >= 1)//If left of the block
  {
    ptVelocity = pSpriteHitter->GetVelocity();
    if(ptVelocity.x > 0)
      ptVelocity.x = -ptVelocity.x;
    pSpriteHitter->SetVelocity(ptVelocity);
    return true;
  }

  return false;
}

void DoDialog(int Key)
{
  if(g_iMenuPos == KEYCONFIG_MENU && g_pKeyToSet != NULL)
  {
      if(Key != HGEK_ESCAPE)    //Esc key exits out from hitting key. (Handled elsewhere)
      {
          *g_pKeyToSet = Key;
          g_pKeyToSet = NULL;
          g_pGame->ShowMouse();
      }
  }

  //If in last level fighting boss, any keypress advances dialogue
  if(g_iCurrentLevel == 19 && g_iFinalBossDialogLocation < DIALOG_OVER && g_bPlayingGame)
  {
      if(Key == HGEK_ESCAPE)    //ESC doesn't do anything. It's paused anyway. You won't miss anything.
        return;
      if(!g_bIsFalling)
      {
          g_iFinalBossDialogLocation++;
          PlayMySound(IDW_SHOOTFORK);
      }
      //if(Key == HGEK_ESCAPE)
      //  g_iFinalBossDialogLocation = DIALOG_OVER;
  }

  if(!g_bTypeName) //Return if not supposed to be typing
    return;

  //Set blink delay back to 0, so see cursor when we're typing
  g_iBlinkDelay = 0;

  int iCurSlot = 100;
  bool bShift = false;

  for(int i = 0; i < 20; i++)
  {
    if(g_cName[i] == '~')
    {
      iCurSlot = i;
      break;
    }
  }

  //First test and see if user pressed enter
  if(g_pGame->GetAsyncKeyState(HGEK_ENTER))// || JumpKey())
  {
      g_bTypeName = false;
      g_bEnter = true;
      PlayMySound(IDW_GOTFORK);
  }

  //If we're past the end slot (yeah, all this code doesn't make any sense. At least it works. I think.)
  if(iCurSlot == 100)
  {
    if(g_pGame->GetAsyncKeyState(HGEK_BACKSPACE) ||
       g_pGame->GetAsyncKeyState(HGEK_LEFT) ||
       g_pGame->GetAsyncKeyState(HGEK_DELETE))
    {
        g_cName[19] = '~';
        iCurSlot = 19;
    }
    return; //No room for anything else
  }

  if(g_pGame->GetAsyncKeyState(HGEK_SHIFT)) //If pressing shift
  {
    bShift = true;
  }

  //Get the key we're pressing
  switch(Key)
  {
    case 65: //a
      if(bShift)
        g_cName[iCurSlot] = 'A';
      else
        g_cName[iCurSlot] = 'a';
      break;
    case 66:  //Et cetera.........
      if(bShift)
        g_cName[iCurSlot] = 'B';
      else
        g_cName[iCurSlot] = 'b';
      break;
    case 67:
      if(bShift)
        g_cName[iCurSlot] = 'C';
      else
        g_cName[iCurSlot] = 'c';
      break;
    case 68:
      if(bShift)
        g_cName[iCurSlot] = 'D';
      else
        g_cName[iCurSlot] = 'd';
      break;
    case 69:
      if(bShift)
        g_cName[iCurSlot] = 'E';
      else
        g_cName[iCurSlot] = 'e';
      break;
    case 70:
      if(bShift)
        g_cName[iCurSlot] = 'F';
      else
        g_cName[iCurSlot] = 'f';
      break;
    case 71:
      if(bShift)
        g_cName[iCurSlot] = 'G';
      else
        g_cName[iCurSlot] = 'g';
      break;
    case 72:
      if(bShift)
        g_cName[iCurSlot] = 'H';
      else
        g_cName[iCurSlot] = 'h';
      break;
    case 73:
      if(bShift)
        g_cName[iCurSlot] = 'I';
      else
        g_cName[iCurSlot] = 'i';
      break;
    case 74:
      if(bShift)
        g_cName[iCurSlot] = 'J';
      else
        g_cName[iCurSlot] = 'j';
      break;
    case 75:
      if(bShift)
        g_cName[iCurSlot] = 'K';
      else
        g_cName[iCurSlot] = 'k';
      break;
    case 76:
      if(bShift)
        g_cName[iCurSlot] = 'L';
      else
        g_cName[iCurSlot] = 'l';
      break;
    case 77:
      if(bShift)
        g_cName[iCurSlot] = 'M';
      else
        g_cName[iCurSlot] = 'm';
      break;
    case 78:
      if(bShift)
        g_cName[iCurSlot] = 'N';
      else
        g_cName[iCurSlot] = 'n';
      break;
    case 79:
      if(bShift)
        g_cName[iCurSlot] = 'O';
      else
        g_cName[iCurSlot] = 'o';
      break;
    case 80:
      if(bShift)
        g_cName[iCurSlot] = 'P';
      else
        g_cName[iCurSlot] = 'p';
      break;
    case 81:
      if(bShift)
        g_cName[iCurSlot] = 'Q';
      else
        g_cName[iCurSlot] = 'q';
      break;
    case 82:
      if(bShift)
        g_cName[iCurSlot] = 'R';
      else
        g_cName[iCurSlot] = 'r';
      break;
    case 83:
      if(bShift)
        g_cName[iCurSlot] = 'S';
      else
        g_cName[iCurSlot] = 's';
      break;
    case 84:
      if(bShift)
        g_cName[iCurSlot] = 'T';
      else
        g_cName[iCurSlot] = 't';
      break;
    case 85:
      if(bShift)
        g_cName[iCurSlot] = 'U';
      else
        g_cName[iCurSlot] = 'u';
      break;
    case 86:
      if(bShift)
        g_cName[iCurSlot] = 'V';
      else
        g_cName[iCurSlot] = 'v';
      break;
    case 87:
      if(bShift)
        g_cName[iCurSlot] = 'W';
      else
        g_cName[iCurSlot] = 'w';
      break;
    case 88:
      if(bShift)
        g_cName[iCurSlot] = 'X';
      else
        g_cName[iCurSlot] = 'x';
      break;
    case 89:
      if(bShift)
        g_cName[iCurSlot] = 'Y';
      else
        g_cName[iCurSlot] = 'y';
      break;
    case 90:
      if(bShift)
        g_cName[iCurSlot] = 'Z';
      else
        g_cName[iCurSlot] = 'z';
      break;
    case 48:
      g_cName[iCurSlot] = '0';
      break;
    case 49: //Exclaimation point, or 1
      if(bShift)
        g_cName[iCurSlot] = '!';
      else
        g_cName[iCurSlot] = '1';
      break;
    case 50:
      g_cName[iCurSlot] = '2';
      break;
    case 51:
      g_cName[iCurSlot] = '3';
      break;
    case 52:
      g_cName[iCurSlot] = '4';
      break;
    case 53:
      g_cName[iCurSlot] = '5';
      break;
    case 54:
      g_cName[iCurSlot] = '6';
      break;
    case 55:
      g_cName[iCurSlot] = '7';
      break;
    case 56:
      g_cName[iCurSlot] = '8';
      break;
    case 57:
      g_cName[iCurSlot] = '9';
      break;
    case HGEK_SPACE:
      g_cName[iCurSlot] = ' ';
      break;
    case HGEK_BACKSPACE:
    case HGEK_LEFT:
    case HGEK_DELETE:
      if(iCurSlot != 0)
        g_cName[iCurSlot - 1] = '~'; //Kill last character
      break;
  }
}

void UpdateViewport()
{
  myRECT& rcChef = g_pPersonSprite->GetPosition();  //Chef's current position

    //Use old (flat rate) screen-scrolling method
    g_rcViewport.left = rcChef.left - (g_pGame->GetWidth() - g_pPersonSprite->GetWidth()) / 2;
    g_rcViewport.top = rcChef.top - (g_pGame->GetHeight() - g_pPersonSprite->GetHeight()) / 2;

    g_rcViewport.right = g_rcViewport.left + g_pGame->GetWidth();
    g_rcViewport.bottom = g_rcViewport.top + g_pGame->GetHeight();

    KeepViewportOnScreen();
}

void CheckJetboots(void)
{
    if(!g_bJetBoots)
        return;
    if(g_iCurrentLevel == 19 && g_iFinalBossDialogLocation < DIALOG_OVER)
        return;

    if(!(JumpKey()))
    {
        g_bDoubleJumpPressThisCycle = true;
    }

    if(g_iJetbootsTime && g_bDoubleJumpPressThisCycle && JumpKey() && ((g_iJumpAmount != 0 || g_bIsJumping) || g_iFallAmount != 0))
    {
        if(g_bPlayingGame)     //For some reason this could play in the menu before. Weird.
        {
            if(++g_iJetbootSoundDelay >= JETBOOT_SOUND_DELAY)
            {
                if(!g_bDying && !g_bWinning)
                    PlayMySound(IDW_JETBOOTBLAST);
                g_iJetbootSoundDelay = 0;
            }
        }
        if(g_bIsFalling)
        {
            g_iFallAmount-=THRUST_FORCE;
            if(g_iFallAmount <= 0)
            {
                g_bIsFalling = false;
                g_bIsJumping = true;
                g_iJumpAmount = THRUST_FORCE;
            }
        }
        else if(g_bIsJumping)
        {
            g_iJumpAmount += THRUST_FORCE;
            if(g_iJumpAmount > MAX_JUMP_AMOUNT)
                g_iJumpAmount = MAX_JUMP_AMOUNT;
        }
        --g_iJetbootsTime;
    }
}

bool JetbootBlasting()
{
    return ((!g_bDying) && (!g_bWinning) && g_bJetBoots && g_iJetbootsTime && g_bDoubleJumpPressThisCycle && JumpKey() && ((g_iJumpAmount != 0 || g_bIsJumping) || g_iFallAmount != 0));
}

//Play a sound using the game engine
void PlayMySound(string sFilename)
{
    g_pGame->PlaySound(sFilename);
}

//Add this sprite to the list of objects to respawn on death/checkpoint
void AddToRespawn(Sprite* pSprite)
{
    if(g_bDying || g_iCurrentLevel == 19)
        return; //Don't add stuff to respawn queue if you're dying. Just leave them put
    RespawnItem riSprite;
    riSprite.pSprite = pSprite;
    riSprite.ptVelocity = pSprite->GetVelocity();
    riSprite.rcPosition = pSprite->GetPosition();
    riSprite.iCurFrame = pSprite->GetFrame();
    riSprite.iMovePointer = pSprite->GetMovePointer();
    g_lRespawnList.push_front(riSprite);
    SpriteDying(pSprite);   //Make any graphics or whatever for this dying
    pSprite->SetHidden(true);
}

void CheckRespawnBitmap(Sprite* pSprite)    //Make sure to set global booleans right if key is respawned
{
    if(pSprite->GetBitmap() == g_pGreenkeyBitmap)
        g_bGreenKey = false;
    else if(pSprite->GetBitmap() == g_pYellowkeyBitmap)
        g_bYellowKey = false;
    else if(pSprite->GetBitmap() == g_pRedkeyBitmap)
        g_bRedKey = false;
    else if(pSprite->GetBitmap() == g_pOrangekeyBitmap)
        g_bOrangeKey = false;
    else if(pSprite->GetBitmap() == g_pBluekeyBitmap)
        g_bBlueKey = false;
    else if(pSprite->GetBitmap() == g_pNoodleBitmap)
        g_iPointsGotten -= 10;
    else if(pSprite->GetBitmap() == g_pCupcakeBitmap)
        g_iPointsGotten -= 30;
    else if(pSprite->GetBitmap() == g_pEggsBitmap)
        g_iPointsGotten -= 100;
    else if(pSprite->GetBitmap() == g_pGrapeJuiceBitmap)
        g_iPointsGotten -= 50;
    else if(pSprite->GetBitmap() == g_pOrangeBitmap)
        g_iPointsGotten -= 20;
    else if(pSprite->GetBitmap() == g_pMilkshakeBitmap)
        g_iPointsGotten -= 500;
    else if(pSprite->GetBitmap() == g_pJetBootsBitmap ||
            pSprite->GetBitmap() == g_pSuperJetBootsBitmap)
    {
        g_bCurLevelPart = false;
        if(g_iCurrentLevel == 1 && !g_bParts[g_iCurrentLevel])  //If in second level
        {
            g_bJetBoots = false;
            g_pPersonSprite->SetBitmap(g_pPersonBitmap);
        }
        if(g_iCurrentLevel == 16 && !g_bParts[g_iCurrentLevel])  //If in secret level
        {
            g_bSuperJetBoots = false;
            g_pPersonSprite->SetBitmap(g_pPersonJetBoots);
            MAX_JETBOOT_TIME = 15;
        }
    }
    else if(pSprite->GetBitmap() == g_pBlueHubBitmap)
    {
        g_bBlueHub = false;
        pSprite->SetFrame(0);
    }
    else if(pSprite->GetBitmap() == g_pYellowHubBitmap)
    {
        g_bYellowHub = false;
        pSprite->SetFrame(0);
    }
    else if(pSprite->GetBitmap() == g_pOrangeHubBitmap)
    {
        g_bOrangeHub = false;
        pSprite->SetFrame(0);
    }
    else if(pSprite->GetBitmap() == g_pGreenHubBitmap)
    {
        g_bGreenHub = false;
        pSprite->SetFrame(0);
    }
    else if(pSprite->GetBitmap() == g_pRedAmoebaBitmap)    //Regenerate lives of any enemies
        pSprite->SetLives(3);
    else if(pSprite->GetBitmap() == g_pAlienBitmap)
        pSprite->SetLives(2);
    else if(pSprite->GetBitmap() == g_pBlueAmoebaBitmap)
        pSprite->SetLives(2);
    else if(pSprite->GetBitmap() == g_pYellowAmoebaBitmap)
        pSprite->SetLives(20);
    else if(pSprite->GetBitmap() == g_pAlienFighterBitmap)
        pSprite->SetLives(2);
    else if(pSprite->GetBitmap() == g_pAmoebaOozeBitmap)
    {
        Sprite* pSprite2 = new Sprite(g_pAmoebaBlockBitmap, g_rcAll, BA_BOUNCE);
        pSprite2->SetPosition(pSprite->GetPosition().left, pSprite->GetPosition().top);
        pSprite2->SetZOrder(pSprite->GetZOrder());
        g_pGame->AddSprite(pSprite2);
    }
    else if(pSprite->GetBitmap() == g_pMissileHubBitmap)
    {
        pSprite->SetFrame(0);
        if(g_iCurrentLevel == 15)   //Last level
        {
            --g_iMissileCount;
            g_bCurLevelPart = false;
        }
        else
        {
            g_bCurLevelPart = false;
            if(g_pExitSprite != NULL)
            {
                g_pExitSprite->SetBitmap(g_pExitClosedBitmap);  //Close door again on hub respawn
            }
        }
    }
    else if(pSprite->GetBitmap() == g_pBlockingPlatformBitmap)
    {
        g_pBlockingPlatform = pSprite;
    }
    else if(pSprite->GetBitmap() == g_pElectrosphereBitmap) //Pop the got-this-electrosphere flag
        g_lElectrospheresCollected.pop_front(); //We no longer have this one
}

void RespawnAll()
{
    list<RespawnItem>::iterator siSprite;
    //Respawn all respawning objects
    for(siSprite = g_lRespawnList.begin(); siSprite != g_lRespawnList.end(); siSprite++)
    {
        siSprite->pSprite->SetHidden(false);
        siSprite->pSprite->SetPosition(siSprite->rcPosition);
        siSprite->pSprite->SetVelocity(siSprite->ptVelocity);
        siSprite->pSprite->SetFrame(siSprite->iCurFrame);
        siSprite->pSprite->SetMovePointer(siSprite->iMovePointer);
        CheckRespawnBitmap(siSprite->pSprite);
    }
    //And despawn everything that should be
    list<Sprite*>::iterator siDespawn;
    for(siDespawn = g_lDespawnList.begin(); siDespawn != g_lDespawnList.end(); siDespawn++)
    {
        (*siDespawn)->SetZOrder(HACK_ZORDER_NOSPAWN);   //Don't spawn objects off this object.
        (*siDespawn)->Kill();
    }
    //Empty respawn and despawn lists
    EmptyRespawn();
}

void EmptyRespawn()
{
    while(!g_lRespawnList.empty())
    {
        g_lRespawnList.pop_front();
    }
    while(!g_lDespawnList.empty())  //Also empty the despawn list, since we'll always want to do this every time we empty the respawn list.
    {
        g_lDespawnList.pop_front();
    }
}

void AddToDespawn(Sprite* pSprite)
{
    g_lDespawnList.push_front(pSprite);
}

//O(n), but needed for segfault issues
void RemoveFromDespawn(Sprite* pSprite)
{
    for(list<Sprite*>::iterator si = g_lDespawnList.begin(); si != g_lDespawnList.end(); si++)
    {
        if((*si) == pSprite)
        {
            g_lDespawnList.erase(si);
            return;
        }
    }
    ofile << "Error: Fail on despawn remove" << endl;
}

//----------------------------------------------------------------------------
//Functions to deal with the giant enemy crab boss
//----------------------------------------------------------------------------
int GetEnemyCrabFrame()
{
    Bitmap* pB = g_pEnemyCrab->GetBitmap();
    for(int i = 0; i < NUM_BOSS_FRAMES; i++)
        if(pB == g_pCrabBitmap[i])
            return i;
    return -1;
}

float GetMaxBossMoveSpeed()
{
    if(g_iBossHealth <= BOSS_MAX_HEALTH/4.0)
    {
        return BOSS_MAX_MOVE_SPEED + 4.0;
    }
    return BOSS_MAX_MOVE_SPEED;
}

//Keep track of where the boss is in relation to the viewport, and set volume accordingly
void SetBossSoundVol()
{
    //Get center of screen and boss
    myPOINT ptBossCenter = GetCrabCenter();
    myPOINT ptScreenCenter = {(g_pGame->GetWidth() / 2) + g_rcViewport.left, (g_pGame->GetHeight() / 2) + g_rcViewport.top};

    //Now find distance between them
    int xDist = ptBossCenter.x - ptScreenCenter.x;

    int iMaxDist = g_pGame->GetWidth() / 2.0 + 225; //Anything farther away than this will be volume = 0

    if(abs(xDist) > iMaxDist)
    {
        g_pGame->SetSoundVol(g_iBossWalkLoopChannel, 0);
        return;
    }
    float fVol = 1.0 - ((float)(abs(xDist))/(float)(iMaxDist));

    g_pGame->SetSoundVol(g_iBossWalkLoopChannel, fVol * 128.0);// + 128.0);
}

void SetEnemyCrabFrame(int iFrame)
{
    if(iFrame < 0)
    {
        iFrame = 0;
        ofile << "Enemy crab frame too small!" << endl;
    }
    if(iFrame >= NUM_BOSS_FRAMES)
    {
        iFrame = NUM_BOSS_FRAMES - 1;
        ofile << "Enemy crab frame too large!" << endl;
    }
    g_pEnemyCrab->SetBitmap(g_pCrabBitmap[iFrame]);
}

static bool g_bHackBossNoDoubleSpawn = false;
void UpdateBoss()
{
    //Deal with boss sound
    if(g_pEnemyCrab->GetVelocity().x == 0)  //If he's not moving, mute sound
    {
        g_pGame->SetSoundVol(g_iBossWalkLoopChannel, 0);
    }
    else    //Otherwise, make sure it's playing
    {
        //Set sound volume depending on where he is compared to the viewport
        SetBossSoundVol();
    }

    if(g_bBossDying)
    {
        if(g_bWinning)
            g_ptPanTo = g_pPersonSprite->GetCenter();
        else
            g_ptPanTo = g_pEnemyCrab->GetCenter();  //Force camera pan to stay on boss
    }

    //Deal with boss dialog here
    if(g_iFinalBossDialogLocation < DIALOG_OVER)
    {
        SetEnemyCrabFrame(6);
        if(g_iFallAmount < 2)
        {
            //Set dialog text and image
            DisplayMsg(g_sFinalBossDialog[g_iFinalBossDialogLocation], 200);
            g_pDiagBmp = g_pDialogProfileBmp[0];
            g_iDialogBitmapAlign = DIALOG_BITMAP_ALIGN_LEFT;
            if(g_iFinalBossDialogLocation == 1)
            {
                g_pDiagBmp = g_pDialogProfileBmp[1];
                g_iDialogBitmapAlign = DIALOG_BITMAP_ALIGN_RIGHT;
            }
        }

        return; //Stop here; don't update boss if we're inside the boss dialog
    }

    if(!(--g_iTimeLeftInThisAction))
    {
        SetNextBossAction();
    }
    else
    {
        UpdateBossFrame();
    }
    g_iBossJumpDelay++; //Update boss jumping delay
    //See if crab should jump up at player
    myPOINT ptChef = GetPlayerCenter();
    myPOINT ptCrab = GetCrabCenter();
    if(ptCrab.y - ptChef.y > 325 &&
       abs(ptCrab.x - ptChef.x) < g_pEnemyCrab->GetWidth() / 4)   //Greater than 5 blocks above, and right below
    {
        if(g_iCurBossAction != BOSS_JUMP && g_iBossJumpDelay > BOSS_JUMP_DELAY)
        {
            g_iCurBossAction = BOSS_JUMP;
            g_iCurBossJumpAmt = -BOSS_JUMP_AMOUNT;
            //Roar jump noise
            PlayMySound(IDW_CRABJUMP);
            g_iTimeLeftInThisAction = MAX_TIME;
            g_iBossFrame = -1;
            UpdateBossFrame();  //Update frame for next animation
            g_iBossJumpDelay = 0;
        }
    }

    //action-specific stuff
    myPOINT ptCurVel = g_pEnemyCrab->GetVelocity();
    if(g_iCurBossAction != BOSS_SPAWNAMOEBA)
        g_bHackBossNoDoubleSpawn = false;
    switch(g_iCurBossAction)
    {
        case BOSS_HULKSMASH:
            //nothing special, just stop moving
            if(ptCurVel.x < 0)
                ptCurVel.x = min(ptCurVel.x + BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            if(ptCurVel.x > 0)
                ptCurVel.x = max(ptCurVel.x - BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            break;
        case BOSS_AVOIDPLAYER:
        {
            //Accelerate away from player
            int iAmount = -GetFastestWayToPlayer() * BOSS_ACCELERATE_AMOUNT/4;
            if(rand() % 5)
                ptCurVel.x += iAmount;
            if(ptCurVel.x > GetMaxBossMoveSpeed())
                ptCurVel.x = GetMaxBossMoveSpeed();
            if(ptCurVel.x < -GetMaxBossMoveSpeed())
                ptCurVel.x = -GetMaxBossMoveSpeed();
            if(g_iBossEnemiesKilled >= g_iBossEnemiesSpawned)
            {
                SetNextBossAction();
            }
            break;
        }
        case BOSS_JUMP:
            //Stop moving in the x direction
            if(ptCurVel.x < 0)
                ptCurVel.x = min(ptCurVel.x + BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            if(ptCurVel.x > 0)
                ptCurVel.x = max(ptCurVel.x - BOSS_MAX_MOVE_SPEED/3.0, 0.0);

            //Jump in the y direction if not crouching
            if(g_pEnemyCrab->GetPosition().top <= g_iBossFloorYLevel)
            {
                ptCurVel.y = g_iCurBossJumpAmt;
                g_iCurBossJumpAmt += BOSS_GRAVITY_AMOUNT;
                if(g_bBossDying)
                {
                    unsigned char cAlpha = 255;
                    float fAlphaMultiplier = (float)g_iCurBossJumpAmt / (float)-BOSS_JUMP_AMOUNT;
                    cAlpha *= fAlphaMultiplier;
                    if(ptCurVel.y < 0 && g_iCurBossJumpAmt >= 0) //Reached apex of jump
                    {
                        DoWon(g_pPersonSprite); //Finish level
                        g_iWinCount = 60;
                        cAlpha = 0;
                    }
                    g_pEnemyCrab->SetAlpha(cAlpha);
                }
                else
                    g_pEnemyCrab->SetAlpha(255);
            }
            else
            {
                ptCurVel.y = 0;
                g_pEnemyCrab->SetPosition(g_pEnemyCrab->GetPosition().left, g_iBossFloorYLevel);
                SetNextBossAction();
            }
            break;
        case BOSS_PAUSE:
            //Stop moving
            if(ptCurVel.x < 0)
                ptCurVel.x = min(ptCurVel.x + BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            if(ptCurVel.x > 0)
                ptCurVel.x = max(ptCurVel.x - BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            break;
        case BOSS_RUNL:
            ptCurVel.x = max((float)(ptCurVel.x - BOSS_ACCELERATE_AMOUNT), -GetMaxBossMoveSpeed());    //Accelerate left
            break;
        case BOSS_RUNR:
            ptCurVel.x = min((float)(ptCurVel.x + BOSS_ACCELERATE_AMOUNT), GetMaxBossMoveSpeed());     //Accelerate right
            break;
        case BOSS_SPAWNAMOEBA:
            //Stop moving
            if(ptCurVel.x < 0)
                ptCurVel.x = min(ptCurVel.x + BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            if(ptCurVel.x > 0)
                ptCurVel.x = max(ptCurVel.x - BOSS_MAX_MOVE_SPEED/3.0, 0.0);
            if(GetEnemyCrabFrame() == 6 && !g_bHackBossNoDoubleSpawn)   //Crouching
            {
                g_bHackBossNoDoubleSpawn = true;
                g_iBossEnemiesSpawned = rand() % 3+1;
                g_iBossEnemiesKilled = 0;
                for(int i = 0; i < g_iBossEnemiesSpawned; i++)
                {
                  // Create blue amoebas
                  BubbleBlowerSprite* pBBSprite = new BubbleBlowerSprite(g_pBlueAmoebaBitmap, g_rcAll, BA_DIE);
                  pBBSprite->SetZOrder(2);
                  pBBSprite->SetLives(2);  //Two lives
                  pBBSprite->SetPosition(GetCrabCenter().x + (rand() % 100) - 50, GetCrabCenter().y + (rand() % 100) - 50);
                  pBBSprite->SetNumFrames(10);
                  if(rand() % 2 == 1)   //Set his starting direction randomly
                    pBBSprite->SetVelocity(-5, 0);
                  else
                    pBBSprite->SetVelocity(5, 0);
                  g_pGame->AddSprite(pBBSprite);
                }
            }
            break;
        default:
            break;

    }
    g_pEnemyCrab->SetVelocity(ptCurVel);
}

void SetNextBossAction()
{
    switch(g_iCurBossAction)
    {
        case BOSS_HULKSMASH:
            if(GetFastestWayToPlayer() < 0)
                g_iCurBossAction = BOSS_RUNL;
            else
                g_iCurBossAction = BOSS_RUNR;
            g_iTimeLeftInThisAction = 200;  //Run for 5 seconds
            //Play roar sound as runs towards player
            PlayMySound(IDW_CRABROAR);
            break;
        case BOSS_RUNL:
            g_iCurBossAction = BOSS_SPAWNAMOEBA;
            g_iTimeLeftInThisAction = MAX_TIME; //Until animation is done
            break;
        case BOSS_RUNR:
            g_iCurBossAction = BOSS_SPAWNAMOEBA;
            g_iTimeLeftInThisAction = MAX_TIME;
            break;
        case BOSS_AVOIDPLAYER:
            g_iCurBossAction = BOSS_PAUSE;
            g_iTimeLeftInThisAction = 80;   //Pause for 2 seconds
            break;
        case BOSS_SPAWNAMOEBA:
            g_iCurBossAction = BOSS_AVOIDPLAYER;
            g_iTimeLeftInThisAction = MAX_TIME; //Avoid player until they kill amoebas
            break;
        case BOSS_PAUSE:
            if(g_bBossDying)
            {
                g_iCurBossAction = BOSS_JUMP;
                g_iCurBossJumpAmt = -BOSS_JUMP_AMOUNT;
                //Dying noise
                PlayMySound(IDW_CRABROAR);
                g_iTimeLeftInThisAction = MAX_TIME;
                g_iBossFrame = -1;
                g_iBossJumpDelay = 0;
                break;
            }
            g_iCurBossAction = BOSS_HULKSMASH;
            g_iTimeLeftInThisAction = MAX_TIME;
            break;
        case BOSS_JUMP:
            g_iCurBossAction = BOSS_PAUSE;
            g_iTimeLeftInThisAction = 80;   //Pause for 2 seconds
            break;
    }
    g_iBossAvoidToleranceCount = 0;
    g_iBossFrame = -1;
    UpdateBossFrame();  //Update frame for next animation
}

void UpdateBossFrame()
{
    if(++g_iBossFrameDelayCounter < g_pEnemyCrab->GetFrameDelay())
        return;     //Don't increment frame if frame delay is set and not exheeded

    g_iBossFrameDelayCounter = 0;
    g_pEnemyCrab->SetFrameDelay(1); //Reset frame delay and counter

    g_iBossFrame++; //Go to next frame
    switch(g_iCurBossAction)
    {
        case BOSS_HULKSMASH:
            switch(g_iBossFrame)
            {
                case 0:
                    SetEnemyCrabFrame(6);
                    break;
                case 1:
                    SetEnemyCrabFrame(16);
                    g_pEnemyCrab->SetFrameDelay(20);
                    break;
                case 2:
                    SetEnemyCrabFrame(6);
                    break;
                case 3:
                    SetEnemyCrabFrame(17);
                    break;
                default:
                    SetNextBossAction();
                    break;
            }
            break;
        case BOSS_RUNL:
            switch(g_iBossFrame)
            {
                case 0:
                    SetEnemyCrabFrame(10);
                    break;
                case 1:
                    SetEnemyCrabFrame(12);
                    break;
                case 2:
                    SetEnemyCrabFrame(11);
                    break;
                case 3:
                    SetEnemyCrabFrame(12);
                    break;
                default:
                    SetEnemyCrabFrame(10);
                    g_iBossFrame = 0;   //Loop back around
                    break;
            }
            break;
        case BOSS_RUNR:
            switch(g_iBossFrame)
            {
                case 0:
                    SetEnemyCrabFrame(13);
                    break;
                case 1:
                    SetEnemyCrabFrame(15);
                    break;
                case 2:
                    SetEnemyCrabFrame(14);
                    break;
                case 3:
                    SetEnemyCrabFrame(15);
                    break;
                default:
                    SetEnemyCrabFrame(13);
                    g_iBossFrame = 0;
                    break;
            }
            break;
        case BOSS_AVOIDPLAYER:
            if(g_pEnemyCrab->GetVelocity().x < 0)   //Going left
            {
                switch(g_iBossFrame)
                {
                    case 0:
                        SetEnemyCrabFrame(0);
                        break;
                    case 1:
                        SetEnemyCrabFrame(2);
                        break;
                    case 2:
                        SetEnemyCrabFrame(1);
                        break;
                    case 3:
                        SetEnemyCrabFrame(2);
                        break;
                    default:
                        SetEnemyCrabFrame(0);
                        g_iBossFrame = 0;
                        break;
                }
            }
            else                                    //Going right
            {
                switch(g_iBossFrame)
                {
                    case 0:
                        SetEnemyCrabFrame(3);
                        break;
                    case 1:
                        SetEnemyCrabFrame(5);
                        break;
                    case 2:
                        SetEnemyCrabFrame(4);
                        break;
                    case 3:
                        SetEnemyCrabFrame(5);
                        break;
                    default:
                        SetEnemyCrabFrame(3);
                        g_iBossFrame = 0;
                        break;
                }
            }
            break;
        case BOSS_SPAWNAMOEBA:
            switch(g_iBossFrame)
            {
                case 0:
                    SetEnemyCrabFrame(6);
                    break;
                case 1:
                    SetEnemyCrabFrame(7);
                    break;
                case 2:
                    SetEnemyCrabFrame(6);
                    break;
                default:
                    SetNextBossAction();
                    break;
            }
            break;
        case BOSS_PAUSE:
            SetEnemyCrabFrame(6);
            break;
        case BOSS_JUMP:
            if(g_pEnemyCrab->GetPosition().top < g_iBossFloorYLevel)
            {
                if(g_pEnemyCrab->GetVelocity().y < 0)
                    SetEnemyCrabFrame(8);
                else
                    SetEnemyCrabFrame(9);
                break;
            }
            SetEnemyCrabFrame(7);
            break;
    }
}

static myPOINT ptResult;
myPOINT& GetCrabCenter()
{
    myRECT rcCrab = g_pEnemyCrab->GetPosition();
    ptResult.x = (rcCrab.right - rcCrab.left) / 2 + rcCrab.left;
    ptResult.y = (rcCrab.bottom - rcCrab.top) / 2 + rcCrab.top;
    return ptResult;
}

static myPOINT ptResult2;
myPOINT& GetPlayerCenter()
{
    myRECT rcPlayer = g_pPersonSprite->GetPosition();
    ptResult2.x = (rcPlayer.right - rcPlayer.left) / 2 + rcPlayer.left;
    ptResult2.y = (rcPlayer.bottom - rcPlayer.top) / 2 + rcPlayer.top;
    return ptResult2;
}

int GetFastestWayToPlayer()          //Returns <0 for left, >0 for right
{
    int iAreaWidth = g_rcAll.right - g_rcAll.left;
    int iStraightDistance, iWrapDistance;
    myPOINT ptPlayer = GetPlayerCenter();
    myPOINT ptCrab = GetCrabCenter();
    if(ptCrab.x > ptPlayer.x)   //Crab is to the right of the player
    {
        iStraightDistance = ptCrab.x - ptPlayer.x;
        iWrapDistance = ptPlayer.x + (iAreaWidth - ptCrab.x);
        if(iWrapDistance < iStraightDistance)
            return 1;
        return -1;
    }
    else                        //Crab is left of the player
    {
        iStraightDistance = ptPlayer.x - ptCrab.x;
        iWrapDistance = ptCrab.x + (iAreaWidth - ptPlayer.x);
        if(iWrapDistance < iStraightDistance)
            return -1;
        return 1;
    }

    //Unreachable
    return 0;
}

void GetClawRects(myRECT* rcClaw1, myRECT* rcClaw2)
{
    int iFrame = GetEnemyCrabFrame();
    if(iFrame <= 7) //First six frames of scuttling back and forth, & crouching
    {
        rcClaw1->top = rcClaw2->top = 171;
        rcClaw1->bottom = rcClaw2->bottom = 229;
        rcClaw1->left = 182;
        rcClaw1->right = rcClaw1->left + 85;
        rcClaw2->left = 314;
        rcClaw2->right = rcClaw2->left + 85;
        if(iFrame == 7) //Crouching frame is offset 10 pixels
        {
            rcClaw1->top += 10;
            rcClaw1->bottom += 10;
            rcClaw2->top += 10;
            rcClaw2->bottom += 10;
        }
    }
    //Arms raised frames
    else if(iFrame == 8 ||
            iFrame == 9 ||
            iFrame == 16)
    {
        rcClaw1->top = rcClaw2->top = 11;
        rcClaw1->bottom = rcClaw2->bottom = 91;
        rcClaw1->left = 173;
        rcClaw1->right = rcClaw1->left + 55;
        rcClaw2->left = 348;
        rcClaw2->right = rcClaw2->left + 55;
    }
    //Attack left
    else if(iFrame >= 10 && iFrame <= 12)
    {
        rcClaw2->top = 171;
        rcClaw2->bottom = 229;
        rcClaw2->left = 182;
        rcClaw2->right = rcClaw1->left + 85;
        rcClaw1->left = 12;
        rcClaw1->right = rcClaw2->left + 76;
        rcClaw1->top = 205;
        rcClaw1->bottom = 262;
    }
    //Attack right
    else if(iFrame >= 13 && iFrame <= 15)
    {
        rcClaw1->top = 171;
        rcClaw1->bottom = 229;
        rcClaw1->left = 182;
        rcClaw1->right = rcClaw1->left + 85;
        rcClaw2->top = 215;
        rcClaw2->bottom = rcClaw2->top + 57;
        rcClaw2->left = 499;
        rcClaw2->right = rcClaw2->left + 75;
    }
    //Groundpound
    else
    {
        rcClaw1->top = 288;
        rcClaw1->bottom = rcClaw1->top + 61;
        rcClaw1->left = 170;
        rcClaw1->right = rcClaw1->left + 71;
        rcClaw2->top = 289;
        rcClaw2->bottom = rcClaw2->top + 51;
        rcClaw2->left = 337;
        rcClaw2->right = rcClaw2->left + 73;
    }
    //Now turn these relative coordinates into global coordinates
    myRECT rcCrab = g_pEnemyCrab->GetPosition();
    rcClaw1->top += rcCrab.top;
    rcClaw1->bottom += rcCrab.top;
    rcClaw2->top += rcCrab.top;
    rcClaw2->bottom += rcCrab.top;
    rcClaw1->left += rcCrab.left;
    rcClaw1->right += rcCrab.left;
    rcClaw2->left += rcCrab.left;
    rcClaw2->right += rcCrab.left;
    //Done
}

static myRECT rcBody;
myRECT& GetBodyRect()
{
    //Collision rectangles for this guy are hardcoded
    rcBody.left = 188;
    rcBody.right = 386;
    rcBody.top = 140;
    rcBody.bottom = 246;

    if(GetEnemyCrabFrame() == 7)   //Crouching frame - body is ten pixels lower
    {
        rcBody.top = 150;
        rcBody.bottom = 256;
    }

    //Get the actual rect from this relative rect
    myRECT rcCrab = g_pEnemyCrab->GetPosition();
    rcBody.left += rcCrab.left;
    rcBody.right += rcCrab.left;
    rcBody.top += rcCrab.top;
    rcBody.bottom += rcCrab.top;

    return rcBody;
}

//Get the place to save savegames and config data. I store both in one location, because I think it's stupid to store config
//in a different place than savegames. Games shouldn't explode tiny pieces of themselves all over the hard drive. Deal with it.
//Everything is in the savegame folder.
string GetSavegameLocation()
{
    string sFilename;
#ifdef BUILD_FOR_LINUX
    //return "savegame/";
    char * cLoc = getenv("XDG_DATA_HOME");
    if(cLoc == NULL)    //Failed to get the location
    {
        //Just grab user dir, and append what we need to it
        sFilename = ttvfs::GetUserDir() + "/.local/share/ChefBereft/";
    }
    else
    {
        sFilename = cLoc;
        sFilename += "/ChefBereft/";
    }
#elif defined BUILD_FOR_MAC
    //TODO: Test and see if it should be "Saved\\ Games" instead of "Saved Games", or if that breaks TTVFS or something
    sFilename = ttvfs::GetUserDir() + "/Documents/Saved Games/ChefBereft/";
#else
    //For Windows, use Saved Games folder, no matter if we're using XP or not. Deal with it, XP users.
    sFilename = ttvfs::GetUserDir() + "/Saved Games/Chef Bereft/";
    //Note that this only works with the English localization of Windows, but I don't want to deal
    // with Unicode anyway.
#endif
    return sFilename;
}

bool LeftKey()
{
    if(g_pGame->GetAsyncKeyState(LEFT_KEY) || g_jsCurrJoyState & JOY_LEFT)
        return true;
    return false;
}

bool RightKey()
{
    if(g_pGame->GetAsyncKeyState(RIGHT_KEY) || g_jsCurrJoyState & JOY_RIGHT)
        return true;
    return false;
}

bool JumpKey()
{
    if((g_pGame->GetAsyncKeyState(JUMP_KEY) || g_jsCurrJoyState & JOY_FIRE1))
    {
        return true;
    }
    return false;
}

bool FireKey()
{
    if(g_pGame->GetAsyncKeyState(FIRE_KEY) || g_jsCurrJoyState & JOY_FIRE2)
        return true;
    return false;
}

bool UpKey()
{
    if(g_pGame->GetAsyncKeyState(UP_KEY) || g_jsCurrJoyState & JOY_UP)
        return true;
    return false;
}

bool DownKey()
{
    if(g_pGame->GetAsyncKeyState(DOWN_KEY) || g_jsCurrJoyState & JOY_DOWN)
        return true;
    return false;
}

void KeepViewportOnScreen()
{
    if(g_rcViewport.left < g_rcAll.left)       //if past the left side
    {
      g_rcViewport.left = g_rcAll.left;
      g_rcViewport.right = g_rcViewport.left + g_pGame->GetWidth();
    }
    if(g_rcViewport.right > g_rcAll.right)       //if past the right side
    {
      g_rcViewport.right = g_rcAll.right;
      g_rcViewport.left = g_rcViewport.right - g_pGame->GetWidth();
    }
    if(g_rcViewport.top < g_rcAll.top)       //if past the top
    {
      g_rcViewport.top = g_rcAll.top;
      g_rcViewport.bottom = g_rcViewport.top + g_pGame->GetHeight();
    }
    if(g_rcViewport.bottom > g_rcAll.bottom)       //if past the bottom
    {
      g_rcViewport.bottom = g_rcAll.bottom;
      g_rcViewport.top = g_rcViewport.bottom - g_pGame->GetHeight();
    }

    //Now crop if it's a larger screen than viewport area
    g_bIncompleteViewport = false;
    if(g_rcViewport.left < g_rcAll.left)
    {
        g_rcViewport.left = g_rcAll.left;
        g_bIncompleteViewport = true;
    }
    if(g_rcViewport.right > g_rcAll.right)
    {
        g_rcViewport.right = g_rcAll.right;
        g_bIncompleteViewport = true;
    }
    if(g_rcViewport.top < g_rcAll.top)
    {
        g_rcViewport.top = g_rcAll.top;
        g_bIncompleteViewport = true;
    }
    if(g_rcViewport.bottom > g_rcAll.bottom)
    {
        g_rcViewport.bottom = g_rcAll.bottom;
        g_bIncompleteViewport = true;
    }
}

void HandlePauseMenuItem(int iItem)
{
    //Decide what to do about it
    switch (iItem)
    {
        case 0: //Continue game
            g_bPaused = false;
            g_pGame->HideMouse();
            break;
        case 1: //Last checkpoint
            if(g_b3D)
            {
                DisplayMsg("Checkpoints disabled in 3D mode", 2000);
                break;
            }
            g_bPaused = false;
            g_pGame->HideMouse();
            if(g_bWinning)
                break;
            if(g_iCurrentLevel == 19) //Boss level
            {
                g_rcViewport.left = 0;
                g_rcViewport.top = 0;
                g_rcViewport.right = g_pGame->GetWidth();
                g_rcViewport.bottom = g_pGame->GetHeight();
                EmptyRespawn();
                g_iDieCount = 30;      //Reset dying counter
                g_bDying = false;     //He's not dying anymore
                g_pPersonSprite->SetHidden(false);
                StartLevel();   //Restart whole thing
            }
            else
            {
                g_iDieCount = 30;      //Reset dying counter
                g_bDying = false;     //He's not dying anymore
                g_pPersonSprite->SetHidden(false);
                g_pPersonSprite->SetPosition(g_rcLastCheckpointPos.left, g_rcLastCheckpointPos.top);
                CopyRect(&g_rcAll,&g_rcLastViewport);
                g_pPersonSprite->SetVelocity(0,0);        //No movement
                g_iFallAmount = g_iJumpAmount = 0;
                RespawnAll();                             //Make player get all items again that he had to get before
                g_iCheckpointInvincibility = CHECKPOINT_INVINCIBLE_TIME;      //Start checkpoint invincibility timer
                g_ptPanTo.x = g_ptPanTo.y = -1;
//                if(g_pGame->GetFrameRate() != FRAME_RATE)
//                    g_pGame->SetFrameRate(FRAME_RATE);
            }
            break;
        case 2: // Options
            g_iCameToOptionsFrom = -1;
            g_iMenuPos = OPTIONS_MENU;
            g_iMenuPosSelect = -1;
            LoadOptionsMenuStuff();
            break;
        case 3: // End Level
            EndLevel();
            break;
        case 4: // Main menu
            g_bPaused = false;
            EndLevel();
            g_iMenuPos = MAIN_MENU;
            g_iMenuPosSelect = -1;
            break;
        default:
            break;
    }
}

void AddBgParticles()
{
    if(g_pBackground[0] == NULL)
        return;
      for(int i = 0; STARBGS[i] != -1; i++)
      {
          for(int j = 0; j < NUMSTARS_SPACEBG; j++)
          {
              myPOINT ptPos = {rand() % (int)(g_rcAll.right - g_rcAll.left), rand() % (int)(g_rcAll.bottom - g_rcAll.top)};
              g_pBackground[STARBGS[i]]->AddParticles(PARTICLE_STAR_TWINKLE, BITMAP_PARTICLES, 0, 25, 25, 25, ptPos);
          }
      }
}

void RemoveBgParticles()
{
    if(g_pBackground[0] == NULL)
        return;
      for(int i = 0; STARBGS[i] != -1; i++)
      {
          g_pBackground[STARBGS[i]]->RemoveParticles(); //Remove any particles
      }
}

//Read config data from our XML file
void ReadConfig()
{
    int iScreenWidth, iScreenHeight;
    bool bFullscreen = true;
    bool bError = false;
    string sConfigFile = GetSavegameLocation() + "config.xml";
    FILE* fp = fopen(sConfigFile.c_str(), "rb");
    if(!fp)
    {
        ofile << "Note: No config.xml, using defaults." << endl;
        return;     //We don't care
    }
    XMLDocument xmlConfig;
    xmlConfig.LoadFile(fp);
    if(xmlConfig.ErrorID() != 0)
    {
        ofile << "Error parsing config.xml." << endl;
        fclose(fp);
        return;
    }
    XMLElement* root = xmlConfig.RootElement();
    if(root == NULL)
    {
        ofile << "Error: config.xml missing root element." << endl;
        fclose(fp);
        return;
    }

    //These three HAVE to be read in. If something else doesn't work, we don't really care.
    XMLElement* temp = root->FirstChildElement("screenwidth");
    if(temp == NULL || temp->QueryIntAttribute("value", &iScreenWidth) != XML_NO_ERROR)
    {
        ofile << "screenwidth malformed" << endl;
        bError = true;
    }
    temp = root->FirstChildElement("screenheight");
    if(temp == NULL || temp->QueryIntAttribute("value", &iScreenHeight) != XML_NO_ERROR)
    {
        ofile << "screenheight malformed" << endl;
        bError = true;
    }
    temp = root->FirstChildElement("fullscreen");
    if(temp != NULL)
        temp->QueryBoolAttribute("enabled", &bFullscreen);

    //Don't care about errors for the rest of these
    temp = root->FirstChildElement("savegameslot");
    if(temp != NULL)
        temp->QueryIntAttribute("value", &g_iGame);   //Current savegame slot

    //Volume of sound and music
    int iTemp;
    bool bTemp;
    temp = root->FirstChildElement("sound");
    if(temp != NULL && temp->QueryIntAttribute("vol", &iTemp) == XML_NO_ERROR)
        g_pGame->SetSoundVol(iTemp);
    if(temp != NULL && temp->QueryBoolAttribute("enabled", &bTemp) == XML_NO_ERROR)
        g_pGame->SetSound(bTemp);
    temp = root->FirstChildElement("music");
    if(temp != NULL && temp->QueryIntAttribute("vol", &iTemp) == XML_NO_ERROR)
        g_pGame->SetMusicVol(iTemp);
    if(temp != NULL && temp->QueryBoolAttribute("enabled", &bTemp) == XML_NO_ERROR)
        g_pGame->SetMusic(bTemp);

    //Fps and vsync
    temp = root->FirstChildElement("vsync");
    if(temp != NULL && temp->QueryBoolAttribute("enabled", &bTemp) == XML_NO_ERROR)
        g_pGame->SetVsync(bTemp);
    temp = root->FirstChildElement("framerate");
    if(temp != NULL && temp->QueryIntAttribute("value", &iTemp) == XML_NO_ERROR)
        g_pGame->SetFrameRate(iTemp);

    //Joystick stuff, just in case they want a different joystick than default, or if it's falsely detecting one (so they can disable)
    temp = root->FirstChildElement("joystick");
    if(temp != NULL && temp->QueryIntAttribute("sticknumber", &iTemp) == XML_NO_ERROR)
        g_pGame->UseJoystick(iTemp-1);
    if(temp != NULL && temp->QueryBoolAttribute("enabled", &bTemp) == XML_NO_ERROR)
        g_pGame->SetForceJoyDisable(!bTemp);

    //Key config
    XMLElement* keyconfig = root->FirstChildElement("keys");
    if(keyconfig != NULL)
    {
        temp = keyconfig->FirstChildElement("jump");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &JUMP_KEY);

        temp = keyconfig->FirstChildElement("fire");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &FIRE_KEY);

        temp = keyconfig->FirstChildElement("left");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &LEFT_KEY);

        temp = keyconfig->FirstChildElement("right");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &RIGHT_KEY);

        temp = keyconfig->FirstChildElement("up");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &UP_KEY);

        temp = keyconfig->FirstChildElement("down");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &DOWN_KEY);

        int iTempJoyKey = 1;
        temp = keyconfig->FirstChildElement("joyjump");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &iTempJoyKey);
        g_pGame->SetJoystickFire1(iTempJoyKey-1);

        iTempJoyKey = 2;
        temp = keyconfig->FirstChildElement("joyfire");
        if(temp != NULL)
            temp->QueryIntAttribute("value", &iTempJoyKey);
        g_pGame->SetJoystickFire2(iTempJoyKey-1);

    }

    //Set to this resolution and such
    if(!bError)
        g_pGame->SetInitialResolution(iScreenWidth, iScreenHeight, bFullscreen);
    else
    {
        ofile << "Using defaults for screen resolution" << endl;
        g_pGame->SetInitialFullscreen(bFullscreen);
    }

    fclose(fp);
}

void WriteConfig()
{
    string sConfigFile = GetSavegameLocation() + "config.xml";
    FILE* fp = fopen(sConfigFile.c_str(), "wb");
    if(!fp)
    {
        ofile << "Write access to " << sConfigFile << " denied. Cannot write config." << endl;
        return;
    }
    XMLDocument xmlConfig;
    char defaultconfig[1024];
    sprintf(defaultconfig, "<!-- Warning: only edit these values if you know what you're doing. -->"
                           "<?xml version=\"1.0\"?>"
                           "<config>"
                           "<screenwidth value=\"%d\" />"
                           "<screenheight value=\"%d\" />"
                           "<fullscreen enabled=\"%s\" />"
                           "<savegameslot value=\"%d\" />"
                           "<sound vol=\"%d\" enabled=\"%s\" />"
                           "<music vol=\"%d\" enabled=\"%s\" />"
                           "<vsync enabled=\"%s\" />"
                           "<framerate value=\"%d\" />"
                           "<joystick enabled=\"%s\" sticknumber=\"%d\" />"
                           "<keys>"
                           "<jump value=\"%d\" />"
                           "<fire value=\"%d\" />"
                           "<left value=\"%d\" />"
                           "<right value=\"%d\" />"
                           "<up value=\"%d\" />"
                           "<down value=\"%d\" />"
                           "<joyjump value=\"%d\" />"
                           "<joyfire value=\"%d\" />"
                           "</keys>"
                           "</config>",
            g_pGame->GetWidth(),
            g_pGame->GetHeight(),
            ((g_pGame->GetFullscreen())?("true"):("false")),
            g_iGame,
            g_pGame->GetSoundVol(),
            ((g_pGame->GetSound())?("true"):("false")),
            g_pGame->GetMusicVol(),
            ((g_pGame->GetMusic())?("true"):("false")),
            ((g_pGame->GetVsync())?("true"):("false")),
            g_pGame->GetFrameRate(),
            ((g_pGame->GetForceJoyDisable())?("false"):("true")),
            g_pGame->GetJoystickIndex() + 1,
            JUMP_KEY,
            FIRE_KEY,
            LEFT_KEY,
            RIGHT_KEY,
            UP_KEY,
            DOWN_KEY,
            g_pGame->GetJoystickFire1() + 1,
            g_pGame->GetJoystickFire2() + 1);
    //ofile << "Writing config:" << endl << defaultconfig << endl << endl;

    if(xmlConfig.Parse(defaultconfig) != XML_NO_ERROR)
        ofile << "Error parsing XML string to write." << endl;
    else if(xmlConfig.SaveFile(fp) != XML_NO_ERROR)
        ofile << "Error saving config.xml. Back up everything important on your hard disk, because it may die soon." << endl;
    fclose(fp);
}

void EndLevel()
{
    if(!g_b3D)
    {
        g_pBackground[g_iCurrentLevel]->RemoveParticles();
        g_pGame->UncacheBitmaps();
    }
    if(g_b3D)
    {
        //End 3D level thing
        g_b3D = false;
        ShutdownACK();
    }
    g_iMenuPosSelect = NO_MENU;
    g_bPaused = false;
    g_rcViewport.left = 0;
    g_rcViewport.top = 0;
    g_rcViewport.right = g_pGame->GetWidth();
    g_rcViewport.bottom = g_pGame->GetHeight();
    g_bPlayingGame = false;
    //Respawn points gotten
    g_iPointsGotten = g_iPointsLevelStart;
//    ShowCursor(true);     //Show mouse cursor again
    g_pGame->CleanupSong();   //Stop the MIDI playing
    RespawnAll();
    EmptyRespawn();
    g_pLastCheckpoint = NULL;
    //Play the menu music
//    if(!g_bNoSound)
        g_pGame->PlaySong(MUSIC_MENU);
    if(g_iCurrentLevel == 1 && !g_bParts[g_iCurrentLevel])
    {
        g_bJetBoots = false;
    }
    if(g_iCurrentLevel == 16 && !g_bParts[g_iCurrentLevel])
    {
        if(!g_bParts[1])
            g_bJetBoots = false;    //If they didn't finish level 1, don't give them jet boots
        g_bSuperJetBoots = false;
        MAX_JETBOOT_TIME = 15;
    }
    if(g_iCurrentLevel == 19)
        g_pGame->KillChannel(g_iBossWalkLoopChannel);
}

string GetMenuText(int Menu, int MenuPos)
{
    switch(Menu)
    {
        case MAIN_MENU:
            switch(MenuPos)
            {
                case -1:
                {
                    int i = -1;
                    for(i = 0; i < 21; i++)
                    {
                        if(g_cName[i] == '~')
                        {
                            g_cName[i] = '\0';
                            break;
                        }
                    }
                    string sReturn = g_cName;
                    if(i != -1)
                        g_cName[i] = '~';
                    return sReturn;
                }
                case MAINMENU_OPTIONS:
                    return "Options";
                case MAINMENU_NEWGAME:
                    return "New Game";
                case MAINMENU_CONTINUE:
                    return "Continue";
                case MAINMENU_HIGHSCORES:
                    return "High Scores";
                case MAINMENU_QUIT:
                    return "Quit";
                case MAINMENU_PROFILES:
                    return "Change Profile";
            }
            break;
        case PROFILES_MENU:
            switch(MenuPos)
            {
                case PROFILEMENU_PROFILE1:
                case PROFILEMENU_PROFILE2:
                case PROFILEMENU_PROFILE3:
                    if(g_sProfileNames[MenuPos] == "*")
                        return "(empty)";
                    return g_sProfileNames[MenuPos];
                case PROFILEMENU_BACK:
                    return "Back to Main Menu";
                case PROFILEMENU_DEL1:
                    if(g_sProfileNames[0] == "*")
                        return "";
                    return ("Delete \"" + g_sProfileNames[0] + "\"");
                case PROFILEMENU_DEL2:
                    if(g_sProfileNames[1] == "*")
                        return "";
                    return ("Delete \"" + g_sProfileNames[1] + "\"");
                case PROFILEMENU_DEL3:
                    if(g_sProfileNames[2] == "*")
                        return "";
                    return ("Delete Profile");
                case -1:
                    return "Profile Select";
            }
            break;
        case OPTIONS_MENU:
            switch(MenuPos)
            {
                case -1:
                {
                    myPOINT ptMouse = g_ptLastMousePos; //HACK: test using last mouse pos in rect
                    ptMouse.x -= (g_pGame->GetWidth() - g_pGameScreen[0]->GetWidth()) / 2.0;
                    ptMouse.y -= (g_pGame->GetHeight() - g_pGameScreen[0]->GetHeight()) / 2.0;
                    if(PtInRect(&g_rcOptionsSliderPos[0], &ptMouse))
                    {
                        return "Sound Volume";
                    }
                    if(PtInRect(&g_rcOptionsSliderPos[1], &ptMouse))
                    {
                        return "Music Volume";
                    }
                    return "Options";
                }
                case OPTIONSMENU_SOUNDSLIDER:
                    return "Sound Volume";
                case OPTIONSMENU_MUSICSLIDER:
                    return "Music Volume";
                case OPTIONSMENU_BACK:
                    if(g_iCameToOptionsFrom == MAIN_MENU)
                        return "Back to Main Menu";
                    return "Back to Game";
                case OPTIONSMENU_FULLSCREENTOGGLE:
                    return "Toggle Fullscreen";
                case OPTIONSMENU_KEYCONFIGMENU:
                    return "Key Config";
                case OPTIONSMENU_MUSICTOGGLE:
                    return "Music On/Off";
                case OPTIONSMENU_RESOLUTIONMENU:
                    return "Display Resolution";
                case OPTIONSMENU_SOUNDTOGGLE:
                    return "Sound On/Off";
            }
            break;
        case RESOLUTION_MENU:
            switch(g_iMenuPosSelect)
            {
                case RESOLUTIONMENU_UP:
                    return "Scroll Up";
                case RESOLUTIONMENU_DOWN:
                    return "Scroll Down";
                case RESOLUTIONMENU_BACK:
                    return "Back to Options Menu";
                default:
                    return "Resolution Select";
            }
            break;
        case KEYCONFIG_MENU:
            switch(g_iMenuPosSelect)
            {
                case KEYCONFIG_UP:
                    return "Up";
                case KEYCONFIG_DOWN:
                    return "Down";
                case KEYCONFIG_LEFT:
                    return "Left";
                case KEYCONFIG_RIGHT:
                    return "Right";
                case KEYCONFIG_JUMP:
                    return "Jump";
                case KEYCONFIG_FIRE:
                    return "Fire";
                case KEYCONFIG_BACK:
                    return "Back to Options Menu";
                case -1:
                    return "Key Config";
            }
    }
    return "";
}

void LoadOptionsMenuStuff()
{
    //Create sprites for candle particles
    for(int i = 0; i < 3; i++)
    {
        myRECT rc = {0,0,g_pGame->GetWidth(), g_pGame->GetHeight()};
        myPOINT ptOffset = {0,0};
        g_pFlameSprites[i] = new Sprite(g_pMissileExplodeBitmap, rc, BA_BOUNCE);
        g_pFlameSprites[i]->SetPosition(g_ptOptionsFlamePos[i]);
        g_pFlameSprites[i]->AddParticles(PARTICLE_CANDLEFLAME, BITMAP_PARTICLES, 25, 25, 25, 25, ptOffset);
        g_pFlameSprites[i]->FireParticles();
        g_pGame->AddSprite(g_pFlameSprites[i]);
    }
}

void HandleMenuSelect(int Menu, int MenuPos)
{
    switch(Menu)
    {
        case MAIN_MENU:
            switch(MenuPos)
            {
                case MAINMENU_OPTIONS:
                    //Options menu init stuffz
                    g_iMenuPos = OPTIONS_MENU;
                    g_iCameToOptionsFrom = MAIN_MENU;
                    g_iMenuPosSelect = -1;
                    //Load options-menu candles
                    LoadOptionsMenuStuff();
                    break;
                case MAINMENU_NEWGAME:
                    //Start a new game
                    NewGame();
                    g_iMenuPos = -1;
                    g_iMenuPosSelect = NO_MENU;
                    break;
                case MAINMENU_CONTINUE: //Continue where we left off
                    g_iMenuPos = -1;
                    g_iMenuPosSelect = NO_MENU;
                    break;
                case MAINMENU_HIGHSCORES:   //High Scores
                    g_bHighScores = true;
                    g_iCameToHighScoresFrom = g_iMenuPos;
                    g_iMenuPos = -1;
                    g_iMenuPosSelect = NO_MENU;
                    break;
                case MAINMENU_QUIT: //End game
                    g_pGame->End();
                    break;
                case MAINMENU_PROFILES:
                    SaveGame(); //Save game first, so profiles stuff works
                    g_iMenuPos = PROFILES_MENU;
                    g_iMenuPosSelect = -1;
                    //Load names
                    LoadProfileNames();
                    break;
            }
            break;
        case PROFILES_MENU:
            if(g_bDeleteWarning)
            {
                if(MenuPos == DELETE_YES)
                    DeleteProfile(g_iProfileToDelete);
                g_bDeleteWarning = false;
                g_iMenuPosSelect = -1;
                break;
            }
            switch(MenuPos)
            {
                case -1:
                    break;
                case PROFILEMENU_BACK:  //Back to main menu
                    g_iMenuPos = MAIN_MENU;
                    g_iMenuPosSelect = -1;
                    //Kill particles for flames
                    for(int i = 0; i < NUM_PROFILES; i++)
                        g_pFlameSprites[i]->Kill();
                    //Save config
                    WriteConfig();
                    break;
                case PROFILEMENU_PROFILE1:
                case PROFILEMENU_PROFILE2:
                case PROFILEMENU_PROFILE3:
                    //Select profile
                    g_iGame = MenuPos + 1;  //Set to right profile
                    if(!RedeemGame())   //Load this save
                        NewGame();  //Start new game if couldn't load save (save corrupted or not there)
                    //Go back to main menu
                    g_iMenuPos = MAIN_MENU;
                    g_iMenuPosSelect = -1;
                    //Kill particles for flames
                    for(int i = 0; i < NUM_PROFILES; i++)
                        g_pFlameSprites[i]->Kill();
                    break;
                case PROFILEMENU_DEL1:
                case PROFILEMENU_DEL2:
                case PROFILEMENU_DEL3:
                    if(!exists(MenuPos - 3))
                        break;  //Don't delete a profile that isn't here
                    g_iProfileToDelete = MenuPos - 3;
                    g_iMenuPosSelect = -1;
                    g_bDeleteWarning = true;
                    break;
            }
            break;
        case OPTIONS_MENU:
            switch(MenuPos)
            {
                case OPTIONSMENU_FULLSCREENTOGGLE:
                    //Test to see if display resolution is supported
                    if(!g_pGame->GetFullscreen())   //But only if we're going into fullscreen mode
                    {
                        g_bConfirmResolution = true;
                        g_iOldHeight = g_pGame->GetHeight();
                        g_iOldWidth = g_pGame->GetWidth();
                        g_bOldFullscreen = g_pGame->GetFullscreen();
                        g_iMenuPosSelect = -1;
                        g_iStartedCountdownAt = g_pGame->GetTime();
                    }
                    g_pGame->ToggleFullscreeen();
                    break;
                case OPTIONSMENU_SOUNDTOGGLE:
                    g_pGame->ToggleSound();
                    break;
                case OPTIONSMENU_MUSICTOGGLE:
                    g_pGame->ToggleMusic();
                    break;
                case OPTIONSMENU_KEYCONFIGMENU:
                    g_iMenuPos = KEYCONFIG_MENU;
                    g_iMenuPosSelect = NO_MENU;
                    for(int i = 0; i < NUM_PROFILES; i++)
                        g_pFlameSprites[i]->Kill();
                    WriteConfig();
                    break;
                case OPTIONSMENU_RESOLUTIONMENU:
                    //Resolution select menu
                    g_iMenuPos = RESOLUTION_MENU;
                    g_iMenuPosSelect = -1;
                    for(int i = 0; i < NUM_PROFILES; i++)
                        g_pFlameSprites[i]->Kill();
                    WriteConfig();
                    break;
                case OPTIONSMENU_BACK:
                    g_iMenuPos = g_iCameToOptionsFrom;
                    g_iMenuPosSelect = -1;
                    for(int i = 0; i < NUM_PROFILES; i++)
                        g_pFlameSprites[i]->Kill();
                    WriteConfig();
                    break;
            }
            break;
        case RESOLUTION_MENU:
            switch(g_iMenuPosSelect)
            {
                case RESOLUTIONMENU_UP:
                    if(--g_iResolutionScrollPos < -3)
                        g_iResolutionScrollPos = -3;
                    break;
                case RESOLUTIONMENU_DOWN:
                    if(++g_iResolutionScrollPos > g_pGame->GetNumScreenModes() - RESOLUTION_NUM_DRAW + 3)
                        g_iResolutionScrollPos = g_pGame->GetNumScreenModes() - RESOLUTION_NUM_DRAW + 3;
                    break;
                case RESOLUTIONMENU_BACK:
                    g_iMenuPos = OPTIONS_MENU;
                    g_iMenuPosSelect = -1;
                    LoadOptionsMenuStuff();
                    WriteConfig();
                    break;
                default:
                    //Select resolution
                    if(g_iMenuPosSelect != -1)
                    {
                        int iPos = g_iMenuPosSelect + g_iResolutionScrollPos - 2;
                        if(iPos < 0 ||
                           iPos > g_pGame->GetNumScreenModes())
                            break;
                        int iWidth, iHeight;
                        //ofile << iPos << endl;
                        g_pGame->GetScreenMode(iPos, &iWidth, &iHeight);
                        if(iWidth < 800 ||
                           iHeight < 600)
                        {
                            DisplayMsg("Error: screen resolutions below 800x600 are not supported.");
                            break;  //Stop here. We don't support resolutions below 800x600
                        }
                        //ofile << iWidth << "x" << iHeight << endl << endl;
                        //Test for a bit, and reset unless they press OK or such
                        if(g_pGame->GetFullscreen())
                        {
                            g_bConfirmResolution = true;
                            g_iOldHeight = g_pGame->GetHeight();
                            g_iOldWidth = g_pGame->GetWidth();
                            g_bOldFullscreen = g_pGame->GetFullscreen();
                            g_iMenuPosSelect = -1;
                            g_iStartedCountdownAt = g_pGame->GetTime();
                        }
                        //Change the resolution
                        g_pGame->ChangeResolution(iWidth, iHeight, g_pGame->GetFullscreen());
                    }
                    break;
            }
            break;
        case KEYCONFIG_MENU:
            switch(MenuPos)
            {
                case KEYCONFIG_BACK:
                    g_iMenuPos = OPTIONS_MENU;
                    g_iMenuPosSelect = -1;
                    g_bEscPressed = true;
                    LoadOptionsMenuStuff();
                    WriteConfig();
                    break;
                case KEYCONFIG_UP:
                    g_pKeyToSet = &UP_KEY;
                    g_pGame->HideMouse();
                    break;
                case KEYCONFIG_DOWN:
                    g_pKeyToSet = &DOWN_KEY;
                    g_pGame->HideMouse();
                    break;
                case KEYCONFIG_LEFT:
                    g_pKeyToSet = &LEFT_KEY;
                    g_pGame->HideMouse();
                    break;
                case KEYCONFIG_RIGHT:
                    g_pKeyToSet = &RIGHT_KEY;
                    break;
                case KEYCONFIG_JUMP:
                    g_pKeyToSet = &JUMP_KEY;
                    g_pGame->HideMouse();
                    break;
                case KEYCONFIG_FIRE:
                    g_pKeyToSet = &FIRE_KEY;
                    g_pGame->HideMouse();
                    break;
            }
            break;
    }
}

void LoadProfileNames()
{
    for(int i = 0; i < NUM_PROFILES; i++)
    {
        //Create sprites for particles
        myRECT rc = {0,0,g_pGame->GetWidth(), g_pGame->GetHeight()};
        myPOINT ptOffset = {0,0};
        g_pFlameSprites[i] = new Sprite(g_pMissileExplodeBitmap, rc, BA_BOUNCE);
        g_pFlameSprites[i]->SetPosition(g_ptProfileFlamePos[i]);
        g_pFlameSprites[i]->AddParticles(PARTICLE_CANDLEFLAME, BITMAP_PARTICLES, 25, 25, 25, 25, ptOffset);
        //g_pFlameSprites[i]->SetHidden(true);
        g_pGame->AddSprite(g_pFlameSprites[i]);


        char cData[64];
        sprintf(cData, "Profile%d.CB3", i+1);
        string sFilename = GetSavegameLocation() + cData;

        FILE* hFile = fopen(sFilename.c_str(), "rb");
        if (hFile == NULL)  //File isn't there
        {
            g_sProfileNames[i] = "*";
            continue;
        }

        //Read in the name
        if(fread(cData, 1, 21, hFile) != 21)
        {
            //Couldn't read in filename --- malformed profile
            fclose(hFile);
            g_sProfileNames[i] = "*";
            continue;
        }

        //Terminate it correctly
        for(int j = 0; j < 21; j++)
        {
            if(cData[j] == '~')
            {
                cData[j] = '\0';
                break;
            }
        }

        //Store name
        g_sProfileNames[i] = cData;
        g_pFlameSprites[i]->FireParticles();   //This profile is here
        fclose(hFile);
    }
}

void DeleteProfile(int iProfile)
{
    if(iProfile == -1)
        return;

    char cData[64];
    sprintf(cData, "Profile%d.CB3", iProfile);

    string sFilename = GetSavegameLocation() + cData;

    //ofile << "Deleting profile " << sFilename << endl;
    if(unlink(sFilename.c_str()))  //Delete the file
        ofile << "Warning: Unable to delete profile " << sFilename << endl;

    //Now refresh profile data
    //Kill particles for flames
    for(int i = 0; i < NUM_PROFILES; i++)
        g_pFlameSprites[i]->Kill();
    //Load names
    LoadProfileNames();

    //Now test and see if this was our profile
    if(iProfile == g_iGame)
    {
        //Find a profile to switch to, so we don't accidentally create a new profile from this one
        int iProfileToGoto = -1;
        for(int i = 0; i < NUM_PROFILES; i++)
        {
            g_iGame = i;
            if(RedeemGame())
            {
                iProfileToGoto = i;
                break;
            }
        }
        if(iProfileToGoto == -1)    //If there's no profiles, reset to profile 1
            g_iGame = 1;
    }
}

//Thanks to ropez from the cplusplus forum http://www.cplusplus.com/forum/general/1796/
bool exists(int iProfile)
{
  char cData[64];
  sprintf(cData, "Profile%d.CB3", iProfile);
  string sFilename = GetSavegameLocation() + cData;

  ifstream ifile(sFilename.c_str());
  return ifile;
}

int GetSliderLoc(int iSlider)
{
    if(iSlider == 0)
        return ((float)(g_pGame->GetSoundVol()) * 339.0)/(128.0);
    return ((float)(g_pGame->GetMusicVol()) * 385.0)/(128.0);
}

void PlayPointsNoise()
{
    switch (rand() % 4)
    {
        case 0:
            PlayMySound(IDW_GETPOINTS_1);
            break;
        case 1:
            PlayMySound(IDW_GETPOINTS_2);
            break;
        case 2:
            PlayMySound(IDW_GETPOINTS_3);
            break;
        case 3:
            PlayMySound(IDW_GETPOINTS_4);
            break;
    }
}

void ResetResolution()
{
    g_pGame->ChangeResolution(g_iOldWidth, g_iOldHeight, g_bOldFullscreen);
    g_bConfirmResolution = false;
    g_iMenuPosSelect = -1;
}

void DisplayMsg(string sStr, int iMilliseconds)
{
    g_sDialogText = sStr;
    g_iStartedDisplayDiag = g_pGame->GetTime();
    g_iDisplayDiagTime = iMilliseconds;
    g_pDiagBmp = NULL;
}

void StartCountdown()
{
    g_iStartedCountdownAt = g_pGame->GetTime();
}

bool CheckElectrosphere(myPOINT pt) //Make sure we haven't gotten this electrosphere already before we spawn it
{
    for(list<myPOINT>::iterator i = g_lElectrospheresCollected.begin(); i != g_lElectrospheresCollected.end(); i++)
    {
        if((*i).x == pt.x && (*i).y == pt.y)
            return true;
    }
    return false;
}

bool CheckElectrosphere(int x, int y)
{
    myPOINT pt = {x,y};
    return CheckElectrosphere(pt);
}

void EmptyElectrosphereList()
{
    while(g_lElectrospheresCollected.size())
        g_lElectrospheresCollected.pop_front();
}

void ReadInBgStory()
{
    for(int i = 0; i < NUM_BGSTORY_PANELS; i++)
    {
        g_sBgStory[i] = "";
        char cFilename[256];
        sprintf(cFilename, "res/bgstory/bgstory%d.CB3", i+1);
        VFILE* hFile = vfopen(cFilename, "rb");
        if (hFile == NULL)
        {
            ofile << "Error: Can't read in background story file " << cFilename << " for some reason. Bg story dialog may cause a crash." << endl;
            continue;   //Something went wrong, but try to ignore
        }

        //Read in the bg story
        for(;;)
        {
            char cTemp = '\0';
            if(vfread(&cTemp, sizeof(cTemp), 1, hFile) != 1)
            {
              vfclose(hFile);
              break;   //End if we read last char
            }

            g_sBgStory[i].push_back(cTemp);    //Append this character to the end
        }
    }

    //Also read in endgame story while we're at it
    for(int i = 0; i < NUM_EGSTORY_PANELS; i++)
    {
        g_sEndgameStory[i] = "";
        char cFilename[256];
        sprintf(cFilename, "res/bgstory/end%d.CB3", i+1);
        VFILE* hFile = vfopen(cFilename, "rb");
        if (hFile == NULL)
        {
            ofile << "Error: Can't read in endgame story file " << cFilename << " for some reason. Endgame story dialog may cause a crash." << endl;
            continue;   //Something went wrong, but try to ignore
        }

        //Read in the story
        for(;;)
        {
            char cTemp = '\0';
            if(vfread(&cTemp, sizeof(cTemp), 1, hFile) != 1)
            {
              vfclose(hFile);
              break;   //End if we read last char
            }

            g_sEndgameStory[i].push_back(cTemp);    //Append this character to the end
        }
    }
}

bool ShouldUnchache(Bitmap* pBitmap)
{
    for(int i = 0; i < 20; i++)
    {
      if(i == 16 || i == 17 || i == 18)
        continue;
      if(pBitmap == g_pGameScreen[i])
        return false;
    }
    return true;
}

