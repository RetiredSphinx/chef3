//-----------------------------------------------------------------
// Background Object
// C++ Source - Background.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "Background.h"
#include "GameEngine.h"
#include <algorithm>
#include <fstream>
using namespace std;
extern ofstream ofile;

extern int g_iCurrentLevel;
extern GameEngine* g_pGame;
extern HGE* hge;
//-----------------------------------------------------------------
// Background Constructor(s)/Destructor
//-----------------------------------------------------------------
Background::Background(int iWidth, int iHeight, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
  // Initialize the member variables
  m_iWidth = iWidth;
  m_iHeight = iHeight;
  m_r = r;
  m_g = g;
  m_b = b;
  m_a = a;
  m_pBitmap = NULL;
  m_fParallax = 1.0;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
//  m_bIsTiled = false;
}

Background::Background(Bitmap* pBitmap)
{
  // Initialize the member variables
//  m_crColor = 0;
  m_pBitmap = pBitmap;
  m_pBitmap->AddUser();
  m_iWidth = pBitmap->GetWidth();
  m_iHeight = pBitmap->GetHeight();
  m_fParallax = 1.0;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
  //if(m_iWidth <= 200)
  //{
  //  m_bIsTiled = true;
  //}
  //else
  //{
  //  m_bIsTiled = false;
  //}
}

Background::~Background()
{
    if(m_pBitmap != NULL)
        m_pBitmap->RemoveUser();
    RemoveParticles();
}

//-----------------------------------------------------------------
// Background General Methods
//-----------------------------------------------------------------
void Background::Update(myRECT rcView)
{
  for(list<hgeParticleSystem*>::iterator i = m_lParticles.begin(); i != m_lParticles.end(); i++)
  {
      //myPOINT pt = GetParticleDrawPos(i, rcView);
      //InflateRect(&rcView, 150, 150);
      //if(PtInRect(&rcView, &pt))

      //Update all
      (*i)->Update(hge->Timer_GetDelta());
  }
}

void Background::Draw(myRECT rcView, int ixOffset, int iyOffset)
{

//  int Width;
//  int Height;

  // Draw the background
  if (m_pBitmap != NULL)
  {
      int ixStart, iyStart, ixStop, iyStop;

      ixStart = (int)((float)rcView.left / m_fParallax) % m_iWidth;   //Calculate the starting and stopping positions on the bitmap to draw
      iyStart = (int)((float)rcView.top / m_fParallax) % m_iHeight;
      ixStop = m_iWidth - ixStart;
      iyStop = m_iHeight - iyStart;
      m_pBitmap->DrawPart(ixOffset, iyOffset, ixStart, iyStart, ixStop, iyStop);  //Draw the upper left corner

      for(int i = 0; (i * m_iWidth) + ixStop < (rcView.right - rcView.left); i++)   //Then the top side
      {
        m_pBitmap->DrawPart(ixOffset + (i * m_iWidth) + ixStop, iyOffset, 0, iyStart, m_iWidth, m_iHeight - iyStart);
      }

      for(int i = 0; (i * m_iHeight) + iyStop < (rcView.bottom - rcView.top); i++)  //Then the left side
      {
        m_pBitmap->DrawPart(ixOffset, iyOffset + (i * m_iHeight) + iyStop, ixStart, 0, m_iWidth - ixStart, m_iHeight);
      }

      for(int i = 0; (i * m_iWidth) + ixStop < (rcView.right - rcView.left); i++)  //And finally, the rest
      {
        for(int j = 0; (j * m_iHeight) + iyStop < (rcView.bottom - rcView.top); j++)
        {
          m_pBitmap->Draw(ixOffset + (i * m_iWidth) + ixStop, iyOffset + (j * m_iHeight) + iyStop);
        }
      }
    //} //Whew! But it works great!
  }
  else
  {
    myRECT    rect = rcView;
    if(rect.right > (m_iWidth - 1))
      rect.right = m_iWidth - 1;
    if(rect.bottom > (m_iHeight - 1))
      rect.bottom = m_iHeight - 1;
    if(rect.left < 0)
      rect.left = 0;
    if(rect.top < 0)
      rect.top = 0;

    rect.top -= rcView.top;
    rect.bottom -= rcView.top;
    rect.left -= rcView.left;
    rect.right -= rcView.left;

    //Check to stay inside viewport offsets
    if(rect.left < ixOffset)
      rect.left = ixOffset;
    if(rect.top < iyOffset)
      rect.top = iyOffset;

    rect.bottom += iyOffset;
    rect.right += ixOffset;

    //HBRUSH  hBrush = CreateSolidBrush(m_crColor);
//    ofile << rect.left << " " << rect.top << " " << rect.right << " " << rect.bottom << endl;
    g_pGame->FillRect(&rect, m_r, m_g, m_b, m_a);
    //DeleteObject(hBrush);
  }

  //OK! Now draw particles!
  for(list<hgeParticleSystem*>::iterator i = m_lParticles.begin(); i != m_lParticles.end(); i++)
  {
      myRECT rc = rcView;
      InflateRect(&rc, 150, 150);
      myPOINT pt = GetParticleDrawPos(i, rcView);
      myPOINT ptTest = pt;
      ptTest.x += rcView.left;
      ptTest.y += rcView.top;
      if(PtInRect(&rc, &ptTest))
      {
          myPOINT ptOrig;
          (*i)->GetPosition(&(ptOrig.x),&(ptOrig.y));
          (*i)->MoveTo(pt.x + ixOffset, pt.y + iyOffset, true);
          //(*i)->MoveTo(250, 50, true);
          (*i)->Render();
          (*i)->MoveTo(ptOrig.x, ptOrig.y, true);
      }
  }
  //Done
}

void Background::AddParticles(string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptPos, bool bAdditive)
{
    //Create textures and such only if we haven't before
    if(!m_ParticleTex)
    {
        m_ParticleTex = hge->Texture_Load(sImageFilename.c_str());
        if(!m_ParticleTex)
        {
            ofile << "Error loading " << sImageFilename << " for bg particle system" << endl;
            return;
        }
    }
    //Create sprite
    if(m_ParticleSprite == NULL)
    {
        m_ParticleSprite = new hgeSprite(m_ParticleTex, x, y, w, h);
        if(m_ParticleSprite == NULL)
            ofile << "NULL particle sprite for bg" << endl;
        m_ParticleSprite->SetHotSpot(w / 2.0, h / 2.0); //Center
        if(bAdditive)
            m_ParticleSprite->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE);
    }
    hgeParticleSystem* hgePSys = new hgeParticleSystem(sParticleFilename.c_str(), m_ParticleSprite);
    if(hgePSys == NULL)
        ofile << "NULL particle system for bg" << endl;
    hgePSys->MoveTo(ptPos.x, ptPos.y);
    hgePSys->Fire();
    m_lParticles.push_back(hgePSys);
}

void Background::RemoveParticles()
{
    for(list<hgeParticleSystem*>::iterator i = m_lParticles.begin(); i != m_lParticles.end(); i++)
    {
        delete (*i);
    }
    if(m_ParticleSprite != NULL)
        delete m_ParticleSprite;
    if(m_ParticleTex)
        hge->Texture_Free(m_ParticleTex);
    m_ParticleSprite = NULL;
    m_ParticleTex = 0;
    while(!m_lParticles.empty())
        m_lParticles.pop_front();
}

myPOINT Background::GetParticleDrawPos(list<hgeParticleSystem*>::iterator i, myRECT rcView)
{
    myPOINT ptOrig, pt;
      myRECT rc = rcView;
      InflateRect(&rc, 150, 150);   //Account for some particles being possibly out of the view by 2 tile widths (past that I care not)
      (*i)->GetPosition(&(ptOrig.x),&(ptOrig.y));
      pt.x = ptOrig.x - rcView.left;
      pt.y = ptOrig.y - rcView.top;
      float fXDist = (rcView.right - rcView.left) / 2.0 - pt.x;
      float fYDist = (rcView.bottom - rcView.top) / 2.0 - pt.y;
      fXDist *= -1.0 / m_fParallax;
      fYDist *= -1.0 / m_fParallax;
      pt.x = (rcView.right - rcView.left) / 2.0 + fXDist;
      pt.y = (rcView.bottom - rcView.top) / 2.0 + fYDist;
      return pt;
}




