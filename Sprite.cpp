//-----------------------------------------------------------------
// Sprite Object
// C++ Source - Sprite.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "Sprite.h"
#include <algorithm>
using namespace std;

#include <fstream>
extern ofstream ofile;

#include <SDL.h>
#include "GameEngine.h"
extern GameEngine* g_pGame;

//-----------------------------------------------------------------
// External but neccassary variables
//-----------------------------------------------------------------

extern float g_iFallAmount;
extern float g_iJumpAmount;
extern bool g_bIsFalling;
extern bool g_bIsJumping;
extern void DieChef(int iDieTime = 30);
extern int  g_iJetbootsTime;
extern int MAX_JETBOOT_TIME;
extern HGE* hge;
extern Sprite* g_pPersonSprite;

//extern void SpriteParticleManage(Sprite* pSprite, string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptOffset, bool bAdditive);

//-----------------------------------------------------------------
// Sprite Constructor(s)/Destructor
//-----------------------------------------------------------------
Sprite::Sprite(Bitmap* pBitmap)
{
  // Initialize the member variables
  m_bHorizFrames = false;
  m_pBitmap = pBitmap;
  m_pBitmap->AddUser();
  m_iNumFrames = 1;
  m_iCurFrame = m_iFrameDelay = m_iFrameTrigger = 0;
  SetRect(&m_rcPosition, 0, 0, pBitmap->GetWidth(), pBitmap->GetHeight());
  CalcCollisionRect();
  m_ptVelocity.x = m_ptVelocity.y = 0;
  m_iZOrder = 0;
  m_iLives = 1;
  SetRect(&m_rcBounds, 0, 0, 640, 480);
  m_baBoundsAction = BA_STOP;
  m_bHidden = false;
  m_bDying = false;
  m_bOneCycle = false;
  m_bIsJumping = false;
  m_iDripCounter = (rand() % 60);
  m_iMovePointer = (rand() % 100);
  if(m_pBitmap->GetWidth() == 75)
  {
    m_iDripCounter = 0;
    m_iMovePointer = 0;
  }
  m_iCounter = 0;
  m_Particles = NULL;
  m_bParticlesInFront = false;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
  m_ptMoveTo.x = 0;
  m_ptMoveTo.y = 0;
  m_ptParticleOffset.x = m_ptParticleOffset.y = 0;
  m_bAlwaysDrawParticles = false;
  m_iTileParticlesXCount = m_iTileParticlesXIncrement = 0;
  m_bDieWhenParticlesDone = false;
  m_iFlashCount = 0;
  m_ptOrigPos.x = m_ptOrigPos.y = -1;
//  m_iLastParticleUpdate = SDL_GetTicks();
}

Sprite::Sprite(Bitmap* pBitmap, myRECT& rcBounds, BOUNDSACTION baBoundsAction)
{
  // Calculate a random position
  int iXPos = rand() % (int)(rcBounds.right - rcBounds.left);
  int iYPos = rand() % (int)(rcBounds.bottom - rcBounds.top);
  m_bHorizFrames = false;

  // Initialize the member variables
  m_pBitmap = pBitmap;
  m_pBitmap->AddUser();
  m_iNumFrames = 1;
  m_iCurFrame = m_iFrameDelay = m_iFrameTrigger = 0;
  SetRect(&m_rcPosition, iXPos, iYPos, iXPos + pBitmap->GetWidth(),
    iYPos + pBitmap->GetHeight());
  CalcCollisionRect();
  m_ptVelocity.x = m_ptVelocity.y = 0;
  m_iZOrder = 0;
  m_iLives = 1;
  CopyRect(&m_rcBounds, &rcBounds);
  m_baBoundsAction = baBoundsAction;
  m_bHidden = false;
  m_bDying = false;
  m_bOneCycle = false;
  m_bIsJumping = false;
  m_iDripCounter = (rand() % 60);
  m_iMovePointer = (rand() % 100);
  if(m_pBitmap->GetWidth() == 75)
  {
    m_iDripCounter = 0;
    m_iMovePointer = 0;
  }
  m_iCounter = 0;
  m_Particles = NULL;
  m_bParticlesInFront = false;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
  m_ptMoveTo.x = 0;
  m_ptMoveTo.y = 0;
  m_ptParticleOffset.x = m_ptParticleOffset.y = 0;
  m_bAlwaysDrawParticles = false;
  m_iTileParticlesXCount = m_iTileParticlesXIncrement = 0;
  m_bDieWhenParticlesDone = false;
  m_iFlashCount = 0;
  m_ptOrigPos.x = m_ptOrigPos.y = -1;
//  m_iLastParticleUpdate = SDL_GetTicks();
}

Sprite::Sprite(Bitmap* pBitmap, myPOINT ptPosition, myPOINT ptVelocity, int iZOrder,
    myRECT& rcBounds, BOUNDSACTION baBoundsAction)
{
  // Initialize the member variables
  m_bHorizFrames = false;
  m_pBitmap = pBitmap;
  m_pBitmap->AddUser();
  m_iNumFrames = 1;
  m_iCurFrame = m_iFrameDelay = m_iFrameTrigger = 0;
  SetRect(&m_rcPosition, ptPosition.x, ptPosition.y,
    ptPosition.x + pBitmap->GetWidth(), ptPosition.y + pBitmap->GetHeight());
  CalcCollisionRect();
  m_ptVelocity = ptVelocity;
  m_iZOrder = iZOrder;
  m_iLives = 1;
  CopyRect(&m_rcBounds, &rcBounds);
  m_baBoundsAction = baBoundsAction;
  m_bHidden = false;
  m_bDying = false;
  m_bOneCycle = false;
  m_bIsJumping = false;
  m_iDripCounter = (rand() % 60);
  m_iMovePointer = (rand() % 100);
  if(m_pBitmap->GetWidth() == 75)
  {
    m_iDripCounter = 0;
    m_iMovePointer = 0;
  }
  m_iCounter = 0;
  m_Particles = NULL;
  m_bParticlesInFront = false;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
  m_ptMoveTo.x = 0;
  m_ptMoveTo.y = 0;
  m_ptParticleOffset.x = m_ptParticleOffset.y = 0;
  m_bAlwaysDrawParticles = false;
  m_iTileParticlesXCount = m_iTileParticlesXIncrement = 0;
  m_bDieWhenParticlesDone = false;
  m_iFlashCount = 0;
  m_ptOrigPos.x = m_ptOrigPos.y = -1;
//  m_iLastParticleUpdate = SDL_GetTicks();
}

Sprite::~Sprite()
{
  m_pBitmap->RemoveUser();
  RemoveParticles();
}

//-----------------------------------------------------------------
// Sprite General Methods
//-----------------------------------------------------------------
SPRITEACTION Sprite::Update(float fTimestep)
{
  //float fDiff = 0.0;

  if(m_iFlashCount)
  {
        m_iFlashCount--;    //Decrement our counter for flashing this sprite a different color
  }

  // See if the sprite needs to be killed
  if (m_bDying)
    return SA_KILL;

  if(m_Particles != NULL)
  {
      //int iTempTime = SDL_GetTicks();
      //fDiff = iTempTime - m_iLastParticleUpdate;
      m_Particles->Update(1.0 / (float)(g_pGame->GetFrameRate()));//fDiff / 1000.0);//hge->Timer_GetDelta());
//      m_iLastParticleUpdate = iTempTime;
      //If we're supposed to die when this particle system is done, and it's done
      if(m_bDieWhenParticlesDone && !(m_Particles->GetParticlesAlive()) && (m_Particles->GetAge() == -2.0))
      {
          m_bDying = true;  //Draw last amount of particles, don't die right now
      }
  }

  if(m_bHidden)
    return SA_NONE;

  // Update the position, but only if we have to
  if(m_ptVelocity.x == 0 && m_ptVelocity.y == 0 && m_baBoundsAction != BA_STOP)
    return SA_NONE; //Don't move if there's nothing to move

  myPOINT ptNewPosition, ptSpriteSize, ptBoundsSize;
  ptNewPosition.x = m_rcPosition.left + m_ptVelocity.x*fTimestep;
  ptNewPosition.y = m_rcPosition.top + m_ptVelocity.y*fTimestep;
  ptSpriteSize.x = m_rcPosition.right - m_rcPosition.left;
  ptSpriteSize.y = m_rcPosition.bottom - m_rcPosition.top;
  ptBoundsSize.x = m_rcBounds.right - m_rcBounds.left;
  ptBoundsSize.y = m_rcBounds.bottom - m_rcBounds.top;

  // Check the bounds
  // Wrap?
  if (m_baBoundsAction == BA_WRAP)
  {
    if ((ptNewPosition.x + ptSpriteSize.x) < m_rcBounds.left)
      ptNewPosition.x = m_rcBounds.right;
    else if (ptNewPosition.x > m_rcBounds.right)
      ptNewPosition.x = m_rcBounds.left - ptSpriteSize.x;
    if ((ptNewPosition.y + ptSpriteSize.y) < m_rcBounds.top)
      ptNewPosition.y = m_rcBounds.bottom;
    else if (ptNewPosition.y > m_rcBounds.bottom)
      ptNewPosition.y = m_rcBounds.top - ptSpriteSize.y;
  }
  // Bounce?
  else if (m_baBoundsAction == BA_BOUNCE)
  {
    bool bBounce = false;
    myPOINT ptNewVelocity = m_ptVelocity;
    if (ptNewPosition.x < m_rcBounds.left)
    {
      bBounce = true;
      ptNewPosition.x = m_rcBounds.left;
      ptNewVelocity.x = -ptNewVelocity.x;
    }
    else if ((ptNewPosition.x + ptSpriteSize.x) > m_rcBounds.right)
    {
      bBounce = true;
      ptNewPosition.x = m_rcBounds.right - ptSpriteSize.x;
      ptNewVelocity.x = -ptNewVelocity.x;
    }
    if (ptNewPosition.y < m_rcBounds.top)
    {
      bBounce = true;
      ptNewPosition.y = m_rcBounds.top;
      ptNewVelocity.y = -ptNewVelocity.y;
      StopJump();                             //Stop the jumping
    }
    else if ((ptNewPosition.y + ptSpriteSize.y) > m_rcBounds.bottom)
    {
      bBounce = true;
      ptNewPosition.y = m_rcBounds.bottom - ptSpriteSize.y;
      ptNewVelocity.y = -ptNewVelocity.y;
    }
    if (bBounce)
      SetVelocity(ptNewVelocity);
  }
  // Die?
  else if (m_baBoundsAction == BA_DIE)
  {
    if ((ptNewPosition.x + ptSpriteSize.x) < m_rcBounds.left ||
      ptNewPosition.x > m_rcBounds.right ||
      (ptNewPosition.y + ptSpriteSize.y) < m_rcBounds.top ||
      ptNewPosition.y > m_rcBounds.bottom)
      return SA_KILL;
  }
  // Stop (default)
  else
  {
    if (ptNewPosition.x  < m_rcBounds.left ||
      ptNewPosition.x > (m_rcBounds.right - ptSpriteSize.x))
    {
      ptNewPosition.x = max(m_rcBounds.left, min(ptNewPosition.x,
        m_rcBounds.right - ptSpriteSize.x));
    }
    if (ptNewPosition.y  < m_rcBounds.top ||
      ptNewPosition.y > m_rcBounds.bottom)
    {
      if (ptNewPosition.y  < m_rcBounds.top) //If chef hit the ceiling
      {
        g_bIsJumping = false;                //Make him quit trying to jump
        g_iJumpAmount = 0;                   //Set jump amount to zero
        g_iFallAmount = 0;                   //Make sure fall amount is zero
        g_bIsFalling = true;                 //Make him fall
      }
      if(ptNewPosition.y > m_rcBounds.bottom) //Let Chef fall through ground
      {                                      //If chef is past the ground
        DieChef();                           //Make Chef die
      }
      ptNewPosition.y = max(m_rcBounds.top, ptNewPosition.y);
    }
  }
  SetPosition(ptNewPosition);

  //Offset particle emitter position, making particles trail behind moving sprite
  if(m_Particles != NULL)
  {
      m_Particles->MoveTo(m_rcPosition.left + m_ptParticleOffset.x, m_rcPosition.top + m_ptParticleOffset.y);
      if(g_pPersonSprite == this)
        m_Particles->Update(1.0 / (float)(g_pGame->GetFrameRate()));//fDiff / 1000.0); //HACK
  }

  return SA_NONE;
}

Sprite* Sprite::AddSprite()
{
  return NULL;
}

void Sprite::Draw(myRECT rcViewport, int xOffset, int yOffset)
{
  if(m_bDying)
    return; //Don't draw if we're dying and just haven't been removed
  //If particles exist
  if(m_Particles != NULL)
  {
      m_Particles->MoveTo(m_rcPosition.left + m_ptParticleOffset.x - rcViewport.left, m_rcPosition.top + m_ptParticleOffset.y - rcViewport.top, true); //Move particles to here
      m_Particles->MoveTo(m_rcPosition.left + m_ptParticleOffset.x - rcViewport.left + m_ptMoveTo.x / 2.0, m_rcPosition.top + m_ptParticleOffset.y - rcViewport.top + m_ptMoveTo.y / 2.0);
      m_ptMoveTo.x /= 2.0;
      m_ptMoveTo.y /= 2.0;//= m_ptMoveTo.y = 0;
      //Draw particles first if they're behind sprite
      if(!m_bParticlesInFront)
      {
          m_Particles->Transpose(xOffset,yOffset);
          m_Particles->Render();

          //Render tiled particles
          for(int i = 0; i < m_iTileParticlesXCount; i++)
          {
              m_Particles->Transpose((i+1) * m_iTileParticlesXIncrement + xOffset, yOffset);
              m_Particles->Render();
          }
          m_Particles->Transpose(0,0);

          m_Particles->MoveTo(m_rcPosition.left + m_ptParticleOffset.x, m_rcPosition.top + m_ptParticleOffset.y, true);
      }
  }
  // Draw the sprite if it isn't hidden
  if (m_pBitmap != NULL && !m_bHidden)
  {
    // Draw the appropriate frame, if necessary
    if(m_iFlashCount)
    {
        m_pBitmap->Colorize(m_r,m_g,m_b,255);
    }
    if (m_iNumFrames == 1)
      m_pBitmap->Draw(m_rcPosition.left - rcViewport.left + xOffset, m_rcPosition.top - rcViewport.top + yOffset);
    else
    {
        if(!m_bHorizFrames)
            m_pBitmap->DrawPart(m_rcPosition.left - rcViewport.left + xOffset, m_rcPosition.top - rcViewport.top + yOffset,
                                0, m_iCurFrame * GetHeight(), GetWidth(), GetHeight());
        else    //Draw frames horizontally
            m_pBitmap->DrawPart(m_rcPosition.left - rcViewport.left + xOffset, m_rcPosition.top - rcViewport.top + yOffset,
                                m_iCurFrame * GetWidth(), 0, GetWidth(), GetHeight());
    }
    m_pBitmap->ClearColor();
  }
  if(m_Particles != NULL && m_bParticlesInFront)
  {
      //Draw particles if they're in front
      m_Particles->Transpose(xOffset,yOffset);
      m_Particles->Render();

      //Render tiled particles
      for(int i = 0; i < m_iTileParticlesXCount; i++)
      {
        m_Particles->Transpose((i+1) * m_iTileParticlesXIncrement + xOffset, yOffset);
        m_Particles->Render();
      }
      m_Particles->Transpose(0,0);

      m_Particles->MoveTo(m_rcPosition.left + m_ptParticleOffset.x, m_rcPosition.top + m_ptParticleOffset.y, true);
  }
}

void Sprite::Gravity()
{
  myPOINT ptVelocity;
  ptVelocity = GetVelocity();

  if(!m_bIsJumping)
  {
    if(ptVelocity.y < 10)
      ptVelocity.y++;
    if(ptVelocity.y == 1)
      ptVelocity.y ++;     // Make y velocity of 2 minimum
  }
  else
  {
    ptVelocity.y = m_iJumpAmount++;
    if(m_iJumpAmount == 0)
      m_bIsJumping = false;
  }
  SetVelocity(ptVelocity);
}

int Sprite::GetWidth()
{
    if(m_bHorizFrames)
        return (m_pBitmap->GetWidth() / m_iNumFrames);
    return m_pBitmap->GetWidth();
}

int Sprite::GetHeight()
{
    if(!m_bHorizFrames)
        return (m_pBitmap->GetHeight() / m_iNumFrames);
    return m_pBitmap->GetHeight();
}

myPOINT Sprite::GetCenter()
{
    myPOINT pt;
    pt.x = m_rcPosition.left + GetWidth() / 2.0;
    pt.y = m_rcPosition.top + GetHeight() / 2.0;
    return pt;
}

//Particle system functions
void Sprite::AddParticles(string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptOffset, bool bAdditive)
{
    m_pHolder.sParticleFilename = sParticleFilename;
    m_pHolder.sImageFilename = sImageFilename;
    m_pHolder.x = x;
    m_pHolder.y = y;
    m_pHolder.w = w;
    m_pHolder.h = h;
    m_pHolder.bAdditive = bAdditive;

    //Free anything left over
    RemoveParticles();

    //Create texture
    m_ParticleTex = hge->Texture_Load(sImageFilename.c_str());
    if(!m_ParticleTex)
    {
        ofile << "Error loading " << sImageFilename << " for particle system" << endl;
        return;
    }
    //Create sprite
    m_ParticleSprite = new hgeSprite(m_ParticleTex, x, y, w, h);
    if(m_ParticleSprite == NULL)
        ofile << "Error: NULL particle sprite" << endl;
    m_ParticleSprite->SetHotSpot(w / 2.0, h / 2.0); //Center
    if(bAdditive)
        m_ParticleSprite->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE);
    m_Particles = new hgeParticleSystem(sParticleFilename.c_str(), m_ParticleSprite);
    if(m_Particles == NULL)
        ofile << "Error: NULL particle system" << endl;
    m_ptParticleOffset = ptOffset;
    m_Particles->MoveTo(m_rcPosition.left + ptOffset.x, m_rcPosition.top + ptOffset.y);
}

void Sprite::RemoveParticles()
{
  if(m_Particles != NULL)
    delete m_Particles;
  if(m_ParticleSprite != NULL)
    delete m_ParticleSprite;
  if(m_ParticleTex)
    hge->Texture_Free(m_ParticleTex);
  m_Particles = NULL;
  m_ParticleSprite = NULL;
  m_ParticleTex = 0;
}

void Sprite::FireParticles()
{
    if(m_Particles != NULL)
    {
        m_Particles->Fire();
        //m_Particles->Update(hge->Timer_GetDelta());
    }
}

void Sprite::OffsetParticles(myPOINT pt, bool bOffsetAll)
{
    if(bOffsetAll)
        m_ptParticleOffset = pt;
    else
    {
        m_ptMoveTo.x += pt.x;
        m_ptMoveTo.y += pt.y;
    }
}

bool Sprite::ParticlesFiring()
{
    if(m_Particles != NULL)
    {
        return(m_Particles->GetParticlesAlive() > 0);
    }
    return false;
}

void Sprite::FillInParticleData(Sprite_particle_holder* pData)
{
    pData->pSprite = this;
    pData->bIsFiring = ParticlesFiring();
    pData->sParticleFilename = m_pHolder.sParticleFilename;
    pData->sImageFilename = m_pHolder.sImageFilename;
    pData->x = m_pHolder.x;
    pData->y = m_pHolder.y;
    pData->w = m_pHolder.w;
    pData->h = m_pHolder.h;
    pData->ptOffset = m_ptParticleOffset;
    pData->bAdditive = m_pHolder.bAdditive;
    pData->bParticlesInFront = m_bParticlesInFront;
    pData->bDieOnFinish = m_bDieWhenParticlesDone;
}

//HACK
void Sprite::SetAlpha(unsigned char cAlpha)
{
    hgeSprite* hSprite = m_pBitmap->GetSprite();
    if(hSprite == NULL)
        return; //Cannot set alpha on null sprite
    hSprite->SetColor(ARGB(cAlpha,255,255,255));
}

