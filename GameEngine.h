//-----------------------------------------------------------------
// Game Engine Object
// C++ Header - GameEngine.h
//-----------------------------------------------------------------

#ifndef GAMEENGINE_H
#define GAMEENGINE_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
//#include <windows.h>
//#include <mmsystem.h>
#include "buildtarget.h"
#include <list>
#include <string>
#include <map>
using namespace std;
#include "Sprite.h"
#include <SDL.h>
#include <SDL_mixer.h>
#include <hge.h>
#include <hgefont.h>

//-----------------------------------------------------------------
// Joystick Flags
//-----------------------------------------------------------------
typedef int32_t JOYSTATE;
const JOYSTATE  JOY_NONE  = 0x0000L,
                JOY_LEFT  = 0x0001L,
                JOY_RIGHT = 0x0002L,
                JOY_UP    = 0x0004L,
                JOY_DOWN  = 0x0008L,
                JOY_FIRE1 = 0x0010L,
                JOY_FIRE2 = 0x0020L;

//-----------------------------------------------------------------
// Windows Function Declarations
//-----------------------------------------------------------------
//int WINAPI        WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
//                    PSTR szCmdLine, int iCmdShow);
void              WndProc(hgeInputEvent event);

//-----------------------------------------------------------------
// Game Engine Function Declarations
//-----------------------------------------------------------------
bool GameInitialize();//HINSTANCE hInstance);
void GameStart();//HWND hWindow);
void GameEnd();
bool GameActivate();//HWND hWindow);
bool GameDeactivate();//HWND hWindow);
void GamePaint();//HDC hDC);
void DrawOverlay(); //Draw an overlay on top of all painted stuff
void GameCycle();
void HandleKeys();
void MouseButtonDown(int x, int y, bool bLeft);
bool MouseButtonUp(int x, int y, bool bLeft);
void MouseMove(int x, int y);
void MouseWheel(int iAmt);
void HandleJoystick(JOYSTATE jsJoystickState);
bool SpriteCollision(Sprite* pSpriteHitter, Sprite* pSpriteHittee);
void SpriteDying(Sprite* pSpriteDying);
bool IsSpriteBadGuy(Sprite* pSprite);
//void SizedWindow(LPARAM Dimensions); //Function that's called if the window is sized
void DoDialog(int Key); //Function that's called if typing in a box
string GetSavegameLocation();           //For portability. Savegames are stored in different locations depending on platform
void AddBgParticles();
void RemoveBgParticles();
void StartCountdown();  //HACK: Reset resolution countdown, so always starts at 10s, as it's supposed to
bool ShouldUnchache(Bitmap* pBitmap);   //If we should remove this bitmap or no
//-----------------------------------------------------------------
// GameEngine Class
//-----------------------------------------------------------------
class GameEngine
{
protected:
  // Member Variables
  static GameEngine*  m_pGameEngine;
//  HINSTANCE           m_hInstance;
//  HWND                m_hWindow;
//  TCHAR               m_szWindowClass[32];
  string              m_szTitle;
  int32_t                m_wIcon;//, m_wSmallIcon;
  int                 m_iWidth, m_iHeight;
  bool                m_bFullscreen;
  int                 m_iFrameRate;
  bool                m_bVsync;
  bool                m_bSleep;
  unsigned int                m_uiJoystickID;
  myRECT                m_rcJoystickTrip;
  list<Sprite*>       m_lSprites;
  list<Sprite*>       m_lLaterSprites;
  bool                m_bSpriteLooping;
 // UINT                m_uiMIDIPlayerID;
  Mix_Music*          m_music;
  string              m_sMusicToPlay;
  int                 m_iSoundVol;
  int                 m_iMusicVol;
  bool                m_bMusic;
  bool                m_bSound;

  hgeFont*            m_Font;
  string              m_sFontFilename;
  float               m_fMoveMultiplier;
  SDL_Rect**          m_rcScreenModes;
  int                 m_iNumScreenModes;

  //Mouse stuff
  bool                m_bShowMouse;
  HTEXTURE            m_texMouseCursor;
  hgeSprite*          m_sMouseCursorSprite;
  string              m_sMouseFilename;
  myPOINT             m_ptMouseHotSpot;

  //Bitmap handler stuff
  list<Bitmap*>       m_lBitmapList;

  //Joystick
  SDL_Joystick* m_Joystick;
  Uint8         m_iJoyFire1;
  Uint8         m_iJoyFire2;
  bool          m_bJoystickDisabled;

  // Helper Methods
  void                CheckSpriteCollision(Sprite* pTestSprite);

  //Sound helper methods
  map<string, Mix_Chunk*>   m_mSounds;
  Mix_Chunk* GetChunk(string sFilename);    //If a chunk isn't loaded, load it. Otherwise, return it

public:
  // Constructor(s)/Destructor
          GameEngine(string szTitle, int32_t wIcon, int iWidth = 0, int iHeight = 0, bool bFullscreen = true);
  virtual ~GameEngine();

  // General Methods
  static GameEngine*  GetEngine() { return m_pGameEngine; };
  bool                Initialize();
  void                HandleEvent(hgeInputEvent event);
//  void                ErrorQuit(string szErrorMsg);
  //bool                InitJoystick();
  //void                CaptureJoystick();
  //void                ReleaseJoystick();
  void                CheckJoystick();
  SDL_Joystick*       GetJoystick() {return m_Joystick;};   //HACK
  void                AddSprite(Sprite* pSprite);
  void                DrawSprites(myRECT rcViewport, int xOffset = 0, int yOffset = 0);
  void                UpdateSprites();
  void                CleanupSprites();
  Sprite*             IsPointInSprite(int x, int y);
  void                PlaySong(string sFilename);
  void                PauseSong(bool bPause);   //Pause or resume a song
  void                CleanupSong();
  //void                PlayMIDISong(LPTSTR szMIDIFileName = TEXT(""),
  //                      bool bRestart = true);
  //void                PauseMIDISong();
  //void                CloseMIDIPlayer();
  void                PlaySound(string sFilename);  //Play a sound using SDL_Mixer
  int                 PlayLoop(string sFilename);   //Play a sound and let us keep track of it
  void                SetSoundVol(int sound, int vol);
  //void                SetSoundPan(int sound, int pan);
  void                KillChannel(int sound);
  void                SetSoundVol(int iVol) {m_iSoundVol = iVol;};// Mix_Volume(-1, iVol);};
  int                 GetSoundVol()         {return m_iSoundVol;};
  void                  SetMusicVol(int iVol)   {m_iMusicVol = iVol; if(Mix_PlayingMusic()){Mix_VolumeMusic(iVol);}};
  int                   GetMusicVol()           {return m_iMusicVol;};
  //Sound/music toggle
  void                  ToggleSound()           {m_bSound = !m_bSound;};
  void                  ToggleMusic()           {SetMusic(!m_bMusic);};
  bool                  GetSound()              {return m_bSound;};
  bool                  GetMusic()              {return m_bMusic;};
  void                  SetSound(bool bSound)   {m_bSound = bSound;};
  void                  SetMusic(bool bMusic);
  void                  CleanupSounds();

  void                FillRect(const myRECT* rc, unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

  // Accessor Methods
//  HINSTANCE GetInstance() { return m_hInstance; };
//  HWND      GetWindow() { return m_hWindow; };
//  void      SetWindow(HWND hWindow) { m_hWindow = hWindow; };
  string    GetTitle() { return m_szTitle; };
  int32_t      GetIcon() { return m_wIcon; };
//  WORD      GetSmallIcon() { return m_wSmallIcon; };
  int       GetWidth() { return m_iWidth; };
  int       GetHeight() { return m_iHeight; };
  int       GetNumScreenModes() {return m_iNumScreenModes;};
  void      GetScreenMode(int iMode, int* iWidth, int* iHeight);
  int       GetFrameRate() { return m_iFrameRate; };
  void      SetFrameRate(int iFrameRate);
  void      SetVsync(bool bVsync);
  bool      GetVsync() { return m_bVsync; };
  bool      GetSleep() { return m_bSleep; };
  void      SetSleep(bool bSleep) { m_bSleep = bSleep; };
  bool      GetAsyncKeyState(int key);
  void      SetMovementMultiplier(float fMul) { m_fMoveMultiplier = fMul; };
  float     GetMovementMultiplier() {return m_fMoveMultiplier;};
  void      SetMouse(string sFilename, int xHotSpot = 0, int yHotSpot = 0);
  void      ShowMouse();        //Show the mouse cursor
  void      HideMouse();        //Hide the mouse cursor
  bool      IsMouseVisible()    {return (m_bShowMouse);};   //Is the mouse visible or not?
  myPOINT   GetMousePos();      //Get the mouse position
  void      DrawCursor();       //NOT TO BE CALLED BY THE PROGRAM. Hide or show the mouse and set the cursor to draw it.
  bool      GetFullscreen();
  void      SetFullscreen(bool bFullscreen);
  void      ToggleFullscreeen();
  void      End();              //End the game
  void      SetJoystickFire1(Uint8 iFire1)    {m_iJoyFire1 = iFire1;};
  void      SetJoystickFire2(Uint8 iFire2)    {m_iJoyFire2 = iFire2;};
  Uint8     GetJoystickFire1()  {return m_iJoyFire1;};
  Uint8     GetJoystickFire2()  {return m_iJoyFire2;};
  bool      HasJoystick()   {return(m_Joystick != NULL);};
  bool      GetForceJoyDisable()      {return m_bJoystickDisabled;};
  void      SetForceJoyDisable(bool bDisable);
  int       GetJoystickIndex()  {if(m_Joystick == NULL)return 0; return SDL_JoystickIndex(m_Joystick);};
  void      UseJoystick(int iIndex);

  //Font helper methods
  void      SetFont(string sFilename);
  void      SetTextColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
  void      DrawText(const char* cText, const myRECT* rc, int align);
  float     GetStringWidth(string s);
  float     GetStringHeight();
  //Draw the supported screen resolutions to the screen
  void      DrawScreenResolutions(const myRECT* rcPosStart, float fOffsetEachx, float fOffsetEachy, int iResStart, int iCount, int iHighlight = -1);
  const char* GetKeyName(int key);

  //Bitmap handler methods
  void      AddBitmap(Bitmap* pBitmap)  { m_lBitmapList.push_front(pBitmap); };
  void      CleanupBitmaps();

  //Resolution helper methods
  void      ChangeResolution(int iWidth, int iHeight, bool bFullscreen);
  void      SetInitialResolution(int iWidth, int iHeight, bool bFullscreen);    //Identical to the above, but doesn't trigger a resolution-change message.
  void      SetInitialFullscreen(bool bFullscreen);             //Set the initial fullscreen value
  Uint32    GetTime()   {return SDL_GetTicks();};

  //misc
  int       GetPowerLevel();

  //ENGINE USE ONLY
  void                SetupWindow();    //Creates all the HGE stuff, cleaning up anything left over if we're changing resolution

  //etc
  void UncacheBitmaps();    //Free memory associated with the bitmaps in the game
};

//void PlaySoundCallback(int channel);   //To clean up SDL_Mixer channels. For game engine use only!

#endif
