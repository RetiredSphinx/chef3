//-----------------------------------------------------------------
// Chef Bereft Episode 3: Malice of the Malevolent Moon Monsters
// C++ header - 3D.h
//-----------------------------------------------------------------

#ifndef ENG3D_H
#define ENG3D_H

#include "buildtarget.h"

#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL.h>
#endif
#include "AckWrapper.h"

#include <string>
using namespace std;

typedef unsigned short WORD;
typedef unsigned char UCHAR;
#define UINT unsigned int

int MessageLoop(void);
void AckWndProc( hgeInputEvent event );
bool CheckStatus(void);
int LoadBackDrop(void);
int AckProcessInfoFile(void);
void AckPaint(int iWidth, int iHeight); // Actual paint routine for 3D Window
void ProcessKeys(hgeInputEvent event);
void DoCycle(void);
void ObjectCollision(NEWOBJECT* pObj, UCHAR nIndex, short nResult); //Our callback for when an object hits something
void MoveObjects(void);
void CheckBadGuys(UCHAR nIndex);
void HurtPlayer(int iAmount);   //Hurt the player by specified amount
void PrimeACK(string sFilename);    //Start the ACK engine rolling
void ShutdownACK();                 //And stop it
void InitDoors();          //Initialize the doors and force blocks to be shut to begin with
void CheckKeys(int iKey);   //When we get a key, open all applicable doors and force blocks

#define BOMB_START_INDEX    18      // Starting object number
#define BOMB_END_INDEX      22      // Ending object number

#define LAVA_START_BITMAP   20
#define LAVA_END_BITMAP     23

//MEH Stretch an image. But it's too slow.
//void StretchImage(unsigned char* src, unsigned char* dest,
//                  unsigned int srcW, unsigned int srcH, unsigned int destW, unsigned int destH);


#endif  // defined ENG3D_H
