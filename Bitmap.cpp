//-----------------------------------------------------------------
// Bitmap Object
// C++ Source - Bitmap.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "Bitmap.h"
//#include <Windows.h>
#include "FileAPI.h"
#include "BitmapLoad.h"
#include <stdio.h>
using std::endl;

extern HGE* hge;
extern void AddBitmap(Bitmap*);

//-----------------------------------------------------------------
// Bitmap Constructor(s)/Destructor
//-----------------------------------------------------------------

// Create a bitmap from a file
Bitmap::Bitmap(string szFileName)
{
    m_sFilename = szFileName;
    //m_hDC = hDC;
    m_iNumUsers = 0;
    m_hBitmap = 0;
    m_iWidth = 0;
    m_iHeight = 0;
    AddBitmap(this);
    m_r = m_g = m_b = m_a = -1;
    m_hSprite = NULL;
  //Create(hDC, szFileName);
}

Bitmap::~Bitmap()
{
  Free();
}

//-----------------------------------------------------------------
// Bitmap Helper Methods
//-----------------------------------------------------------------
void Bitmap::Free()
{
  // Delete the bitmap graphics object
  if (m_hBitmap != 0)
  {
    //DeleteObject(m_hBitmap);
    ofile << "Freeing bitmap: " << m_sFilename << endl;
    hge->Texture_Free(m_hBitmap);
    m_hBitmap = 0;
  }
  if(m_hSprite != NULL)
  {
      delete m_hSprite;
      m_hSprite = NULL;
  }
}

//-----------------------------------------------------------------
// Bitmap General Methods
//-----------------------------------------------------------------
bool Bitmap::Create(string szFileName)
{
    // Free any previous bitmap info. Because we can.
    Free();
    ofile << "Loading bitmap: " << szFileName << endl;
//    #ifdef BUILD_FOR_LINUX
//    szFileName = "./" + szFileName;
//    #endif
    //Load texture using BitmapLoad class
    CBitmap* cB = new CBitmap(szFileName.c_str());
    m_iWidth = cB->GetWidth();
    m_iHeight = cB->GetHeight();
    m_hBitmap = hge->Texture_Create(cB->GetWidth(), cB->GetHeight());
    if(!m_hBitmap)
        return false;   //Out of memory or something
    DWORD* img = hge->Texture_Lock(m_hBitmap, false);
    if(img == NULL)
    {
        return false;   //Fail
    }
    unsigned int size = cB->GetWidth() * cB->GetHeight() * cB->GetBitCount() + 256;
    DWORD* bits = (DWORD*)malloc(size);
    //TODO: Fix for big/little endian?
    if(!cB->GetBits(bits, size, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000, false))
    {
        delete cB;
        return false;
    }
    //memfrob(bits, size-256);  //WHEEEEEE
    for(unsigned int i = 0; i < cB->GetHeight(); i++)
    {
        for(unsigned int j = 0; j < cB->GetWidth(); j++)
        {
            DWORD dwTempPixel;
            int iPos = i*hge->Texture_GetWidth(m_hBitmap)+j;
            #ifdef BUILD_FOR_WINDOWS
            //bottom-up for DirectX
            dwTempPixel = bits[(cB->GetHeight() - i - 1)*cB->GetWidth()+j];
            #else
            //top-down for OpenGL
            dwTempPixel = bits[i*cB->GetWidth()+j];
            #endif
            dwTempPixel = SETA(dwTempPixel, 255);   //Set to alpha 255 by default
            //RGB 255,0,255 (magenta) is our transparent color for bitmap images
            if(GETR(dwTempPixel) == 255 && GETG(dwTempPixel) == 0 && GETB(dwTempPixel) == 255)
            {
                dwTempPixel = 0x00000000; // Set this pixel to black, alpha 0 (black for low-alpha drawing issues on fractional pixels)
            }
            img[iPos] = dwTempPixel;
            #ifdef BUILD_FOR_WINDOWS
            //Flip blue and red components to go from BGR to RGB, since that's what DirectX expects
            img[iPos] = SETR(img[iPos], GETB(dwTempPixel));
            img[iPos] = SETB(img[iPos], GETR(dwTempPixel));
            #endif
        }
    }
    hge->Texture_Unlock(m_hBitmap);
    free(bits); //Free allocated memory
    if(!m_hBitmap)
    {
        delete cB;
        return false;
    }
    delete cB;

    m_hSprite = new hgeSprite(m_hBitmap, 0, 0, m_iWidth, m_iHeight);    //Create our sprite for drawing this bitmap

  return true;
}

void Bitmap::Draw(int x, int y)
{
  DrawPart(x, y, 0, 0, GetWidth(), GetHeight());
}

void Bitmap::DrawPart(int x, int y, int xPart, int yPart,
  int wPart, int hPart)
{
  CheckUsers();
  if (m_hBitmap != 0 && m_hSprite != NULL)
  {
    //Set sprite to right rectangle for drawing
    m_hSprite->SetTextureRect(xPart, yPart, wPart, hPart);
    if(m_r != -1)
    {
        DWORD dCol = (m_a << 24) + (m_r << 16) + (m_g << 8) + m_b;
        m_hSprite->SetColor(dCol);    //Colorize this sprite
        //hSprite->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_NOZWRITE);
    }
    else
    {
        DWORD dAl = GETA(m_hSprite->GetColor());
        m_hSprite->SetColor(SETA(0xFFFFFFFF,dAl));    //Clear color on this sprite
    }
    m_hSprite->SetHotSpot(0,0);
    m_hSprite->Render(x, y);
    //delete hSprite;
  }
  else
  {
      ofile << "Bitmap 0!" << endl;
  }
}

void Bitmap::AddUser()
{
    //if(++m_iNumUsers == 1)
    //{
    //    Create(m_sFilename);
    //}
}

void Bitmap::RemoveUser()
{
    //if(--m_iNumUsers == 0)
    //{
    //    Free();
    //}
}

void Bitmap::Uncache()
{
    m_iNumUsers = 0;
    Free();
}

void Bitmap::CheckUsers()
{
    if(!m_iNumUsers)
    {
        //ofile << "Checking bitmap users for bitmap " << m_sFilename << endl;
        m_iNumUsers++;
        Create(m_sFilename);
    }
}

void Bitmap::ReloadTex() //To reload a free'd texture
{
    if(m_iNumUsers)
    {
        Create(m_sFilename);    //Reread data
    }
}





