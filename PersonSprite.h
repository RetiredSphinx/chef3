//-----------------------------------------------------------------
// Person Sprite Object
// C++ Header - PersonSprite.h
//-----------------------------------------------------------------

#ifndef PERSONSPRITE_H
#define PERSONSPRITE_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
//#include <windows.h>
#include "buildtarget.h"
#include "Sprite.h"

//-----------------------------------------------------------------
// PersonSprite Class
//-----------------------------------------------------------------
class PersonSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          PersonSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~PersonSprite();

  // Helper Methods
  virtual void  UpdateFrame();

  // General Methods
  void Walk();
  void WalkLeft();
  void Gravity();
  void StopWalking();
  void Fire();
  void Jumping();
  void Falling();

  //virtual void    Draw(myRECT rcViewport, int xOffset = 0, int yOffset = 0);
};

#endif
