//-----------------------------------------------------------------
// Alien Sprite Object
// C++ Header - AlienSprite.h
//-----------------------------------------------------------------

#ifndef ALIENSPRITE_H
#define ALIENSPRITE_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
//#include <windows.h>
#include "buildtarget.h"
#include "Sprite.h"
#include "PersonSprite.h"

//-----------------------------------------------------------------
//External variables
//-----------------------------------------------------------------

extern bool g_bYellowHub;
extern bool g_bOrangeHub;
extern bool g_bBlueHub;
extern bool g_bGreenHub;
extern bool g_bYellowStair;
extern bool g_bOrangeStair;
extern bool g_bBlueStair;
extern Bitmap* g_pYellowStairsBitmap;
extern Bitmap* g_pOrangeStairsBitmap;
extern Bitmap* g_pBlueStairsBitmap;
extern Bitmap* g_pLaserGunDownBitmap;
extern Bitmap* g_pLaserGunRightBitmap;
extern Bitmap* g_pLaserBoltHorizontalBitmap;
extern Bitmap* g_pLaserBoltVerticalBitmap;
extern Bitmap* g_pStalagtiteBitmap;
extern Bitmap* g_pYellowStairBlockBitmap;
extern Bitmap* g_pBlueStairBlockBitmap;
extern Bitmap* g_pOrangeStairBlockBitmap;
extern Bitmap* g_pAlienBulletBitmap;
extern PersonSprite* g_pPersonSprite;

//-----------------------------------------------------------------
// Actions for the evil creatures
//-----------------------------------------------------------------

const char AlienMove[] =        "P                                                                                 P                           P  j                                               P             J            P                                       R                                        J                                                                  P  ";
const char AlienFighterMove[] = "P                                 PSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS      PFFFF                       P  j                                               P             J            PFFFF                                   R                                        J                                                                  P  ";
//const char AlienFighterMove[] = "R                  J              PSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS      PFFFF                       P  j                                               P             J            PFFFF                        R                                                   J                                           j                      R  ";
const char BubbleMove[] =       "                   B                                    P                                                     P                         B                        P                          P    B                                                                           B                   P                                                 ";

//-----------------------------------------------------------------
// AlienSprite Class
//-----------------------------------------------------------------
class PoisonSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          PoisonSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~PoisonSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
  virtual Sprite*       AddSprite();
};

class LaserSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          LaserSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~LaserSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
};

class TrapSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          TrapSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~TrapSprite();

  // General Method
  virtual void  UpdateFrame();

};

class TruckSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          TruckSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~TruckSprite();

  // General Method
  virtual void  UpdateFrame();

};

class TeleporterSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          TeleporterSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~TeleporterSprite();

  // General Method
  virtual void  UpdateFrame();

};

class AlienSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          AlienSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~AlienSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
  virtual void  UpdateFrame();

};

class AlienFighterSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          AlienFighterSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~AlienFighterSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
  virtual void  UpdateFrame();
  virtual Sprite*       AddSprite();

};

class BubbleBlowerSprite : public Sprite
{
private:
    int m_iBlowCount;
public:
  // Constructor(s)/Destructor
          BubbleBlowerSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~BubbleBlowerSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
  virtual void  UpdateFrame();
  virtual Sprite*       AddSprite();

};
class BadGuySprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          BadGuySprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~BadGuySprite();

  // General Methods
  virtual void  UpdateFrame();

};
class RollerSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          RollerSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~RollerSprite();

  // General Methods
  virtual void  UpdateFrame();

};

class VortexSprite : public Sprite
{
public:
  // Constructor(s)/Destructor
          VortexSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~VortexSprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);

};

class ButtonSprite : public Sprite
{

public:
  // Constructor(s)/Destructor
          ButtonSprite(Bitmap* pBitmap, myRECT& rcBounds,
            BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~ButtonSprite();

  // Helper Method
  virtual void  UpdateFrame();

};

#endif
