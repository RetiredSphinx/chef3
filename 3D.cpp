//-----------------------------------------------------------------
// Chef Bereft Episode 3: Malice of the Malevolent Moon Monsters
// C++ Source - 3D.cpp
//-----------------------------------------------------------------
#include <hge.h>
#include <hgesprite.h>
extern HGE* hge;

#include <stdlib.h>
#include <string.h>
#include "3D.h"
#include <stdio.h>
#include <math.h>
#include <fstream>
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL.h>
#endif

#include "keys.h"
void AckDoubleBuffer(UCHAR *Src,UCHAR *Dest);
extern  UCHAR   colordat[];

// Some globals to use
SDL_Surface* MainScreen;
UCHAR       kbPressed[128];
int         StretchFactor = 1;
int         nWindowW;
int         nWindowH;
short       nInfoMode;
bool        bNoShow;        // Do not display if true
bool        bInStatus;      // Recursive flag for status checks
UINT        nTimerID;
UCHAR       SavePal[768];

//MEH Ack wrapper class variable
AckWrapper* g_Ack3D = NULL;

// Keeps the list of movable objects
short   nMoveObjectList[] = {
  5,6,7,8,9,10,11,12,13,14,15,16,17,
  0}; // Denotes end of table

   #define  ACK3D_SCREENWIDTH   320
   #define  ACK3D_SCREENHEIGHT  200
   #define  BYTES_PER_PIXEL     4

   #define  START_KEY 1
   #define  END_KEY   5

   #define  BLUE_KEY    1
   #define  ORANGE_KEY  2
   #define  YELLOW_KEY  3
   #define  GREEN_KEY   4
   #define  RED_KEY     5

   #define  RED_DOOR    11
   #define  ORANGE_DOOR 12
   #define  YELLOW_DOOR 13
   #define  GREEN_DOOR  14
   #define  BLUE_DOOR   15
   #define  GREEN_FORCE 6

   SDL_Rect rcResolution;

   int iHurtThisCycle; //If the player has been hit this cycle, flash the screen red


   //Global variables to deal with health
   #define MAX_HEALTH           320
   #define HEALTH_PLASMA_HIT    5
   #define HEALTH_SPACEMAN_HIT  15

   int g_iHealth = MAX_HEALTH;

   hgeSprite* pDraw = new hgeSprite(0, 0, 0, 320, 200); //Global sprite to use for drawing ACK-3D areas to screen

//So we can play sounds
#include "Resource.h"
extern void PlayMySound(string sFilename);

//For input stuffz
extern int UP_KEY, DOWN_KEY, JUMP_KEY, FIRE_KEY, LEFT_KEY, RIGHT_KEY;

//****************************************************************************
// The main program window
//****************************************************************************
extern int g_iCurrentLevel;
int g_iFirstCycle = 3;
int g_iTutorialWASDCount = 0;
extern bool g_bTutorialWASD;
void PrimeACK(string sFilename)
{
//---------------MEH Begin ACK-3D specific, rather than SDL-specific, code
ShutdownACK();  //Shut down anything that's currently running
g_Ack3D = new AckWrapper(sFilename.c_str());
g_Ack3D->AssignObjectCallback(ObjectCollision); //Set our collision callback function
InitDoors();
//---------------MEH End ACK-3D - specific code
for(int i = 0; i < 128; i++)
{
    kbPressed[i] = 0;   //Init to not pressing any keys
}
g_iHealth = MAX_HEALTH; //Start at max health
iHurtThisCycle = 0;
g_iFirstCycle = 3;
if(g_iCurrentLevel == 21)
    g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);
else
    g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
if(!g_bTutorialWASD)
    g_iTutorialWASDCount = 0;
}

void ShutdownACK()
{
    //Clean up the ACK-3D engine
    if(g_Ack3D != NULL)
        delete g_Ack3D;
    g_Ack3D = NULL;

}

//****************************************************************************
// Here is where we loop until ACK-3D terminates. Returns 0 to keep going, 1 to end
//****************************************************************************
int MessageLoop(void)
{
    if(g_iFirstCycle)
        g_iFirstCycle--;
    if(g_Ack3D == NULL)
        return 1;

    //See if game should end
    if(CheckStatus())
        return 1;

        DoCycle();  // Display the 3D window, check key presses, etc
return 0;
}

//****************************************************************************
// Our collision callback - called when an object moves each cycle
//****************************************************************************
void ObjectCollision(NEWOBJECT* pObj, UCHAR nIndex, short nResult)
{
    //First test: see if it's a spaceman
    bool bSpaceman = false;
    for(int i = 0; nMoveObjectList[i]; i++)
    {
        if(nMoveObjectList[i] == nIndex)
        {
            bSpaceman = true;
            break;
        }
    }
    if(bSpaceman && pObj->CurrentType == NO_CREATE)
    {
        if(g_iCurrentLevel == 21 && nIndex == 5)
            return;
        //FOLLOW PLAYER! AHAHA
        short dx = ae->xPlayer - pObj->x;
        short dy = ae->yPlayer - pObj->y;
        short dir = AckGetObjectAngle(dx,dy);
        //But only if within two tile widths, and not farther away than 8
        if(sqrt(dx * dx + dy * dy) > GRID_WIDTH * 2 &&
           sqrt(dx * dx + dy * dy) < GRID_WIDTH * 8)
            pObj->Dir = dir;
        if(sqrt(dx * dx + dy * dy) < GRID_WIDTH / 1.5)    //If close to player, bounce away and hurt player
        {
            HurtPlayer(HEALTH_SPACEMAN_HIT);

            pObj->Dir = WrapAngle(dir + INT_ANGLE_180);    //Go away from player
            AckMoveObjectPOV(nIndex,pObj->Dir, 32); //knock enemy back
        }
        //If it is a spaceman, and he hit something
        if (nResult != POV_NOTHING)
        {
            //Turn around (bounce off objects)
            pObj->Dir += (short)((rand() % INT_ANGLE_180));
            pObj->Dir = WrapAngle(pObj->Dir);

        }

    }

    //Check if it's a fork
    if(nIndex >= BOMB_START_INDEX && nIndex <= BOMB_END_INDEX)
    {
        if (nResult != POV_NOTHING)
        {
            if (nResult != POV_PLAYER)
            {
                pObj->Active = 0;
                if (nResult == POV_OBJECT)
                    CheckBadGuys(AckGetObjectHit());
                else
                {
                    PlayMySound(IDW_HITGROUND); //Hit some wall or another
                }
            }
        }
    }

}

//****************************************************************************
// Checks to see if the bomb thrown by the player hits one of the bad guys.
//****************************************************************************
void CheckBadGuys(UCHAR nIndex)
{
  short   i;
  UCHAR   ObjIndex;
  NEWOBJECT   *pObj;

i = 0;
while (nMoveObjectList[i])  // Check the list of movable objects
  {
  ObjIndex = nMoveObjectList[i++];
  if (ObjIndex == nIndex)
    {
    pObj = ae->ObjList[ObjIndex];
    if (pObj != NULL)
      {
      PlayMySound(IDW_FORKHITAMOEBA);
      AckSetObjectType(ae, ObjIndex, NO_DESTROY);  // Bad guy has been hit--make die animation
      }
    break;
    }
  }
}


//****************************************************************************
// Fires off a bomb from the player.
//****************************************************************************
void ShootBomb(void)
{
    if(g_iFirstCycle)
        return;

    if(!g_bTutorialWASD && g_iTutorialWASDCount == 2)
        g_iTutorialWASDCount++;
  NEWOBJECT   *pBomb = NULL;

// Get the bomb to fire
for(int i = BOMB_START_INDEX; i <= BOMB_END_INDEX; i++)
{
    pBomb = ae->ObjList[i];
    if (pBomb == NULL)
        continue;
    if (pBomb->Active)
        continue;
    break;
}
if(pBomb == ae->ObjList[BOMB_END_INDEX + 1])
    return;

PlayMySound(IDW_SHOOTFORK);

// Set the bomb up so that it shoot the direction the player is facing
pBomb->Dir = ae->PlayerAngle;
pBomb->x = ae->xPlayer;
pBomb->y = ae->yPlayer;
pBomb->Speed = 32;
pBomb->Active = 1;
pBomb->mPos = GetMapPosn(pBomb->x,pBomb->y);
}

//****************************************************************************
// Checks the status of the game and determines if the the game should be
// timed out or if the player has won. Returns true if game should end.
//****************************************************************************
bool CheckStatus()
{
//See if player ran out of health (died)
  if(g_iHealth <= 0)
      return true;      //game over
  return false;       //Keep going
}

//****************************************************************************
// Displays the current scene in the opened window.
//****************************************************************************
#include "GameEngine.h"
extern GameEngine* g_pGame;
extern Bitmap* g_pKeyScreenBitmap;
extern void DisplayMsg(string sStr, int iMilliseconds = 3000);
void AckPaint(int iWidth, int iHeight)
{
    rcResolution.w = iWidth;
    rcResolution.h = iHeight;
    if(g_Ack3D == NULL)
        return;
    //Fill in our screenbuffer from ACK's buffer
    const unsigned char* pBufferData = g_Ack3D->RenderView();

    HTEXTURE tempSurface = hge->Texture_Create(320,200);    //Create our surface
    int width = hge->Texture_GetWidth(tempSurface);
    const unsigned char* pSrc = pBufferData;

    DWORD* dest = hge->Texture_Lock(tempSurface, false);    //And lock it

    //Copy over
    for(int y = 0; y < 200; y++)
    {
        for(int x = 0; x < 320; x++)
        {
            dest[x+width*y] = 0xFFFFFFFF;
            #ifdef BUILD_FOR_WINDOWS
            dest[x+width*y] = SETR(dest[x+width*y],*pSrc++);
            dest[x+width*y] = SETG(dest[x+width*y],*pSrc++);
            dest[x+width*y] = SETB(dest[x+width*y],*pSrc++);
            #else   //BGR for hge-unix
            dest[x+width*y] = SETB(dest[x+width*y],*pSrc++);
            dest[x+width*y] = SETG(dest[x+width*y],*pSrc++);
            dest[x+width*y] = SETR(dest[x+width*y],*pSrc++);
            #endif
            dest[x+width*y] = SETA(dest[x+width*y],*pSrc++);
        }
    }

    hge->Texture_Unlock(tempSurface);

    //Draw it
    pDraw->SetTexture(tempSurface);
    //hgeSprite* pDraw = new hgeSprite(tempSurface, 0, 0, 320, 200);
    #ifndef BUILD_FOR_WINDOWS
    pDraw->SetFlip(false, true);    //Flip vertically this way, rather than copying over differently
    #endif
    pDraw->RenderStretch(0,0,iWidth,iHeight);

    //Draw health left in form of a green/yellow/red bar at bottom of screen
    int r = 0;
    int g = 255;    //Green if above half health
    int b = 0;

    //Yellow if less than half health
    if(g_iHealth < MAX_HEALTH / 2)
    {
        r = 255;
        g = 255;
        b = 0;
    }
    //Red if less than 1/5th health
    if(g_iHealth < MAX_HEALTH / 5)
    {
        r = 255;
        g = 0;
        b = 0;
    }

    //Draw the rectangle, 15 pixels tall
    myRECT rc = {0,rcResolution.h - 15, (g_iHealth * (rcResolution.w - g_pKeyScreenBitmap->GetWidth())) / MAX_HEALTH, rcResolution.h};
    g_pGame->FillRect(&rc, r,g,b,128);

    //If we've been hit, flash screen red for a sort of visual cue
    if(iHurtThisCycle)
    {
        myRECT rcScreen = {0,0,rcResolution.w, rcResolution.h};
        g_pGame->FillRect(&rcScreen,255,0,0,128);
    }

    //Show tutorial if we should
    if(!g_bTutorialWASD)
    {
        SDL_Joystick* joy = g_pGame->GetJoystick();
        //if(joy != NULL)
        //return;

        bool bDoubleJoystick = false;       //If we have two sticks on this gamepad
        if(joy != NULL && SDL_JoystickNumAxes(joy) > 2)
            bDoubleJoystick = true;
        switch(g_iTutorialWASDCount)
        {
            case 0:
            {
                if(joy == NULL)
                    DisplayMsg("WASD + Mouse may be easier in 3D areas.",10);
                else
                {
                    if(!bDoubleJoystick)
                        DisplayMsg("WASD + Mouse or a two-stick gamepad may be easier in 3D areas.",10);
                    else
                        DisplayMsg("Use Stick 1 to strafe, Stick 2 to turn.",10);
                }
                break;
            }
            case 1:
            {
                if(joy == NULL)
                {
                    string s = "Hold Shift or ";
                    s += hge->Input_GetKeyName(JUMP_KEY);
                    s += " to run.";
                    DisplayMsg(s,10);
                }
                else
                {
                    string s = "Hold Button ";
                    s += g_pGame->GetJoystickFire1() + 1 + '0';
                    s += " to run.";
                    DisplayMsg(s,10);
                }
                break;
            }
            case 2:
            {
                if(joy == NULL)
                {
                    string s = "Press Left Mouse Button or ";
                    s += hge->Input_GetKeyName(FIRE_KEY);
                    s += " to throw spoons.";
                    DisplayMsg(s,10);
                }
                else
                {
                    string s = "Press Button ";
                    s += g_pGame->GetJoystickFire2() + 1 + '0';
                    s += " to throw spoons.";
                    DisplayMsg(s,10);
                }
                break;
            }
            case 3:
                if(joy == NULL)
                    DisplayMsg("Press Spacebar or Right Mouse Button to open doors.",10);
                else
                {
                    int iDoorOpenButton = 0;
                    for(;;)
                    {
                        if(g_pGame->GetJoystickFire1() == iDoorOpenButton ||
                           g_pGame->GetJoystickFire2() == iDoorOpenButton)
                            iDoorOpenButton++;
                        else
                            break;
                    }
                    string s = "Press Button ";
                    s += iDoorOpenButton + 1 + '0';
                    s += " to open doors.";
                    DisplayMsg(s,10);
                }
                break;
        }
    }

//delete pDraw;
hge->Texture_Free(tempSurface);
pDraw->SetTexture(0);   //Clear this, in case we try to draw again

}

//****************************************************************************
// Message handler for the 3D window
//****************************************************************************
extern bool g_b3D;
extern bool g_bPlayingGame;
extern bool g_bPaused;
void AckWndProc( hgeInputEvent event )
{
if(!g_b3D || !g_bPlayingGame || g_bPaused)
    return;

switch (event.type)
  {
  case INPUT_KEYDOWN:
    if (event.key == FIRE_KEY)
      {
      ShootBomb();
      break;
      }

// Fall through here
  case INPUT_KEYUP:
    ProcessKeys(event);
    break;

  case INPUT_MBUTTONDOWN:
    if(event.key == HGEK_RBUTTON)
    {
        short val = g_Ack3D->OpenDoorForPlayer();
        if(!(val == POV_NODOOR || val & POV_DOORLOCKED))
        {
            PlayMySound(IDW_GODOOR);
            if(!g_bTutorialWASD)
                g_bTutorialWASD = true;
        }
    }
    else if(event.key == HGEK_LBUTTON)
        ShootBomb();
    break;

  default:
    break;
  }
}

//Update the animated floor tiles (lava in this case)
extern  unsigned    short   FloorMap[];
void UpdateFloors()
{
    static int iUpdate = 0;
    if(++iUpdate < 4)
        return;
    iUpdate = 0;
    for(int i = 0; i < GRID_ARRAY; i++)
    {
        if(FloorMap[i] >= LAVA_START_BITMAP &&
           FloorMap[i] <= LAVA_END_BITMAP)
        {
            FloorMap[i]++;
            if(FloorMap[i] > LAVA_END_BITMAP)
                FloorMap[i] = LAVA_START_BITMAP;
        }
    }
}

//Handle joystick movement separately here, since we want dual-stick movement if we can
void PollJoystick()
{
    SDL_Joystick* joy = g_pGame->GetJoystick();
    if(joy == NULL)
        return;

    bool bDoubleJoystick = false;       //If we have two sticks on this gamepad
    if(SDL_JoystickNumAxes(joy) > 2)
        bDoubleJoystick = true;

    //grab first axis - left/right
    Sint16 iAxisMove = SDL_JoystickGetAxis(joy, 0);
    //10% buffer for movement
    if(iAxisMove < -3200)   // left
    {
        if(!bDoubleJoystick)
            g_Ack3D->AddToPlayerAngle((iAxisMove * 32.0) / 32768.0);
        else
            kbPressed[kbLeftArrow] = 1;
    }
    else
    {
        if(bDoubleJoystick)
            kbPressed[kbLeftArrow] = 0;
    }
    if(iAxisMove > 3200)   // right
    {
        if(!bDoubleJoystick)
            g_Ack3D->AddToPlayerAngle((iAxisMove * 32.0) / 32768.0);
        else
            kbPressed[kbRightArrow] = 1;
    }
    else
    {
        if(bDoubleJoystick)
            kbPressed[kbRightArrow] = 0;
    }

    //grab second axis - up/down
    iAxisMove = SDL_JoystickGetAxis(joy, 1);
    //10% buffer for movement
    if(iAxisMove < -3200)   //up
        kbPressed[kbUpArrow] = 1;
    else
        kbPressed[kbUpArrow] = 0;
    if(iAxisMove > 3200)   //down
        kbPressed[kbDownArrow] = 1;
    else
        kbPressed[kbDownArrow] = 0;     //COMPLETELY locks out keyboard input if they have a gamepad attached. Ah, well.

    if(bDoubleJoystick)
    {
        //Grab third axis - left/right on second stick
        Sint16 iAxisMove = SDL_JoystickGetAxis(joy, 2);
        //10% buffer for movement
        if(iAxisMove < -3200)   // left
        {
            g_Ack3D->AddToPlayerAngle((iAxisMove * 32.0) / 32768.0);
        }
        if(iAxisMove > 3200)   // right
        {
            g_Ack3D->AddToPlayerAngle((iAxisMove * 32.0) / 32768.0);
        }
    }

    //Check joystick buttons
    if(SDL_JoystickGetButton(joy, g_pGame->GetJoystickFire1()))   //Jump/run key
        kbPressed[kbLeftShift] = 1;
    else
        kbPressed[kbLeftShift] = 0;

    if(SDL_JoystickGetButton(joy, g_pGame->GetJoystickFire2()))  //Fire key
    {
        if(!kbPressed[kbCtrl])
        {
            kbPressed[kbCtrl] = 1;
            ShootBomb();
        }
    }
    else
        kbPressed[kbCtrl] = 0;

    int iDoorOpenButton = 0;
    for(;;)
    {
        if(g_pGame->GetJoystickFire1() == iDoorOpenButton ||
           g_pGame->GetJoystickFire2() == iDoorOpenButton)
            iDoorOpenButton++;
        else
            break;
    }

    if(SDL_JoystickGetButton(joy, iDoorOpenButton))    //Open door with button higher than fire1 and fire2, or whatever
    {
        short val = g_Ack3D->OpenDoorForPlayer();
        if(!(val == POV_NODOOR || val & POV_DOORLOCKED))
        {
            PlayMySound(IDW_GODOOR);
            if(!g_bTutorialWASD)
                g_bTutorialWASD = true;
        }
    }

}

//****************************************************************************
// Application-specific code for each cycle of the game should go here
//****************************************************************************
extern void EndLevel();
void DoCycle(void)
{
    if(g_iFirstCycle)
    {
        hge->Input_SetMousePos(rcResolution.w / 2, rcResolution.h / 2);
        return;
    }

    PollJoystick();

    short   Amt,nRow,nCol,SoundDelay;
    bool bMoved = false;
    static int iMoveSoundDelay = 0;

    Amt = 9;
    SoundDelay = 8;
    if (kbPressed[kbLeftShift])
    {
        Amt = 18;
        SoundDelay = 4;
    }

    if (kbPressed[kbLeftArrow])
    {
        g_Ack3D->MovePlayer(Amt, -INT_ANGLE_90);
        bMoved = true;
    }
    if (kbPressed[kbRightArrow])
    {
        g_Ack3D->MovePlayer(Amt, INT_ANGLE_90);
        bMoved = true;
    }
    if (kbPressed[kbDownArrow])
    {
        g_Ack3D->MovePlayer(Amt, INT_ANGLE_180);
        bMoved = true;
    }
    if (kbPressed[kbUpArrow])
    {
        g_Ack3D->MovePlayer(Amt);
        bMoved = true;
    }

    //Play movement noise
    if(bMoved)
    {
        if(iMoveSoundDelay == 0)
        {
            PlayMySound(IDW_CHEFWALK);
        }
        if(++iMoveSoundDelay > SoundDelay)
            iMoveSoundDelay = 0;
        if(!g_bTutorialWASD && g_iTutorialWASDCount == 0)
            g_iTutorialWASDCount++;
        if(!g_bTutorialWASD && g_iTutorialWASDCount == 1 && Amt > 9)
            g_iTutorialWASDCount++;
    }
    else
        iMoveSoundDelay = 0;

    //Open any doors for the player - here's the spot to do it if we want automatic doors
    //AckCheckDoorOpen(ae->xPlayer,ae->yPlayer,ae->PlayerAngle);

    //MEH Make moving the mouse pan the view
    float xCursor, yCursor;
    hge->Input_GetMousePos(&xCursor, &yCursor);
    int iDistMoved = xCursor - rcResolution.w / 2;
    //Of course, we can multiply this by a scaling factor if we want to, but one ACK-3D 0.2-degree increment per pixel seems to work fine
    if(iDistMoved != 0)
        g_Ack3D->AddToPlayerAngle(iDistMoved*0.5);
    hge->Input_SetMousePos(rcResolution.w / 2, rcResolution.h / 2);

    //If pressing left or right arrow, turn view (silly person not using WASD)
    if(hge->Input_GetKeyState(LEFT_KEY))
    {
        if(Amt > 9)
            g_Ack3D->AddToPlayerAngle(-25);
        else
            g_Ack3D->AddToPlayerAngle(-16);
    }
    if(hge->Input_GetKeyState(RIGHT_KEY))
    {
        if(Amt > 9)
            g_Ack3D->AddToPlayerAngle(25);
        else
            g_Ack3D->AddToPlayerAngle(16);
    }

    if(iHurtThisCycle)
        iHurtThisCycle--; //Haven't been hit this cycle yet
    //Update the objects and such
    g_Ack3D->Update();

    //Check and see if we're on top of any of the objects we need!
    //Works a whole lot better than moving into non-passable objects
    short nPlayerPos = GetMapPosn(ae->xPlayer, ae->yPlayer);
    for(int i = START_KEY; i <= END_KEY; i++)
    {
        if(i == END_KEY && g_iCurrentLevel == 20)
            break;  //This key isn't here in secret level 1
        short nObjPos = GetMapPosn(ae->ObjList[i]->x, ae->ObjList[i]->y);
        if(nPlayerPos == nObjPos && ae->ObjList[i]->Active)
        {
            ae->ObjList[i]->Active = 0;
            PlayMySound(IDW_GOTKEY);
            CheckKeys(i);
        }
    }

    // This code determines if the player is in a special location
    // of the game map and toggles the light shading if the player
    // reaches this region. This is used to create a unique
    // dynamic light shading effect
    nRow = (short)(ae->yPlayer >> 6);
    nCol = (short)(ae->xPlayer >> 6);
    if(g_iCurrentLevel == 20)
    {
        if (nRow == 60)
        {
          if (nCol == 17)
          {
              g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);
              if(ae->PlayerAngle > INT_ANGLE_45 && ae->PlayerAngle < INT_ANGLE_315)
                g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
          }
          else if (nCol == 19)
          {
              g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
              if(ae->PlayerAngle < INT_ANGLE_135 || ae->PlayerAngle > INT_ANGLE_225)
                g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);

          }
          //Handle stuff around here if they back out of the square facing the other way or such
          else if(nCol == 16)
            g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
          else if(nCol == 20)
            g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);
        }
        else if(nRow == 59 || nRow == 61)
        {
            if(nCol == 19 || nCol == 20)
                g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);
            else if(nCol == 17 || nCol == 16)
                g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
        }

        //Check and see if we won
        if(nRow == 37 &&
           nCol == 33)
        {
            PlayMySound(IDW_WINLEVEL);
            EndLevel();
        }
    }
    else if(g_iCurrentLevel == 21)
    {
        UpdateFloors(); //Animate floor tiles that should be

        //Check and see if player is on lava tile
        short sPos = GetMapPosn(ae->xPlayer, ae->yPlayer);
        if(FloorMap[sPos] >= LAVA_START_BITMAP && FloorMap[sPos] <= LAVA_END_BITMAP)
        {
            //Hurt player
            HurtPlayer(10);
        }

        if(nRow == 17 && nCol == 56)
            g_Ack3D->SetLightShading(ACK_LIGHTSHADING_OFF);
        if(nRow == 16 && nCol == 56)
            g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);

        //if(nRow == 6 && nCol == 1)
        //    g_Ack3D->SetLightShading(ACK_LIGHTSHADING_ON);

        if(nRow == 35 &&
           nCol == 44)
        {
            PlayMySound(IDW_WINLEVEL);
            EndLevel();
        }
    }

}


//****************************************************************************
// Use this routine to process the SDL messages SDL_KEYDOWN and SDL_KEYUP
// as they are received. It will set a global array of virtual keyboard elements
// to correspond to the key being pressed or released. The routine DoCycle will
// then look at this keyboard array to determine if the POV should be moved
// or rotated.
//****************************************************************************
void ProcessKeys(hgeInputEvent event)
{

switch (event.type)
    {
    case INPUT_KEYDOWN:
        switch (event.key)
            {
            //MEH WASD instead of arrows
            case HGEK_W:
                kbPressed[kbUpArrow] = 1;
                break;
            case HGEK_S:
                kbPressed[kbDownArrow] = 1;
                break;
            case HGEK_A:
                kbPressed[kbLeftArrow] = 1;
                break;
            case HGEK_D:
                kbPressed[kbRightArrow] = 1;
                break;
            case HGEK_CTRL:
                kbPressed[kbCtrl] = 1;
                break;
            case HGEK_ESCAPE:
                kbPressed[kbEsc] = 1;
                break;
            case HGEK_SPACE:
            {
                short val = g_Ack3D->OpenDoorForPlayer();
                if(!(val == POV_NODOOR || val & POV_DOORLOCKED))
                {
                    PlayMySound(IDW_GODOOR);
                    if(!g_bTutorialWASD)
                        g_bTutorialWASD = true;
                }
                break;
            }
            case HGEK_SHIFT:
                kbPressed[kbLeftShift] = 1;
                break;
            default:
                break;
            }
        //Derp can't do this in the switch. Daargh
        if(event.key == UP_KEY)
        {
            kbPressed[kbUpArrow] = 1;
        }
        if(event.key == DOWN_KEY)
        {
            kbPressed[kbDownArrow] = 1;
        }
        if(event.key == JUMP_KEY)
        {
            kbPressed[kbLeftShift] = 1;
        }
        break;

    case INPUT_KEYUP:
        switch (event.key)
            {
            case HGEK_W:
                kbPressed[kbUpArrow] = 0;
                break;
            case HGEK_S:
                kbPressed[kbDownArrow] = 0;
                break;
            case HGEK_A:
                kbPressed[kbLeftArrow] = 0;
                break;
            case HGEK_D:
                kbPressed[kbRightArrow] = 0;
                break;
            case HGEK_CTRL:
                kbPressed[kbCtrl] = 0;
                break;
            case HGEK_ESCAPE:
                kbPressed[kbEsc] = 0;
                break;
            case HGEK_SPACE:
                kbPressed[kbSpace] = 0;
                break;
            case HGEK_SHIFT:
                kbPressed[kbLeftShift] = 0;
                break;
            default:
                break;
            }
        //Derp can't do this in the switch. Daargh
        if(event.key == UP_KEY)
        {
            kbPressed[kbUpArrow] = 0;
        }
        if(event.key == DOWN_KEY)
        {
            kbPressed[kbDownArrow] = 0;
        }
        if(event.key == JUMP_KEY)
        {
            kbPressed[kbLeftShift] = 0;
        }
        break;
    }

}

void HurtPlayer(int iAmount)
{
    g_iHealth -= iAmount;
    iHurtThisCycle = 3;      //Ouch! Flash screen red
    PlayMySound(IDW_CHEFHURT);
    if(g_iHealth <= 0)
    {
        PlayMySound(IDW_DIE);
    }
}

void InitDoors()
{
    for(int i = 0; i < GRID_ARRAY; i++)
    {
        //x doors
        int iCode = ae->xGrid[i];
        int iBmp = iCode & 0xFF;
        if(iBmp >= RED_DOOR &&
           iBmp <= BLUE_DOOR)
        {
            iCode |= DOOR_LOCKED;
        }
        ae->xGrid[i] = iCode;

        //y doors
        iCode = ae->yGrid[i];
        iBmp = iCode & 0xFF;
        if(iBmp >= RED_DOOR &&
           iBmp <= BLUE_DOOR)
        {
            iCode |= DOOR_LOCKED;
        }
        ae->yGrid[i] = iCode;
    }
}

void UnlockDoor(int iCode)
{
    for(int i = 0; i < GRID_ARRAY; i++)
    {
        //If this is a locked door, unlock it
        if((ae->xGrid[i] & 0xFF) == iCode)
            ae->xGrid[i] ^= DOOR_LOCKED;//(iCode & DOOR_TYPE_SPLIT);
        if((ae->yGrid[i] & 0xFF) == iCode)
            ae->yGrid[i] ^= DOOR_LOCKED;//(iCode & DOOR_TYPE_SPLIT);
    }
}
extern bool g_bBlueKey;
extern bool g_bRedKey;
extern bool g_bYellowKey;
extern bool g_bGreenKey;
extern bool g_bOrangeKey;
void CheckKeys(int iKey)
{
    switch (iKey)
    {
        case BLUE_KEY:
            UnlockDoor(BLUE_DOOR);
            g_bBlueKey = true;
            break;
        case ORANGE_KEY:
            UnlockDoor(ORANGE_DOOR);
            g_bOrangeKey = true;
            break;
        case YELLOW_KEY:
            UnlockDoor(YELLOW_DOOR);
            g_bYellowKey = true;
            break;
        case GREEN_KEY:
            UnlockDoor(GREEN_DOOR);
            //also open force blocks
            for(int i = 0; i < GRID_ARRAY; i++)
            {
                if((ae->xGrid[i] & 0xFF) == GREEN_FORCE)
                    ae->xGrid[i] |= WALL_TYPE_PASS;
                if((ae->yGrid[i] & 0xFF) == GREEN_FORCE)
                    ae->yGrid[i] |= WALL_TYPE_PASS;
            }
            g_bGreenKey = true;
            break;
        case RED_KEY:
            UnlockDoor(RED_DOOR);
            g_bRedKey = true;
            break;
    }
}

/*
#include <fstream>
using std::endl;
using std::ofstream;
//MEH stretch this image onto a larger one - WAY TOO SLOW
void StretchImage(unsigned char* src, unsigned char* dest,
                  unsigned int srcW, unsigned int srcH, unsigned int destW, unsigned int destH)
{
    ofstream ofile("output.txt");
    ofile << "w: " << destW << ", h: " << destH << endl;
    //Do some sanity checking first
    if(srcW > destW ||
       srcH > destH)
    {
        printf("Error in StretchImage(): Cannot stretch an image smaller than the original.\n");
        return;
    }

    if(srcW == 0 ||
       srcH == 0 ||
       destW == 0 ||
       destH == 0)
    {
        printf("Error in StretchImage(): Cannot stretch an image to/from 0 width or height.\n");
        return;
    }

    for(int i = 0; i < destH; i++)
    {
        for(int j = 0; j < destW; j++)
            dest[j + i*destW] = 0;
    }

    //Grab the target stretch factor x/y
    float fTargetStretchX = (float)(destW) / (float)(srcW);
    float fTargetStretchY = (float)(destH) / (float)(srcH);

    ofile << "stretchX: " << fTargetStretchX << " stretchY: " << fTargetStretchY << endl;

    //Our running totals of where we're at
    float fCurrentX = fTargetStretchX;
    float fCurrentY = fTargetStretchY;

    unsigned char* pCurrentDest = dest; //Where on the destination image we are
    unsigned char* pCurrentSrc = src;

    unsigned int iWhereAtY = 0;
    //unsigned int iWhereAtX = 0;
    int iTemp = 0;

    //Loop through the source image, copying what we can into the dest image
    for(int ySrc = 0; ySrc < srcH; ySrc++)  //Loop through y first
    {
        //if((int)(pCurrentDest - dest) > 100000 && iTemp < 5)
        //{
            ofile << "y: " << ySrc << endl;
            ofile << "destpos: " << (int)(pCurrentDest - dest) << endl;
            iTemp++;
        //}
        fCurrentX = fTargetStretchX;    //Reset our current x
        //pCurrentDest = dest + iWhereAtY * destW;    //and our current x dest
        for(int xSrc = 0; xSrc < srcW; xSrc++)  //Then loop through x
        {
            int iCurrentX = fCurrentX;  //No interpolation means we only deal with integer values per pixel
            //Loop through how many pixels we should write for one pixel of this row
            for(int i = 0; i < iCurrentX; i++)
            {
                //if((int)(pCurrentDest - dest) > 100000 && iTemp < 5)
                //{
                    //ofile << xSrc << ", " << ySrc << "xscale: " << fTargetStretchX << " yscale: " << fTargetStretchY << endl;
                    //ofile << (int)(pCurrentDest - dest) << endl;
                //}
                *pCurrentDest = *pCurrentSrc;
                pCurrentDest++;
            }
            pCurrentSrc++;  //Move to next pixel in source
            fCurrentX -= iCurrentX; //Remove the pixels we wrote
            fCurrentX += fTargetStretchX;   //Add back in for next pass. Fractional parts should add up to all work out
        }

        //Copy this past row in the vertical direction yrepeat times
        //pCurrentDest = dest + (iWhereAtY + 1) * destW;
        int iCurrentY = fCurrentY;
        for(int j = 0; j < iCurrentY - 1; j++)
        {

            for(int i = 0; i < destW; i++)
            {
                *pCurrentDest = *(pCurrentDest - destW);
                pCurrentDest++;
            }

        }
        //Add back in our stretch leftover stuff
        fCurrentY -= iCurrentY;
        fCurrentY += fTargetStretchY;
        //iWhereAtY++;
    }

}*/








//**** End of Source ****


