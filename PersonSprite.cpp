//-----------------------------------------------------------------
// Person Sprite Object
// C++ Source - PersonSprite.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "PersonSprite.h"
#include "GameEngine.h"
extern GameEngine* g_pGame;

//-----------------------------------------------------------------
// External but neccassary variables
//-----------------------------------------------------------------
extern float g_iFallAmount;
extern float g_iJumpAmount;
extern bool g_bIsJumping;
extern bool g_bIsWalkingRight;
extern bool g_bFacingLeft;
extern bool g_bIsFiring;
extern myPOINT g_ptPanTo;
extern myRECT g_rcViewport;

extern Sprite* g_pJetBootBlast;

#define         MAX_FALL_AMOUNT 20
//-----------------------------------------------------------------
// PersonSprite Constructor(s)/Destructor
//-----------------------------------------------------------------
PersonSprite::PersonSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

PersonSprite::~PersonSprite()
{
}

//-----------------------------------------------------------------
// PersonSprite Virtual Methods
//-----------------------------------------------------------------
void PersonSprite::UpdateFrame()
{
}


//-----------------------------------------------------------------
// PersonSprite General Methods
//-----------------------------------------------------------------
void PersonSprite::Walk()
{
  if(m_iCurFrame > 6) // Make sure we aren't already moving left
    m_iCurFrame = 0;
  else
    m_iCurFrame++;  // Go between the eight frames to give the illusion of walking
}
void PersonSprite::WalkLeft()
{
  if(m_iCurFrame < 8) // Make sure we aren't already moving right
    m_iCurFrame = 8;
  else
  {
    m_iCurFrame++;  // Go between the eight frames to give the illusion of walking
    if(m_iCurFrame > 15)
     m_iCurFrame = 8;
  }
}
void PersonSprite::Gravity()
{
  if(g_ptPanTo.x != -1 && g_ptPanTo.y != -1)
    return; //Don't fall if panning (don't fall off screen in vortical volvox vertigo)
  if(g_iFallAmount < MAX_FALL_AMOUNT)
  {
    g_iFallAmount ++;//= g_pGame->GetMovementMultiplier();
  }
  OffsetPosition(0, g_iFallAmount);
  Falling();
}
void PersonSprite::StopWalking()
{
  if(g_bIsWalkingRight)
    m_iCurFrame = 0;
  else if(!g_bIsWalkingRight)
    m_iCurFrame = 8;
}
void PersonSprite::Fire()
{
  if(g_bIsWalkingRight)
    m_iCurFrame = 17;
  else if(!g_bIsWalkingRight)
    m_iCurFrame = 16;
}
void PersonSprite::Jumping()
{
  if(g_bIsWalkingRight)
    m_iCurFrame = 19;
  else if(!g_bIsWalkingRight)
    m_iCurFrame = 18;
}
void PersonSprite::Falling()
{
  if(g_bFacingLeft && !g_bIsFiring)
    m_iCurFrame = 18;
  else if(!g_bFacingLeft && !g_bIsFiring)
    m_iCurFrame = 19;
}

