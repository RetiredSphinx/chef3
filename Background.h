//-----------------------------------------------------------------
// Background Object
// C++ Header - Background.h
//-----------------------------------------------------------------

#ifndef BACKGROUND_H
#define BACKGROUND_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "myPOINT.h"
#include "buildtarget.h"
#include "Bitmap.h"
#include <hgeparticle.h>
#include <list>
using namespace std;

//-----------------------------------------------------------------
// Background Class
//-----------------------------------------------------------------
class Background
{
protected:
  // Member Variables
  int       m_iWidth, m_iHeight;
  unsigned char m_r,m_g,m_b,m_a;
  Bitmap*   m_pBitmap;
  float     m_fParallax;    //MEH Multiplier for parallax backgrounds

  //MEH For particles
  bool          m_bParticlesInFront;
  list<hgeParticleSystem*> m_lParticles; //More than one
  hgeSprite*    m_ParticleSprite;
  HTEXTURE      m_ParticleTex;

  //Helper
  myPOINT GetParticleDrawPos(list<hgeParticleSystem*>::iterator i, myRECT rcView);

public:
  // Constructor(s)/Destructor
          Background(int iWidth, int iHeight, unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
          Background(Bitmap* pBitmap);
  virtual ~Background();

  // General Methods
  virtual void  Update(myRECT rcView);
  virtual void  Draw(myRECT rcView, int ixOffset = 0, int iyOffset = 0);

  // Accessor Methods
  int   GetWidth()              { return m_iWidth; };
  int   GetHeight()             { return m_iHeight; };
  void  SetParallax(float fPar) { m_fParallax = fPar; };
  float GetParallax()           { return m_fParallax; };

  //Almost identical to Sprite::AddParticles, with difference being that you specify coordinates
  void  AddParticles(string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptPos, bool bAdditive = true);
  void  RemoveParticles();  //Kill all particles and clear memory and such
};



#endif
