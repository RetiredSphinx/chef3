//-----------------------------------------------------------------
// Chef Bereft 3
// C++ Header - Chef.h
//-----------------------------------------------------------------

#ifndef CHEF_H
#define CHEF_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
//#include <windows.h>
#include "buildtarget.h"
#include "myPOINT.h"
#include "Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"
#include "Sprite.h"
#include "Background.h"
#include "PersonSprite.h"
#include "AlienSprite.h"
#include "pnpoly.h"
#include "3D.h"  //For our nifty 3D engine. From 1992. Yeah, whatever.

//-----------------------------------------------------------------
// Functions
//-----------------------------------------------------------------
void MoveChef(int idir);  //Function to move Chef Bereft
void ControlJump();       //Function to see if he's jumping and do something about it
//Function to control Chef or amoeba hitting a block
bool DoBlock(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Function to take care of doing a block when something hits it
bool DoLeftSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Function to do a left slant when something hits it
bool DoRightSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Function to do a right slant when something hits it
bool DoLeftCielingSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Function to do a left cieling slant when something hits it
bool DoRightCielingSlant(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Function to do a right cieling slant when something hits it
bool ReadMapFile();  //Function to read in a map from a file
bool IsInView(Sprite* pSprite); //Function to check if an object is in view
bool IsNearby(Sprite* pSprite);//, bool bBiggerArea = false);  //Function to check if an object should be updated
void StartLevel();               //Function for a new level
void DieChef(int iDieTime = 30);                  //Function responsible for handling Chef dying
void DoWon(Sprite* pExit);      //Function for making Chef win
void NewGame();            //Function to start a new game
//void DoHelp();             //Function to display a help message
//void BackgroundStory();    //Function to display background story
void UpdateHiScores();  //Function to update the high scores
bool ReadHiScores();   //Function to read high scores
bool WriteHiScores();  //Function to write high scores
bool CheckWon();         //Function to check and see if player won
void WonGame();          //Function for doing a won game
bool SaveGame();         //Function for saving the game
bool RedeemGame();       //Function for redeeming a saved game
bool ElevatorHit(Sprite* pSpriteHitter, Sprite* pSpriteHittee);      //Function for doing a block an elevator platform hit
//void g_pGame->AddSprite(Sprite* pSprite);          //Function for later adding a sprite
//void AddSprites();                       //Function for adding later sprites
//bool UpKeyPressed();                     //Function for checking if c,v,b,n, or m was pressed
void Left(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Keep Chef from going left out of a block
void Right(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Keep Chef from going right out of a block
void Top(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Keep Chef from going up out of a block
void Bottom(Sprite* pSpriteHitter, Sprite* pSpriteHittee, bool bIsChef); //Keep Chef from going down out of a block
void UpdateViewport(); //Set initial viewport around Chef Bereft
void CheckJetboots();   //Make jet boots fire if need be
bool JetbootBlasting(); //Returns true if using jet boots, false otherwise
void KeepViewportOnScreen();    //Makes sure the viewport is on the screen, and updates creen start positions if needed
void ReadConfig();          //Read config data from our XML file
void WriteConfig();         //Write config to XML
void EndLevel();            //Deal with level-end conditions if game closed or player chooses to end the level prematurely
void PlayPointsNoise(); //Play one of 4 random get-points sounds
void LoadOptionsMenuStuff();    //Load candle flame sprites for options menu
void ResetResolution();     //Reset the screen resolution to the point it was before we changed it
void DisplayMsg(string sStr, int iMilliseconds = 3000);   //Display message at bottom of screen
void ReadInBgStory();   //Read in the background story from res.zip/bgstory.CB3
//-----------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------
//HINSTANCE             g_hInstance;
GameEngine*           g_pGame;
//HDC                   g_hOffscreenDC;
//HBITMAP               g_hOffscreenBitmap;
Background*           g_pBackground[20];  //The backgrounds
Background*           g_p2ndBackground[20]; //Second backgrounds
Background*           g_pLevel15Background; //Third background for level 15
const int STARBGS[] = {0,1,2,3,14,16,17,-1};
#define               NUMSTARS_SPACEBG  10 //How many stars are in a level with a space bg
Bitmap*               g_pPersonBitmap; //The person bitmap
Bitmap*               g_pPersonJetBoots;
Bitmap*               g_pPersonSuperJetBoots;
Bitmap*               g_pChefDieLBitmap; //Bitmap for Chef dying left
Bitmap*               g_pChefDieRBitmap; //Bitmap for Chef dying right
Bitmap*               g_pPlatformBitmap; //Green platform
Bitmap*               g_pSinkingPlatformBitmap;  //Sinking platform
Bitmap*               g_pUpsideDownPlatformBitmap; //Upside - Down Green platform
Bitmap*               g_pLeftPlatformBitmap; //Left-facing platform
Bitmap*               g_pRightPlatformBitmap; //Right - facing platform
Bitmap*               g_pHiddenBlockBitmap;   //Nearly invisible block
Bitmap*               g_pNullBlockBitmap;     //Toatally invisible block that only affects bad guys
//Bitmap*               g_pLogsBitmap;          //Log cabin logs
Bitmap*               g_pGroundBitmap;        //Dirt you walk over
//Bitmap*               g_pGrassBitmap;         //Grass you walk through
Bitmap*               g_pCrackedLogsBitmap;   //Cracked log cabin logs
Bitmap*               g_pBlueForceBitmap;    //block you need blue key to walk on
Bitmap*               g_pGreenForceBitmap;    //block you need green key to walk on
Bitmap*               g_pRedForceBitmap;    //block you need red key to walk on
//Bitmap*               g_pBushBitmap;          //Bush you can jump on
Bitmap*               g_pCloud3Bitmap;       //Bitmap for third clouds
Bitmap*               g_pSlantedLeftBitmap;  //Bitmap for slanted left ground (/)
Bitmap*               g_pSlantedRightBitmap; //Bitmap for slanted right ground (\)
Bitmap*               g_pSlantedLeftCornerBitmap; //Bitmap for the corner on slanted left ground
Bitmap*               g_pSlantedRightCornerBitmap; //Bitmap for the corner on slanted right ground

Bitmap*               g_pChromeblockBitmap; //Chrome block
Bitmap*               g_pCoolblockBitmap; //cool-looking block
Bitmap*               g_pGreyholeBitmap;  //grey block with hole in it
Bitmap*               g_pSlimeRockBitmap; //Rock with slime on it
Bitmap*               g_pCaveRockBitmap;  //Rock in caves
Bitmap*               g_pGreenkeyBitmap;   //Green key
Bitmap*               g_pBluekeyBitmap;   //Blue key
Bitmap*               g_pYellowkeyBitmap;   //yellow key
Bitmap*               g_pRedkeyBitmap;   //red key
Bitmap*               g_pOrangekeyBitmap;   //orange key
Bitmap*               g_pGreendoorBitmap;  //green door
Bitmap*               g_pBluedoorBitmap;  //blue door
Bitmap*               g_pReddoorBitmap;  //red door
Bitmap*               g_pYellowdoorBitmap;  //yellow door
Bitmap*               g_pOrangedoorBitmap;  //orange door
Bitmap*               g_pRedAmoebaBitmap;    //Red Amoeba
Bitmap*               g_pGreenAmoebaBitmap; //Green amoeba
Bitmap*               g_pYellowAmoebaBitmap;  //Yellow amoeba
Bitmap*               g_pFireBlowBitmap;    //Fire the yellow amoeba blows
Bitmap*               g_p100PointsBitmap;  //Bitmap for getting 100 points
Bitmap*               g_p200PointsBitmap;  //Bitmap for getting 200 points
Bitmap*               g_p300PointsBitmap;  //Bitmap for getting 300 points
//Bitmap*               g_p400PointsBitmap;  //Bitmap for getting 400 points
Bitmap*               g_p500PointsBitmap;  //Bitmap for getting 500 points
Bitmap*               g_p1000PointsBitmap;  //Bitmap for getting 1000 points
Bitmap*               g_p5000PointsBitmap;  //Bitmap for getting 5000 points
//Bitmap*               g_pGetLifeBitmap;     //Bitmap for getting life graphic
Bitmap*               g_pAlienBitmap;     //Alien
Bitmap*               g_pWormBitmap;      //Worm
Bitmap*               g_pRollerBitmap;    //Orange rolling thingee
Bitmap*               g_pBouncyBitmap;    //Purple bouncy thingee
Bitmap*               g_pBlueAmoebaBitmap;     //Blob blowing amoeba
Bitmap*               g_pBubbleBitmap;           //Bubble the alien blows
//Bitmap*               g_pMandMGBitmap;      //Green m&m
Bitmap*               g_pNoodleBitmap;      //Blue m&m
//Bitmap*               g_pMandMYBitmap;      //Yellow m&m
//Bitmap*               g_pMandMOBitmap;      //Orange m&m
//Bitmap*               g_pMandMRBitmap;      //Red m&m
//Bitmap*               g_pMandMBrownBitmap;      //Brown m&m
Bitmap*               g_pPoisonVatBitmap;    //vat of poison
Bitmap*               g_pStalagtiteBitmap; //Stalagtite
Bitmap*               g_pPoisonBlobBitmap; //Stuff the stalagtites drip
Bitmap*               g_pPoisonPopBitmap;  //Popping poison bitmap
Bitmap*               g_pGrassyThingBitmap;  //Bear trap
Bitmap*               g_pIceFloorBitmap; //Ice on floor
Bitmap*               g_pOrangeBitmap;  //orange
Bitmap*               g_pCupcakeBitmap;  //cupcake
Bitmap*               g_pGrapeJuiceBitmap;  //grape juice
Bitmap*               g_pEggsBitmap;  //Dozen Eggs
Bitmap*               g_pMilkshakeBitmap;  //Banana milkshake
//Bitmap*               g_pForkBitmap;   //Forks to get
Bitmap*               g_pJumpBlockBitmap;  //Block that makes you jump
Bitmap*               g_pFloatingPlatformBitmap;  //Floating Platform
Bitmap*               g_pElevatorBitmap;     //Elevator platform
Bitmap*               g_pYellowLaserBitmap;    //Yellow Laser
Bitmap*               g_pOrangeLaserBitmap;  //Orange Laser
Bitmap*               g_pBlueLaserBitmap;    //Blue laser
Bitmap*               g_pStarBlockBitmap;    //Block with star-like thing in middle
Bitmap*               g_pStarBlockCrackedBitmap;    //And 2 cracked versions for the final boss fight
Bitmap*               g_pStarBlockCracked2Bitmap;
Bitmap*               g_pXblockBitmap;  //block with x in it
Bitmap*               g_pYellowHubBitmap; //Yellow Hub
Bitmap*               g_pOrangeHubBitmap; //Orange Hub
Bitmap*               g_pBlueHubBitmap;       //Blue hub
Bitmap*               g_pUFOBitmap;   //Ufo ship
Bitmap*               g_pMissileBitmap;   //missile you need to destroy (carefully)
Bitmap*               g_pMissileHubBitmap; //the actual missile hub you have to shoot

Bitmap*               g_pCheckpointBitmap;

Bitmap*               g_pHighScoreBitmap; //High score screen bitmap
Bitmap*               g_pExitBitmap;   //Exit
Bitmap*               g_pExitClosedBitmap;
//Bitmap*               g_pLifeBitmap; //Life to draw
Bitmap*               g_pFlySpoonRBitmap;  //The spoon you shoot facing right
Bitmap*               g_pFlySpoonLBitmap;  // and left
Bitmap*               g_pDefeatedBitmap;  //bitmap for defeating a level
Bitmap*               g_pGameScreen[22];    //Game screens for different levels
Bitmap*               g_pGameOverBitmap; //Game over bitmap
//Bitmap*               g_pKitchenBackground; //kitchen background (taken out because 4 MB file, too big)
Bitmap*               g_pCavesBackground;   //Background tile for all the cave levels.
Bitmap*               g_pShipBackground;    // Background tile for all the ship levels
Bitmap*               g_pShipBackground2;   // except two levels.
Bitmap*               g_pShipBackground3;
Bitmap*               g_pBottomBitmap;   //Bottom of screen (where you die)
Bitmap*               g_pSignBitmap;     //Bitmap for sign warning you of falling to your death
Bitmap*               g_pBeeBitmap;      //Bitmap for the flying bee creature
Bitmap*               g_pElectrosphereBitmap;  //Bitmap for the ball that gives you a life
//Bitmap*               g_pLoadingLevelBitmap;   //Bitmap for loading a level
Bitmap*               g_pJetBootsBitmap;       //Bitmap for jet blaster boots Chef can get
Bitmap*               g_pSuperJetBootsBitmap;
//Bitmap*               g_pJetThrustRBitmap;      //Bitmap for jet thrust when Chef is using the boots facing right
//Bitmap*               g_pJetThrustLBitmap;      //Bitmap for jet thrust when Chef is using the boots facing left
Bitmap*               g_pSlimeBitmap;           //Bitmap for slime stuff, gobbledygook, or whatever you want to call it
Bitmap*               g_pHoleBitmap;            //Bitmap for hole to secret level
Bitmap*               g_pGoThroughGroundBitmap; //Bitmap for ground you can go through
Bitmap*               g_pSmallBlockBitmap;      //Bitmap for really small block
Bitmap*               g_pLaserGunDownBitmap;    //Bitmap for laser gun pointing down
Bitmap*               g_pLaserGunRightBitmap;   //Bitmap for laser gun pointing right
                                                //That's all the laser guns for now
Bitmap*               g_pLaserBoltHorizontalBitmap; //Bitmap for horizontal laser bolt
Bitmap*               g_pLaserBoltVerticalBitmap;   //Bitmap for vertical laser bolt
Bitmap*               g_pMirrorLeftBitmap;          //Bitmap for mirror facing left
Bitmap*               g_pMirrorRightBitmap;         //Bitmap for mirror facing right
Bitmap*               g_pYellowStairsBitmap;        //Bitmap for yellow stairs
Bitmap*               g_pYellowStairBlockBitmap;    //Bitmap for yellow stair blocks
Bitmap*               g_pBlueStairsBitmap;          //Bitmap for blue stairs
Bitmap*               g_pBlueStairBlockBitmap;      //Bitmap for blue stair blocks
Bitmap*               g_pOrangeStairsBitmap;        //Bitmap for orange stairs
Bitmap*               g_pOrangeStairBlockBitmap;    //Bitmap for... you can probably guess
Bitmap*               g_pBanisterBitmap;            //Bitmap for stair banister (For looks)
Bitmap*               g_pFulcrumBitmap;             //Bitmap for stair fulcrum (also for looks)
Bitmap*               g_pOrangeButtonBitmap;        //Bitmap for orange stair button
Bitmap*               g_pYellowButtonBitmap;        //Bitmap for yellow stair button
Bitmap*               g_pBlueButtonBitmap;          //Bitmap for blue stair button
Bitmap*               g_pSlantedLeftChromeBitmap;         //bitmap for slanted left ground, only chrome
Bitmap*               g_pSlantedLeftChromeCornerBitmap;   //bitmap for slanted left chrome ground corner
Bitmap*               g_pSlantedRightChromeBitmap;        //bitmap for chrome right slanted ground
Bitmap*               g_pSlantedRightChromeCornerBitmap;  //bitmap for chrome right slanted ground corner
Bitmap*               g_pFallBlockBitmap;                 //bitmap for block that can fall on you
Bitmap*               g_pLargeFallBlockBitmap;            //bitmap for larger block that can fall on you
Bitmap*               g_pVortexBitmap;                    //bitmap for vortex
Bitmap*               g_pWoodenBlockBitmap;               //bitmap for wooden block you can shoot
Bitmap*               g_pBatBitmap;                       //bitmap for bat creature
Bitmap*               g_pBalloonBitmap;                   //bitmap for balloon
Bitmap*               g_pBalloonPopBitmap;                //And bitmap for baloon popping
Bitmap*               g_pUndergroundBackground;           //bitmap for background that looks like underground moon rocks
Bitmap*               g_pKeyScreenBitmap;                 //bitmap for screen that shows keys you have
Bitmap*               g_pSmRedKeyBitmap;                  //bitmap for small red key
Bitmap*               g_pSmOrangeKeyBitmap;               //etc.
Bitmap*               g_pSmYellowKeyBitmap;
Bitmap*               g_pSmGreenKeyBitmap;
Bitmap*               g_pSmBlueKeyBitmap;
Bitmap*               g_pFloatingHiddenBitmap;            //Floating hidden platform (Muua-ha-haaaa!)
Bitmap*               g_pAlienFighterBitmap;              //Alien with gun and shield (Double Muua-haa-haa!)
Bitmap*               g_pAlienBulletBitmap;               //Bullet alien fires
Bitmap*               g_pFlySpoonLElectricBitmap;         //bitmap for spoon after it hits alien's shield flying left
Bitmap*               g_pFlySpoonRElectricBitmap;         // and right
Bitmap*               g_pElevatorHiddenBitmap;            //bitmap for hidden elevator platform
Bitmap*               g_pStripedLeftBitmap;               //bitmap for left striped block
Bitmap*               g_pStripedRightBitmap;              //And right
Bitmap*               g_pStripedLGoThroughBitmap;         //Left striped block you can go through
Bitmap*               g_pAmoebaBlockBitmap;               //Block amoebas ooze out of
Bitmap*               g_pAmoebaOozeBitmap;                //Oozing amoeba animation
Bitmap*               g_pAmoebaRockCrumbleBitmap;         //And crumbling rock animation
//Bitmap*               g_pCloud1Bitmap;                    //Bitmaps for clouds (For looks)
//Bitmap*               g_pCloud2Bitmap;
Bitmap*               g_pCoilBitmap;                      //Bitmap for heating coil
Bitmap*               g_pGreenLaserHzBitmap;              //Bitmap for horizontal green laser
Bitmap*               g_pGreenHubBitmap;                  //Bitmap for green hub
Bitmap*               g_pYellowLaserVBitmap;              //Bitmap for vertical yellow laser
Bitmap*               g_pOrangeLaserVBitmap;              //Bitmap for vertical orange laser
Bitmap*               g_pGoThroughRockBitmap;             //Bitmap for rock you go through
Bitmap*               g_pCavesBackground2;                //Bitmap for second cave background
Bitmap*               g_pLeftRockSlant;                   //Bitmap for slanted left rock
Bitmap*               g_pRightRockSlant;                  // etc.
Bitmap*               g_pCLeftRockSlant;
Bitmap*               g_pCRightRockSlant;
Bitmap*               g_pLeftRockCorner;
Bitmap*               g_pRightRockCorner;
Bitmap*               g_pTeleporterBitmap;                //Bitmap for teleporting machine
Bitmap*               g_pMissileExplodeBitmap;             //Bitmap for missile exploding
Bitmap*               g_pGoThroughStarBitmap;             //Bitmap for star block you can go through
Bitmap*               g_pStarSlantR;                      //Bitmap for star slant to the right
Bitmap*               g_pStarSlantL;                      //etc.
Bitmap*               g_pStarSlantCornerR;
Bitmap*               g_pStarSlantCornerL;
//Bitmap*               g_pReedeem[16];                    //Bitmaps for reedeem games
//Bitmap*               g_pSaveRedeem;                     //Background for saving or redeeming a game
Bitmap*               g_pCaveBackground3;                //Third cave background
Bitmap*               g_pCoolRock2Bitmap;                //Second rock
Bitmap*               g_pLava1Bitmap;                    //Bitmap for top of lava vat
Bitmap*               g_pLava2Bitmap;                    //Bitmap for inside lava vat
Bitmap*               g_pLavaSlopeR;                     //Slope for lava block right
Bitmap*               g_pLavaSlopeL;                     //Et cetera (or however you spell it)
Bitmap*               g_pLavaSlopeRCorner;
Bitmap*               g_pLavaSlopeLCorner;
Bitmap*               g_pLavaSlopeCR;
Bitmap*               g_pLavaSlopeCL;
Bitmap*               g_pLavaGoThrough;
//Bitmap*               g_pVacantBitmap[2];                //Bitmaps for vacant and not vacant
Bitmap*               g_pTunnelHzBitmap;                 //Bitmaps for horizontal tunnel and etc.
Bitmap*               g_pTunnelVBitmap;
Bitmap*               g_pTunnelCrossBitmap;
Bitmap*               g_pTunnelFBitmap;
Bitmap*               g_pTunnelTBitmap;
Bitmap*               g_pTunnelPerpBitmap;
Bitmap*               g_pTunnelLBitmap;
Bitmap*               g_pTunnelBackLBitmap;
Bitmap*               g_pTunnelPBitmap;
Bitmap*               g_pTunnelBackPBitmap;
Bitmap*               g_pTunnelBackground;                 //Bitmap for level 15 background
Bitmap*               g_pSewerVatBitmap;                   //Bitmap for vat of sewer stuff (Eeewwww...)
Bitmap*               g_pSewerBackground;                  //Bitmap for background of sewers
Bitmap*               g_pTruckBitmap;                      //Bitmap for sewage-dumping truck
Bitmap*               g_pSludgeDumpBitmap;                 //Bitmap for sewage the truck is dumping
Bitmap*               g_pSludgeFlowBitmap;                 //Sludge flow bitmap
Bitmap*               g_pSludgeFlowCornerBitmap;           //And corner
Bitmap*               g_pSludgeFallBitmap;                 //And falling sludge
Bitmap*               g_pLaserDieUpBitmap;                 //Dying lasers
Bitmap*               g_pLaserDieDownBitmap;
Bitmap*               g_pLaserDieLeftBitmap;
Bitmap*               g_pLaserDieRightBitmap;
Bitmap*               g_pStarCielingLeft;                  //Slanted cieling for star block
Bitmap*               g_pStarCielingRight;
Bitmap*               g_pSuperJumpBlockBitmap;             //Super jump block in level 16
Bitmap*               g_pShootJumpBitmap;                  //Block you shoot twice to turn it into a jump block
Bitmap*               g_pRedDoorDieBitmap;                 //Bitmap for red door dying
Bitmap*               g_pOrangeDoorDieBitmap;              //etc.
Bitmap*               g_pYellowDoorDieBitmap;
Bitmap*               g_pGreenDoorDieBitmap;
Bitmap*               g_pBlueDoorDieBitmap;
Bitmap*               g_pCrackedLogsDieBitmap;              //Bitmap for ground crumbling away
Bitmap*               g_pStarBgBitmap;
Bitmap*               g_pBlockingPlatformBitmap;            //Platform blocking exit in level
Bitmap*               g_pBlockingPlatformDieBitmap;
Sprite*               g_pBlockingPlatform = NULL;

PersonSprite*         g_pPersonSprite;  //Chef that you control
//list<Sprite*>               g_pLaterSprites;//[100]; //sprite list for sprites to add later
TeleporterSprite*     g_pTeleporterSprite[2];  //Teleporter sprites
int                   g_iPointsGotten;  //Points accumulated so far
int                   g_iPointsLevelStart;  //Points at the start of a level (in case they exit level)
int                   g_iLives;         //Amount of lives you have
int                   g_iMoveHorizontal; //An int used to determine which way you shoot
//int                   g_iMIDISound;      //Used to determine when to play a new song
int                   g_iCurrentLevel;//Used to determine current level chef's on
//int                   g_iSpoons;     //Used for amount of spoons player has to shoot
int                   g_iDieCount;      //Used to count how long it takes Chef to die
//int                   g_iInputDelay;  //Used to delay selecting levels (Too fast without)
//int                   g_iPointLifeCounter;//Used to count up to see if player should get another life
//int                   g_iGameOverCount; //Used to see if new game should start
int                   g_iWinCount;      //Used to count player's time winning
int                   g_iHiScores[5]; //High Scores
int                   g_iGame = 1;           //Game to save or redeem
//int                   g_iMIDICount = 2500;    //Midi player counter
int                   g_iTeleporterCount = 0;     // # of times used teleporter
#define FINAL_MISSILE_COUNT 9
int                   g_iMissileCount = 0;        //# of missiles destroyed in last level
bool                  g_bIsFiring;       //Used to see if you fired or not
bool                  g_bFired;     //Used so you can only shoot once per spacebar press
bool                  g_bGreenKey;  //Bool used to determine if player got green key yet
bool                  g_bBlueKey;  //Bool used to determine if player got blue key yet
bool                  g_bYellowKey;  //Bool used to determine if player got yellow key yet
bool                  g_bRedKey;  //Bool used to determine if player got red key yet
bool                  g_bOrangeKey;  //Bool used to determine if player got orange key yet
bool                  g_bYellowHub; //bool for yellow hub
bool                  g_bOrangeHub; //bool for orange hub
bool                  g_bBlueHub;   //bool for blue hub
bool                  g_bGreenHub;  //bool for green hub
bool                  g_bYellowStair;  //bool for yellow stairs
bool                  g_bOrangeStair;  //bool for orange stairs
bool                  g_bBlueStair;    //bool for blue stairs
bool                  g_bPlayingGame; //boolean used to see if currently playing a level
bool                  g_bParts[20];    //Used to record parts gotten
bool                  g_bCurLevelPart;  //If they've done what they need to beat this level, but haven't beaten it yet
bool                  g_bDying; //Used to see if Chef is dying
bool                  g_bFacingLeft;  //Used for dying to see if facing left or right
bool                  g_bWinning; //Used to determine if player is winning
bool                  g_bHighScores; //Used to determine if should display high scores
//bool                  g_bNoSound = false;      //Used to see if sound is off or on
bool                  g_bBeside = false;   //Used to see if beside one block before going on top of another
bool                  g_bOnPlatform = false;  //Used to see if on a floating platform
bool                  g_bOnLeftSlant = false; //If Chef is on a left slant
bool                  g_bOnRightSlant = false;  //if chef is on a right slant
//bool                  g_bLoadingLevel = false;   //If working on loading level
bool                  g_bJetBoots = false;      //Gotten jet boots in game
bool                  g_bSuperJetBoots = false; //Gotten super jet boots in game
bool                  g_bHole = false;          //Boolean for hitting a hole
bool                  g_bEnter = false;       //Record enter key presses and prevent levels from starting at wrong times
bool                  g_bRight = false;       //Record right key presses
bool                  g_bLeft = false;       //Record left key presses
bool                  g_bInvincible = false;  //Cheat code to make it so you can't die
//bool                  g_bFullScreen = false;  //If playing in a maximized window
//int                   g_iWidth, g_iHeight;    //Width and height of current playing screen
myPOINT                 g_ptDissolve[60][200];  //Used for dissolve effect
myRECT                  g_rcViewport;  //Current viewing screen
myRECT                  g_rcAll = {0, 0, 3299, 2624}; //The whole playing area
//Variables for the dialog box and menus
//Bitmap*               g_pMenuButtonBitmap;
//Bitmap*               g_pMenuBitmap;
//Bitmap*               g_pDialogBoxBitmap;
//Bitmap*               g_pXButtonBitmap;
//ButtonSprite*         g_pMenu;
//ButtonSprite*         g_pM;
//ButtonSprite*         g_pXButton;
//bool                  g_bMenu = false;
bool                  g_bTypeName = false;
char                  g_cName[21] = "~~~~~~~~~~~~~~~~~~~~";
char                  g_cNames[5][21] = { {'S', 'u', 'p', 'e', 'r', ' ', 'C', 'h', 'e', 'f', 'm', 'a', 'n', '~', '~', '~', '~', '~', '~', '~', '~'},
                                          {'B', 'l', 'u', 'e', ' ', 'F', 'i', 'r', 'e', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~'},
                                          {'A', 'l', 'i', 'e', 'n', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~'},
                                          {'B', 'i', 'g', ' ', 'Y', 'e', 'l', 'l', 'o', 'w', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~'},
                                          {'B', 'u', 'z', 'z', 'z', 'z', 'z', 'z', 'z', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~', '~'} };
int                   g_iHighScoreSlot = 100;
int                   g_iBlinkDelay = 0;
//Positions to draw high scores and names
myRECT                g_rcHighScoreNamePos[] = {
{106, 239, 383, 272},
{86, 283, 383, 318},
{63,335,383,373},
{36,395,383,436},
{4,464,383,509}};
myRECT                g_rcHighScoreScorePos[] = {
{415,239,693,273},
{415,283,712,318},
{415,335,736,373},
{415,395,763,436},
{415,464,794,509}};

//int                   g_iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
//int                   g_iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

//int                   g_ixStart = (GetSystemMetrics(SM_CXSCREEN) - g_iScreenWidth) / 2;
//int                   g_iyStart = (GetSystemMetrics(SM_CYSCREEN) - g_iScreenHeight) / 2;  //Starting position to draw at



//Better controls for firing/jumping/stuff like dat
int             FIRE_KEY        = 88;
int             JUMP_KEY        = 90;
int             LEFT_KEY        = HGEK_LEFT;
int             RIGHT_KEY       = HGEK_RIGHT;
int             UP_KEY          = HGEK_UP;
int             DOWN_KEY        = HGEK_DOWN;

#define         COLLISION_EDGE_ADD  10
int             MAX_JETBOOT_TIME    =15;

#define         THRUST_FORCE    2
#define         MAX_JUMP_AMOUNT 18

int             g_iJetbootsTime = MAX_JETBOOT_TIME;
bool            g_bDoubleJumpPressThisCycle = false;

int             g_iJetbootSoundDelay = 0;
#define         JETBOOT_SOUND_DELAY     3

int             g_iMoveSoundDelayTime = 2;  //Used to make sounds of chef moving only every 4 cycles
#define         WALK_SOUND_DELAY        4

int             g_iJumpSoundDelay = 0;      //Only play jump sound every few cycles
#define         JUMP_SOUND_DELAY        10

myRECT            g_rcLastCheckpointPos = {0,0,0,0};  //Last position of checkpoint
myRECT            g_rcLastViewport = {0,0,0,0};
Sprite*         g_pLastCheckpoint = NULL;

#define FRAME_RATE 45
bool g_bFadeWhite = false;

//HACK: For panning towards missile during explosion
myPOINT g_ptPanTo = {-1, -1};
bool g_bPanBackToPlayer = false;

//For holding data so we can respawn everything
typedef struct {
    Sprite* pSprite;
    myPOINT   ptVelocity;
    myRECT    rcPosition;
    int     iCurFrame;
    int     iMovePointer;   //For enemies with AI
}RespawnItem;

list<RespawnItem>   g_lRespawnList;         //List of objects to respawn on checkpoint
list<Sprite*>       g_lDespawnList;         //List of objects to despawn on a checkpoint
void AddToRespawn(Sprite* pSprite);     //Add this sprite to the list of objects to respawn on death/checkpoint
void AddToDespawn(Sprite* pSprite);     //Add this sprite to the despawn list
void RemoveFromDespawn(Sprite* pSprite);    //Grr segfaults
void RespawnAll();                      //Respawn all cached sprites
void EmptyRespawn();                    //Empty the respawn queue

void PlayMySound(string sFilename);     //My function for playing sounds, just calls game engine. Just using it so I don't have to
                                        // cross-include everything

bool g_bIncompleteViewport = false;       //Set to true to PatBlt() everything black around viewport that doesn't take up whole screen
bool g_bIncompleteViewportDrawn = false;

#define HACK_ZORDER_NOSPAWN 314159          //Hack, hack. Zorder = pi*100,000 means SpriteDying() isn't called on it

#define CHECKPOINT_INVINCIBLE_TIME  40
int  g_iCheckpointInvincibility = 0;   //The player is invincible for a second after respawning

//For crab enemy final boss thing
void    UpdateBoss();                   //Our main workhorse for setting the boss to the right animation, etc
void    UpdateBossFrame();              //Set the boss to the right frame of animation
void    SetNextBossAction();            //Set the boss to his next action
myPOINT&  GetCrabCenter();
myPOINT&  GetPlayerCenter();              //For getting position of player and crab
int     GetFastestWayToPlayer();        //Because boss wraps, fastest way may be through wall. Returns <0 for left, >0 for right
void    GetClawRects(myRECT* rcClaw1, myRECT* rcClaw2);       //Gets the positions of the crab's claws
myRECT&   GetBodyRect();                  //Gets the position of the crab's body
TrapSprite* g_pEnemyCrab;               //Final boss guy
int         g_iBossHealth;              //and how much health he has
int         g_iCurBossJumpAmt;                //For when he jumps in the air
int         g_iBossWalkLoopChannel = -2;         //What channel the boss sound is playing on
#define BOSS_MAX_HEALTH     50          //How much health the boss has (how many shots to kill)
#define BOSS_JUMP_AMOUNT    43          //How powerful his jump is
#define BOSS_GRAVITY_AMOUNT  2          //How much this jump is decreased each cycle
#define BOSS_ACCELERATE_AMOUNT  4.0      //How fast the boss can accelerate
#define BOSS_MAX_MOVE_SPEED     12.5      //How fast the boss moves
int         g_iBossFloorYLevel;         //How far down the boss can go (so doesn't fall through floor)

#define BOSS_HULKSMASH      0           //Pound the ground, destroying blocks
#define BOSS_RUNL           1           //charge left
#define BOSS_RUNR           2           //charge right
#define BOSS_AVOIDPLAYER    3           //Run away from the player
#define BOSS_SPAWNAMOEBA    4           //Create blue amoeba
#define BOSS_PAUSE          5           //Stand still for a sec
#define BOSS_JUMP           6           //Jump up at the player
int         g_iCurBossAction = BOSS_PAUSE;              //What the boss is doing (so we can determine what frame to set him to)

#define MAX_TIME            9999999  // a while, basically
int         g_iTimeLeftInThisAction = 40;     //how long the boss will remain in this g_iCurrBossAction

bool g_bBossDying = false;

int g_iBossFrame = 0;
int g_iBossFrameDelayCounter = 0;
#define BOSS_JUMP_DELAY     200         //How long of a delay before boss jumps again
int g_iBossJumpDelay = BOSS_JUMP_DELAY;

#define BOSS_MAX_HIT_TOLERANCE  5       //If boss is avoiding, how many hits he'll sustain before attacking again
int g_iBossAvoidToleranceCount = 0;

int g_iBossEnemiesSpawned = 0;      //How many blue amoebas he spawned for player to kill
int g_iBossEnemiesKilled  = 0;      //How many the player has killed so far (or have fallen out of the level and died)

int g_iBossHitThisCycle = 0;   //If the boss has been hit this cycle, flash bg of boss health meter red

#define NUM_BOSS_FRAMES     18
Bitmap*               g_pCrabBitmap[NUM_BOSS_FRAMES];         //Giant Enemy Crab final boss animation frames are stored in an array,
                                                              //because it's too large of an image otherwise
int  g_iViewportShakeAmt = 0;
//#define SHAKE_DELAY_AMT 4
//int  g_iViewportShakeDelay = 0;

//Joystick global thingy
JOYSTATE g_jsCurrJoyState = 0;

//Abstraction for ease of joystick support
bool LeftKey();
bool RightKey();
bool JumpKey();
bool FireKey();
bool UpKey();
bool DownKey();

//Pause menu stuff
bool g_bPaused = false;
Bitmap* g_pPauseBmp;
Bitmap* g_pPauseMenuBmps[5];
myRECT g_rcPauseMenuLocations[] = {
{186,183,324,214},
{110,223,400,264},
{184,267,327,308},
{172,315,339,347},
{159,359,351,392},
};
int g_iPausePos = 0;

void HandlePauseMenuItem(int iItem);

bool g_bUpKeyPressed = false;
bool g_bDownKeyPressed = false;
myPOINT g_ptLastMousePos = {-1,-1};

//Levels that have hubs
static int g_iHubLevels[] = {2, 4, 5, 6, 9, 12, 13, -1};
Sprite* g_pExitSprite = NULL;
bool g_bExitOpen = false;

//Dialog/error msg stuff----------------------------------------------------------
string g_sDialogText = "";
Bitmap* g_pDiagBmp = NULL; //If a bitmap is supposed to be drawn for this dialog, this will point to it
#define DIALOG_MARGIN   5   //How many pixels on each side of the above bitmap, how large of margins on each side, etc
#define DISPLAY_FOREVER -1
//#define DISPLAY_DEFAULT 3   //3 seconds
int g_iDisplayDiagTime = DISPLAY_FOREVER;    //How long to display dialog for.
int g_iStartedDisplayDiag = 0;               //When we started displaying this dialog
#define DIALOG_BITMAP_ALIGN_RIGHT   0
#define DIALOG_BITMAP_ALIGN_LEFT    1
int g_iDialogBitmapAlign = DIALOG_BITMAP_ALIGN_LEFT;

//Main menu stuffz--------------------------------------------------------------
string GetMenuText(int Menu, int MenuPos);
void   HandleMenuSelect(int Menu, int MenuPos);

int g_iCameToHighScoresFrom = -1;

#define NUM_MAINMENU_ITEMS  6

#define MAINMENU_OPTIONS    0
#define MAINMENU_NEWGAME    1
#define MAINMENU_CONTINUE   2
#define MAINMENU_HIGHSCORES 3
#define MAINMENU_QUIT       4
#define MAINMENU_PROFILES   5

string g_sMenuText;     //For displaying what user is mousing over

const myRECT g_rcMainMenuTextPos = {262,225,537,258};

bool g_bEscPressed = false;

const float g_fMainMenuPosx[NUM_MAINMENU_ITEMS][4] = {
{564,632,648,575},
{213,248,221,180},
{272,528,565,236},
{541,575,613,572},
{587,622,661,620},
{257,542,543,256},
};

const float g_fMainMenuPosy[NUM_MAINMENU_ITEMS][4] = {
{273,273,320,320},
{359,359,495,495},
{315,315,522,522},
{335,335,495,495},
{359,359,495,495},
{220,220,262,262},
};

const myPOINT g_ptMainMenuDrawPos[NUM_MAINMENU_ITEMS] = {
{562, 259},
{179, 357},
{232, 313},
{539, 333},
{585, 357},
{261,225},
};

Bitmap* g_pMainMenuOverBmp[NUM_MAINMENU_ITEMS];
#define NUM_MENUS   5
Bitmap* g_pMenuBgBitmap[NUM_MENUS];
int g_iMenuPosSelect = MAINMENU_CONTINUE;


//Profiles menu stuffz---------------------------------------------
void LoadProfileNames();    //Grab the profile names from the files and stuff 'em into g_sProfileNames
#define NUM_PROFILES            3
#define NUM_PROFILEMENU_ITEMS   7
#define PROFILEMENU_PROFILE1    0
#define PROFILEMENU_PROFILE2    1
#define PROFILEMENU_PROFILE3    2
#define PROFILEMENU_BACK        3
#define PROFILEMENU_DEL1        4
#define PROFILEMENU_DEL2        5
#define PROFILEMENU_DEL3        6
Bitmap* g_pProfileSelectBmp[NUM_PROFILEMENU_ITEMS];

//For delete profile warning box thing
void    DeleteProfile(int iProfile);
bool    exists(int iProfile);
bool    g_bDeleteWarning = false;
int     g_iProfileToDelete = -1;
Bitmap* g_pDeleteWarningBmp;
Bitmap* g_pDeleteYNBmp[2];
#define DELETE_YES  0
#define DELETE_NO   1
const myPOINT g_ptDeleteYNDraw[2] = {
{306,355},
{436,355}
};
const myRECT g_rcDeleteYNClick[2] = {
{306,355,372,389},
{436,355,493,389}
};

string g_sProfileNames[3];

const myPOINT g_ptProfileFlamePos[NUM_PROFILES] = {
{228,472},
{491,472},
{753,472}
};

Sprite* g_pFlameSprites[NUM_PROFILES];  //Sprites for flames, for profile and for options menus

const float g_fProfileSelectx[NUM_PROFILEMENU_ITEMS][23] = {
{190,223,244,253,256,252,241,227,208,182,150,128,107,87,74,70,70,77,86,103,119,142,167},
{399,428,451,473,486,489,480,465,448,421,399,373,344,327,313,311,321,335,352,377},
{611,659,703,726,730,718,695,664,628,589,559,541,548,572},
{257,542,543,256},
{229,239,243,249,253,251,243,227,215,206,210,214,223},
{490,500,505,512,512,504,494,479,470,467,468,475,485},
{747,759,766,766,771,770,763,754,740,726,723,727,736,742}
};

const float g_fProfileSelecty[NUM_PROFILEMENU_ITEMS][23] = {
{313,322,341,360,385,410,438,458,479,496,509,510,505,492,471,454,422,398,380,359,344,327,316},
{309,314,329,356,387,418,453,478,494,506,510,506,490,469,440,398,365,344,328,314},
{311,325,363,411,451,483,504,510,504,480,444,390,348,321},
{220,220,262,262},
{465,467,473,475,483,495,506,510,505,492,482,475,466},
{466,469,476,484,494,505,510,508,498,489,481,473,466},
{466,469,477,482,490,498,505,510,509,498,482,474,470,466}
};

const int g_iProfileSelectNumPts[NUM_PROFILEMENU_ITEMS] = {23, 20, 14, 4, 13, 13, 14};

const myPOINT g_ptProfileMenuBmpDrawLoc[NUM_PROFILEMENU_ITEMS] = {
{68,266},
{309,266},
{541,266},
{261,224},
{212,465},
{470,465},
{725,465}
};

const myRECT g_rcProfileTextDrawPos = {262,225,537,257};

//Options menu stuffz---------------------------------------------------------------------------------
int g_iCameToOptionsFrom = -1;
//#define CAME_FROM_MAIN_MENU     0
//#define CAME_FROM_PAUSE_MENU    1

#define SOUNDSLIDER_MAXX    339
#define MUSICSLIDER_MAXX    385

#define OPTIONSMENU_FULLSCREENTOGGLE    0
#define OPTIONSMENU_SOUNDTOGGLE         1
#define OPTIONSMENU_MUSICTOGGLE         2
#define OPTIONSMENU_KEYCONFIGMENU       3
#define OPTIONSMENU_RESOLUTIONMENU      4
#define OPTIONSMENU_BACK                5
#define OPTIONSMENU_SOUNDSLIDER         6
#define OPTIONSMENU_MUSICSLIDER         7

int GetSliderLoc(int iSlider);

const float g_fOptionsMenuItemsx[8][10] = {
{586,598,593,598,593,579,566,562,571,575},
{576,587,597,586,570,557,558,566,570},
{600,613,623,622,608,596,581,577},
{69,213,203,52},
{611,756,774,624},
{257,542,543,256},
{116,476,476,116},
{81,488,488,81}
};
const float g_fOptionsMenuItemsy[8][10] = {
{229,234,266,272,283,286,279,268,261,232},
{329,332,348,363,363,350,338,334,330},
{426,431,446,452,465,466,455,437},
{252,251,286,287},
{250,250,286,286},
{220,220,262,262},
{333,333,365,365},
{425,425,457,457}
};
const myPOINT g_ptOptionsMenuDrawPos[6] = {
{566,228},
{561,328},
{581,425},
{61,224},
{624,224},
{261,224}
};
const int g_iOptionsMenuCount[8] = {10,9,8,4,4,4,4,4};
Bitmap* g_pOptionsMenuBmp[6];   //For the bitmaps other than sliders

const myPOINT g_ptOptionsFlamePos[NUM_PROFILES] = {
{587,231},  //Fullscreen toggle
{579,332},  //Sound toggle
{603,431}   //Music toggle
};

Bitmap* g_pSoundSliderBmps[2];

int g_iSliderDrag = -1; //If we're dragging a slider

const myRECT g_rcOptionsTextPos = {261,224,538,257};    //Where to draw text for the plaque in the options menu
const myRECT g_rcOptionsSliderPos[2] = {{116,333,476,365},{81,425,488,457}};

//Screen resolution select stuffz---------------------------------------------------------------------
Bitmap* g_pResolutionMenuButtons[3];
#define RESOLUTIONMENU_UP   0
#define RESOLUTIONMENU_DOWN 1
#define RESOLUTIONMENU_BACK 2

const myPOINT g_ptResolutionDrawPos[3] = {   //For drawing the bitmaps when you mouseover menu items
{651,250},
{716,420},
{267,197}
};

const myRECT g_rcResolutionTextPos = {267,197,532,229}; //For drawing the text as you select menu items
const myRECT g_rcResolutionPos = {132,255,634,274};    //For drawing the actual resolution text
#define RESOLUTION_NUM_DRAW     13  //How many resolution items to draw
#define RESOLUTION_INCREMENT_Y  20  //How far in the y direction we add for drawing each item

int g_iResolutionScrollPos = 0;    //Where the scroll menu starts drawing

const float g_fResolutionMenuItemx[3][4] = {
{652,671,695,674},
{716,740,780,753},
{263,536,536,263}
};
const float g_fResolutionMenuItemy[3][4] = {
{251,251,310,310},
{421,421,518,518},
{193,193,232,232}
};

bool g_bConfirmResolution = false;  //Dialog box to confirm screen resolution change
Bitmap* g_pConfirmResolutionBg;
Bitmap* g_pConfirmResolutionClick[2];
int     g_iStartedCountdownAt = 0;      //When we started counting down to reset screen res
#define RESOLUTION_CHANGE_PAUSE     10  //How many seconds the game waits until resetting the resolution
#define RESOLUTION_CONFIRM_YES      0
#define RESOLUTION_CONFIRM_NO       1
int     g_iOldWidth, g_iOldHeight;
bool    g_bOldFullscreen;       //To hold onto our last screen settings
myRECT  g_rcConfirmResolutionClick[2] = {
{307,360,378,393},
{437,360,497,393}
};
myRECT  g_rcConfirmResolutionText = {285,248,514,268};

//Key config menu stuffz------------------------------------------------------------------------------
#define KEYCONFIG_UP    0
#define KEYCONFIG_DOWN  1
#define KEYCONFIG_LEFT  2
#define KEYCONFIG_RIGHT 3
#define KEYCONFIG_JUMP  4
#define KEYCONFIG_FIRE  5
#define KEYCONFIG_BACK  6

const myRECT    g_rcKeyConfigItemsPos[7] = {
{89,276,388,312},
{66,328,387,366},
{39,387,386,429},
{7,457,385,503},
{412,301,721,338},
{414,417,774,460},
{261,224,538,257}
};

int*    g_pKeyToSet = NULL;

//Level select UI stuffz------------------------------------------------------------------------------
#define LEVELSELECT_PLAY_LEVEL      0
#define LEVELSELECT_PREV            1
#define LEVELSELECT_NEXT            2
#define LEVELSELECT_BACK            3

myPOINT g_ptLevelSelectDrawPos[4] = {
{22,195},
{0,305},
{720,305},
{19,556}
};

float g_fLevelSelectx[4][4] = {
{153,646,777,22},
{29,79,53,0},
{720,769,798,746},
{19,292,292,19}
};

float g_fLevelSelecty[4][4] = {
{195,195,519,519},
{314,314,371,371},
{314,314,371,371},
{556,556,584,584}
};

Bitmap* g_pBackBitmap;
Bitmap* g_pLevelSelectBitmap[4];

//Generic menu stuffz---------------------------------------------------------------------------------
int g_iMenuPos = 0; //What menu we're currently on
#define NO_MENU         -1
#define MAIN_MENU       0
#define PROFILES_MENU   1
#define OPTIONS_MENU    2
#define RESOLUTION_MENU 3
#define KEYCONFIG_MENU  4


//Tutorial stuffz-------------------------------------------------------------------------------------
bool    g_bTutorialMoved = false;
bool    g_bTutorialJumped = false;
bool    g_bTutorialFired = false;
bool    g_bTutorialSlopeFired = false;
bool    g_bTutorialElectrosphere = false;
bool    g_bTutorialWASD = false;
#define TUTORIAL_JUMP       1
#define TUTORIAL_FIRE       2
#define TUTORIAL_SLOPEFIRE  3
Bitmap* g_pTutorialBitmap;

//Stuffz for final level dialog-----------------------------------------------------------------------
#define PROFILE_CHEF    0
#define PROFILE_CRAB    1
Bitmap* g_pDialogProfileBmp[2];
int     g_iFinalBossDialogLocation = 0;
const string  g_sFinalBossDialog[3] = {"Oh, look. A giant enemy crab. How original.", "RAAAAAAAAAAAAAAAAAAAAAAAAAAWR!!!", "Yeah, yeah. I hear you. Let's get it over with already."};
#define DIALOG_OVER     3

//Stuffz for power management------------------------------------------------------------------------
int g_iPowerLastChecked = 0;
#define POWER_CHECK_FREQUENCY   300000  //Check every 5 minutes

//Electrosphere/secret level stuffz------------------------------------------------------------------
list<myPOINT>   g_lElectrospheresCollected;
bool CheckElectrosphere(myPOINT pt);    //Return true if we have this already, false if not
bool CheckElectrosphere(int x, int y);    //Return true if we have this already, false if not
void EmptyElectrosphereList();
bool g_b3D = false;
int  g_iOldElectroCount = 0;

#define NUM_ELECTROSPHERES_3D1  19  //How many electrospheres you need for secret 3D level 1
#define NUM_ELECTROSPHERES_3D2  38  //and 2

//Bg story stuff-------------------------------------------------------------------------------------
#define NUM_BGSTORY_PANELS  4
#define NUM_EGSTORY_PANELS  2
string g_sBgStory[NUM_BGSTORY_PANELS];
string g_sEndgameStory[NUM_EGSTORY_PANELS];
Bitmap* g_pBgStoryBmp[NUM_BGSTORY_PANELS];
Bitmap* g_pEgStoryBmp[NUM_EGSTORY_PANELS];
bool g_bBgStory = true; //If we're reading the bg story or no
bool g_bEgStory = false;
int  g_iBgStoryPanel = 0;   //What part of the bg story we're reading

#define BG_STORY_PREV   0
#define BG_STORY_NEXT   1
#define BG_STORY_SKIP   2

Bitmap* g_pBgOverBmp[3];

const myRECT g_rcBgStoryClickPos[3] = {
{27,520,173,554},
{626,520,773,554},
{358,499,442,573}
};

//For debugging purposes
#include <fstream>
ofstream ofile;

//Random stuff to have fun with Jason
int g_iKillCounter = 0;

#endif  //Defined CHEF_H
