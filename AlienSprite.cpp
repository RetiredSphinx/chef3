//-----------------------------------------------------------------
// Alien Sprite Object
// C++ Source - AlienSprite.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "myPOINT.h"
#include "AlienSprite.h"
#include "Resource.h"
#include <string>
#include "GameEngine.h"
using namespace std;

//-----------------------------------------------------------------
// External Global Variables
//-----------------------------------------------------------------
extern Bitmap* g_pPoisonBlobBitmap;
extern Bitmap* g_pBubbleBitmap;
extern Bitmap* g_pFireBlowBitmap;
extern Bitmap* g_pYellowAmoebaBitmap;
extern Bitmap* g_pGreenLaserHzBitmap;
extern Bitmap* g_pBlueLaserBitmap;
extern Bitmap* g_pYellowLaserBitmap;
extern Bitmap* g_pOrangeLaserBitmap;
extern Bitmap* g_pYellowLaserVBitmap;
extern Bitmap* g_pOrangeLaserVBitmap;
extern myRECT g_rcAll;
//extern bool g_bNoSound;
extern bool g_bDying;
//extern HINSTANCE g_hInstance;
extern bool IsInView(Sprite* pSprite);
extern GameEngine* g_pGame;
//extern void g_pGame->AddSprite(Sprite* pSprite);

bool FireKey();
void PlayMySound(string sFilename);
void AddToRespawn(Sprite* pSprite);

//For boss fight
extern int g_iBossEnemiesKilled;
extern int g_iBossEnemiesSpawned;
extern int g_iCurrentLevel;

extern myPOINT g_ptPanTo;
extern bool g_bPanBackToPlayer;

//-----------------------------------------------------------------
// AlienSprite Constructor(s)/Destructor
//-----------------------------------------------------------------
PoisonSprite::PoisonSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

LaserSprite::LaserSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

TrapSprite::TrapSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

TeleporterSprite::TeleporterSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

AlienSprite::AlienSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

AlienFighterSprite::AlienFighterSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

BubbleBlowerSprite::BubbleBlowerSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
    m_iBlowCount = 10;
}

BadGuySprite::BadGuySprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

RollerSprite::RollerSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

VortexSprite::VortexSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

TruckSprite::TruckSprite(Bitmap* pBitmap, myRECT& rcBounds,
  BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds,
  baBoundsAction)
{
}

PoisonSprite::~PoisonSprite()
{
}

LaserSprite::~LaserSprite()
{
}

TrapSprite::~TrapSprite()
{
}

TruckSprite::~TruckSprite()
{
}

TeleporterSprite::~TeleporterSprite()
{
}

AlienSprite::~AlienSprite()
{
}

AlienFighterSprite::~AlienFighterSprite()
{
}

#include <fstream>
using namespace std;
extern ofstream ofile;
BubbleBlowerSprite::~BubbleBlowerSprite()
{
    if(g_iCurrentLevel == 19)
    {
        g_iBossEnemiesKilled++;     //Increment enemy kill count if in final boss level
    }
}

BadGuySprite::~BadGuySprite()
{
}

RollerSprite::~RollerSprite()
{
}

VortexSprite::~VortexSprite()
{
}

//-----------------------------------------------------------------
//General Methods
//-----------------------------------------------------------------
SPRITEACTION PoisonSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  // Call the base sprite Update() method
  SPRITEACTION saSpriteAction;
  saSpriteAction = Sprite::Update(fTimestep);

  // See if the alien should fire a missile
  if (++m_iDripCounter >= 95)
  {
    saSpriteAction |= SA_ADDSPRITE;
    m_iDripCounter = 0;
  }

  return saSpriteAction;
}

SPRITEACTION LaserSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  // Call the base sprite Update() method
  SPRITEACTION saSpriteAction;
  saSpriteAction = Sprite::Update(fTimestep);

  // See if the laser should die
  if (g_bYellowStair && (GetBitmap() == g_pYellowStairsBitmap))
  {
    //Create the staircase cascade effect
    if(IsInView(this))
    {
        myRECT    rcPos = GetPosition();
        saSpriteAction |= SA_KILL;
        Sprite* pSprite = new Sprite(g_pYellowStairBlockBitmap, g_rcAll, BA_BOUNCE);
        pSprite->SetVelocity(0, 0);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 75, rcPos.top);
        g_pGame->AddSprite(pSprite);
        pSprite = new Sprite(g_pYellowStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 4);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 150, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pYellowStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 6);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 225, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pYellowStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 8);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.left, rcPos.top);
        g_pGame->AddSprite(pSprite);
    }
    g_ptPanTo = GetCenter();
    g_bPanBackToPlayer = true;
  }
  else if (g_bOrangeStair && (GetBitmap() == g_pOrangeStairsBitmap))
  {
    if(IsInView(this))
    {
        //Create the staircase cascade effect
        myRECT    rcPos = GetPosition();
        saSpriteAction |= SA_KILL;
        Sprite* pSprite = new Sprite(g_pOrangeStairBlockBitmap, g_rcAll, BA_BOUNCE);
        pSprite->SetVelocity(0, 0);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 75, rcPos.top);
        g_pGame->AddSprite(pSprite);
        pSprite = new Sprite(g_pOrangeStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 4);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 150, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pOrangeStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 6);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 225, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pOrangeStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 8);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.left, rcPos.top);
        g_pGame->AddSprite(pSprite);
    }
    g_ptPanTo = GetCenter();
    g_bPanBackToPlayer = true;
  }
  else if (g_bBlueStair && (GetBitmap() == g_pBlueStairsBitmap))
  {
    if(IsInView(this))
    {
        //Create the staircase cascade effect
        myRECT    rcPos = GetPosition();
        saSpriteAction |= SA_KILL;
        Sprite* pSprite = new Sprite(g_pBlueStairBlockBitmap, g_rcAll, BA_BOUNCE);
        pSprite->SetVelocity(0, 0);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.left, rcPos.top);
        g_pGame->AddSprite(pSprite);
        pSprite = new Sprite(g_pBlueStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 4);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.left + 75, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pBlueStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 6);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.left + 150, rcPos.top);
        g_pGame->AddSprite(pSprite);
        rcPos.bottom += 75;
        pSprite = new Sprite(g_pBlueStairBlockBitmap, rcPos, BA_DIE);
        pSprite->SetVelocity(0, 8);
        pSprite->SetZOrder(3);
        pSprite->SetNumFrames(4);
        pSprite->SetFrameDelay(2);
        pSprite->SetPosition(rcPos.right - 75, rcPos.top);
        g_pGame->AddSprite(pSprite);
    }
    g_ptPanTo = GetCenter();
    g_bPanBackToPlayer = true;
  }
  else if(g_bGreenHub && m_pBitmap == g_pGreenLaserHzBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }
  else if(g_bBlueHub && m_pBitmap == g_pBlueLaserBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }
  else if(g_bYellowHub && m_pBitmap == g_pYellowLaserBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }
  else if(g_bOrangeHub && m_pBitmap == g_pOrangeLaserBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }
  else if(g_bYellowHub && m_pBitmap == g_pYellowLaserVBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }
  else if(g_bOrangeHub && m_pBitmap == g_pOrangeLaserVBitmap)
  {
    //saSpriteAction |= SA_KILL;
    AddToRespawn(this);
  }

  return saSpriteAction;
}

SPRITEACTION AlienSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  char c = AlienMove[m_iMovePointer];
  myPOINT ptVelocity = GetVelocity();
  myRECT rcPerson = g_pPersonSprite->GetPosition();
  myRECT rcAlien = GetPosition();

  if(++m_iMovePointer > 337)
    m_iMovePointer = 0;

  switch (c)
  {
    case 'R':
      ptVelocity.x = -ptVelocity.x;
      break;
    case 'P': //Attack player
      if((rcPerson.left > rcAlien.left) && (ptVelocity.x < 0))
        ptVelocity.x = -ptVelocity.x;
      else if((rcPerson.left < rcAlien.left) && (ptVelocity.x > 0))
        ptVelocity.x = -ptVelocity.x;
      break;
    case 'J':
      m_bIsJumping = true;
      m_iJumpAmount = -21;
      break;
    case 'j':
      m_bIsJumping = true;
      m_iJumpAmount = -12;
      break;
    default:
      break;
  }
  SetVelocity(ptVelocity);

  // Call the base sprite Update() method
  SPRITEACTION saSpriteAction;
  saSpriteAction = Sprite::Update(fTimestep);

  return saSpriteAction;
}

SPRITEACTION AlienFighterSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  myPOINT ptVelocity = GetVelocity();
  myRECT rcPerson = g_pPersonSprite->GetPosition();
  myRECT rcAlien = GetPosition();
  SPRITEACTION saSpriteAction = SA_NONE;

  if(++m_iMovePointer > 337)
    m_iMovePointer = 0;

  if(FireKey())     //If player firing
  {
    m_iMovePointer = 28;     //Set to put shield up in just a second
  }

  char c = AlienFighterMove[m_iMovePointer];

  switch (c)
  {
    case 'R':
      if(rand() % 2 == 0)                //Make a little unpredictable
        ptVelocity.x = -ptVelocity.x;
      break;
    case 'P': //Attack player
      if((rcPerson.left > rcAlien.left) && (ptVelocity.x < 0))
        ptVelocity.x = -ptVelocity.x;
      else if((rcPerson.left < rcAlien.left) && (ptVelocity.x > 0))
        ptVelocity.x = -ptVelocity.x;
      break;
    case 'J':
      m_bIsJumping = true;
      m_iJumpAmount = -21;
      break;
    case 'j':
      m_bIsJumping = true;
      m_iJumpAmount = -12;
      break;
    case 'F':
      if(ptVelocity.x != 0)
        saSpriteAction |= SA_ADDSPRITE;
      if(ptVelocity.x > 0)
        m_iCurFrame = 19;
      else if(ptVelocity.x < 0)
        m_iCurFrame = 18;
      ptVelocity.x = 0;
      break;
    case 'S':
      if(ptVelocity.x != 0)
      {
        if(ptVelocity.x > 0)
        {
          if(rcAlien.top - rcPerson.top > 150)  //If player above the alien
            m_iCurFrame = 21;     //make the alien hold his shield up
          else
            m_iCurFrame = 17;
        }
        else
        {
          if(rcAlien.top - rcPerson.top > 150)  //If player above the alien
            m_iCurFrame = 20;     //make the alien hold his shield up
          else
            m_iCurFrame = 16;
        }
        ptVelocity.x = 0;
      }
      break;
    default:
      if(ptVelocity.x == 0)
      {
        if(m_iCurFrame == 17 || m_iCurFrame == 21)
          ptVelocity.x = 8;
        else if(m_iCurFrame == 16 || m_iCurFrame == 20)
          ptVelocity.x = -8;
        else if(m_iCurFrame == 19)
          ptVelocity.x = 8;
        else if(m_iCurFrame == 18)
          ptVelocity.x = -8;
      }
      break;
  }
  SetVelocity(ptVelocity);

  // Call the base sprite Update() method
  saSpriteAction |= Sprite::Update(fTimestep);

  return saSpriteAction;
}

SPRITEACTION BubbleBlowerSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  char c = BubbleMove[m_iMovePointer];
  myPOINT ptVelocity = GetVelocity();
  myRECT rcPerson = g_pPersonSprite->GetPosition();
  myRECT rcAlien = GetPosition();

  // Call the base sprite Update() method
  SPRITEACTION saSpriteAction;
  saSpriteAction = Sprite::Update(fTimestep);

  if(++m_iMovePointer > 337)
    m_iMovePointer = 0;

  switch (c)
  {
    case 'R':
      ptVelocity.x = -ptVelocity.x;
      break;
    case 'P':   //Attack player
      if((rcPerson.left > rcAlien.left) && (ptVelocity.x < 0))
        ptVelocity.x = -ptVelocity.x;
      else if((rcPerson.left < rcAlien.left) && (ptVelocity.x > 0))
        ptVelocity.x = -ptVelocity.x;
      m_bIsJumping = true;
      m_iJumpAmount = -12;
      break;
    case 'B':
      saSpriteAction |= SA_ADDSPRITE;
      if(ptVelocity.x > 0)
        m_iCurFrame = 9;
      else
        m_iCurFrame = 8;
      break;
    default:
      break;
  }
  SetVelocity(ptVelocity);

  return saSpriteAction;
}

SPRITEACTION VortexSprite::Update(float fTimestep)
{
  if(m_bHidden)
    return SA_NONE;

  myPOINT ptVelocity = GetVelocity();

  // Call the base sprite Update() method
  SPRITEACTION saSpriteAction;
  saSpriteAction = Sprite::Update(fTimestep);

  if(m_iDripCounter == 0)
  {
    ptVelocity.x = min(ptVelocity.x + 1.0, 17.0);
    if(ptVelocity.x == 17)
      m_iDripCounter = 1;
  }
  else
  {
    ptVelocity.x = max(ptVelocity.x - 1.0, -17.0);
    if(ptVelocity.x == -17)
      m_iDripCounter = 0;
  }

  if(m_iMovePointer == 0)
  {
    ptVelocity.y = min(ptVelocity.y + 1.0, 17.0);
    if(ptVelocity.y == 17)
      m_iMovePointer = 1;
  }
  else
  {
    ptVelocity.y = max(ptVelocity.y - 1.0, -17.0);
    if(ptVelocity.y == -17)
      m_iMovePointer = 0;
  }

  SetVelocity(ptVelocity);

  return saSpriteAction;
}

Sprite* PoisonSprite::AddSprite()
{
  // Create a new missile sprite
  myRECT    rcBounds = g_rcAll;
  myRECT    rcPos = GetPosition();
  Sprite* pSprite = NULL;

  if(GetBitmap() == g_pStalagtiteBitmap)
  {
    // Drop blob
    pSprite = new Sprite(g_pPoisonBlobBitmap, rcBounds, BA_DIE);
    pSprite->SetVelocity(0, 12);
    pSprite->SetZOrder(6);

    // Set the blob sprite's position and return it
    pSprite->SetPosition(rcPos.left + ((GetWidth() / 2) - (pSprite->GetWidth() / 2)), rcPos.bottom - 20);
  }
  else if(GetBitmap() == g_pLaserGunDownBitmap)
  {
    // make laser bolt
    pSprite = new Sprite(g_pLaserBoltVerticalBitmap, rcBounds, BA_DIE);
    pSprite->SetVelocity(0, 25);
    pSprite->SetZOrder(6);
    //pSprite->SetNumFrames(5);

    // Set the laser bolt sprite's position and return it
    pSprite->SetPosition(rcPos.left + 30, rcPos.top + 21);
    if(IsInView(this) && (!g_bDying))
    {
      // Play the shoot laser sound
      PlayMySound(IDW_SHOOTLASER);
    }
  }
  else if(GetBitmap() == g_pLaserGunRightBitmap)
  {
    // make laser bolt
    pSprite = new Sprite(g_pLaserBoltHorizontalBitmap, rcBounds, BA_DIE);
    pSprite->SetVelocity(25, 0);
    pSprite->SetZOrder(6);
    //pSprite->SetNumFrames(5);

    // Set the laser bolt sprite's position and return it
    pSprite->SetPosition(rcPos.left + 21, rcPos.top + 30);
    if(IsInView(this) && (!g_bDying))
    {
      // Play the shoot laser sound
      PlayMySound(IDW_SHOOTLASER);
    }
  }
  return pSprite;
}

Sprite* BubbleBlowerSprite::AddSprite()
{
  // Create a new blob sprite
  myRECT    rcBounds = g_rcAll;
  myRECT    rcPos = GetPosition();
  Sprite* pSprite = NULL;
  myPOINT   pt = GetVelocity();
  bool    bYellow;

  if(GetBitmap() == g_pYellowAmoebaBitmap)
    bYellow = true;
  else
    bYellow = false;

  // Blow blob (or fire)
  if(!bYellow)
    pSprite = new Sprite(g_pBubbleBitmap, rcBounds, BA_DIE);
  else
    pSprite = new Sprite(g_pFireBlowBitmap, rcBounds, BA_DIE);
  if(pt.x < 0)
  {
      pSprite->SetVelocity(-10, 0);
      m_iCurFrame = 8;
  }
  else
  {
      pSprite->SetVelocity(10, 0);
      m_iCurFrame = 9;
  }
  pSprite->SetZOrder(6);
  pSprite->SetNumFrames(5);

  if(!bYellow)
  {
    // Set the blob sprite's position
    if(pt.x > 0)
      pSprite->SetPosition(rcPos.right, rcPos.top + 10);
    else
      pSprite->SetPosition(rcPos.left - 23, rcPos.top + 10);
    if(IsInView(this))
        PlayMySound(IDW_BLUEBLOW);
  }
  else
  {
    // Set the fire sprite's position
    if(pt.x > 0)
      pSprite->SetPosition(rcPos.right, rcPos.top + 25);
    else
      pSprite->SetPosition(rcPos.left - 23, rcPos.top + 25);
    if(IsInView(this))
        PlayMySound(IDW_YELLOWBLOW);
  }

  myPOINT pt2 = {pSprite->GetWidth()/2.0 + pSprite->GetVelocity().x,pSprite->GetHeight()/2.0};
  if(!bYellow)
    pSprite->AddParticles(PARTICLE_BLUEAMOEBADEATHFLARETHINGY, BITMAP_PARTICLES, 25,0,25,25, pt2);
  else
    pSprite->AddParticles(PARTICLE_YELLOWAMOEBABLOW, BITMAP_PARTICLES, 25,0,25,25, pt2);
  pSprite->SetParticlesInFront(true);
  pSprite->FireParticles();
  m_iBlowCount = 0;

  return pSprite;
}

Sprite* AlienFighterSprite::AddSprite()
{
  // Create a new bullet sprite
  myRECT    rcBounds = g_rcAll;
  myRECT    rcPos = GetPosition();
  Sprite* pSprite = NULL;
  myPOINT   pt = GetVelocity();

  pSprite = new Sprite(g_pAlienBulletBitmap, rcBounds, BA_DIE);
  if(m_iCurFrame == 18)
    pSprite->SetVelocity(-12, 0);
  else if(m_iCurFrame == 19)
    pSprite->SetVelocity(12, 0);
  pSprite->SetZOrder(6);
  pSprite->SetFrameDelay(5);
  pSprite->SetNumFrames(9, true);

  // Set the bullet sprite's position
  if(m_iCurFrame == 19)
    pSprite->SetPosition(rcPos.right, rcPos.top + 25);
  else
    pSprite->SetPosition(rcPos.left - 23, rcPos.top + 25);

  myPOINT pt2 = {pSprite->GetWidth()/2.0 + pSprite->GetVelocity().x,pSprite->GetHeight()/2.0};
  pSprite->AddParticles(PARTICLE_BLUEAMOEBADEATHFLARETHINGY, BITMAP_PARTICLES, 25,0,25,25, pt2, false);
  pSprite->SetParticlesInFront(false);
  pSprite->FireParticles();

  if(IsInView(this))
    PlayMySound(IDW_ALIENFIRE);

  return pSprite;
}

//-----------------------------------------------------------------
// TrapSprite Methods
//-----------------------------------------------------------------
inline void TrapSprite::UpdateFrame()
{
  if(m_iNumFrames > 2 && m_iCurFrame > 0)
  {
    if ((m_iFrameDelay >= 0) && (--m_iFrameTrigger <= 0))
    {
      // Reset the frame trigger;
      m_iFrameTrigger = m_iFrameDelay;

      // Increment the frame
      if (++m_iCurFrame >= m_iNumFrames)
      {
        // If it's a one-cycle frame animation, kill the sprite
        if (m_bOneCycle)
          m_bDying = true;
        else
          m_iCurFrame = m_iNumFrames - 1;  //Freeze the animation at the last frame
      }
    }
  }
}

inline void TruckSprite::UpdateFrame()
{
  if ((m_iFrameDelay >= 0) && (--m_iFrameTrigger <= 0))
  {
    // Reset the frame trigger;
    m_iFrameTrigger = m_iFrameDelay;

    // Increment the frame
    if (++m_iCurFrame >= m_iNumFrames)
    {
      m_iCurFrame = m_iNumFrames - 1;  //Freeze the animation at the last frame
      m_ptVelocity.x = 20;             //And make the truck move
    }
  }
}

inline void TeleporterSprite::UpdateFrame()
{
  if(m_iNumFrames > 2 && m_iCurFrame > 0)  //Only if past first frame
  {
    if ((m_iFrameDelay >= 0) && (--m_iFrameTrigger <= 0))
    {
      // Reset the frame trigger;
      m_iFrameTrigger = m_iFrameDelay;

      // Increment the frame
      if (++m_iCurFrame >= m_iNumFrames)
      {
        // If it's a one-cycle frame animation, kill the sprite
        if (m_bOneCycle)
          m_bDying = true;
        else
          m_iCurFrame = 0;  //Go back to first frame
      }
    }
  }
}

inline void AlienSprite::UpdateFrame()
{
  myPOINT ptVelocity = GetVelocity();
  bool  bLeft;

  if (--m_iFrameTrigger <= 0)
  {
    // Reset the frame trigger
    m_iFrameTrigger = m_iFrameDelay;

    if(ptVelocity.x > 0)
      bLeft = false;
    else
      bLeft = true;

    if(bLeft)
    {
      m_iCurFrame++;
      if(m_iCurFrame > ((m_iNumFrames / 2) - 1))   //This code depends on the first half of the bitmap image being
        m_iCurFrame = 0;                           //facing left
    }
    else
    {
      m_iCurFrame++;
      if(m_iCurFrame < (m_iNumFrames / 2) || (m_iCurFrame >= m_iNumFrames)) //and the next half facing right
        m_iCurFrame = m_iNumFrames / 2;
    }
  }
}

inline void AlienFighterSprite::UpdateFrame()
{
  myPOINT ptVelocity = GetVelocity();
  bool  bLeft;

  if (--m_iFrameTrigger <= 0)
  {
    // Reset the frame trigger
    m_iFrameTrigger = m_iFrameDelay;

    if(ptVelocity.x > 0)
      bLeft = false;
    else if(ptVelocity.x < 0)
      bLeft = true;
    else
      return;  //Don't update frame if sitting still

    if(bLeft)
    {
      if(AlienFighterMove[m_iMovePointer] != 'F')
        m_iCurFrame++;
      else
        m_iCurFrame = 18;
      if(m_iCurFrame > 7)   //This code depends on the first half of the bitmap image being
        m_iCurFrame = 0;                           //facing left
    }
    else
    {
      if(AlienFighterMove[m_iMovePointer] != 'F')
        m_iCurFrame++;
      else
        m_iCurFrame = 19;
      if(m_iCurFrame < 8 || m_iCurFrame >= 15) //and the next half facing right
        m_iCurFrame = 8;
    }
  }
}

inline void BubbleBlowerSprite::UpdateFrame()
{
  myPOINT ptVelocity = GetVelocity();
  bool  bLeft;

  if (--m_iFrameTrigger <= 0)
  {
    // Reset the frame trigger
    m_iFrameTrigger = m_iFrameDelay;

    if(m_iBlowCount < 5)
    {
        m_iBlowCount++;
        return;
    }

    if(ptVelocity.x > 0)
      bLeft = false;
    else
      bLeft = true;

    if(bLeft)
    {
      m_iCurFrame++;
      if(m_iCurFrame > 7 || m_iCurFrame < 4)
        m_iCurFrame = 4;
    }
    else
    {
      if(++m_iCurFrame > 3)
        m_iCurFrame = 0;
    }
  }
}

inline void BadGuySprite::UpdateFrame()
{
  myPOINT ptVelocity = GetVelocity();
  bool  bLeft;

  if (--m_iFrameTrigger <= 0)
  {
    // Reset the frame trigger
    m_iFrameTrigger = m_iFrameDelay;

    if(ptVelocity.x > 0)
      bLeft = false;
    else
      bLeft = true;

    if(bLeft)
    {
      m_iCurFrame++;
      if(m_iCurFrame > ((m_iNumFrames / 2) - 1))   //This code depends on the first half of the bitmap image being
        m_iCurFrame = 0;                           //facing left
    }
    else
    {
      m_iCurFrame++;
      if(m_iCurFrame < (m_iNumFrames / 2) || (m_iCurFrame >= m_iNumFrames)) //and the next half facing right
        m_iCurFrame = m_iNumFrames / 2;
    }
  }
}

inline void RollerSprite::UpdateFrame()
{
  myPOINT ptVelocity = GetVelocity();
  bool  bLeft;

  if (--m_iFrameTrigger <= 0)
  {
    // Reset the frame trigger
    m_iFrameTrigger = m_iFrameDelay;

    if(ptVelocity.x > 0)
      bLeft = false;
    else
      bLeft = true;

    if(bLeft && m_iCurFrame > 4)
    {
      m_iCurFrame++;
      if(m_iCurFrame > 8)   //This code depends on the first half of the bitmap image being
        m_iCurFrame = 5;                           //facing left
      if(m_iCurFrame < 5)
        m_iCurFrame = 5;
    }
    else if(m_iCurFrame > 4)
    {
      m_iCurFrame++;
      if(m_iCurFrame > 12)   // and the other half facing right
        m_iCurFrame = 9;
      if(m_iCurFrame < 9)
        m_iCurFrame = 9;
    }
    else
    {
      m_iCurFrame++;
      if(m_iCurFrame > 4)
        m_iCurFrame = 0;
    }
  }
}

//-----------------------------------------------------------------
// ButtonSprite Constructor(s)/Destructor
//-----------------------------------------------------------------
ButtonSprite::ButtonSprite(Bitmap* pBitmap, myRECT& rcBounds, BOUNDSACTION baBoundsAction) : Sprite(pBitmap, rcBounds, baBoundsAction)
{
}

ButtonSprite::~ButtonSprite()
{
}

//-----------------------------------------------------------------
// ButtonSprite Virtual Methods
//-----------------------------------------------------------------
inline void ButtonSprite::UpdateFrame()
{
}



















