//-----------------------------------------------------------------
// Sprite Object
// C++ Header - Sprite.h
//-----------------------------------------------------------------

#ifndef SPRITE_H
#define SPRITE_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include <hgeparticle.h>
#include "buildtarget.h"
#include "myPOINT.h"
#include "Bitmap.h"

//-----------------------------------------------------------------
// Custom Data Types
//-----------------------------------------------------------------
typedef int32_t        SPRITEACTION;
const SPRITEACTION  SA_NONE      = 0x0000L,
                    SA_KILL      = 0x0001L,
                    SA_ADDSPRITE = 0x0002L;

typedef int32_t        BOUNDSACTION;
const BOUNDSACTION  BA_STOP   = 0,
                    BA_WRAP   = 1,
                    BA_BOUNCE = 2,
                    BA_DIE    = 3;

typedef struct {
    void* pSprite;
    bool bIsFiring;
    string sParticleFilename;
    string sImageFilename;
    float x;
    float y;
    float w;
    float h;
    myPOINT ptOffset;
    bool bAdditive;
    bool bParticlesInFront;
    bool bDieOnFinish;
}Sprite_particle_holder;

//-----------------------------------------------------------------
// Sprite Class
//-----------------------------------------------------------------
class Sprite
{
protected:
  // Member Variables
  Bitmap*       m_pBitmap;
  int           m_iNumFrames, m_iCurFrame;
  int           m_iFrameDelay, m_iFrameTrigger;
  int           m_iDripCounter;
  int           m_iMovePointer;
  int           m_iCounter;
  int           m_iJumpAmount;
  myRECT          m_rcPosition,
                m_rcCollision;
  myPOINT         m_ptVelocity;
  int           m_iZOrder;
  int           m_iLives;
  myRECT          m_rcBounds;
  BOUNDSACTION  m_baBoundsAction;
  bool          m_bHidden;
  bool          m_bDying;
  bool          m_bOneCycle;
  bool          m_bIsJumping;
//  int m_iLastParticleUpdate;

  //For flashing red when hit
  int           m_iFlashCount;
  int           m_r, m_g, m_b;

  //For particles
  bool          m_bParticlesInFront;
  hgeParticleSystem* m_Particles;
  hgeSprite*    m_ParticleSprite;
  HTEXTURE      m_ParticleTex;
  myPOINT       m_ptParticleOffset;
  myPOINT       m_ptMoveTo;
  int           m_iTileParticlesXCount;
  int           m_iTileParticlesXIncrement;
  bool          m_bDieWhenParticlesDone;
  Sprite_particle_holder m_pHolder;

  //HACK
  bool          m_bHorizFrames;
  bool          m_bAlwaysDrawParticles;
  myPOINT       m_ptOrigPos;    //For dealing with electrospheres

  // Helper Methods

  virtual void  CalcCollisionRect();

public:
  // Constructor(s)/Destructor
  Sprite(Bitmap* pBitmap);
  Sprite(Bitmap* pBitmap, myRECT& rcBounds,
    BOUNDSACTION baBoundsAction = BA_STOP);
  Sprite(Bitmap* pBitmap, myPOINT ptPosition, myPOINT ptVelocity, int iZOrder,
    myRECT& rcBounds, BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~Sprite();

  // General Methods
  virtual SPRITEACTION  Update(float fTimestep);
  virtual Sprite*       AddSprite();
  virtual void          Draw(myRECT rcViewport, int xOffset = 0, int yOffset = 0);
  bool                  IsPointInside(int x, int y);
  bool                  TestCollision(Sprite* pTestSprite);
  bool                  Kill()      { if(--m_iLives == 0){m_bDying = true; return true;} return false; };
  virtual void  UpdateFrame();
  void                  FlashColor(int iFlashCount, int r, int g, int b) {m_iFlashCount = iFlashCount; m_r = r; m_g = g; m_b = b;};    //Flash red for a few cycles

  // Accessor Methods
  Bitmap* GetBitmap()               { return m_pBitmap; };
  void    SetBitmap(Bitmap* pBitmap) { m_pBitmap = pBitmap; }; //HACK. (for final boss and stuff)
  void    SetNumFrames(int iNumFrames, bool bOneCycle = false);
  int     GetNumFrames()            { return m_iNumFrames; };
  void    SetFrameDelay(int iFrameDelay) { m_iFrameDelay = iFrameDelay; };
  int     GetFrameDelay()           { return m_iFrameDelay; };
  void    SetFrame(int iFrame)           {if((iFrame <= m_iNumFrames) && (iFrame >= 0)){ m_iCurFrame = iFrame; } };
  int     GetFrame()                { return m_iCurFrame; };
  void    Jump()                    { m_bIsJumping = true; m_iJumpAmount = -18; }; //Jumping for bouncy creature
  myRECT&   GetPosition()             { return m_rcPosition; };
  myPOINT   GetCenter();
  virtual void    SetPosition(int x, int y);
  virtual void    SetPosition(myPOINT ptPosition);
  virtual void    SetPosition(myRECT& rcPosition);
  virtual void    OffsetPosition(float x, float y);
  myRECT&   GetCollision()            { return m_rcCollision; };
  myRECT*   GetCollisionPtr()         { return &m_rcCollision; };   //MEH Pointers are faster, right?
  //MEH HACK. Don't use. Ever. Like I do.
  void    SetCollision(myRECT& rcCollision) { CopyRect(&m_rcCollision, &rcCollision); };
  myPOINT   GetVelocity()             { return m_ptVelocity; };
  virtual void    SetVelocity(int x, int y);
  virtual void    SetVelocity(myPOINT ptVelocity);
  int     GetZOrder()               { return m_iZOrder; };
  void    SetZOrder(int iZOrder)    { m_iZOrder = iZOrder; };
  void    SetLives(int iLives)      { m_iLives = iLives; };
  int     GetLives()                { return m_iLives; };
  void    SetBounds(myRECT& rcBounds) { CopyRect(&m_rcBounds, &rcBounds); };
  void    SetBoundsAction(BOUNDSACTION ba) { m_baBoundsAction = ba; };
  bool    IsHidden()                { return m_bHidden; };
  void    SetHidden(bool bHidden)   { m_bHidden = bHidden; };
  int     GetWidth();
  int     GetHeight();
  void    Gravity();
  void    StopJump()                    {m_bIsJumping = false; m_ptVelocity.y = 5;};
  int     GetMovePointer()              {return m_iMovePointer;};
  void    SetMovePointer(int iMove)     {m_iMovePointer = iMove;};

  //HACK
  bool GetHorizFrames() {return m_bHorizFrames;};
  void SetHorizFrames(bool b) {m_bHorizFrames = b;};
  bool GetAlwaysDrawParticles() {return m_bAlwaysDrawParticles;};
  void SetAlwaysDrawParticles(bool b) { m_bAlwaysDrawParticles = b; };
  void SetOrigPos(myPOINT ptOrigPos)    {m_ptOrigPos = ptOrigPos;};
  myPOINT GetOrigPos()  {return m_ptOrigPos;};
  void SetAlpha(unsigned char cAlpha);
  //ALSO HACK. DON'T USE LIKE ME
  hgeParticleSystem* GetParticles() {return m_Particles;};

  //Particle system functions
  void    AddParticles(string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptOffset, bool bAdditive = true);
  void    RemoveParticles();
  void    FireParticles();
  void    OffsetParticles(myPOINT pt, bool bOffsetAll = true);
  myPOINT GetParticleOffset()           { return m_ptParticleOffset; };
  bool    ParticlesFiring();
  bool    HasParticles()                { return m_Particles != NULL; };
  //void    SetParticleOffset(myPOINT ptOffset)   { m_ptParticleOffset = ptOffset; };
  void    SetParticlesInFront(bool bInFront)    {m_bParticlesInFront = bInFront;};
  bool    GetParticlesInFront()         { return m_bParticlesInFront; };
  void    SetParticleTile(int iCount, int iOffset)  { m_iTileParticlesXCount = iCount; m_iTileParticlesXIncrement = iOffset; };
  void    SetParticleTileX(int iCount)  { m_iTileParticlesXCount = iCount; };
  void    SetParticleTileOffsetX(int iOffset) { m_iTileParticlesXIncrement = iOffset; };
  void    DieOnParticleFinish()             {m_bDieWhenParticlesDone = true;};
  //Game engine use
  void    FillInParticleData(Sprite_particle_holder* pData);
};

//-----------------------------------------------------------------
// Sprite Inline Helper Methods
//-----------------------------------------------------------------
inline void Sprite::UpdateFrame()
{
  if ((m_iFrameDelay >= 0) && (--m_iFrameTrigger <= 0))
  {
    // Reset the frame trigger;
    m_iFrameTrigger = m_iFrameDelay;

    // Increment the frame
    if (++m_iCurFrame >= m_iNumFrames)
    {
      // If it's a one-cycle frame animation, kill the sprite
      if (m_bOneCycle)
        m_bDying = true;
      else
        m_iCurFrame = 0;
    }
  }
}

inline void Sprite::CalcCollisionRect()
{
  int iXShrink = (m_rcPosition.left - m_rcPosition.right) / 12;
  if((GetWidth()) > 75)
  {
    iXShrink = -6;
  }
  int iYShrink = -1;
  CopyRect(&m_rcCollision, &m_rcPosition);
  InflateRect(&m_rcCollision, iXShrink, iYShrink);
}

//-----------------------------------------------------------------
// Sprite Inline General Methods
//-----------------------------------------------------------------
inline bool Sprite::TestCollision(Sprite* pTestSprite)
{
  myRECT* rcTest = pTestSprite->GetCollisionPtr();
  return m_rcCollision.left <= rcTest->right &&
         rcTest->left <= m_rcCollision.right &&
         m_rcCollision.top <= rcTest->bottom &&
         rcTest->top <= m_rcCollision.bottom;
}

inline bool Sprite::IsPointInside(int x, int y)
{
  myPOINT ptPoint;
  ptPoint.x = x;
  ptPoint.y = y;
  return PtInRect(&m_rcPosition, &ptPoint);
}

//-----------------------------------------------------------------
// Sprite Inline Accessor Methods
//-----------------------------------------------------------------
inline void Sprite::SetNumFrames(int iNumFrames, bool bOneCycle)
{
  // Set the number of frames and the one-cycle setting
  m_iNumFrames = iNumFrames;
  m_bOneCycle = bOneCycle;

  // Recalculate the position
  myRECT rect = GetPosition();
  if(!m_bHorizFrames)
    rect.bottom = rect.top + ((rect.bottom - rect.top) / iNumFrames);
  else
    rect.right = rect.left + ((rect.right - rect.left) / iNumFrames);
  SetPosition(rect);
}

inline void Sprite::SetPosition(int x, int y)
{
  OffsetRect(&m_rcPosition, x - m_rcPosition.left, y - m_rcPosition.top);
  CalcCollisionRect();
}

inline void Sprite::SetPosition(myPOINT ptPosition)
{
  OffsetRect(&m_rcPosition, ptPosition.x - m_rcPosition.left,
    ptPosition.y - m_rcPosition.top);
  CalcCollisionRect();
}

inline void Sprite::SetPosition(myRECT& rcPosition)
{
  CopyRect(&m_rcPosition, &rcPosition);
  CalcCollisionRect();
}

inline void Sprite::OffsetPosition(float x, float y)
{
  OffsetRect(&m_rcPosition, x, y);
  CalcCollisionRect();
}

inline void Sprite::SetVelocity(int x, int y)
{
  m_ptVelocity.x = x;
  m_ptVelocity.y = y;
}

inline void Sprite::SetVelocity(myPOINT ptVelocity)
{
  m_ptVelocity.x = ptVelocity.x;
  m_ptVelocity.y = ptVelocity.y;
}


#endif
