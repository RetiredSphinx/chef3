//-----------------------------------------------------------------
// Bitmap Object
// C++ Header - Bitmap.h
//-----------------------------------------------------------------

#ifndef BITMAP_H
#define BITMAP_H

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
//#include <windows.h>
#include "buildtarget.h"
#include <string>
using std::string;
#include <hge.h>
#include <hgesprite.h>

#include <fstream>
using namespace std;
extern ofstream ofile;

//-----------------------------------------------------------------
// Bitmap Class
//-----------------------------------------------------------------
class Bitmap
{
protected:
  // Member Variables
  HTEXTURE m_hBitmap;
  hgeSprite* m_hSprite;
  int     m_iWidth, m_iHeight;
  int     m_iNumUsers;
  string  m_sFilename;
  int     m_r,m_g,m_b,m_a;  //For colorize

  // Helper Methods
  Bitmap(){};       //Can't call default constructor
  void CheckUsers();    //To make sure we don't accidentally not draw this sometime...
  bool Create(string szFileName);
  //bool Create(HDC hDC, UINT uiResID, HINSTANCE hInstance);
  //bool Create(HDC hDC, int iWidth, int iHeight, COLORREF crColor);

public:
  // Constructor(s)/Destructor
  //Bitmap();
  Bitmap(string szFileName);
  //Bitmap(HDC hDC, UINT uiResID, HINSTANCE hInstance);
  //Bitmap(HDC hDC, int iWidth, int iHeight, COLORREF crColor = RGB(0, 0, 0));
  virtual ~Bitmap();

  // General Methods
  virtual void Draw(int x, int y);
  void DrawPart(int x, int y, int xPart, int yPart, int wPart, int hPart);
  void Colorize(int r, int g, int b, int a) {m_r = r; m_b = b; m_g = g; m_a = a;};
  void ClearColor() {m_r = m_g = m_b = m_a = -1;};
  int  GetWidth() { CheckUsers(); return m_iWidth; };
  int  GetHeight() { CheckUsers(); return m_iHeight; };
  string GetFilename()  {return m_sFilename;};

  //FOR GAME ENGINE USE ONLY
  void AddUser();       //deprecated
  void RemoveUser();    //deprecated
  void Uncache();       //Free memory associated with this bitmap
  void Free();  //Also used internally by the bitmap class itself
  void ReloadTex(); //To reload a free'd texture
  //HACK
  hgeSprite* GetSprite()    {return m_hSprite;};
};

#endif
