//myPOINT.cpp
//Definitions of POINT and RECT structures with floating-point values rather than integer values
//(c) Mark Hutcheson 2012

#include "myPOINT.h"

void CopyRect(myRECT* dest, const myRECT* src)
{
    dest->left = src->left;
    dest->top = src->top;
    dest->right = src->right;
    dest->bottom = src->bottom;
}

void InflateRect(myRECT* rc, float fXShrink, float fYShrink)
{
    rc->left -= fXShrink;
    rc->right += fXShrink;
    rc->top -= fYShrink;
    rc->bottom += fYShrink;
}

bool PtInRect(const myRECT* rc, const myPOINT* pt)
{
    if(pt->x < rc->left ||
       pt->x > rc->right ||
       pt->y < rc->top ||
       pt->y > rc->bottom)
        return false;
    return true;
}

bool PtInRect(const myRECT* rc, float x, float y)
{
    myPOINT pt = {x,y};
    return PtInRect(rc, &pt);
}

void OffsetRect(myRECT* rc, float x, float y)
{
    rc->left += x;
    rc->right += x;
    rc->top += y;
    rc->bottom += y;
}

void SetRect(myRECT* rc, float left, float top, float right, float bottom)
{
    rc->left = left;
    rc->right = right;
    rc->top = top;
    rc->bottom = bottom;
}

bool IntersectRect(const myRECT* rc1, const myRECT* rc2)
{
    return (rc1->left <= rc2->right &&
            rc2->left <= rc1->right &&
            rc1->top <= rc2->bottom &&
            rc2->top <= rc1->bottom);
}
