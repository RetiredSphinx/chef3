//-----------------------------------------------------------------
// Game Engine Object
// C++ Source - GameEngine.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "GameEngine.h"
#include <VFS.h>
#include <VFSTools.h>
#include <VFSZipArchiveLoader.h>
#include <hge.h>
#include <hgesprite.h>
#include <hgefont.h>
#include "BitmapLoad.h"

//-----------------------------------------------------------------
// Globals
//-----------------------------------------------------------------

bool g_bIsFalling;
bool g_bIsJumping;
float g_iFallAmount;
float g_iJumpAmount;
bool g_bIsWalkingRight;
static bool g_bEndGame = false;

//HACK
extern bool g_bTypeName;

HGE *hge = NULL;

   //For ttvfs

extern bool IsInView(Sprite* pSprite);//external function to check if an object is in view
extern bool IsNearby(Sprite* pSprite);//, bool bBiggerArea = false);  //Function to check if an object should be updated
extern void AckWndProc( hgeInputEvent event );

//-----------------------------------------------------------------
// Static Variable Initialization
//-----------------------------------------------------------------
GameEngine *GameEngine::m_pGameEngine = NULL;

ttvfs::VFSHelper vfs;


//-----------------------------------------------------------------------------------------
//Helper function for cleaning up bitmaps easier and reloading bitmaps on resolution change
//-----------------------------------------------------------------------------------------
void AddBitmap(Bitmap* pBitmap)
{
    GameEngine::GetEngine()->AddBitmap(pBitmap);
}

//void SpriteParticleManage(Sprite* pSprite, string sParticleFilename, string sImageFilename, float x, float y, float w, float h, myPOINT ptOffset, bool bAdditive)
//{
//    GameEngine::GetEngine()->SpriteParticleManage(pSprite, sParticleFilename, sImageFilename, x, y, w, h, ptOffset, bAdditive);
//}


//WHEE screenchange hack
bool g_bResolutionChange = false;
//-----------------------------------------------------------------
// Frame and render functions that HGE expects
//-----------------------------------------------------------------
bool FrameFunc()
{
    if(g_bEndGame)
        return true;    //End
    //ofile << "FrameFunc" << endl;
    static float fLastCycle = hge->Timer_GetTime();
    if(g_bResolutionChange)
    {
        fLastCycle = 0;    //Reset, since we'll be starting back over
//        ofile << "Resolution change" << endl;
        return true;    //End
    }
    //Use the target frame rate to limit our movement, but not our drawing
    float fTargetFrameDelay = 1.0 / (GameEngine::GetEngine()->GetFrameRate());   //How many seconds per cycle
    float fCurTime = hge->Timer_GetTime();

    //Be sure to process all our events every cycle, that way we don't miss any keystrokes or mouse clicks or what-have-you
    hgeInputEvent event;
    while(hge->Input_GetEvent(&event))
    {
        WndProc(event);
        AckWndProc(event);
    }

    if(hge->System_GetState(HGE_FPS) != HGEFPS_VSYNC ||     //If vsync off, update every cycle
       fLastCycle + fTargetFrameDelay <= fCurTime)
    {
        //ofile << "Frame " << endl;
        fLastCycle += fTargetFrameDelay;

        //Check and see if we're bogging down and missing too many frames
        if(fLastCycle + fTargetFrameDelay * 3 < fCurTime)
            fLastCycle = fCurTime;  //Force a catch-up

        //ofile << "Time: " << fCurTime << "s" << endl;
        //fLastCycle -= fTimeElapsed;

        // Make sure the game engine isn't sleeping
        if (!GameEngine::GetEngine()->GetSleep())
        {
            GameEngine::GetEngine()->CheckJoystick();
            HandleKeys();
            GameCycle();
        }
    }
    return false;   //Keep going
}

bool RenderFunc()
{
    hge->Gfx_BeginScene();
	hge->Gfx_Clear(0);

	GamePaint();    //Draw stuff
	DrawOverlay();  //Draw overlay stuff

	//Draw mouse cursor
	GameEngine::GetEngine()->DrawCursor();

	hge->Gfx_EndScene();
    return false;
}

//-----------------------------------------------------------------
// Windows Functions
//-----------------------------------------------------------------
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
//  PSTR szCmdLine, int iCmdShow)
#ifdef main
    #undef main     //To avoid Windows errors when it expects WinMain() instead
#endif
int main(int argc, char** argv)
{
  //MEH Init ttvfs for our use
  // Make the VFS able to load Zip files
  vfs.AddArchiveLoader(new ttvfs::VFSZipArchiveLoader);
  // Load all files from current directory.
  // Here, it is again required to load everything recursively so that the mounting will work.
  vfs.LoadFileSysRoot(false);
  // Make the VFS usable
  vfs.Prepare();
  //Add our archive into the VFS library
  vfs.AddArchive("res.zip", false, "");
  // Make the user's savegame directory accessible as the root folder
  string sFilename = GetSavegameLocation(); //Save in a resonable location
  //Create directory if it isn't there already
  //ofstream othingy("out.txt");
  //othingy << sFilename << endl;
  if(!ttvfs::IsDirectory(sFilename.c_str()))
    ttvfs::CreateDirRec(sFilename.c_str());// << endl;
  //othingy.close();

  if( SDL_Init( SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK ) < 0 ) //Using audio (not video, but need to init it for SDL_ListModes)
  {
        char cErrMsg[256];
        sprintf(cErrMsg, "Unable to initialize SDL: %s", SDL_GetError());
        #ifdef BUILD_FOR_WINDOWS
        MessageBox(NULL, cErrMsg, "Chef Bereft", MB_OK|MB_ICONERROR);
        #else
        ofile << "CRITICAL: Unable to initialize SDL." << endl;
        #endif
        return 1;
  }

  //Open our sound mixer
  int audio_rate = 22050;
  Uint16 audio_format = AUDIO_S16SYS;
  int audio_channels = 2;
  int audio_buffers = 1024;

  if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0)
  {
    char cErrMsg[256];
    sprintf(cErrMsg, "Unable to initialize SDL_Mixer: %s", Mix_GetError());
    #ifdef BUILD_FOR_WINDOWS
    MessageBox(NULL, cErrMsg, "Chef Bereft", MB_OK|MB_ICONERROR);
    #else
    ofile << "Error: Unable to initialize SDL_Mixer." << endl;
    #endif
    return 1;
  }

  // make sure SDL cleans up before exit
  atexit(SDL_Quit);

  //ofile << "Initializing game" << endl;
  if (GameInitialize())
  {
    //Loop
    while(!g_bEndGame)
    {
        if(!g_bResolutionChange)
        {
            if (!GameEngine::GetEngine()->Initialize())
            {
                ofile << "Error initializing game engine." << endl;
                return false;
            }
        }
        else
        {
            ofile << "Setting up window.." << endl;
            GameEngine::GetEngine()->SetupWindow();
            g_bResolutionChange = false;
        }
        hge->System_Start();
        StartCountdown();
        if(!g_bResolutionChange)
        {
            break;
        }
    }
  }
  ofile << "Ending game." << endl;
  // End the game
  GameEnd();
  SDL_CloseAudio();
  SDL_Quit();       //End SDL
  hge->System_Shutdown();   //End HGE
  hge->Release();
  ofile << "Exiting happily." << endl;
  return 0;
}

void WndProc(hgeInputEvent event)
{
  // Route all Windows messages to the game engine
  return GameEngine::GetEngine()->HandleEvent(event);
}

//-----------------------------------------------------------------
// Game Engine Helper Methods
//-----------------------------------------------------------------
void GameEngine::CheckSpriteCollision(Sprite* pTestSprite)
{
  // See if the sprite has collided with any other moveable sprites
  list<Sprite*>::iterator siSprite, siEnd;
  siEnd = m_lSprites.end();
  for (siSprite = m_lSprites.begin(); siSprite != siEnd; siSprite++)
  {
    Sprite* p = (*siSprite);
    // Make sure not to check for collision with itself
    if (pTestSprite == p)
      continue;

//    if(IsNearby((*siSprite), true))
    //{
      // Test the collision
      if (pTestSprite->TestCollision(p))
      {
        // Collision detected
        SpriteCollision(p, pTestSprite);
      }
    //}
  }
}

//-----------------------------------------------------------------
// GameEngine Constructor(s)/Destructor
//-----------------------------------------------------------------
GameEngine::GameEngine(string szTitle, int32_t wIcon, int iWidth, int iHeight, bool bFullscreen)
{
  // Set the member variables for the game engine
  m_Joystick = NULL;
  m_bJoystickDisabled = false;
  //Init joysticks, if there are any
  if(SDL_NumJoysticks() > 0)
  {
      SDL_JoystickEventState(SDL_QUERY);    //Query for joystick updates, rather than event-driven input
      //Print joystick names, just for debugging purposes
      if(SDL_NumJoysticks() > 1)
      {
          ofile << SDL_NumJoysticks() << " Joysticks attached:" << endl;
          for(int i = 0; i < SDL_NumJoysticks(); i++)
          {
              ofile << "    " << SDL_JoystickName(i) << endl;
          }
      }
      ofile << "Using joystick 1: " << SDL_JoystickName(0) << endl;
      m_Joystick = SDL_JoystickOpen(0); //Open first joystick
      ofile << "Number of joystick axes: " << SDL_JoystickNumAxes(m_Joystick) << endl;
      if(SDL_JoystickNumAxes(m_Joystick) < 2)
      {
          ofile << "Not enough axes on joystick. Using keyboard input..." << endl;
          m_Joystick = NULL;
      }
  }
  else
    ofile << "No joysticks found." << endl;
  m_iJoyFire1 = 0;
  m_iJoyFire2 = 1;
  m_bVsync = true;
  m_pGameEngine = this;
  m_bFullscreen = bFullscreen;
  m_bMusic = m_bSound = true;
  m_szTitle = szTitle;
  //ofile << "Setting icon and title" << endl;
  //hge->System_SetState(HGE_TITLE, szTitle.c_str()); //Set our window to this title
  //hge->System_SetState(HGE_ICON, MAKEINTRESOURCE(wIcon));
  m_wIcon = wIcon;
  //ofile << "Getting screen resolution" << endl;
  //Find the max resolution of the screen
  //SDL_Rect **modes;
  int iMaxMode = 0;
  int iMaxVal = 0;
  m_iNumScreenModes = 0;
  // Get available fullscreen modes
  m_rcScreenModes = SDL_ListModes(NULL, SDL_FULLSCREEN|SDL_HWSURFACE);

  // Check is there are any modes available
  if(m_rcScreenModes == (SDL_Rect **)0)
  {
    ofile << "No fullscreen screen resolutions available! Setting to 800x600..." << endl;
    iWidth = 800;
    iHeight = 600;
  }
  // Check if our resolution is restricted
  else if(m_rcScreenModes == (SDL_Rect **)-1)
  {
    ofile << "All fullscreen resolutions available. Setting to 800x600..." << endl;
    iWidth = 800;
    iHeight = 600;
  }
  else
  {
    // Grab largest supported screen resolution
    for(int i = 0; m_rcScreenModes[i]; i++)
    {
        if(iMaxVal < (m_rcScreenModes[i]->w * m_rcScreenModes[i]->h))
        {
            iMaxVal = m_rcScreenModes[i]->w * m_rcScreenModes[i]->h;
            iMaxMode = i;
        }
    }
    for(m_iNumScreenModes = 0;m_rcScreenModes[m_iNumScreenModes];m_iNumScreenModes++);  //Store the # of screen modes
  }
  if(iWidth <= 0)   //Passing 0,0 or -1,-1 or such makes it the size of the current screen
  {
      iWidth = m_rcScreenModes[iMaxMode]->w;
  }
  if(iHeight <= 0)
  {
      iHeight = m_rcScreenModes[iMaxMode]->h;
  }
  ofile << "Maximum supported screen resolution: " << iWidth << "x" << iHeight << endl;
  m_iWidth = iWidth;
  m_iHeight = iHeight;
  //hge->System_SetState(HGE_SCREENWIDTH, iWidth);
  //hge->System_SetState(HGE_SCREENHEIGHT, iHeight);
  //hge->System_SetState(HGE_WINDOWED, !bFullscreen);
  m_iFrameRate = 60;   // 60 FPS default
  m_bSleep = false;
  m_uiJoystickID = 0;
  m_Font = NULL;
  m_fMoveMultiplier = 1.0;
  m_bShowMouse = false;
  m_texMouseCursor = 0;
  m_sMouseCursorSprite = NULL;
  m_ptMouseHotSpot.x = m_ptMouseHotSpot.y = 0;
  m_bSpriteLooping = false;
  m_iSoundVol = m_iMusicVol = 96;   //Start at 3/4 volume
}

GameEngine::~GameEngine()
{
    CleanupSong();
    CleanupSprites();
    CleanupBitmaps();
    if(m_Font != NULL)
        delete m_Font;
    if(m_Joystick != NULL)
        SDL_JoystickClose(m_Joystick);
    m_Joystick = NULL;

    CleanupSounds();
}

//-----------------------------------------------------------------
// Game Engine General Methods
//-----------------------------------------------------------------
bool GameEngine::Initialize()   //Create our window
{
    //hge->System_Initiate(); //Start 'er up!
    SetupWindow();  //Create HGE and all that good stuff
    GameStart();    //Create bitmaps

  return true;
}

void GameEngine::HandleEvent(hgeInputEvent event)
{
  // Route Windows messages to game engine member functions
  switch (event.type)
  {
    case INPUT_MBUTTONDOWN:
      // Handle left mouse button press
      if(event.key == HGEK_LBUTTON)
        MouseButtonDown(event.x, event.y, true);
      // Handle right mouse button press
      if(event.key == HGEK_RBUTTON)
        MouseButtonDown(event.x, event.y, false);
      break;

    case INPUT_MBUTTONUP:
      // Handle right mouse button release
      if(event.key == HGEK_RBUTTON)
        MouseButtonUp(event.x, event.y, false);
      // Handle left mouse button release
      if(event.key == HGEK_LBUTTON)
        MouseButtonUp(event.x, event.y, true);
      break;

    case INPUT_MOUSEMOVE:
      // Handle mouse movement
      MouseMove(event.x, event.y);
      break;

    case INPUT_KEYDOWN:
      DoDialog(event.key); //do key press
      break;

    case INPUT_MOUSEWHEEL:
      MouseWheel(event.wheel);
      break;
  }
}

//void GameEngine::ErrorQuit(string szErrorMsg)
//{
//  MessageBox(NULL, szErrorMsg.c_str(), TEXT("Critical Error"), MB_OK | MB_ICONERROR);
//  PostQuitMessage(0);
//}

bool GameEngine::GetAsyncKeyState(int key)
{
    return hge->Input_GetKeyState(key);
}

void GameEngine::UseJoystick(int iIndex)
{
    //Don't reopen a joystick that's already open
    if(m_Joystick != NULL && SDL_JoystickIndex(m_Joystick) == iIndex)
        return;
    //Close the current joystick
    if(m_Joystick != NULL)
    {
        ofile << "Closing joystick " << SDL_JoystickName(SDL_JoystickIndex(m_Joystick)) << endl;
        SDL_JoystickClose(m_Joystick);
        m_Joystick = NULL;
    }
    if(iIndex > SDL_NumJoysticks()-1)
    {
        if(iIndex > 0)
            ofile << "Requested joystick is not currently plugged in." << endl;
        return;
    }
    m_Joystick = SDL_JoystickOpen(iIndex);
    ofile << "Opening joystick " << SDL_JoystickName(iIndex) << endl;
}

void GameEngine::SetForceJoyDisable(bool bDisable)
{
    if(m_bJoystickDisabled == bDisable)
        return;
    m_bJoystickDisabled = bDisable;
    if(m_bJoystickDisabled) //If we're disabling the joystick
    {
        if(m_Joystick != NULL)
        {
            SDL_JoystickClose(m_Joystick);
            m_Joystick = NULL;
        }
    }
}

void GameEngine::CheckJoystick()
{
    if(m_Joystick == NULL)
        return;
    SDL_JoystickUpdate();   //Update our joystick stuff
    JOYSTATE js = JOY_NONE;

    //Check axis motion
    //Axis 0 - left/right
    Sint16 iAxisMove = SDL_JoystickGetAxis(m_Joystick, 0);
    //10% buffer for movement
    if(iAxisMove < -3200)   // < 0 is left, and > 0 is right
        js |= JOY_LEFT;
    if(iAxisMove > 3200)   //Move right
        js |= JOY_RIGHT;

    //Axis 1 - up/down
    iAxisMove = SDL_JoystickGetAxis(m_Joystick, 1);
    if(iAxisMove < -3200)   // < 0 is up, and > 0 is down
        js |= JOY_UP;
    if(iAxisMove > 3200)   //Move down
        js |= JOY_DOWN;

    //Check button presses
    //Button 1 - jump
    if(SDL_JoystickGetButton(m_Joystick, m_iJoyFire1))
        js |= JOY_FIRE1;
    //Button 2 - fire
    if(SDL_JoystickGetButton(m_Joystick, m_iJoyFire2))
        js |= JOY_FIRE2;

    // Allow the game to handle the joystick
    HandleJoystick(js);
}

void GameEngine::AddSprite(Sprite* pSprite)
{
  //HACK: Hang on to sprite's original location--------------
  if(pSprite != NULL && pSprite->GetOrigPos().x == -1 && pSprite->GetOrigPos().y == -1)
  {
      myRECT rc = pSprite->GetPosition();
      myPOINT pt = {rc.left, rc.top};
      pSprite->SetOrigPos(pt);
  }
  //---------------------------------------------------------

  if(m_bSpriteLooping)
  {
      m_lLaterSprites.push_front(pSprite);
      return;   //Don't add sprite if looping
  }
  // Add a sprite to the sprite list
  if (pSprite != NULL)
  {
    // See if there are sprites already in the sprite list
    if (m_lSprites.size() > 0)
    {
      // Find a spot in the sprite list to insert the sprite by its z-order
      list<Sprite*>::iterator siSprite;
      for (siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
        if (pSprite->GetZOrder() < (*siSprite)->GetZOrder())
        {
          // Insert the sprite into the sprite list
          m_lSprites.insert(siSprite, pSprite);
          return;
        }
    }
    // The sprite's z-order is highest, so add it to the end of the list
    m_lSprites.push_back(pSprite);
  }
}

void GameEngine::DrawSprites(myRECT rcViewport, int xOffset, int yOffset)
{
  // Draw the sprites in the sprite list
  list<Sprite*>::iterator siSprite;
  for (siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
  {
   if(IsInView((*siSprite))) //If sprite isn't in view, we don't need to draw
    {
      (*siSprite)->Draw(rcViewport, xOffset, yOffset);
    }
  }
}

void GameEngine::UpdateSprites()
{
  // Update the sprites in the sprite list
  myRECT          rcOldSpritePos;
  SPRITEACTION  saSpriteAction;
  list<Sprite*>::iterator siSprite;
  m_bSpriteLooping = true;
  for (siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
  {
    // Save the old sprite position in case we need to restore it
    Sprite* p = (*siSprite);
    rcOldSpritePos = p->GetPosition();

    // Update the sprite
    if(IsNearby(p))  //If sprite isn't nearby, we don't need to update or check collisions
    {
      saSpriteAction = p->Update(1.0);//hge->Timer_GetDelta() * (float)(GetFrameRate()) * m_fMoveMultiplier);

      // Handle the SA_ADDSPRITE sprite action
      if (saSpriteAction & SA_ADDSPRITE)
        // Allow the sprite to add its sprite
        AddSprite(p->AddSprite());

      // Handle the SA_KILL sprite action
      if (saSpriteAction & SA_KILL)
      {
        // Notify the game that the sprite is dying
        SpriteDying(p);

        // Kill the sprite
        delete (p);
        siSprite = m_lSprites.erase(siSprite);
        siSprite--;
        continue;
      }

      // See if the sprite collided with any others
      CheckSpriteCollision(p);//)
        // Restore the old sprite position
        //(*siSprite)->SetPosition(rcOldSpritePos);

      //Make bad guys fall
      if(IsSpriteBadGuy(p))
      {
        p->Gravity();
      }
    }
  }
  m_bSpriteLooping = false;
  //Also update frames while we're at it
  for (siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
  {
      (*siSprite)->UpdateFrame();
  }

  //Add later sprites if there are any
  for(siSprite = m_lLaterSprites.begin(); siSprite != m_lLaterSprites.end(); siSprite++)
  {
      AddSprite(*siSprite);
  }
  //And empty later sprite list
  while(m_lLaterSprites.size())
    m_lLaterSprites.pop_front();
}

void GameEngine::CleanupSprites()
{
  if(m_lSprites.empty())
    return;
  // Delete and remove the sprites in the sprite list
  list<Sprite*>::iterator siSprite;
  for (siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
  {
    delete (*siSprite);
    siSprite = m_lSprites.erase(siSprite);
    siSprite--;
  }
  //Delete later sprites
  for(siSprite = m_lLaterSprites.begin(); siSprite != m_lLaterSprites.end(); siSprite++)
  {
      delete (*siSprite);
  }
  //And empty later sprite list
  while(m_lLaterSprites.size())
    m_lLaterSprites.pop_front();
}

Sprite* GameEngine::IsPointInSprite(int x, int y)
{
  // See if the point is in a sprite in the sprite list
  list<Sprite*>::reverse_iterator siSprite;
  for (siSprite = m_lSprites.rbegin(); siSprite != m_lSprites.rend(); siSprite++)
    if (!(*siSprite)->IsHidden() && (*siSprite)->IsPointInside(x, y))
      return (*siSprite);

  // The point is not in a sprite
  return NULL;
}

void GameEngine::PlaySong(string sFilename)
{
    const char* filename = sFilename.c_str();
    CleanupSong();//Stop any music that's playing
    ofile << "Playing song " << sFilename << endl;
    m_music = Mix_LoadMUS((strlen(filename) == 0) ? (m_sMusicToPlay.c_str()) : (filename)); //Play passed song or past song, depending on if "" was passed or no, and what the past passed string was. Huh?
    if(m_bMusic)
    {
        Mix_PlayMusic(m_music, -1); //Play teh music
        Mix_VolumeMusic(m_iMusicVol);   //Make sure the music is at the right volume
    }
    if(strlen(filename))
       m_sMusicToPlay = filename;
}

void GameEngine::PauseSong(bool bPause)
{
    if(bPause)
    {
        //if(!Mix_PausedMusic())
            Mix_PauseMusic();
    }
    else
    {
        //if(Mix_PausedMusic())
            Mix_ResumeMusic();
    }

}

void GameEngine::CleanupSong()
{
    if(!Mix_PlayingMusic())
        return;     //Don't clean it up if there's nothing to clean up
    //Close the song player
    //ofile << "Ending song" << endl;
    Mix_HaltMusic();
    Mix_FreeMusic(m_music);
}

void GameEngine::SetMusic(bool bMusic)
{
    if(bMusic == m_bMusic)
        return;
    m_bMusic = bMusic;

    if(bMusic)
        PlaySong("");
    else
        CleanupSong();
}

Mix_Chunk* GameEngine::GetChunk(string sFilename)
{
    map<string,Mix_Chunk*>::iterator it = m_mSounds.find(sFilename);
    if(it == m_mSounds.end())
    {
        ofile << "Loading sound " << sFilename << endl;
        Mix_Chunk* chSound = Mix_LoadWAV(sFilename.c_str());    //Load this chunk
        if(chSound == NULL)
            ofile << "Error loading sound: " << Mix_GetError() << endl;
        m_mSounds[sFilename] = chSound; //And insert into list
        return chSound;
    }
    return it->second;
}

void GameEngine::CleanupSounds()
{
    //Clean up sounds that we've loaded
    for(map<string,Mix_Chunk*>::iterator i = m_mSounds.begin(); i != m_mSounds.end(); i++)
    {
        ofile << "Freeing sound data for " << i->first << endl;
        Mix_FreeChunk(i->second);
    }
    m_mSounds.clear();
}

//MEH play a sound using SDL_Mixer
void GameEngine::PlaySound(string sFilename)
{
    if(!m_bSound)
        return;     //Don't play sounds if shouldn't
    Mix_Chunk * chSound = GetChunk(sFilename);    //Open the file to play it
    if(chSound == NULL) //Test for failure
    {
        return;   //failure
    }
    //Mix_VolumeChunk(chSound, m_iSoundVol);    //Set to the current volume we're using
    int iChannel = Mix_PlayChannel(-1, chSound, 0);    //Just play the sound on the nearest channel, no looping
    Mix_Volume(iChannel, m_iSoundVol);  //Set to the right volume
//    ofile << "Setting sound vol: " << m_iSoundVol << endl;
}

int GameEngine::PlayLoop(string sFilename)
{
    Mix_Chunk* chSound = GetChunk(sFilename);
    if(chSound == NULL)
        return -2;
    int iChannel = Mix_PlayChannel(-1, chSound, -1);    //Loop
    Mix_Volume(iChannel, m_iSoundVol);
    return iChannel;
}

void GameEngine::SetSoundVol(int sound, int vol)    //128 = max, 0 = none
{
    if(sound == -2)
        return;
    Mix_Volume(sound, (int)((float)(m_iSoundVol) / (128.0) * (float)(vol)));
}

/*void GameEngine::SetSoundPan(int sound, int pan)    //255 = fully left, 0 = fully right
{
    if(sound == -2)
        return;
    Mix_SetPanning(sound, pan, 255 - pan);//(int)((float)(m_iSoundVol) / (128.0) * (float)(pan)), (int)((float)(m_iSoundVol) / (128.0) * (255.0)) - (int)((float)(m_iSoundVol) / (128.0) * (float)(pan)));
}*/

void GameEngine::KillChannel(int sound)
{
    if(sound == -2)
        return;
    Mix_HaltChannel(sound);
}

//void PlaySoundCallback(int channel)
//{
//    Mix_FreeChunk(Mix_GetChunk(channel));  //Free the memory associated with this chunk. That's all.
//}



//WinGDI alternatives for HGE-based engine

//Fill a rectangle with the specified color
void GameEngine::FillRect(const myRECT* rc, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    hgeSprite* hSprite = new hgeSprite(0, 0, 0, 32, 32);
    DWORD col = ARGB(a,r,g,b);
    hSprite->SetColor(col);
    hSprite->Render4V(rc->left, rc->top, rc->right, rc->top, rc->right, rc->bottom, rc->left, rc->bottom);
    delete hSprite;
}

//Font helper methods
void GameEngine::SetFont(string sFilename)
{
    if(m_Font != NULL)
        delete m_Font;
    m_Font = new hgeFont(sFilename.c_str());
    m_sFontFilename = sFilename;
}

void GameEngine::SetTextColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    if(m_Font != NULL)
    {
        m_Font->SetColor(ARGB(a,r,g,b));
    }
}

void GameEngine::DrawText(const char* cText, const myRECT* rc, int align)
{
    if(m_Font != NULL)
    {
        m_Font->printfb(rc->left, rc->top, rc->right - rc->left, rc->bottom - rc->top, align, cText);
    }
}

void GameEngine::GetScreenMode(int iMode, int* iWidth, int* iHeight)
{
    *iWidth = 800;
    *iHeight = 600; //In case anything goes wrong, return 800x600 as a reasonable default

    if(m_rcScreenModes == (SDL_Rect **)0 ||
       m_rcScreenModes == (SDL_Rect **)-1)
    {
        ofile << "Unable to obtain supported screen resolutions." << endl;
        return;
    }

    if(iMode > m_iNumScreenModes ||
       iMode < 0)
    {
        ofile << "Invalid screen mode requested." << endl;
        return;
    }

    *iWidth = m_rcScreenModes[iMode - 1]->w;
    *iHeight = m_rcScreenModes[iMode - 1]->h;
}

void GameEngine::DrawScreenResolutions(const myRECT* rcPosStart, float fOffsetEachx, float fOffsetEachy, int iResStart, int iCount, int iHighlight)
{
    if(m_rcScreenModes == (SDL_Rect **)0 ||
       m_rcScreenModes == (SDL_Rect **)-1)
    {
        DrawText("Unable to obtain supported screen resolutions.", rcPosStart, HGETEXT_CENTER | HGETEXT_MIDDLE);
        return;
    }

    myRECT rc = *rcPosStart;
    char cText[64];
    for(int i = iResStart; i - iResStart < iCount; i++, OffsetRect(&rc, fOffsetEachx, fOffsetEachy))
    {
        if(i >= m_iNumScreenModes)
            break;  //Done
        if(i < 0)
            continue;   //Ignore this pass, see if we can go higher
        if(!m_rcScreenModes[i])
            break; //Reached the end of our list

        if(m_rcScreenModes[i]->w < 800 ||
           m_rcScreenModes[i]->h < 600) //If below 800x600, we don't support this
        {
            SetTextColor(128,128,128);  //Draw text greyed out
            //Color bg bluish grey if this is the one we're selecting
            if(iHighlight == i - iResStart)
                FillRect(&rc, 108, 100, 133, 128);
        }
        else
        {
            SetTextColor(0,0,0);    //Set text to black
            if(m_rcScreenModes[i]->w == m_iWidth &&
               m_rcScreenModes[i]->h == m_iHeight)
                SetTextColor(0,0,255);  //Set text to blue if this is our current screen resolution

            //Color bg blue if this is the one we're selecting
            if(iHighlight == i - iResStart)
                FillRect(&rc, 80, 65, 133);
        }

        //Now render text
        sprintf(cText, "%dx%d", m_rcScreenModes[i]->w, m_rcScreenModes[i]->h);
        DrawText(cText, &rc, HGETEXT_MIDDLE | HGETEXT_CENTER);
    }
}

const char* GameEngine::GetKeyName(int key)
{
    return hge->Input_GetKeyName(key);
}

float GameEngine::GetStringWidth(string s)
{
    if(m_Font != NULL)
    {
        return m_Font->GetStringWidth(s.c_str(), false);
    }
    return 0.0;
}

float GameEngine::GetStringHeight()
{
    if(m_Font != NULL)
    {
        return m_Font->GetHeight();
    }
    return 0.0;
}


void GameEngine::SetFrameRate(int iFrameRate)
{
    if(iFrameRate < 10)
    {
        ofile << "Warning: fps < 10Hz is just dumb. Setting to 10 fps." << endl;
        m_iFrameRate = 10;
    }
    else
    {
        m_iFrameRate = iFrameRate;
    }
    if(hge == NULL)
        return;
    if(!m_bVsync)
            hge->System_SetState(HGE_FPS, m_iFrameRate);
}

void GameEngine::SetVsync(bool bVsync)
{
    m_bVsync = bVsync;
    if(hge == NULL)
        return;
    if(bVsync)
        hge->System_SetState(HGE_FPS, HGEFPS_VSYNC);
    else
        hge->System_SetState(HGE_FPS, m_iFrameRate);
}

/*bool GameEngine::GetVsync()
{
    return (hge->System_GetState(HGE_FPS) == HGEFPS_VSYNC);
}*/

void GameEngine::SetMouse(string sFilename, int xHotSpot, int yHotSpot)
{
    if(sFilename == "")
        return;
    if(m_sMouseCursorSprite != NULL)
        delete m_sMouseCursorSprite;
    if(m_texMouseCursor != 0)
        hge->Texture_Free(m_texMouseCursor);
    m_texMouseCursor = hge->Texture_Load(sFilename.c_str());
    m_sMouseCursorSprite = new hgeSprite(m_texMouseCursor, 0, 0, hge->Texture_GetWidth(m_texMouseCursor, true), hge->Texture_GetHeight(m_texMouseCursor, true));
    m_sMouseCursorSprite->SetHotSpot(xHotSpot, yHotSpot);
    m_sMouseFilename = sFilename;
    m_ptMouseHotSpot.x = xHotSpot;
    m_ptMouseHotSpot.y = yHotSpot;
}

void GameEngine::ShowMouse()
{
    m_bShowMouse = true;
}


void GameEngine::HideMouse()
{
    m_bShowMouse = false;
}

void GameEngine::DrawCursor()
{
    //Only draw mouse cursor if we can, and should
    if(!m_bShowMouse ||
       m_sMouseCursorSprite == NULL ||
       m_texMouseCursor == 0)
    {
        return;
    }

    float x = 0, y = 0;
    hge->Input_GetMousePos(&x, &y);
    m_sMouseCursorSprite->Render(x,y);
}

myPOINT GameEngine::GetMousePos()
{
    myPOINT pt;
    hge->Input_GetMousePos(&(pt.x), &(pt.y));
    return pt;
}

//Functions for dealing with fullscreen/windowed modes
bool GameEngine::GetFullscreen()
{
    m_bFullscreen = !(hge->System_GetState(HGE_WINDOWED));  //Force this to be right...
    return m_bFullscreen;
}

void GameEngine::SetFullscreen(bool bFullscreen)
{
    m_bFullscreen = !(hge->System_GetState(HGE_WINDOWED));
    if(m_bFullscreen != bFullscreen)
        ToggleFullscreeen();
}

void GameEngine::ToggleFullscreeen()
{
    m_bFullscreen = !(hge->System_GetState(HGE_WINDOWED));
    m_bFullscreen = !m_bFullscreen;
    hge->System_SetState(HGE_WINDOWED, !m_bFullscreen);
}

void GameEngine::CleanupBitmaps()
{
    for(;;)
    {
        list<Bitmap*>::iterator biBitmap = m_lBitmapList.begin();
        if(biBitmap == m_lBitmapList.end())
            return;
        delete (*biBitmap);
        m_lBitmapList.pop_front();
    }
}

void GameEngine::ChangeResolution(int iWidth, int iHeight, bool bFullscreen)
{
    if(m_iWidth == iWidth &&
       m_iHeight == iHeight)
    {
        m_bFullscreen = !(hge->System_GetState(HGE_WINDOWED));
        if(m_bFullscreen != bFullscreen)
            ToggleFullscreeen();
        return; //Don't change resolution if we don't have to
    }
    m_iWidth = iWidth;
    m_iHeight = iHeight;
    m_bFullscreen = bFullscreen;
    g_bResolutionChange = true;
    ofile << "Changing resolution to " << iWidth << "x" << iHeight << ". Fullscreen: " << ((bFullscreen)?("true"):("false")) << endl;
}

void GameEngine::SetInitialResolution(int iWidth, int iHeight, bool bFullscreen)
{
    m_iWidth = iWidth;
    m_iHeight = iHeight;
    m_bFullscreen = bFullscreen;
    //ofile << "Initializing screen: " << iWidth << "x" << iHeight << endl;
    //ofile << "Fullscreen: " << ((bFullscreen)?("true"):("false")) << endl;
}

void GameEngine::SetInitialFullscreen(bool bFullscreen)
{
    m_bFullscreen = bFullscreen;
}

void GameEngine::SetupWindow()
{
    //Next free all the raw bitmap data
    for(list<Bitmap*>::iterator bi = m_lBitmapList.begin(); bi != m_lBitmapList.end(); bi++)
    {
        (*bi)->Free();
    }

    //Also free all other stuff that uses HGE sprites/textures/particles/whatever
    //Mouse cursor
    if(m_sMouseCursorSprite != NULL)
        delete m_sMouseCursorSprite;
    if(m_texMouseCursor != 0)
        hge->Texture_Free(m_texMouseCursor);
    m_sMouseCursorSprite = NULL;
    m_texMouseCursor = 0;
    //Sprites with particles (No need for later sprites- we're outside a cycle)
    list<Sprite_particle_holder> lParticleSprites;
    list<Sprite*>::iterator siSprite;
    for(siSprite = m_lSprites.begin(); siSprite != m_lSprites.end(); siSprite++)
    {
        if((*siSprite)->HasParticles())
        {
            Sprite_particle_holder spHolder;
            (*siSprite)->FillInParticleData(&spHolder);
            lParticleSprites.push_front(spHolder);
            (*siSprite)->RemoveParticles();
        }
    }
    //Backgrounds with particles
    RemoveBgParticles();
    //Font
    if(m_Font != NULL)
        delete m_Font;
    m_Font = NULL;

    int iFPS = HGEFPS_VSYNC;    //vsync enabled by default. We'll use our own FPS elsewhere
    if(!m_bVsync)
        iFPS = m_iFrameRate;

    bool bRestart = false;

    //Unload HGE
    if(hge != NULL)
    {
        ofile << "HGE restart" << endl;
        bRestart = true;    //Restart SDL stuff
        #ifndef BUILD_FOR_WINDOWS
        CleanupSounds();
        #endif
        iFPS = hge->System_GetState(HGE_FPS);
        hge->System_Shutdown();
        hge->Release();
    }

    //Create HGE again
    hge = hgeCreate(HGE_VERSION);

    string sLogfileLoc = GetSavegameLocation() + "/hge.log";
    hge->System_SetState(HGE_LOGFILE, sLogfileLoc.c_str());
    hge->System_SetState(HGE_USESOUND, false);    //Don't link in bass.dll. We'll be using SDL_Mixer in Windows for licensing reasons.
    hge->System_SetState(HGE_FRAMEFUNC, FrameFunc);
    hge->System_SetState(HGE_RENDERFUNC, RenderFunc);
    hge->System_SetState(HGE_TITLE, m_szTitle.c_str());
    hge->System_SetState(HGE_FPS, iFPS);
//    ofile << hge->System_GetState(HGE_FPS) << endl;
    hge->System_SetState(HGE_SCREENBPP, 32);
    hge->System_SetState(HGE_FOCUSGAINFUNC, GameActivate);
    hge->System_SetState(HGE_FOCUSLOSTFUNC, GameDeactivate);
    hge->System_SetState(HGE_SHOWSPLASH, g_bResolutionChange);//Show splash screen on resolution change, or HGE crash. Wat?

    //Set HGE to all the right settings
    //hge->System_SetState(HGE_TITLE, m_szTitle.c_str()); //Set our window to this title
    #ifdef BUILD_FOR_WINDOWS
    hge->System_SetState(HGE_ICON, MAKEINTRESOURCE(m_wIcon));
    #endif
    hge->System_SetState(HGE_SCREENWIDTH, m_iWidth);
    hge->System_SetState(HGE_SCREENHEIGHT, m_iHeight);
    hge->System_SetState(HGE_WINDOWED, !m_bFullscreen);

    hge->System_Initiate();

    //Add bitmaps back
    for(list<Bitmap*>::iterator bi = m_lBitmapList.begin(); bi != m_lBitmapList.end(); bi++)
    {
        (*bi)->ReloadTex();
    }

    //Add other stuff back
    //Mouse cursor
    SetMouse(m_sMouseFilename, m_ptMouseHotSpot.x, m_ptMouseHotSpot.y);
    //Sprites with particles
    for(list<Sprite_particle_holder>::iterator i = lParticleSprites.begin(); i != lParticleSprites.end(); i++)
    {
        Sprite* pS = (Sprite*)i->pSprite;
        pS->AddParticles(i->sParticleFilename, i->sImageFilename, i->x, i->y, i->w, i->h, i->ptOffset, i->bAdditive);
        pS->SetParticlesInFront(i->bParticlesInFront);
        if(i->bIsFiring)
            pS->FireParticles();
        if(i->bDieOnFinish)
            pS->DieOnParticleFinish();
    }
    //Backgrounds with particles
    AddBgParticles();
    //Font
    if(m_sFontFilename != "")
        SetFont(m_sFontFilename);


    //Done?
}

void GameEngine::End()
{
    g_bEndGame = true;
}

int GameEngine::GetPowerLevel()
{
    return hge->System_GetState(HGE_POWERSTATUS);
}

void GameEngine::UncacheBitmaps()    //Free memory associated with the bitmaps in the game
{
    for(list<Bitmap*>::iterator i = m_lBitmapList.begin(); i != m_lBitmapList.end(); i++)
    {
        if(ShouldUnchache(*i))
            (*i)->Uncache();
    }
}








