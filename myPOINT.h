//myPOINT.h
//Definitions of POINT and RECT structures with floating-point values rather than integer values
//(c) Mark Hutcheson 2012

#ifndef MYPOINT_H
#define MYPOINT_H

#include "buildtarget.h"

typedef struct
{
    float x,y;
}myPOINT;

typedef struct
{
    float left, top, right, bottom;
}myRECT;

void CopyRect(myRECT* dest, const myRECT* src);

void InflateRect(myRECT* rc, float fXShrink, float fYShrink);

bool PtInRect(const myRECT* rc, const myPOINT* pt);
bool PtInRect(const myRECT* rc, float x, float y);

void OffsetRect(myRECT* rc, float x, float y);

void SetRect(myRECT* rc, float left, float top, float right, float bottom);

bool IntersectRect(const myRECT* rc1, const myRECT* rc2);




#endif  //ifndef MYPOINT_H
